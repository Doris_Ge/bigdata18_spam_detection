# -*- coding: utf-8 -*-
'''
Created on Jan 12, 2015

@author: santhosh
'''
import re
from datetime import datetime, timedelta

import networkx
import StatConstants

import SIAUtil


def getDayIncrements(timeSplit): #得到日期增加的天数
    if not re.match('[0-9]+-[WDHMY]', timeSplit): #匹配字符串'至少一个数字'+'-'+'WDMY中任意一个字母' WDMY应该是Week, Day, Month, Year
        print 'Time Increment does not follow the correct Pattern - Time Increment Set to 1 Day'
        timeSplit = '1-D'
    numeric = int(timeSplit.split('-')[0]) #'-'前面的数字
    incrementStr = timeSplit.split('-')[1] #'-'后面的时间单位
    dayIncrement = 1
    if incrementStr == 'D':
        dayIncrement = numeric
    elif incrementStr == 'H':
        dayIncrement = numeric * 15
    elif incrementStr == 'W':
        dayIncrement = numeric * 7
    elif incrementStr == 'M':
        dayIncrement = numeric * 30
    else:
        dayIncrement = numeric * 365
    return dayIncrement

def getGranularityInc(day_granularity_inc):
    granularity_inc = day_granularity_inc
    if StatConstants.MINIMUM_GRANULARITY == StatConstants.HOUR_GRANULARITY:
        granularity_inc *= 24
    elif StatConstants.MINIMUM_GRANULARITY == StatConstants.MINUTE_GRANULARITY:
        granularity_inc *= (24 * 60)
    return granularity_inc

def getDates(firstDatetime, timeKeys, timeLength ='1-M'):
    dayIncrement = getDayIncrements(timeLength)
    return [(firstDatetime+timedelta(timeKey*dayIncrement)).date() for timeKey in timeKeys]


#---------------------------------------------------------------------------------------------------------
class SuperGraph(networkx.Graph):
    def __init__(self, parentUserIdToUserDict=dict(),parentBusinessIdToBusinessDict=dict(), parent_reviews= dict()):
        super(SuperGraph, self).__init__() #在子类中调用父类的初始化方法
        self.userIdToUserDict = parentUserIdToUserDict
        self.businessIdToBusinessDict = parentBusinessIdToBusinessDict
        self.reviewIdToReviewDict = parent_reviews


    def addNodesAndEdge(self, usr, bnss, review):
        self.userIdToUserDict[usr.getId()] = usr
        self.businessIdToBusinessDict[bnss.getId()] = bnss
        self.reviewIdToReviewDict[review.getId()] = review
        super(SuperGraph, self).add_node((usr.getId(),SIAUtil.USER))
        super(SuperGraph, self).add_node((bnss.getId(),SIAUtil.PRODUCT))
        super(SuperGraph, self).add_edge((usr.getId(),SIAUtil.USER),\
                                              (bnss.getId(),SIAUtil.PRODUCT),\
                                               attr_dict={SIAUtil.REVIEW_EDGE_DICT_CONST: review.getId()}) #REVIEW_EDGE_DICT_CONST = 'review'

    def getUser(self, userId):
        return self.userIdToUserDict[userId]

    def getBusiness(self, businessId):
        return self.businessIdToBusinessDict[businessId]

    def getReviewIds(self):
        return [self.get_edge_data(*edge)[SIAUtil.REVIEW_EDGE_DICT_CONST] for edge in self.edges()]

    def getReview(self,usrId, bnssId):
        return self.reviewIdToReviewDict[self.get_edge_data((usrId,SIAUtil.USER), (bnssId,SIAUtil.PRODUCT))[SIAUtil.REVIEW_EDGE_DICT_CONST]]

    def getReviewFromReviewId(self, reviewId):
        return self.reviewIdToReviewDict[reviewId]

    @staticmethod
    def createGraph(userIdToUserDict,bnssIdToBusinessDict, parent_reviews):
        graph = SuperGraph()
        for reviewKey in parent_reviews.iterkeys():
            review = parent_reviews[reviewKey]
            graph.addNodesAndEdge(userIdToUserDict[review.getUserId()],\
                                         bnssIdToBusinessDict[review.getBusinessID()],\
                                         review)
        return graph

class TemporalGraph(networkx.Graph):

    def __init__(self, parentUserIdToUserDict=dict(),parentBusinessIdToBusinessDict=dict(), parent_reviews= dict(), date_time = None):
        super(TemporalGraph, self).__init__()
        self.userIdToUserDict = parentUserIdToUserDict
        self.businessIdToBusinessDict = parentBusinessIdToBusinessDict
        self.reviewIdToReviewDict = parent_reviews
        self.date_time = date_time

    def addNodesAndEdge(self, usr, bnss, review): #usr=user(object), bnss=business(object), review=review(object)
        self.userIdToUserDict[usr.getId()] = usr
        self.businessIdToBusinessDict[bnss.getId()] = bnss
        self.reviewIdToReviewDict[review.getId()] = review
        super(TemporalGraph, self).add_node((usr.getId(),SIAUtil.USER)) #每个节点是一个tuple,ID+TYPE
        super(TemporalGraph, self).add_node((bnss.getId(),SIAUtil.PRODUCT))
        super(TemporalGraph, self).add_edge((usr.getId(),SIAUtil.USER),\
                                              (bnss.getId(),SIAUtil.PRODUCT),\
                                               attr_dict={SIAUtil.REVIEW_EDGE_DICT_CONST: review.getId()})

    def getUserCount(self):
        return len(set([node_id for (node_id, node_type) in self.nodes() if node_type == SIAUtil.USER]))

    def getUserIds(self):
        return [node_id for (node_id, node_type) in self.nodes() if node_type == SIAUtil.USER]

    def getBusinessIds(self):
        return [node_id for (node_id, node_type) in self.nodes() if node_type == SIAUtil.PRODUCT]

    def getBusinessCount(self):
        return len(set([node_id for (node_id, node_type) in self.nodes() if node_type == SIAUtil.PRODUCT]))

    def getReviewIds(self):
        return [self.get_edge_data(*edge)[SIAUtil.REVIEW_EDGE_DICT_CONST] for edge in self.edges()]

    def getReviewCount(self):
        return len(set([self.get_edge_data(*edge)[SIAUtil.REVIEW_EDGE_DICT_CONST] for edge in self.edges()]))

    def getUser(self, userId):
        return self.userIdToUserDict[userId]

    def getBusiness(self, businessId):
        return self.businessIdToBusinessDict[businessId]

    def getReview(self, usrId, bnssId):
        return self.reviewIdToReviewDict[self.get_edge_data(
            (usrId, SIAUtil.USER), (bnssId, SIAUtil.PRODUCT))[SIAUtil.REVIEW_EDGE_DICT_CONST]]

    def getReviewFromReviewId(self, reviewId):
        return self.reviewIdToReviewDict[reviewId]

    def getDateTime(self):
        return self.date_time

    @staticmethod #静态方法,可以直接用类名调用,且不会将类当做第一个参数传递给此函数，所以该函数定义时的第一个参数不是self
    def createTemporalGraph(userIdToUserDict,businessIdToBusinessDict, parent_reviews,\
                          timeSplit ='1-D', initializePriors=True): #TemporalGraph.createTemporalGraph(usrIdToUserDict,bnssIdToBusinessDic,reviewIdToReviewsDict,timeLength, False)
        dayIncrement = getDayIncrements(timeSplit) #得到增加的天数,以天为单位

        all_review_dates = [SIAUtil.getDateForReview(r)\
                 for r in parent_reviews.values() ] #store the date of each review
        minDate = min(all_review_dates)
        maxDate = max(all_review_dates)


        cross_time_graphs = dict()
        time_key = 0

        while time_key < ((maxDate-minDate+timedelta(dayIncrement))/dayIncrement).days:#(最大的日期-最小的日期+时间间隔/时间间隔)得到它的数值,即有几个时间间隔,若时间间隔是一周，则14天就是2个时间间隔,这里多加一个时间间隔是因为这里是小于号
            cross_time_graphs[time_key] = TemporalGraph(date_time=datetime.combine(minDate+timedelta(days=time_key*dayIncrement),\
                                                                              datetime.min.time())) #datetime.min.time()为0分0秒的datetime对象,datetime.combine将date和time结合形成一个datetime的对象
            time_key+=1

        print 'Start Date:', minDate, 'End Date:', maxDate, 'Total Time Ticks:', len(cross_time_graphs.keys()) #len = 有几个时间间隔

        for reviewKey in parent_reviews.iterkeys(): #reviewKey=review dict 的键
            review = parent_reviews[reviewKey]
            reviewDate = SIAUtil.getDateForReview(review)
            timeDeltaKey = ((reviewDate-minDate)/dayIncrement).days
            temporalGraph = cross_time_graphs[timeDeltaKey] #每个图中都存了一个时间间隔内，每个review的user到其评价的business的边
            temporalGraph.addNodesAndEdge(userIdToUserDict[review.getUserId()],\
                                         businessIdToBusinessDict[review.getBusinessID()],\
                                         review)
        return cross_time_graphs

#---------------------------------------------------------------------------------------------------------
def createSuperGraph(usrIdToUserDict,bnssIdToBusinessDict, reviewIdToReviewsDict, timeLength):
    superGraph = SuperGraph.createGraph(usrIdToUserDict, \
                                        bnssIdToBusinessDict, \
                                        reviewIdToReviewsDict)

    print "Super Graph Created"
    return superGraph

def createTemporalGraph(usrIdToUserDict,bnssIdToBusinessDict,reviewIdToReviewsDict, timeLength):
    cross_time_graphs = TemporalGraph.createTemporalGraph(usrIdToUserDict, \
                                                          bnssIdToBusinessDict, \
                                                          reviewIdToReviewsDict, \
                                                          timeLength, False)
    print "Temporal Graph Created"
    return cross_time_graphs

def createGraphs(usrIdToUserDict,bnssIdToBusinessDict,reviewIdToReviewsDict, timeLength):
    beforeGraphConstructionTime = datetime.now()
    superGraph = SuperGraph.createGraph(usrIdToUserDict,\
                                             bnssIdToBusinessDict,\
                                             reviewIdToReviewsDict)

    print "Super Graph Created"

    cross_time_graphs = TemporalGraph.createTemporalGraph(usrIdToUserDict,\
                                             bnssIdToBusinessDict,\
                                             reviewIdToReviewsDict,\
                                             timeLength, False)
    afterGraphConstructionTime = datetime.now()
    print 'TimeTaken for Graph Construction:',afterGraphConstructionTime-beforeGraphConstructionTime

    return superGraph, cross_time_graphs

