import sys
import networkx
from intervaltree import IntervalTree

def OutputHelp(file,module):
     sys.stdout = open(file, 'w')
     help(module)
     sys.stdout.close()
     sys.stdout = sys.__stdout__


OutputHelp(r'D:/HelpGraph.txt',networkx.Graph)
OutputHelp(r'D:/HelpPython/HelpIntervalTree.txt', IntervalTree)
