from main import AppUtil
from util import StatConstants
from util.data_reader_utils.yelp_utils.YelpChiMetaDataReader import YelpChiMetaDataReader
from datetime import datetime, timedelta
from pandas import Series, DataFrame
from pandas.plotting import lag_plot, autocorrelation_plot
from pandas import concat
from statsmodels.graphics.tsaplots import plot_acf
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error as mse
from statsmodels.tsa.ar_model import AR
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.api import VAR, DynamicVAR
import numpy as np
import pickle
import os
import math
from StatUtil import entropyFn
from sympy import solve, Symbol
from scipy.optimize import *
from random import randint



AR_MODEL_LOG = 'predict_by_AR_model.log'
AR_UPDATE_COEFANDDATA_MODEL_LOG = 'predict_by_AR_update_coefanddata_model.log'
AR_UPDATE_DATA_MODEL_LOG ='predict_by_AR_update_data_model.log'
ALL_MODELS_LOG = 'all_models_log'
AR_RETRAIN_MODEL_LOG = 'predict_by_AR_Retrain_model.log'

def lossfunction(yhat, y):
    return 0.5*(yhat-y)**2


def Backtracking_LineSearch(y, coef, _lag, beta= 0.180269688877, alpha=0.5):
    lag = [_lag[i] for i in range(len(_lag))]
    step = 1
    coeftmp = [coef[i] for i in range(len(coef))]
    coefarrtmp = np.array(coeftmp)
    coefarr = np.array(coef)
    if len(coef) > len(lag):
       lag.append(1)
    xlag = lag[::-1]

    while True:
        yhat = np.dot(coefarrtmp, xlag)
        gradient = (yhat - y) * np.array(xlag)
        coefarrtmp = coefarrtmp - step*gradient
        yhattmp = np.dot(coefarrtmp, xlag)
        if lossfunction(yhattmp, y) > (lossfunction(yhat, y) - alpha*step*np.dot(gradient,gradient)):
            step = beta * step
        else:
            break
    return step


def logPrediction(plotDir, bnss_file_name, bnss_key, window, error):
    bnss_file_name = os.path.join(plotDir, bnss_file_name)
    with open(bnss_file_name, 'a') as prediction_log_file:
        prediction_log_file.write(bnss_key + ' ' + 'Lag:%s' % window + ' '+'Test MSE: %.8f' % error +'\n')


def predictByARmodel(plotDir, train, test, bnss_key, errorOfARmodel, problematic_bnss_list):
    model = AR(train)
    try:
       model_fit = model.fit()
       lag = model_fit.k_ar
       predictions = model_fit.predict(start=len(train), end=len(train)+len(test)-1, dynamic=False)
       error = math.sqrt(mse(test, predictions))
       errorOfARmodel.append(error)
       # logPrediction(plotDir, AR_MODEL_LOG, bnss_key, lag, error)
       # logPrediction(plotDir, ALL_MODELS_LOG, bnss_key, lag, error)
       #print bnss_key, 'Lag: %s' % lag, 'Test MSE: %.8f' % error
    except ValueError, e:
       print e.message, bnss_key
       return


def predictByARupdateDatamodel(plotDir, train, test, bnss_key, errorOfARUpDateDatamodel):
    model = AR(train)
    model_fit = model.fit()
    window = model_fit.k_ar
    coef = model_fit.params
    history = train[len(train)-window:]
    history = [history[i] for i in range(len(history))]
    predictions = list()
    for t in range(len(test)):
        length = len(history)
        lag = [history[i] for i in range(length-window, length)]
        if window == len(coef):
            yhat = 0
            for d in range(window):
                yhat += lag[window - 1] * coef[d]
        else:yhat = coef[0]
        for d in range(window):
            try:
               yhat += lag[window-1-d]*coef[d+1]
            except IndexError, e:
                print e.message, bnss_key
                return
        obs = test[t]
        predictions.append(yhat)
        history.append(obs)
    error = math.sqrt(mse(test, predictions))
    errorOfARUpDateDatamodel.append(error)
    # logPrediction(plotDir, AR_UPDATE_DATA_MODEL_LOG, bnss_key, window, error)
    # logPrediction(plotDir, ALL_MODELS_LOG, bnss_key, window, error)
    #print bnss_key, 'Lag: %s' % window, 'Test MSE: %.8f' % error


def trainARmodel(train):
    model = AR(train)
    try:
        model_fit = model.fit()
    except ValueError:
        print "stop"
    return model_fit.k_ar, model_fit.params


def updateCoef(coef, step, yhat, y, lag):
    if len(coef) > len(lag):
       lag.append(1)
    xlag = np.array(lag[::-1])
    coef[:] = coef - step*(yhat - y)*xlag
    return coef


def calculateXtHat(history, window, coef):
    length = len(history)
    lag = [history[i] for i in range(length - window, length)]
    lag.append(1)
    xlag = np.array(lag[::-1])
    yhat = np.dot(xlag, coef)
    lag.pop(window)
    return yhat, lag


def testARupdateDatamodel(history, window, coef):
    length = len(history)
    lag = [history[i] for i in range(length - window, length)]
    if window == len(coef):
        yhat = 0
        for d in range(window):
            yhat += lag[window-1]*coef[d]
        return yhat
    yhat = coef[0]
    for d in range(window):
        yhat += lag[window - 1 - d] * coef[d + 1]
    return yhat


def testARupdateCoefandDatamodel(history, window, coef):
    length = len(history)
    lag = [history[i] for i in range(length-window, length)]
    if window == len(coef):
        yhat = 0
        for d in range(window):
            yhat += lag[window-1]*coef[d]
        return yhat, lag
    yhat = coef[0]
    sum = 0
    for d in range(window):
        yhat += lag[window-1-d]*coef[d+1]
        #sum += lag[window-1-d]**2
    #n=1/sum
    return yhat, lag

def predictByRetrain(plotDir, train, test, bnss_key, errorOfRetrain):
    predictions = list()
    testok = list()
    for i in range(len(test)):
        model = AR(train)
        model_fit = model.fit()
        lag = model_fit.k_ar
        try:
           prediction = model_fit.predict(start=len(train), end=len(train), dynamic=False)
           np.append(train, test[i])
           predictions.append(prediction)
           testok.append(test[i])
        except ValueError, e:
            print e.message, bnss_key
            return
    error = math.sqrt(mse(predictions, testok))
    errorOfRetrain.append(error)
    # logPrediction(plotDir, AR_RETRAIN_MODEL_LOG, bnss_key, lag, error)
    # logPrediction(plotDir, ALL_MODELS_LOG, bnss_key, lag, error)
    return error


def predictByARupdateCoefandDatamodel(plotDir, train, test, bnss_key, statconstant, errorOfARUpDateCoefAndDatamodel, errorOfMultipleBnssInAllTimeWid, seed):
    model = AR(train)
    model_fit = model.fit()
    window = model_fit.k_ar
    coef = model_fit.params
    history = train[len(train)-window:]
    history = [history[i] for i in range(len(history))]
    predictions = list()
    for t in range(len(test)):
        sum = 0
        length = len(history)
        lag = [history[i] for i in range(length-window, length)]
        if window == len(coef):
            yhat = 0
            for d in range(window):
                yhat += lag[window - 1] * coef[d]
        else:
            yhat = coef[0]
            for d in range(window):
                try:
                    yhat += lag[window - 1 - d] * coef[d + 1]
                    # sum  += lag[window-1-d]**2
                except IndexError, e:
                    print e.message, bnss_key
                    return
        obs = test[t]
        predictions.append(yhat)
        errorOfMultipleBnssInAllTimeWid.append(abs(yhat - test[t]))
        #n = 1/sum
        if seed:
            beta = np.random.random()
            np.random.seed(seed)
        else:
            if statconstant == StatConstants.AVERAGE_RATING:
                beta = 0.180269688877
            elif statconstant == StatConstants.NON_CUM_NO_OF_REVIEWS:
                beta = 0.00541333000783

        n = Backtracking_LineSearch(obs, coef, lag, beta, alpha=0.5)
        coef = updateCoef(coef, n, yhat, obs, lag)
        history.append(obs)
    error = math.sqrt(mse(test, predictions))
    if errorOfARUpDateCoefAndDatamodel != None:
        errorOfARUpDateCoefAndDatamodel.append(error)
    # logPrediction(plotDir, AR_UPDATE_COEFANDDATA_MODEL_LOG, bnss_key, window, error)
    # logPrediction(plotDir, ALL_MODELS_LOG, bnss_key, window, error)
    #print bnss_key, 'Lag: %s' % window, 'Test MSE: %.8f' % error
    return beta


def optimizeObjectiveFunction(yhat, noise, history, n, window, coef): #t = t+1
    temp =0
    length = len(history)
    for d in range(window):
        temp+=history[length-1-d]*history[length-2-d]
    return yhat+(coef[1]+n*temp+1)*noise+n*history[length-2]*(noise**2)

def predictedAverageRatingAtNextTime(yhat, noise, history, n, window, coef):
    temp = 0
    length = len(history)
    for d in range(window):
        temp += history[length - 1 - d] * history[length - 2 - d]
    return yhat + (coef[1] + n * temp ) * noise + n * history[length - 2] * (noise ** 2)


def maximizeNoise(t, history, window, coef, coef_for_attack, flag, threshold = 0.1):
    yhatDetect, lag = testARupdateCoefandDatamodel(history, window, coef) #before attack,spam
    yEstimateTest = testARupdateDatamodel(history, window, coef_for_attack)
    print yhatDetect, yEstimateTest
    if yhatDetect > 5:
        flag=1
        print "predicted value is larger than 5 at time %s:" % t, yhatDetect
    elif yhatDetect == 5:
        print "predicted value equals to 5"
    n = Backtracking_LineSearch(yEstimateTest, coef, lag)
    history_for_calculate = [history[i] for i in range(len(history))]
    history_for_calculate.append(yEstimateTest)
    coef_for_calculate = [coef[i] for i in range(len(coef))]
    coef_for_calculate[0] = coef_for_calculate[0] - n * (yhatDetect - yEstimateTest)
    for d in range(window):
        coef_for_calculate[d + 1] = coef_for_calculate[d + 1] - n * (yhatDetect - yEstimateTest) * history[
            len(history) - 1 - d]
    yhatNext, lag1 = testARupdateCoefandDatamodel(history_for_calculate, window, coef_for_calculate)
    symnoise = Symbol('noise')
    lnoise, hnoise = solve(
    predictedAverageRatingAtNextTime(yhatNext, symnoise, history_for_calculate, n, window, coef_for_calculate) - 5,
        symnoise)
    values = [0, yhatDetect - yEstimateTest - threshold, yhatDetect - yEstimateTest + threshold, hnoise, 5-yEstimateTest]
    if min(yhatDetect - yEstimateTest + threshold, hnoise, 5 - yEstimateTest) >= max(0, yhatDetect - yEstimateTest - threshold):
        minNoise = max(0, yhatDetect - yEstimateTest - threshold)
        maxNoise = min(yhatDetect - yEstimateTest + threshold, hnoise, 5 - yEstimateTest)
        valueInMinNoise = optimizeObjectiveFunction(yhatNext, minNoise, history_for_calculate, n, window, coef_for_calculate)
        valueInMaxNoise = optimizeObjectiveFunction(yhatNext, maxNoise, history_for_calculate, n, window, coef_for_calculate)
        if valueInMaxNoise > valueInMinNoise:
            noise = maxNoise
        else:
            noise = minNoise

        n = Backtracking_LineSearch(yEstimateTest + noise, coef, lag)
        coef[0] = coef[0] - n * (yhatDetect - yEstimateTest - noise)
        for d in range(window):
            coef[d + 1] = coef[d + 1] - n * (yhatDetect - yEstimateTest - noise) * history[len(history) - 1 - d]
        history.append(yEstimateTest + noise)
        return noise, flag
    else:
        history.append(yEstimateTest)
        return -100, flag


def maximizeNoiseInReality(test, t, history, window, coef, coef_for_attack, flag, threshold = 0.1):
    yhatDetect, lag = testARupdateCoefandDatamodel(history, window, coef) #before attack,spam
    yEstimateTest = testARupdateDatamodel(history, window, coef_for_attack)
    #print yhatDetect, yEstimateTest, test[t]
    if yhatDetect > 5:
        flag=1
        print "predicted value is larger than 5 at time %s:" % t, yhatDetect
    elif yhatDetect == 5:
        print "predicted value equals to 5"
    n = Backtracking_LineSearch(yEstimateTest, coef, lag)
    history_for_calculate = [history[i] for i in range(len(history))]
    history_for_calculate.append(yEstimateTest)
    coef_for_calculate = [coef[i] for i in range(len(coef))]
    updateCoef(coef_for_calculate, n, yhatDetect, yEstimateTest, lag)
    #coef_for_calculate[0] = coef_for_calculate[0] - n * (yhatDetect - yEstimateTest)
    #for d in range(window):
        #coef_for_calculate[d + 1] = coef_for_calculate[d + 1] - n * (yhatDetect - yEstimateTest) * history[
            #len(history) - 1 - d]
    yhatNext, lag1 = testARupdateCoefandDatamodel(history_for_calculate, window, coef_for_calculate)
    symnoise = Symbol('noise')
    lnoise, hnoise = solve(
    predictedAverageRatingAtNextTime(yhatNext, symnoise, history_for_calculate, n, window, coef_for_calculate) - 5,
        symnoise)
    values = [0, yhatDetect - yEstimateTest - threshold, yhatDetect - yEstimateTest + threshold, hnoise, 5-yEstimateTest]
    if min(yhatDetect-yEstimateTest+threshold, hnoise, 5-yEstimateTest) >= max(0, yhatDetect-yEstimateTest-threshold):
        minNoise = max(0, yhatDetect - yEstimateTest - threshold)
        maxNoise = min(yhatDetect - yEstimateTest + threshold, hnoise, 5 - yEstimateTest)
        valueInMinNoise = optimizeObjectiveFunction(yhatNext, minNoise, history_for_calculate, n, window, coef_for_calculate)
        valueInMaxNoise = optimizeObjectiveFunction(yhatNext, maxNoise, history_for_calculate, n, window, coef_for_calculate)
        if valueInMaxNoise > valueInMinNoise:
            noise = maxNoise
        else:
            noise = minNoise


        for i in range(len(test)):
            test[i] = test[i] + noise
            if test[i]> 5:
                test[i] = 5
        n = Backtracking_LineSearch(test[t], coef, lag)
        #coef[0] = coef[0] - n * (yhatDetect - test[t])
        #for d in range(window):
            #coef[d + 1] = coef[d + 1] - n * (yhatDetect - test[t]) * history[len(history) - 1 - d]
        updateCoef(coef, n, yhatDetect, test[t], lag)
        n = Backtracking_LineSearch(test[t], coef, lag, beta=0.65037424174)
        updateCoef(coef_for_attack, n, yEstimateTest, test[t], lag)
        history.append(test[t])
        return noise, flag
    else:
        flag = 1
        print "no noise found"
        print "yhatDetect-yEstimateTest-threshold: %s, yhatDetect-yEstimateTest+threshold: %s, hnoise: %s, 5-yEstimateTest: %s" % (
         yhatDetect - yEstimateTest - threshold, yhatDetect - yEstimateTest + threshold \
           , hnoise, 5-yEstimateTest)
        history.append(yEstimateTest)
        return -100, flag


def maximizeNoiseinTestForAvgRating(test, window, coef, history, all, t, startTimeKeyForPrediction, threshold, case, threshold_for_diff): # some remained problems: will exceed 5 on our current constraints
    yhatDetect, lag = testARupdateCoefandDatamodel(history, window, coef) #before attack,spam
    #yhatDetect, lag = calculateXtHat(history, window, coef)
    if yhatDetect > 5:
        flag=1
        print "predicted value is larger than 5 at time %s:" % t, yhatDetect
    elif yhatDetect == 5:
        print "predicted value equals to 5"
    #print yhatDetect, test[t]

    n = Backtracking_LineSearch(test[t], coef, lag) #step for gradient descent
    coef_for_calculate = [coef[i] for i in range(len(coef))]
    updateCoef(coef_for_calculate, n, yhatDetect, test[t], lag) # update for t+1 prediction
    history_for_calculate = [history[i] for i in range(len(history))] #update for the t+1 prediction before attack
    history_for_calculate.append(test[t])

    yhatNext, lag1 = testARupdateCoefandDatamodel(history_for_calculate, window, coef_for_calculate) # predict the average rating at t+1 before attack
    symnoise = Symbol('noise', real=True)

    try:
        lnoise, hnoise = solve(predictedAverageRatingAtNextTime(yhatNext, symnoise, history_for_calculate, n, window, coef_for_calculate)-5, symnoise)
    except:
        hnoise = float('inf')

    #values = [0, yhatDetect - test[t] - threshold, yhatDetect - test[t] + threshold, hnoise, 5 - test[t]]
    diff_uppper_bound = all[t+startTimeKeyForPrediction-1]+threshold_for_diff-test[t]
    # print "inspect avg:", all[t+startTimeKeyForPrediction] == test[t]
    if (min(yhatDetect-test[t]+threshold, hnoise, 5-test[t]) >= max(0, yhatDetect-test[t]-threshold)) and case != 4:
        minNoise = max(0, yhatDetect-test[t]-threshold)
        maxNoise = min(yhatDetect-test[t]+threshold, hnoise, 5-test[t])
        valueInMinNoise = optimizeObjectiveFunction(yhatNext, minNoise, history_for_calculate, n, window, coef_for_calculate)
        valueInMaxNoise = optimizeObjectiveFunction(yhatNext, maxNoise, history_for_calculate, n, window, coef_for_calculate)

        if valueInMaxNoise > valueInMinNoise:
            noise = maxNoise
        else:
            noise = minNoise
        return noise, lag, yhatDetect
    elif (min(yhatDetect-test[t]+threshold, hnoise, 5-test[t], diff_uppper_bound) >=
              max(0, yhatDetect-test[t]-threshold)) and case == 4:
        minNoise = max(0, yhatDetect-test[t]-threshold)
        maxNoise = min(yhatDetect-test[t]+threshold, hnoise, 5-test[t], diff_uppper_bound)
        valueInMinNoise = optimizeObjectiveFunction(yhatNext, minNoise, history_for_calculate, n, window, coef_for_calculate)
        valueInMaxNoise = optimizeObjectiveFunction(yhatNext, maxNoise, history_for_calculate, n, window, coef_for_calculate)

        if valueInMaxNoise > valueInMinNoise:
            noise = maxNoise
        else:
            noise = minNoise
        return noise, lag, yhatDetect


    else:
        print "yhatDetect-test[t]-threshold: %s, yhatDetect-test[t]+threshold: %s, hnoise: %s, 5-test[t]: %s" % (
        yhatDetect - test[t] - threshold, yhatDetect - test[t] + threshold \
            , hnoise, 5-test[t])
        print "no viable noise at time %s" % t
        return -100, lag, yhatDetect


def calculateKDEgradient(x, wi, xi, h):
    exponent = [-(x - xi[i]) ** 2 / (2 * (h ** 2)) for i in range(len(xi))]
    term = [- wi[i] * (x - xi[i]) * np.exp(exponent[i]) for i in range(len(xi))]
    # print np.sum(term)
    return np.sum(term)


def findBurst(history):
    k = len(history)
    avg = np.mean(history)
    x = [float(i+1)/k for i in range(k)]
    x = np.array(x)
    w = history
    h = float(1)/(2*k)
    yset = list()
    for i in range(len(x)):
        y = fsolve(calculateKDEgradient, x[i], args=(w, x, h))
        if abs(calculateKDEgradient(y, w, x, h))> 1e-10:
            continue
        if len(yset) == 0:
            if 0<=y and y<=1:
                yset.append(y[0])
        elif abs(y-yset[-1]) > 1e-3 and 0<=y and y<=1:
            yset.append(y[0])
    yset = set(yset)
    xset = dict()
    for i in range(len(x)):
        for y in yset:
            if y <= x[0]:
                xset[0] = w[0]
                continue
            if y > x[i] and y <= x[i+1] and w[i+1] >= avg:
                xset[i+1] = w[i+1]
    return yset, xset


def calculateObjectiveBurstFunction(x, wi, xi, h):
    exponent = np.array([-2*(xi[-1] - xi[i])*x + xi[-1]**2 - xi[i]**2 for i in range(len(xi)-1)])
    exponent /= 2*(h**2)
    term = [-wi[i]*(x - xi[i])/(x - xi[-1])*np.exp(exponent[i]) for i in range(len(xi)-1)]
    return np.sum(term)


def maximizeNoiseInTestForBurst(history):
    k = len(history)
    avg = np.mean(history)
    x = [float(i + 1) / (k+1) for i in range(k+1)]
    x = np.array(x)
    w = history
    h = float(1) / (2 * (k + 1))
    xformaxAfNoise = fmin(calculateObjectiveBurstFunction, x[-2], args=(w, x, h))
    if xformaxAfNoise < x[-2]:
        xformaxAfNoise = x[-2]
    maxAfNoise = calculateObjectiveBurstFunction(xformaxAfNoise, w, x, h)
    if maxAfNoise <= np.sum(history) / k:
        maxAfNoise = np.sum(history) / k
    return maxAfNoise, xformaxAfNoise


def maximizeNoiseInTestForNoOfReviews(test, window, coef, history, t, count_for_Failure, count_for_FP, threshold=10):
    yhatDetect, lag = testARupdateCoefandDatamodel(history, window, coef) #before attack,spam
    #yhatDetect, lag = calculateXtHat(history, window, coef)
    #print yhatDetect, test[t]

    n = Backtracking_LineSearch(test[t], coef, lag) #step for gradient descent
    coef_for_calculate = [coef[i] for i in range(len(coef))]
    updateCoef(coef_for_calculate, n, yhatDetect, test[t], lag) # update for t+1 prediction
    history_for_calculate = [history[i] for i in range(len(history))] #update for the t+1 prediction before attack
    history_for_calculate.append(test[t])

    yhatNext, lag1 = testARupdateCoefandDatamodel(history_for_calculate, window, coef_for_calculate) # predict the average rating at t+1 before attack

    values = [0, yhatDetect - test[t] - threshold, yhatDetect - test[t] + threshold]
    if yhatDetect-test[t]+threshold >= 0:
        values = sorted(values)
        minNoise = max(0, int(yhatDetect-test[t]-threshold))
        maxNoise = int(yhatDetect-test[t]+threshold)
        valueInMinNoise = optimizeObjectiveFunction(yhatNext, minNoise, history_for_calculate, n, window, coef_for_calculate)
        valueInMaxNoise = optimizeObjectiveFunction(yhatNext, maxNoise, history_for_calculate, n, window, coef_for_calculate)

        if valueInMaxNoise > valueInMinNoise:
            noise = maxNoise
        else:
            noise = minNoise

        n = Backtracking_LineSearch(test[t]+noise, coef, lag) # lag contains the historic data 1-d time before
        updateCoef(coef, n, yhatDetect, test[t]+noise, lag)
        test[t] = test[t] + noise
        history.append(test[t])
        return noise, count_for_Failure, count_for_FP
    else:
        if yhatDetect - test[t] <= -threshold:
           count_for_FP+=1
        count_for_Failure+=1
        print "yhatDetect-test[t]-threshold: %s, yhatDetect-test[t]+threshold: %s" % (
        yhatDetect - test[t] - threshold, yhatDetect - test[t] + threshold )
        flag_of_no_noise = 1
        print "no viable noise at time %s" % t
        n = Backtracking_LineSearch(test[t], coef, lag)  # lag contains the historic data 1-d time before
        updateCoef(coef, n, yhatDetect, test[t], lag)
        history.append(test[t])
        return -100, count_for_Failure, count_for_FP


def estimateStep(lag, window):
    sum = 0
    for d in range(window):
        sum += lag[d]
    return 1/sum


def maximizeNoiseinTestForAvgRatingWithLimitedKnowledge(test, t, history, window, coef, threshold=0.1):
    yhatDetect, lag = testARupdateCoefandDatamodel(history, window, coef)  # before attack,spam
    # yhatDetect, lag = calculateXtHat(history, window, coef)
    if yhatDetect > 5:
        flag = 1
        print "predicted value is larger than 5 at time %s:" % t, yhatDetect
    elif yhatDetect == 5:
        print "predicted value equals to 5"
    # print yhatDetect, test[t]

    n = estimateStep(lag, window)  # step for gradient descent
    coef_for_calculate = [coef[i] for i in range(len(coef))]  # update for the t+1 prediction before attack
    coef_for_calculate[0] = coef_for_calculate[0] - n * (yhatDetect - test[t])
    for d in range(window):
        coef_for_calculate[d + 1] = coef_for_calculate[d + 1] - n * (yhatDetect - test[t]) * history[
            len(history) - 1 - d]
    history_for_calculate = [history[i] for i in range(len(history))]  # update for the t+1 prediction before attack
    history_for_calculate.append(test[t])

    yhatNext, lag1 = testARupdateCoefandDatamodel(history_for_calculate, window,
                                                  coef_for_calculate)  # predict the average rating at t+1 before attack
    symnoise = Symbol('noise')
    lnoise, hnoise = solve(
        predictedAverageRatingAtNextTime(yhatNext, symnoise, history_for_calculate, n, window, coef_for_calculate) - 5,
        symnoise)
    values = [0, yhatDetect - test[t] - threshold, yhatDetect - test[t] + threshold, hnoise, 5 - test[t]]
    if min(yhatDetect - test[t] + threshold, hnoise, 5 - test[t]) >= max(0, yhatDetect - test[t] - threshold):
        values = sorted(values)
        minNoise = max(0, yhatDetect - test[t] - threshold)
        maxNoise = min(yhatDetect - test[t] + threshold, hnoise, 5 - test[t])
        valueInMinNoise = optimizeObjectiveFunction(yhatNext, minNoise, history_for_calculate, n, window,
                                                    coef_for_calculate)
        valueInMaxNoise = optimizeObjectiveFunction(yhatNext, maxNoise, history_for_calculate, n, window,
                                                    coef_for_calculate)

        if valueInMaxNoise > valueInMinNoise:
            noise = maxNoise
        else:
            noise = minNoise

        n = estimateStep()  # lag contains the historic data 1-d time before
        coef[0] = coef[0] - n * (yhatDetect - test[t] - noise)  # update for the prediction after attack at t+1
        for d in range(window):
            coef[d + 1] = coef[d + 1] - n * (yhatDetect - test[t] - noise) * history[len(history) - 1 - d]
        # coef = [coef_for_calculate[i] for i in range(len(coef))]
        for i in range(t, len(test)):
            test[i] = test[i] + noise
            if test[i] > 5:
                test[i] = 5
        history.append(test[t])
        return noise
    else:
        # print "yhatDetect-test[t]-threshold: %s, yhatDetect-test[t]+threshold: %s, hnoise: %s, 5-test[t]: %s" % (
        # yhatDetect - test[t] - threshold, yhatDetect - test[t] + threshold \
        #   , hnoise, 5-test[t])
        history.append(test[t])
        return -100

def calculateThreshold(errorList):
    errorList = sorted(errorList)
    for i in range(len(errorList)):
        if float(i+1) / len(errorList) > 0.8:
            threshold = errorList[i]
            break
    return threshold


def ARModelAnalysis(plotDir, problematic_bnss_list, seed, statconstant):
    statistics_for_current_bnss_dir = os.path.join(plotDir, AppUtil.BNSS_STATS_FOLDER, '1-M')
    bnss_list = list()
    errorOfARmodel = list()
    errorOfARUpdateDatamodel = list()
    errorOfARUpdateCoefAndDatamodel = list()
    errorOfRetrainmodel = list()
    errorOfMultipleBnssInAllTimeWid = list()
    loglist = [AR_MODEL_LOG, AR_UPDATE_DATA_MODEL_LOG, AR_UPDATE_COEFANDDATA_MODEL_LOG, AR_RETRAIN_MODEL_LOG, ALL_MODELS_LOG]
    for log in loglist:
        if os.path.exists(os.path.join(plotDir, log)):
            os.unlink(os.path.join(plotDir, log))
            print 'Unlink %s' % log

    if len(bnss_list) == 0:  # if no bnss_list is offered, then run for all businesses
        file_list_size = []
        for root, dirs, files in os.walk(statistics_for_current_bnss_dir):
            for name in files:
                file_list_size.append((name, os.path.getsize(os.path.join(statistics_for_current_bnss_dir, name))))
            file_list_size = sorted(file_list_size, key=lambda x: x[1], reverse=True)

        bnssKeys = [file_name for file_name,
                                  size in file_list_size]
    else:
        bnssKeys = bnss_list

    for bnss_key in bnssKeys:
        if bnss_key == 'zI0E_yruu58ea-xq9aHi-w':
            print 'stop'
        if bnss_key not in problematic_bnss_list:
            with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
                statistics_for_current_bnss = pickle.load(file)
                series = Series(statistics_for_current_bnss[statconstant],
                                name=statconstant)
                if statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY] + 23 \
                        < statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]:
                    series = series[
                             statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]:statistics_for_current_bnss[
                                                                                           StatConstants.LAST_TIME_KEY] + 1]
                    X = series.values
                    no_of_reviews = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS]
                    no_of_reviews = no_of_reviews[statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]:\
                    statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]+1]
                    timeKey = chooseAttackTime(X, no_of_reviews, timeKey=16)
                    print timeKey
                    if timeKey == -1:
                        continue
                    train, test = X[:12], X[12:timeKey]

                    #train, test = X[:len(X) - 6], X[len(X) - 6:]

                    predictByARmodel(plotDir, train, test, bnss_key, errorOfARmodel, problematic_bnss_list)
                    predictByARupdateDatamodel(plotDir, train, test, bnss_key, errorOfARUpdateDatamodel)
                    predictByRetrain(plotDir, train, test, bnss_key, errorOfRetrainmodel)
                    beta = predictByARupdateCoefandDatamodel(plotDir, train, test, bnss_key, statconstant, errorOfARUpdateCoefAndDatamodel, errorOfMultipleBnssInAllTimeWid, seed)

    print beta
    errorOfARmodel = np.array(errorOfARmodel)
    print "The mean of error of AR model is", errorOfARmodel.mean(), "The standard deviation of error of AR model is",errorOfARmodel.std()
    errorOfARUpdateDatamodel = np.array(errorOfARUpdateDatamodel)
    print "The mean of error of AR_Update_Data model is", errorOfARUpdateDatamodel.mean(), "The standard deviation of error of AR_Update_Data model is", \
        errorOfARUpdateDatamodel.std()
    errorOfRetrainmodel = np.array(errorOfRetrainmodel)

    print "The mean of error of AR_Retrain model is", errorOfRetrainmodel.mean(), errorOfRetrainmodel.std()
    errorOfARUpdateCoefAndDatamodel = Series(errorOfARUpdateCoefAndDatamodel)
    #print plt.hist(errorOfARUpdateCoefAndDatamodel, color='k', alpha=0.3)
    #result = plt.hist(errorOfMultipleBnssInAllTimeWid, color='k', alpha=0.3)
    threshold = calculateThreshold(errorOfMultipleBnssInAllTimeWid)
    print "Threshold for detection is %s." % threshold, "Mean of error is %s." % np.array(errorOfMultipleBnssInAllTimeWid).mean()
    print "The mean of error of AR_Update_Coef_and_Data model is", errorOfARUpdateCoefAndDatamodel.mean(), \
        "The standard deviation of error of AR_Update_Coef_and_Data model is", errorOfARUpdateCoefAndDatamodel.std()

    return beta, errorOfARUpdateCoefAndDatamodel.mean(), threshold


def chooseAttackTime(avg_rating, no_of_reviews, timeKey, lastTimeKey): #if return -1, then this business will be skipped
    for i in range(timeKey, lastTimeKey-1):
        if avg_rating[i+1] <= avg_rating[i] and no_of_reviews[i] >= 10:
            break
    # while avg_rating[timeKey + 1] >= avg_rating[timeKey]:
    #     timeKey += 1
    #     if timeKey == len(avg_rating) - 1:
    #         break
    if i + 8 > lastTimeKey:
        return -1
    return i
    #train, test = avg_rating[:timeKey + 2], avg_rating[timeKey + 2:timeKey + 8]
    #return train, test

def randomizeAttackTime(lastTimeKey, timeKey):
    return randint(timeKey, lastTimeKey-8)


def extractSeriesAndARmodel(statistics_for_current_bnss, measure, lengthForPrediction):
    '''
    :param statistics_for_current_bnss: statistics of current business
    :param measure: measure to be extracted
    :param lengthForPrediction: the number of months to be predicted
    :return:
            if not found the satisfied time key to start prediction, then return -1,
            else return the data to be predicted, lag of AR model, coefficent of AR model
            and time key to start predictions and the statistics of current measure
    '''
    series = Series(statistics_for_current_bnss[measure], name=measure)
    X = series.values
    firstTimeKey = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]
    lastTimeKey = statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]
    no_of_reviews = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS]

    _iter = iter(no_of_reviews)
    timeKey = 0
    try:
        while _iter.next() < 4:
            timeKey += 1
    except StopIteration:
        return -1
    startTimeKeyForPrediction = timeKey + 30
    if startTimeKeyForPrediction + 5 > lastTimeKey:
        return -1

    train, test = X[firstTimeKey:startTimeKeyForPrediction], \
                  X[startTimeKeyForPrediction:startTimeKeyForPrediction+lengthForPrediction]

    window, coef = trainARmodel(train)
    history = train[len(train)-window:]
    history = [history[i] for i in range(len(history))]
    return test, window, coef, history, startTimeKeyForPrediction, series


def extractBnssKeys(plotDir, timeLength):
    statistics_for_current_bnss_dir = os.path.join(plotDir, AppUtil.BNSS_STATS_FOLDER, timeLength)
    bnss_list = list()
    if len(bnss_list) == 0:  # if no bnss_list is offered, then run for all businesses
        file_list_size = []
        for root, dirs, files in os.walk(statistics_for_current_bnss_dir):
            for name in files:
                file_list_size.append((name, os.path.getsize(os.path.join(statistics_for_current_bnss_dir, name))))
            file_list_size = sorted(file_list_size, key=lambda x: x[1], reverse=True)

        bnssKeys = [file_name for file_name,
                                  size in file_list_size]
    else:
        bnssKeys = bnss_list
    return statistics_for_current_bnss_dir, bnssKeys


def calculateS(ratingBfAttack, ratingBfAttackKeys, i, alpha, beta):
    return math.log(ratingBfAttack[i]) - 1 - (alpha-beta)* ratingBfAttackKeys[i]


def calculateRatingForAttack(ratingBfAttack, ratingBfAttackKeys, alpha, beta):
    S = [calculateS(ratingBfAttack, ratingBfAttackKeys, i, alpha, beta) for i in range(len(ratingBfAttack))]
    S = np.array(S)
    Z = np.sum(np.exp(S))
    return np.exp(S - np.log(Z)), ratingBfAttackKeys, Z


def f(ratingBfAttack, ratingBfAttackKeys, U, B):
    def LagrangeDualFunction(x):
        ratingForAttack, ratingForAttackKeys, Z = calculateRatingForAttack(ratingBfAttack, ratingBfAttackKeys, x[0], x[1])
        return 0 - (- np.log(Z) - np.sum(ratingForAttack) - \
               x[0] * U - x[1] * B)
    return  LagrangeDualFunction


def fprime(ratingBfAttack, ratingBfAttackKeys, U, B):
    def calculateGradient(x):
        ratingForAttack, ratingForAttackKeys, Z = calculateRatingForAttack(ratingBfAttack, ratingBfAttackKeys, x[0], x[1])
        gradientOfAlpha = (2 - np.sum(ratingForAttack)) * \
                          np.sum([ratingBfAttackKeys[i]*ratingForAttack[i] for i in range(len(ratingForAttack))]) - U
        gradientOfAlpha = - gradientOfAlpha
        gradientOfBeta = -(2 - np.sum(ratingForAttack)) * \
                          np.sum([ratingBfAttackKeys[i]*ratingForAttack[i] for i in range(len(ratingForAttack))]) - B
        gradientOfBeta = - gradientOfBeta
        return np.array((np.float64(gradientOfAlpha), np.float64(gradientOfBeta))).astype(np.float64)
    return calculateGradient


def OptimizeRatingDistributionInTest(AvgRatingBfAttack, yhatDetect, noOfAllReviewsBfAttack, rating_distribution, timeKey, AvgRatingNoise, NoOfReviewsNoise, tolerance, count_min):
    #AvgRatingBfAttack = statistics_for_current_business[StatConstants.AVERAGE_RATING][timeKey]
    #noOfAllReviewsBfAttack = statistics_for_current_business[StatConstants.NO_OF_REVIEWS][timeKey]
    upperBound = ((AvgRatingBfAttack + AvgRatingNoise)*(noOfAllReviewsBfAttack+NoOfReviewsNoise)-AvgRatingBfAttack*noOfAllReviewsBfAttack)/NoOfReviewsNoise
    lowerBound = -((AvgRatingBfAttack + AvgRatingNoise - tolerance)*(noOfAllReviewsBfAttack+NoOfReviewsNoise)-AvgRatingBfAttack*noOfAllReviewsBfAttack)/NoOfReviewsNoise
    if lowerBound < -5:
        print "lower bound is %s." % lowerBound
        lowerBound = - ((yhatDetect*(noOfAllReviewsBfAttack+NoOfReviewsNoise)-AvgRatingBfAttack*noOfAllReviewsBfAttack)/NoOfReviewsNoise)
        if lowerBound < -5:
            print "lower bound %s is still not viable." % lowerBound
    #rating_distribution = statistics_for_current_business[StatConstants.RATING_DISTRIBUTION]
    #noOfReviews = statistics_for_current_business[StatConstants.NON_CUM_NO_OF_REVIEWS][timeKey]
    ratingBfAttackDict = dict()
    for key in rating_distribution.keys():
        if rating_distribution[key][timeKey]:
            ratingBfAttackDict[key] = rating_distribution[key][timeKey]
    ratingBfAttackKeys, ratingBfAttack = zip(*sorted(ratingBfAttackDict.iteritems(), key=lambda x: x[0]))
    ratingBfAttack = [float(ratingBfAttack[key]) / noOfAllReviewsBfAttack for key in
                      range(len(ratingBfAttack))]
    #check = check_grad(f(ratingBfAttack, ratingBfAttackKeys, upperBound, lowerBound),  \
                         #fprime(ratingBfAttack, ratingBfAttackKeys, upperBound, lowerBound), [10,10])
    #print check
    x, nf, rc = fmin_tnc(f(ratingBfAttack, ratingBfAttackKeys, upperBound, lowerBound), [10,10], \
                         fprime(ratingBfAttack, ratingBfAttackKeys, upperBound, lowerBound), bounds = [(0,float("inf")),(0, float("inf"))])
    if rc==1 or rc == 2:
        count_min+= 1
    ratingForAttack, ratingForAttackKeys, Z = calculateRatingForAttack(ratingBfAttack, ratingBfAttackKeys, x[0], x[1])
    return ratingForAttack, ratingForAttackKeys, count_min


def calculateCDF(statistics):
    diff = abs(np.diff(statistics))
    diff = sorted(diff)
    timeKey = 0
    for i in range(len(diff)):
        if float(i)/(len(diff)-1) > 0.95:
            timeKey = i
            break
    return diff[timeKey]

def maximizeNoiseForNoOfReviewsInCDF(statistics_dir, bnss_key, startTimeForPrediction):
    with open(os.path.join(statistics_dir, bnss_key)) as file:
        statistics_for_current_bnss = pickle.load(file)
    series = Series(statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS])
    firstTimeKey = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]
    X = series.values
    train = X[firstTimeKey:startTimeForPrediction]
    halfnoise = calculateCDF(train)
    return halfnoise

def calculateKLdivergence(distribution1, distribution2):
    divergence = 0
    for key in distribution1:
        if distribution1[key]== 0:
            distribution1[key] = 1e-8
        if distribution2[key] == 0:
            distribution2[key] = 1e-8
        #print distribution1[key], distribution2[key]
        divergence += distribution1[key]*math.log(float(distribution1[key])/distribution2[key])
    return divergence


def calculateKLdivergenceInList(distribution1, distribution2):
    divergence = 0
    for i in range(len(distribution1)):
        if distribution1[i]== 0:
            distribution1[i] = 1e-8
        if distribution2[i] == 0:
            distribution2[i] = 1e-8
        #print distribution1[key], distribution2[key]
        divergence += distribution1[i]*math.log(float(distribution1[i])/distribution2[i])
    return divergence


def samplingRating(n, rating_distribution):
    if n>20000:
        n=20000
    from random import random
    rating_counts = np.zeros(len(rating_distribution))
    for i in range(n):
        r = random()
        tmp = 0
        for j in range(len(rating_distribution)):
            tmp += rating_distribution[j]
            if tmp > r:
                rating_counts[j] += 1
                break
    return rating_counts


def searchForBeta(plotDir, problematic_bnss_list, statconstant, rounds):
    seed = 0
    betadict = dict()
    for i in range(rounds):
        result = ARModelAnalysis(plotDir, problematic_bnss_list, seed, statconstant)
        beta, error = result[0], result[1]
        betadict[beta] = error
        seed += 1
    beta_of_min_error = min(betadict.keys(), key=lambda x: betadict[x])
    print "the optimal beta is %s." % beta_of_min_error, "the error corresponding to the optimal beta is %s." % betadict[beta_of_min_error]
