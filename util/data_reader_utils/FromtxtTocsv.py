import pandas as pd
from pandas import DataFrame, Series, concat
from datetime import datetime
import dateutil

def ChiFromtxtTocsv(txtFile,csvFile):
    Datalist=list()
    with open(txtFile) as Data:
        for line in Data:
            Datalist.append(line.split())

    print Datalist[:2]
    df = DataFrame(Datalist,columns=['date','review_id','user_id','bnss_id','label','x','y','z','stars'])
    print df[:2]
    all_review_date = list()
    for review_date in df['date']:
        date_object = dateutil.parser.parse(review_date)
        all_review_date.append(date_object.date())
    mindate = min(all_review_date)
    print mindate
    df.to_csv(csvFile)


def NYCFromtxtTocsv(txtFile, csvFile):
    Datalist = list()
    with open(txtFile) as Data:
        for line in Data:
            Datalist.append(line.split())

    print Datalist[:2]
    df = DataFrame(Datalist, columns=['user_id', 'bnss_id', 'stars', 'label', 'date'])
    df['review_id'] = Series(range(len(df)))
    df = df.reindex(columns=['date', 'review_id', 'user_id', 'bnss_id', 'label', 'stars'])
    print df[:2]
    all_review_date = list()
    for review_date in df['date']:
        date_object = dateutil.parser.parse(review_date)
        all_review_date.append(date_object.date())
    df['date'] = Series(all_review_date)
    mindate = min(df['date'])
    maxdate = max(df['date'])
    print mindate, maxdate
    df.to_csv(csvFile)


def merge2csv(csv1, csv2, csv):
    df1 = pd.read_csv(csv1)
    df2 = pd.read_csv(csv2)
    df1 = df1.ix[:,['date','review_id','user_id','bnss_id','label','stars']]
    df2 = df2.ix[:,['date','review_id','user_id','bnss_id','label','stars']]
    df = concat([df1,df2])
    bnss_set = set(df['bnss_id'])
    df.to_csv(csv, index=None)


# ChiFromtxtTocsv(r'/tmp/guest-pydie6/Downloads/Yelp Dataset/YelpChi/output_meta_yelpHotelData_NRYRcleaned.txt',
#              r'/tmp/guest-pydie6/Downloads/Yelp Dataset/YelpChi/output_meta_yelpHotelData_NRYRcleaned.csv'
#              )

# ChiFromtxtTocsv(r'/Users/geshuaijun/Yelp Dataset/YelpChi/output_meta_yelpResData_NRYRcleaned.txt',
#              r'/Users/geshuaijun/Yelp Dataset/YelpChi/output_meta_yelpResData_NRYRcleaned.csv')

NYCFromtxtTocsv(r'/home/shared/Yelp Dataset/YelpNYC/metadata',
                 r'/home/sjge17/sjge/timeseries/data/YelpNYC/output_meta_yelpResData_NRYRcleaned.csv')

# merge2csv(r'/Users/geshuaijun/Yelp Dataset/YelpNYC/output_meta_yelpResData_NRYRcleaned.csv',
    #      r'/Users/geshuaijun/Yelp Dataset/YelpChi/output_meta_yelpResData_NRYRcleaned.csv',
     #     r'/Users/geshuaijun/Yelp Dataset/YelpChi+YelpNYC/output_meta_yelpResData_NRYRcleaned.csv')
