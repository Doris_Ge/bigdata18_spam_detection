from pandas import DataFrame, concat, Series
import pandas as pd
from datetime import datetime
from datetime import timedelta
import dateutil
import pickle
import os
from matplotlib import pyplot as plt
import numpy as np

from util.SIAUtil import review, user, business

# MIN_DATE = '2004-10-12'
# MAX_DATE = '2012-10-08'
META_FILE = 'output_meta_yelpResData_NRYRcleaned.csv'

MIN_DATE = '2004-10-12'
MAX_DATE = '2015-01-07'

class pro_business(business):
    def __init__(self, _id, name, rating=2.5, url=None, score=(0.5,0.5), reviewCount=0):
        super(pro_business,self).__init__(_id, name, rating=2.5, url=None, score=(0.5,0.5), reviewCount=0)


class pro_review(review):
     def __init__(self, review_id, user_id, bnss_id, stars, review_date, label, timekey):
         super(pro_review, self).__init__(review_id, user_id, bnss_id, stars, review_date)
         self.label = label
         self.timekey = timekey

     def get_no_of_spam_reviews(self, df):
         bnss = self.id
         bnss_df = df[df['bnss_id'] == bnss]
         spam_df = bnss_df[bnss_df['label'] == 'Y']
         return len(spam_df)

     def getTimekeyOfReview(self):
         return self.timekey


def extractStatisticsForBnss(plotDir, dataFile, mindate, timeLength):
    #df = DataFrame(Datalist, columns=['date', 'review_id', 'user_id', 'bnss_id', 'label', 'x', 'y', 'z', 'stars']) #for YelpChi data
    #df = DataFrame(Datalist, columns=['date', 'review_id', 'user_id', 'bnss_id', 'label', 'stars'])
    df = pd.read_csv(dataFile, index_col=0)
    reviewIdToReviewDict = dict()
    bnssIdToBnssDict = dict()
    usrIdToUsrDict = dict()

    if os.path.exists(os.path.join(plotDir, 'bnssIdToBnssDict')):
        os.unlink(os.path.join(plotDir, 'bnssIdToBnssDict'))
    if os.path.exists(os.path.join(plotDir, 'ReviewIdToReviewDict')):
        os.unlink(os.path.join(plotDir, 'ReviewIdToReviewDict'))

    for tup in df.itertuples():
        ls = list(tup)
        review_date, review_id, user_id, bnss_id, label = ls[1:6]
        date_object = dateutil.parser.parse(review_date)
        timekey = ((date_object.date() - mindate.date()) / timeLength).days
        stars = ls[6]

        if bnss_id not in bnssIdToBnssDict:
            bnssIdToBnssDict[bnss_id] = pro_business(bnss_id, bnss_id)
        bnss = bnssIdToBnssDict[bnss_id]
        bnss.no_of_spam_reviews_in_wid = dict()
        bnss.no_of_all_reviews_in_wid = dict()

        if user_id not in usrIdToUsrDict:
            usrIdToUsrDict[user_id] = user(user_id, user_id)
        usr = usrIdToUsrDict[user_id]

        if review_id not in reviewIdToReviewDict:
            reviewIdToReviewDict[review_id] = pro_review(review_id, user_id, bnss_id, stars, date_object, label,
                                                         timekey)

    pickle.dump(bnssIdToBnssDict, open(os.path.join(plotDir, 'bnssIdToBnssDict'), 'wb'))
    pickle.dump(reviewIdToReviewDict, open(os.path.join(plotDir, 'ReviewIdToReviewDict'), 'wb'))



def countSpamReviewsInWid(plotDir, timeLength, mindate, maxdate, doPlot=False):
    reviewIdToReviewDict=pickle.load(open(os.path.join(plotDir, 'ReviewIdToReviewDict')))
    bnssIdToBnssDict=pickle.load(open(os.path.join(plotDir, 'bnssIdToBnssDict')))
    bnssIdToNoOfSpamInWidDict = dict()
    for review_id in reviewIdToReviewDict:
        bnss_id = reviewIdToReviewDict[review_id].getBusinessID()
        time_key = 0
        while time_key < ((maxdate.date() - mindate.date())/timeLength).days + 1:
            current_time_key = reviewIdToReviewDict[review_id].getTimekeyOfReview()
            if (time_key == current_time_key) and (reviewIdToReviewDict[review_id].label == 'Y' ):
                if time_key not in bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid:
                    bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid[time_key] = 0
                bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid[time_key] += 1
                break
            time_key += 1

    if os.path.exists(os.path.join(plotDir, 'ResData_spam_reviews_in_wid_analysis.txt')):
        os.unlink(os.path.join(plotDir, 'ResData_spam_reviews_in_wid_analysis.txt'))

    for bnss_id in bnssIdToBnssDict:
        if bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid.keys():
            WidofmaxNoOfReviews = max(bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid.keys(),
                                  key = lambda x:bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid[x])
            with open(os.path.join(plotDir, 'ResData_spam_reviews_in_wid_analysis.txt'), 'a') as file:
                file.write(str(bnss_id)+' '+str(WidofmaxNoOfReviews)+ ' '+str(bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid[WidofmaxNoOfReviews])+'\n')
                file.close
            print bnss_id, WidofmaxNoOfReviews, bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid[WidofmaxNoOfReviews]
            bnssIdToNoOfSpamInWidDict[bnss_id] = bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid
        else:
             print bnss_id

        if doPlot:
            keys = np.array(bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid.keys())
            values = np.array(bnssIdToBnssDict[bnss_id].no_of_spam_reviews_in_wid.values())
            plt.plot(keys, values)
            plt.show()
    pickle.dump(bnssIdToNoOfSpamInWidDict, open(os.path.join(plotDir,'bnssIdToNoOfSpamInWidDict'), 'wb'))

    return bnssIdToBnssDict


def countAllReviewsInWid(plotDir, timeLength, mindate, maxdate):
    reviewIdToReviewDict = pickle.load(open(os.path.join(plotDir, 'ReviewIdToReviewDict')))
    bnssIdToBnssDict = pickle.load(open(os.path.join(plotDir, 'bnssIdToBnssDict')))
    for review_id in reviewIdToReviewDict:
        bnss_id = reviewIdToReviewDict[review_id].getBusinessID()
        time_key = 0
        while time_key < ((maxdate.date() - mindate.date())/timeLength).days + 1:
            current_time_key = reviewIdToReviewDict[review_id].getTimekeyOfReview()
            if time_key == current_time_key:
                if time_key not in bnssIdToBnssDict[bnss_id].no_of_all_reviews_in_wid:
                    bnssIdToBnssDict[bnss_id].no_of_all_reviews_in_wid[time_key] = 0
                bnssIdToBnssDict[bnss_id].no_of_all_reviews_in_wid[time_key] += 1
                break
            time_key += 1

    for bnss_id in bnssIdToBnssDict:
        if bnssIdToBnssDict[bnss_id].no_of_all_reviews_in_wid.keys():
            WidofmaxNoOfReviews = max(bnssIdToBnssDict[bnss_id].no_of_all_reviews_in_wid.keys(),
                                  key = lambda x:bnssIdToBnssDict[bnss_id].no_of_all_reviews_in_wid[x])
            with open(os.path.join(plotDir, 'ResData_all_reviews_in_wid_analysis.txt'), 'a') as file:
                file.write(str(bnss_id)+' '+str(WidofmaxNoOfReviews)+ ' '+str(bnssIdToBnssDict[bnss_id].no_of_all_reviews_in_wid[WidofmaxNoOfReviews])+'\n')
                file.close
            print bnss_id, WidofmaxNoOfReviews, bnssIdToBnssDict[bnss_id].no_of_all_reviews_in_wid[WidofmaxNoOfReviews]
        else:
             print bnss_id

    #for date in df['date']:
     #   date_object = dateutil.parser.parse(date)
      #  datelist.append(date_object.date())
    #min_date = min(datelist)
    #max_date = max(datelist)


def RankBusinessByNoOfSpams(plotDir, dataFile):
    df = pd.read_csv(dataFile, index_col=0)
    bnss_set = set(df['bnss_id'])
    bnss_dict = dict()
    skipbnss = list()
    for bnss in bnss_set:
        bnss_df = df[df['bnss_id'] == bnss]
        bnss_dict[bnss] = dict()
        bnss_dict[bnss]['no_of_all_reviews'] = len(bnss_df)
        if len(bnss_df)<200:
            skipbnss.append(bnss)
        #print bnss_df[:10]
        #spam_df = bnss_df[bnss_df['label'] == 'Y']
        spam_df = bnss_df[bnss_df['label'] == -1 ]
        bnss_dict[bnss]['no_of_spams'] = len(spam_df)

    sorted_bnss = sorted(bnss_dict.keys(), key = lambda x:bnss_dict[x]['no_of_spams'], reverse=True)
    print len(sorted_bnss)

    if os.path.exists(os.path.join(plotDir, 'meta_ResData_analysis.txt')):
        os.unlink(os.path.join(plotDir, 'meta_ResData_analysis.txt'))

    for bnss in sorted_bnss:
        with open(os.path.join(plotDir, 'meta_ResData_analysis.txt'), 'a') as file:
            file.write(str(bnss)+' '+str(bnss_dict[bnss]['no_of_spams'])+' '+str(bnss_dict[bnss]['no_of_all_reviews'])+'\n')
            file.close
        print bnss, bnss_dict[bnss]['no_of_spams'], bnss_dict[bnss]['no_of_all_reviews']

    with open(os.path.join(plotDir, 'businesses_with_less_than_200_reviews'), 'w') as f:
        pickle.dump(skipbnss,f)

    skipbnss = pickle.load(open(os.path.join(plotDir, 'businesses_with_less_than_200_reviews')))
    print skipbnss


def filterBnss(plotDir, csv, mindate, maxdate, timeLength):
    df = pd.read_csv(csv)
    bnssToStatsDict = dict()
    all_time_keys = ((maxdate.date()-mindate.date())/timeLength).days + 1
    skip_bnss = list()
    df1 = DataFrame()

    for tup in df.itertuples():
        ls = list(tup)
        review_date, review_id, user_id, bnss_id, label, stars = ls[1:7]
        date_object = dateutil.parser.parse(review_date)
        timekey = ((date_object.date() - mindate.date()) / timeLength).days
        if bnss_id not in bnssToStatsDict:
            bnssToStatsDict[bnss_id] = dict()
            bnssToStatsDict[bnss_id]['No of reviews'] = np.zeros(all_time_keys)
            bnssToStatsDict[bnss_id]['First time key'] = timekey
            bnssToStatsDict[bnss_id]['Last time key'] = timekey
        bnssToStatsDict[bnss_id]['No of reviews'][timekey] += 1
        if bnssToStatsDict[bnss_id]['First time key'] > timekey:
            bnssToStatsDict[bnss_id]['First time key'] = timekey
        if bnssToStatsDict[bnss_id]['Last time key'] < timekey:
            bnssToStatsDict[bnss_id]['Last time key'] = timekey

    for bnss_id in bnssToStatsDict:
        firstTimeKey = bnssToStatsDict[bnss_id]['First time key']
        lastTimeKey = bnssToStatsDict[bnss_id]['Last time key']
        no_of_reviews = bnssToStatsDict[bnss_id]['No of reviews'][firstTimeKey:lastTimeKey+1]

        if firstTimeKey + 24 > lastTimeKey or np.mean(no_of_reviews) < 5:
            skip_bnss.append(bnss_id)

    with open(os.path.join(plotDir, 'bnssToStatsDict'), 'wb') as file:
        pickle.dump(bnssToStatsDict, file)


    print skip_bnss
    print len(skip_bnss)

    datalist = list()
    for tup in df.itertuples():
        ls = list(tup)
        review_date, review_id, user_id, bnss_id, label, stars = ls[1:7]
        if bnss_id not in skip_bnss:
            datalist.append([review_date, review_id, user_id, bnss_id, label, stars])

    df1 = DataFrame(datalist,columns=['review_date', 'review_id','user_id','bnss_id','label','stars'])

    df1.to_csv(os.path.join('/Users/geshuaijun/Yelp Dataset/YelpChi+YelpNYC/', 'output_meta_yelpResData_NRYRcleaned_filter_1.csv'))
    print len(df1)
    print len(set(df1['bnss_id']))


if __name__ == '__main__':
    beforeStat= datetime.now()
    plotDir='/Users/geshuaijun/Yelp Dataset/stats/'
    dataFile = os.path.join('/Users/geshuaijun/Yelp Dataset/YelpChi+YelpNYC/', META_FILE)
    #dataFile = os.path.join('/tmp/guest-pydie6/Downloads/Yelp Dataset/YelpNYC/', 'metadata.csv')
    maxdate = dateutil.parser.parse(MAX_DATE)
    mindate = dateutil.parser.parse(MIN_DATE)
    # extractStatisticsForBnss(plotDir, dataFile, mindate, timeLength=30)
    #countAllReviewsInWid(plotDir, 30, mindate, maxdate)
    #countSpamReviewsInWid(plotDir, 30, mindate, maxdate, doPlot=False)
    #RankBusinessByNoOfSpams(dataFile)
    #filterBnssByNoOfReviews('/tmp/guest-pydie6/Downloads/ratings_Apps_for_Android.csv')
    #filterBnssByDensity('/tmp/guest-pydie6/Downloads/ratings_Apps_for_Android_more_than_1500.csv')
    filterBnss(plotDir,dataFile, mindate, maxdate, timeLength=30)
    #bnssStats = pickle.load(open(os.path.join(plotDir, 'bnssToStatsDict')))
    afterStat =datetime.now()
    print afterStat - beforeStat

