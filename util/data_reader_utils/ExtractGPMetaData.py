import json
from pandas import DataFrame, Series, concat
from dateutil.parser import parse
from datetime import timedelta
#def parseStarFromReview(review):

with open('/tmp/guest-pydie6/Downloads/GooglePlay_Spam.json') as file:
    allbnss_df = DataFrame()
    allbnss = 0
    skipbnss = 0
    for line in file:
        jsonData = json.loads(line)
        statistics_for_current_bnss = dict()
        if len(jsonData['reviews']) > 100 and len(jsonData['reviews']) < 2000:
           reviews_df = DataFrame(jsonData['reviews'])
        else:
            skipbnss += 1
            continue
        star_column = reviews_df['star']
        date_column = reviews_df['date']
        for i in range(len(reviews_df)):
            if not isinstance(star_column[i], str):
                star_column[i] = star_column[i].encode('gbk')
                star_column[i] = float(filter(str.isdigit, star_column[i]))
                date_column[i] = parse(date_column[i])
            reviews_df['bnss_id'] = jsonData['_id']['$oid']
        mindate, maxdate = min(date_column), max(date_column)
        days = (maxdate - mindate).days
        if len(reviews_df)/days < 1:
            skipbnss += 1
            continue
        else:
            print float(len(reviews_df))/days
            allbnss += 1
        reviews_df['date'] = date_column
        reviews_df['star'] = star_column
        reviews_df.reindex(columns=['date','bnss_id', 'author','star'])
        allbnss_df = concat([allbnss_df, reviews_df])
    allbnss_df['review_id'] = Series(range(len(allbnss_df)))
    print len(allbnss_df) , skipbnss, allbnss

