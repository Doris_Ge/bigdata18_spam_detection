import math
from datetime import datetime
from os.path import join

import dateutil
import pandas as pd

from util.SIAUtil import user, business, review

#appid,review id,userid,username,stars,version,date,number of helpful votes,total votes,unix timestamp
META_BNSS_ID = 'bnss_id'
META_REVIEW_ID = 'review_id'
META_USER_ID = 'user_id'
#META_USER_NAME = 'username'
META_STARS = 'stars'
#META_BNSS_VERSION = 'version'
META_DATE  = 'date'
#META_HELPFUL_VOTES ='Helpful Votes'
#META_TOTAL_VOTES = 'Total Votes'
META_TIMESTAMP = 'Timestamp'
'''META_COLS = [META_BNSS_ID,  META_REVIEW_ID, META_USER_ID, META_USER_NAME, \
             META_STARS, META_BNSS_VERSION, META_DATE, META_HELPFUL_VOTES, \
             META_TOTAL_VOTES, META_TIMESTAMP]'''

#META_IDX_DICT = {META_COLS[i]:i for i in range(len(META_COLS))}

#appid,review id,title,content
BNSS_ID = 'bnss_id'
REVIEW_ID = 'review_id'
TITLE = 'title'
CONTENT = 'content'
COLS = [BNSS_ID, REVIEW_ID, TITLE, CONTENT]

REVW_IDX_DICT = {COLS[i]:i for i in range(len(COLS))}

META_FILE = 'output_meta_yelpResData_NRYRcleaned.csv'
#REVIEW_FILE = 'swm_reviews_text.csv'

class YelpChiMetaDataReader:
    def __init__(self):
        self.usrIdToUsrDict={}
        self.bnssIdToBnssDict={}
        self.reviewIdToReviewDict={}

    def readData(self, reviewFolder, readReviewsText=False):
        beforeDataReadTime = datetime.now()
        reviewMetaFile = join(reviewFolder, META_FILE)
        #reviewFile = join(reviewFolder, REVIEW_FILE)
        skippedMeta, skippedData = 0,0
        df1 = pd.read_csv(reviewMetaFile, index_col=0, escapechar='\\', dtype=object, error_bad_lines=False)
        for tup in df1.itertuples():
            ls = list(tup)
            if len(ls) < 10:
                skippedMeta+=1
                continue
            review_date, review_id, user_id, bnss_id, label = ls[1:6]
            stars = ls[9]
            try:
                date_object = dateutil.parser.parse(review_date)
                date_object = date_object.date()
            #if date_object.year > 2012 or date_object.year < 2008:
             #   raise Exception('Invalid Date')
                stars = float(stars)
                if math.isnan(stars) or stars < 0 or stars > 5:
                    raise Exception('Invalid Rating')
            except:
                skippedMeta+=1
                continue

            if label == '-1':
                skippedMeta += 1
                continue

            if bnss_id not in self.bnssIdToBnssDict:
                self.bnssIdToBnssDict[bnss_id] = business(bnss_id, bnss_id)
            bnss = self.bnssIdToBnssDict[bnss_id]

            if user_id not in self.usrIdToUsrDict:
                self.usrIdToUsrDict[user_id] = user(user_id, user_id)
            usr = self.usrIdToUsrDict[user_id]

            revw = review(review_id, usr.getId(), bnss.getId(), stars, date_object)

            if review_id in self.reviewIdToReviewDict:
                print 'Already Read Meta - ReviewId:', review_id

            self.reviewIdToReviewDict[review_id] = revw

        print 'Users:', len(self.usrIdToUsrDict.keys()), \
               'Products:', len(self.bnssIdToBnssDict.keys()), \
               'Reviews', len(self.reviewIdToReviewDict.keys())

        print 'Skipped Lines:', skippedMeta

        if not readReviewsText:
            return (self.usrIdToUsrDict, self.bnssIdToBnssDict, self.reviewIdToReviewDict)


