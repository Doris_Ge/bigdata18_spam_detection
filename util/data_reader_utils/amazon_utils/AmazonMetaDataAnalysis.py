import pandas as pd
from pandas import DataFrame, concat
from datetime import datetime
import numpy as np
import pickle
import os
from util import StatConstants
from matplotlib import pyplot as plt

def filterBnssByNoOfReviews(csvfile):
    df_filter = DataFrame()
    df = pd.read_csv(csvfile, header=None)
    df.columns = ['user_id','bnss_id','stars','date']
    bnss_column = df['bnss_id']
    skipbnss = 0
    filtered_bnss = 0
    bnss_set = set(bnss_column)
    print len(bnss_set)
    df = df.sort_index(by='bnss_id')
    bnss_id = df.ix[0, 'bnss_id']
    count_for_review = 0


def filterBnssByDensity(csvfile):
    df = pd.read_csv(csvfile)
    bnss_column = df['bnss_id']
    bnss_set = set(bnss_column)
    for bnss in bnss_set:
        datelist = list()
        df_bnss = df[df["bnss_id"]==bnss]
        for date in df_bnss['date']:
            dateobject = datetime.fromtimestamp(int(date)).strftime('%Y-%m-%d')
            datelist.append(dateobject)
        mindate = min(datelist)
        maxdate = max(datelist)
        print bnss, mindate, maxdate


def AnalyzeAmazonApp(csvfile, timeLength=7):
    df = pd.read_csv(csvfile)
    bnss_column = df['bnss_id']
    bnss_set = set(bnss_column)
    print "the number of products is ", len(bnss_set)
    skip_bnss = list()
    datelist = list()
    mindate = datetime.fromtimestamp(min(df['date']))
    maxdate = datetime.fromtimestamp(max(df['date']))
    timeKeys = ((maxdate - mindate)/timeLength).days + 1
    print timeKeys
    df_parsedate = DataFrame()
    count_lg_20 = 0
    for bnss in bnss_set:
        datelist = list()
        df_bnss = df[df["bnss_id"] == bnss]
        no_of_all_reviews_in_wid = np.zeros(timeKeys)
        first = 1
        for date in sorted(df_bnss['date']):
            dateobject = datetime.fromtimestamp(int(date))
            datelist.append(datetime.strftime(dateobject, '%Y-%m-%d'))
            timeKey = ((dateobject - mindate)/timeLength).days
            if first == 1:
                firstTimeKey=timeKey
                first = 0
            no_of_all_reviews_in_wid[timeKey] += 1
        no_of_all_reviews_in_wid = no_of_all_reviews_in_wid[firstTimeKey:timeKey+1]
        df_bnss['date'] = datelist
        #df_parsedate = concat([df_parsedate, df_bnss])
        print bnss, max(no_of_all_reviews_in_wid), np.mean(no_of_all_reviews_in_wid), np.std(no_of_all_reviews_in_wid)
        length = len(no_of_all_reviews_in_wid)
        no_of_all_reviews_in_wid = np.array(no_of_all_reviews_in_wid)
        argmax = no_of_all_reviews_in_wid.argmax()
        if np.mean(no_of_all_reviews_in_wid) > 20 and len(no_of_all_reviews_in_wid[argmax:][no_of_all_reviews_in_wid[argmax:]<10])<10 \
                and len(no_of_all_reviews_in_wid) > 24:
            print no_of_all_reviews_in_wid
            count_lg_20 += 1
        else:
            skip_bnss.append(bnss)

        print count_lg_20
    print skip_bnss
    #df_parsedate.to_csv('/tmp/guest-pydie6/Downloads/ratings_Apps_for_Android_more_than_1500_parsedate.csv')



if __name__ ==  '__main__':
    filterBnssByNoOfReviews('/Users/geshuaijun/Amazon Dataset/Apps for Android/ratings_Apps_for_Android.csv')
    # AnalyzeAmazonApp('/Users/geshuaijun/Amazon Dataset/Apps for Android/ratings_Apps_for_Android_more_than_1000.csv')
