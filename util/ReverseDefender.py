from DetectAnalysis import *
from sklearn.linear_model import LinearRegression
from scipy.stats import spearmanr

def generateReverseDataset(suspDir_pre, case, min_removal_rate=0.2, max_removal_rate=0.8, is_random=False):
    bnssKeys = list()
    signals_to_extract = [DEVIATION_OF_AVG_RATING, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS,
                               DIFFERENCE_OF_RATING_ENTROPY, NO_OF_REVIEWS, RATING_ENTROPY,
                               NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]
    if is_random:
        suspDir = os.path.join(suspDir_pre, 'random')
    else:
        suspDir = os.path.join(suspDir_pre, 'case_%s' % case)
    print suspDir
    for root, dirs, files in os.walk(os.path.join(suspDir, 'suspicious_scores_signals')):
        for name in files:
            bnssKeys.append(name)

    columns = list()
    df_list = list()
    bnssKeys = bnssKeys[:10]
    for bnss_key in bnssKeys:
        statistics_for_current_bnss = pickle.load(
            open(os.path.join(os.path.join(suspDir, 'suspicious_scores_signals'), bnss_key)))
        for i in statistics_for_current_bnss:
            print i, bnss_key
            suspicious_score_list = list()
            weight_list = list()
            suspicious_score_signals, stopAttackTime, noiseForAvgRating, noiseForAvgRatingAfDetection, \
            noiseForCMR, noiseForCMRAfDetection, noiseForRank, noiseForRankAfDetection = statistics_for_current_bnss[i]
            f_list = [bnss_key, i]
            for signal in suspicious_score_signals:
                f_list.append(calculateAnomalyRatio(suspicious_score_signals[signal]))
                if not columns:
                    columns = [bnss_key, i]
                    for sig in suspicious_score_signals:
                        columns.append('f_'+sig)
            len_of_f = len(f_list)
            for j in signals_to_extract:
                suspicious_score, weight = calculateSuspiciousScoreWithDifferentweight(suspicious_score_signals, [j])
                suspicious_score_list.append(suspicious_score)
                weight_list.append(weight)

            suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
            weight = [1.0/len(suspicious_score_signals) for k in range(len(suspicious_score_signals))]
            weight_list.append(weight)
            suspicious_score_list.append(suspicious_score)
            suspicious_score, weight= calculateSuspiciousScoreWithRandomWeight(suspicious_score_signals)
            suspicious_score_list.append(suspicious_score)
            weight_list.append(weight)
            df_list.append(f_list + suspicious_score_list + weight_list)

    for signal in signals_to_extract:
        columns.append(signal)
    columns.append('All')
    columns.append('Random')
    for signal in signals_to_extract:
        columns.append('weight_'+signal)
    columns.append('weight_All')
    columns.append('weight_random')

    df = DataFrame(df_list, columns=columns)
    print df.head()
    min_score = dict()
    max_score = dict()

    signals_to_extract = signals_to_extract + ['All', 'Random']
    print len(signals_to_extract)
    for signal in signals_to_extract:
        min_score[signal] = min(df[signal])
        max_score[signal] = max(df[signal])

    new_list = list()
    for num in range(len(df)):
        series = df.iloc[num, :]
        print len(series)
        f_list = list(series[2:len(signals_to_extract)].values)
        print f_list, len(f_list)
        weight_list = list(series[-len(signals_to_extract):])
        f_list = map(lambda x: x**2, f_list)
        for signal in signals_to_extract:
            suspicious_score = series[signal]
            max_s, min_s = max_score[signal], min_score[signal]
            remove_rate = min_removal_rate + (suspicious_score - min_s)/(max_s - min_s) * \
                                              (max_removal_rate - min_removal_rate)
            f_list.append((1-remove_rate)**2)
        f_list = f_list + weight_list
        print list(series[:2])
        new_list.append(list(series[:2].values)+f_list)


    new_df = DataFrame(new_list, columns=columns)
    print new_df.head()
    new_df.to_csv(os.path.join(suspDir, 'reverse_dataset_%s.csv' % case))


def getReversedWeight(susp_dir, case, size='large'):
    weight = list()
    data = pd.read_csv(os.path.join(susp_dir, 'case_%s' % case, 'reverse_dataset_%s.csv' % case), index_col=0)
    if size=='small':
        data = data[:20]
        print len(data)
    if size=='medium':
        data = data[::3]
        print len(data)
    # print data.columns
    label_columns = data.columns[11:-11]
    weight_columns = data.columns[-11:]
    X, Y = data.iloc[:, 2:11], data.iloc[:, 11:-11]
    rho_list = list()
    columns = list()
    for i in range(len(label_columns)):
        print case
        y = Y.iloc[:, i]
        if weight_columns[i] == 'weight_All':
            continue
        columns.append(weight_columns[i])
        weight = data[weight_columns[i]][0]
        print weight
        weight = weight.strip('[]')
        weight = weight.split(',')
        weight = map(lambda x: float(x), weight)
        sum_weight = sum(weight)
        weight = map(lambda x: x/sum_weight, weight)
        model = LinearRegression()
        model.fit(X, y)
        print model.coef_
        rho, p = spearmanr(weight, model.coef_)
        rho_list.append(rho)
        print rho, p
    return rho_list, columns

def formRhoMatrix(susp_dir, data_generation=False):
    rho_df_list = list()
    index = ['Evasion %s' % case for case in range(1, 10)]
    for case in range(1, 10):
        if data_generation:
            generateReverseDataset(susp_dir, case)
        rho_list, columns = getReversedWeight(susp_dir, case)
        rho_df_list.append(rho_list)
    df = DataFrame(rho_df_list, index=index, columns=columns)
    print df
    df.to_csv(os.path.join(susp_dir, 'results', 'rho_matrix.csv'))


susp_dir = '/Users/geshuaijun/Amazon Dataset/susp_stats'

generateReverseDataset(susp_dir, 2)
getReversedWeight(susp_dir, 2, 'small')
# formRhoMatrix(susp_dir, False)