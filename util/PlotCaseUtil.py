import pandas as pd
from matplotlib import pyplot as plt
import matplotlib
import os
import pickle
from util import StatConstants

font = {
   'family': 'serif',
   'serif': ['Times New Roman'],
   'size': 13
}

matplotlib.rc('font', **font)


def plotSub(ax, x, col,labels, *args):
    for i in range(len(args)):
        ax.plot(x, args[i][col],label=labels[i])
        ax.legend(loc='upper left', borderaxespad=0.,prop={'size':4})

def plotCase0AndCase2():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    fig, axarr = plt.subplots(3,2)

    df_case0_all = pd.read_csv(os.path.join(plotDir, 'Case 0/top is 0.2 and is_half is 0/case 0.csv'), index_col=0)
    df_case0_half = pd.read_csv(os.path.join(plotDir, 'Case 0/top is 0.2 and is_half is 1/case 0.csv'), index_col=0)
    df_case2_08 = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and ratio is 0.8/case 2.csv'), index_col=0)
    df_case2_05 = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and ratio is 0.5/case 2.csv'), index_col=0)
    df_case2_02 = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and ratio is 0.2/case 2.csv'), index_col=0)
    x = df_case0_all.index
    cols = list()
    labels = ['all 5-star reviews', 'half 5-star reviews', 'ratio=0.8', 'ratio=0.5', 'ratio=0.2']
    for col in df_case0_all.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i*2+j], labels, df_case0_all, df_case0_half, df_case2_08, df_case2_05, df_case2_02)
            axarr[i,j].set_title(cols[i*2+j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case0 and case1.svg'))
    plt.show()

def plotCase1AndCase2():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    fig, axarr = plt.subplots(3, 2)

    df_case1 = pd.read_csv(os.path.join(plotDir, 'Case 1/top is 0.2/case 1.csv'), index_col=0)
    df_case2_20 = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and no of reviews is 20/case 2.csv'), index_col=0)
    df_case2_50 = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and no of reviews is 50/case 2.csv'), index_col=0)
    df_case2_100 = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and no of reviews is 100/case 2.csv'),
                              index_col=0)

    x = df_case1.index
    cols = list()
    labels = ['case 1', 'case 2 with 20 reviews', 'case 2 with 50 reviews', 'case 2 with 100 reviews']
    for col in df_case1.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case1, df_case2_20, df_case2_50, df_case2_100)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case1 and case2.svg'))
    plt.show()


def plotCase1AndCase3():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    fig, axarr = plt.subplots(3, 2)

    df_case1 = pd.read_csv(os.path.join(plotDir, 'Case 1/top is 0.2/case 1.csv'), index_col=0)
    df_case3 =  pd.read_csv(os.path.join(plotDir, 'Case 3/top is 0.2/case 3.csv'), index_col=0)


    x = df_case1.index
    cols = list()
    labels = ['case 1', 'case 3']
    for col in df_case1.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case1, df_case3)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case1 and case3.svg'))
    plt.show()


def plotCase2AndCase3():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    fig, axarr = plt.subplots(3, 2)

    df_case2_20 = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and no of reviews is 20/case 2.csv'), index_col=0)
    df_case2_50 = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and no of reviews is 50/case 2.csv'), index_col=0)
    df_case2_80 = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and no of reviews is 100/case 2.csv'), index_col=0)
    df_case3 = pd.read_csv(os.path.join(plotDir, 'Case 3/top is 0.2/case 3.csv'), index_col=0)

    x = df_case3.index
    cols = list()
    labels = ['case 2 with 20 reviews', 'case 2 with 50 reviews', 'case 2 with 80 reviews', 'case 3']
    for col in df_case3.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case2_20, df_case2_50, df_case2_80, df_case3)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case2 and case3.svg'))
    plt.show()

def plotCase3AndCase4():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    fig, axarr = plt.subplots(3, 2)

    df_case3 = pd.read_csv(os.path.join(plotDir, 'Case 3/top is 0.2/case 3.csv'), index_col=0)
    df_case4 = pd.read_csv(os.path.join(plotDir, 'Case 4/top is 0.2/case 4.csv'), index_col=0)

    x = df_case3.index
    cols = list()
    labels = ['case 3', 'case 4']
    for col in df_case3.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case3, df_case4)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case3 and case4.svg'))
    plt.show()

def plotCase2withAndwithoutDiff():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    fig, axarr = plt.subplots(3, 2)

    df_case3 = pd.read_csv(os.path.join(plotDir, 'Case 3/top is 0.2/case 3.csv'), index_col=0)
    df_case4 = pd.read_csv(os.path.join(plotDir, 'Case 4/top is 0.2/case 4.csv'), index_col=0)

    x = df_case3.index
    cols = list()
    labels = ['case 3', 'case 4']
    for col in df_case3.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case3, df_case4)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case2 with and without Diff.svg'))
    plt.show()

def plotCase0AndCase2WithAndWithoutDifferences():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    fig, axarr = plt.subplots(3, 2)

    df_case0_with = pd.read_csv(os.path.join(plotDir, 'Case 0/top is 0.2 and is_half is 0/case 0.csv'), index_col=0)
    df_case0_without = pd.read_csv(os.path.join(plotDir, 'Case 0/top is 0.2 and is_half is 0 without differences/case 0.csv'), index_col=0)
    df_case2_with = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and ratio is 0.5/case 2.csv'), index_col=0)
    df_case2_wtihout = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and ratio is 0.5 without differences/case 2.csv'), index_col=0)
    df_case2_only = pd.read_csv(os.path.join(plotDir, 'Case 2/top is 0.2 and ratio is 0.5 only reviews/case 2.csv'), index_col=0)

    x = df_case0_with.index
    cols = list()
    labels = ['case 0 with all singals', 'case 0 without diff signals', 'case 2 with all signals', 'case 2 without diff signals', 'case 2 with only reviews signals']
    for col in df_case0_with.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case0_with, df_case0_without, df_case2_with, df_case2_wtihout, df_case2_only)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case0 and case2 with and without diff signals.svg'))
    plt.show()

def plotCase1WithandWithoutReviews():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    fig, axarr = plt.subplots(3, 2)

    df_case1_without_re_and_pre = pd.read_csv(os.path.join(plotDir, 'Case 1/top is 0.2 without reviews and predictions/case 1.csv'), index_col=0)
    df_case1_without_pre = pd.read_csv(
        os.path.join(plotDir, 'Case 1/top is 0.2 without predictions/case 1.csv'), index_col=0)
    df_case1_without_re = pd.read_csv(os.path.join(plotDir, 'Case 1/top is 0.2 without reviews/case 1.csv'), index_col=0)
    df_case1 = pd.read_csv(
        os.path.join(plotDir, 'Case 1/top is 0.2/case 1.csv'), index_col=0)

    x = df_case1.index
    cols = list()
    labels = ['case 1 wo pre and re', 'case 1 wo pre', 'case 1 wo re',
              'case 1']
    for col in df_case1.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case1_without_re_and_pre, df_case1_without_pre, df_case1_without_re, df_case1)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case1 with and without reviews.svg'))

    plt.show()

def plotCase1BfAndAfImprove():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    plotDir_improve = '/Users/geshuaijun/Yelp Dataset/plots_improve/'
    fig, axarr = plt.subplots(3, 2)

    df_case1_bf_improve = pd.read_csv(
        os.path.join(plotDir, 'Case 1/top is 0.2/case 1.csv'), index_col=0)
    df_case1_af_improve = pd.read_csv(
        os.path.join(plotDir_improve, 'Case 1/top is 0.2/case 1.csv'), index_col=0)


    x = df_case1_bf_improve.index
    cols = list()
    labels = ['case 1 bf improve',
              'case 1 af improve']
    for col in df_case1_bf_improve.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case1_bf_improve, df_case1_af_improve)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case1 bf and af improve.svg'))

    plt.show()


def plotCase0BfAndAfImprove():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    plotDir_improve = '/Users/geshuaijun/Yelp Dataset/plots_improve/'
    fig, axarr = plt.subplots(3, 2)

    df_case1_bf_improve = pd.read_csv(
        os.path.join(plotDir, 'Case 0/top is 0.2 and is_half is 1/case 0.csv'), index_col=0)
    df_case1_af_improve = pd.read_csv(
        os.path.join(plotDir_improve, 'Case 0/top is 0.2 and is_half is 1/case 0.csv'), index_col=0)


    x = df_case1_bf_improve.index
    cols = list()
    labels = ['case 0 bf improve',
              'case 0 af improve']
    for col in df_case1_bf_improve.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case1_bf_improve, df_case1_af_improve)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case0 bf and af improve.svg'))

    plt.show()


def plotCase2BfAndAfImprove():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    plotDir_improve = '/Users/geshuaijun/Yelp Dataset/plots_improve/'
    fig, axarr = plt.subplots(3, 2)

    df_case1_bf_improve = pd.read_csv(
        os.path.join(plotDir, 'Case 2/top is 0.2 and no of reviews is 20/case 2.csv'), index_col=0)
    df_case1_af_improve = pd.read_csv(
        os.path.join(plotDir_improve, 'Case 2/top is 0.2 and no of reviews is 20/case 2.csv'), index_col=0)


    x = df_case1_bf_improve.index
    cols = list()
    labels = ['case 2 bf improve',
              'case 2 af improve']
    for col in df_case1_bf_improve.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case1_bf_improve, df_case1_af_improve)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case2 bf and af improve.svg'))

    plt.show()


def plotCase3BfAndAfImprove():
    plotDir = '/Users/geshuaijun/Yelp Dataset/plots/'
    plotDir_improve = '/Users/geshuaijun/Yelp Dataset/plots_improve/'
    fig, axarr = plt.subplots(3, 2)

    df_case1_bf_improve = pd.read_csv(
        os.path.join(plotDir, 'Case 3/top is 0.2/case 3.csv'), index_col=0)
    df_case1_af_improve = pd.read_csv(
        os.path.join(plotDir_improve, 'Case 3/top is 0.2/case 3.csv'), index_col=0)


    x = df_case1_bf_improve.index
    cols = list()
    labels = ['case 3 bf improve',
              'case 3 af improve']
    for col in df_case1_bf_improve.columns:
        if col != 'burst_ratio':
            cols.append(col)

    for i in range(3):
        for j in range(2):
            plotSub(axarr[i, j], x, cols[i * 2 + j], labels, df_case1_bf_improve, df_case1_af_improve)
            axarr[i, j].set_title(cols[i * 2 + j])

    fig.suptitle('top=0.2')
    plt.tight_layout()
    fig.savefig(os.path.join(plotDir, 'compare/case3 bf and af improve.svg'))

    plt.show()


def plotFigure1(bnss, plotDir):
    bnssKey = list()
    print plotDir
    for root, dir, files in os.walk(plotDir):
        for name in files:
            bnssKey.append(name)

    statistics_for_current_bnss = pickle.load(open(os.path.join(plotDir, bnss)))
    last_time_key = statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]
    first_time_key = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]
    NR = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS][first_time_key:last_time_key]
    CAR = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][first_time_key:last_time_key]
    t = range(len(NR))
    plt.plot(t, NR, 'b-')
    plt.xlabel('time windows')
    plt.ylabel('NR')
    plt.show(os.path.join(plotDir, os.pardir, os.pardir, os.pardir, 'plots'))
    plt.savefig(os.path.join(plotDir, os.pardir, os.pardir, os.pardir, 'plots', 'NR.eps'))

    plt.plot(t, CAR, 'r-')
    plt.xlabel('time windows')
    plt.ylabel('CAR')
    plt.savefig(os.path.join(plotDir, os.pardir, os.pardir, os.pardir, 'plots', 'CAR.eps'))
    plt.show()


# plotCase2AndCase3()
# plotCase2withAndwithoutDiff()
# plotCase1AndCase2()
# plotCase0AndCase2WithAndWithoutDifferences()
# plotCase1WithandWithoutReviews()
# plotCase3AndCase4()
# plotCase0AndCase2()
# plotCase1AndCase3()
plotFigure1('B0067VKQLE', '/Users/geshuaijun/Amazon Dataset/stats/bnss_stats/1-W/')