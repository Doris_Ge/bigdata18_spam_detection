import pandas as pd
from pandas import DataFrame, concat
import os
from util.DetectAnalysis import newDefenderUtil, calculateSuspiciousScore

suspDir = '/Users/geshuaijun/Amazon Dataset/susp_stats/'
plotDir = '/Users/geshuaijun/Amazon Dataset/stats/'
suspDir_case = os.path.join(suspDir, 'case_1')
DF_DIR = 'suspicious_scores_signals'
bnssKeys = list()


for root, dirs, files in os.walk(os.path.join(suspDir_case, DF_DIR)):
    for name in files:
        bnssKeys.append(name)

for case in range(1, 10):
    newDefenderUtil(plotDir, calculateSuspiciousScore, case, [], log=True)

case_dict = dict()

df = DataFrame()
for case in range(1, 10):
    df_case = pd.read_csv(os.path.join(suspDir, 'results', 'case_%s_pre.csv' % case), index_col=0)
    df_case['case'] = case
    df = concat([df, df_case])

print df['case']
for bnss_key in bnssKeys:
    for t in range(5):
        df_for_current_bnss = df[df['bnss_key'] == bnss_key]
        df_t = df_for_current_bnss[df_for_current_bnss['attack_time'] == t]
        df_t = df_t[df_t['is_stop_attack'] == 0]
        if df_t.empty:
            continue
        df_t.sort_values(by='noise_for_cmr_af_detection', ascending=False, inplace=True)
        max_series = df_t.iloc[0, :]
        max_case = max_series['case']
        if max_case not in case_dict:
            case_dict[max_case] = 0
        case_dict[max_case] += 1

print case_dict








