# -*- coding: utf-8 -*-
from AttackAnalysis import *
from DetectAnalysis import *
from OptimizeAttack import chooseAttackStrategyForEachBnss, chooseEvasionStrategyForEachBnssWithoutDetection, \
    chooseEarlyAttackStrategyForEachBnss, maximizeNoiseInCDF
from IPython import embed
from datetime import datetime


# 1. n_\delta
# 2. delta_t
# 3. KL-DIV
# 4. EN
# 5. CAR-DEV, \Delta-CAR
# 6. NR and \Delta-NR
# 7. EN, \Delta-EN
# 8. KL-DIV, Entropy lower bound
# 9. KL-DIV, positive ratio


def extractCMRtoDataFrame(plotDir, statistics_for_current_bnss_dir, bnssKeys):
    CMR_list = list()
    for bnss_key in bnssKeys:
        with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
            statistics_for_current_bnsses = pickle.load(file)
        ratingDistribution = statistics_for_current_bnsses[StatConstants.RATING_DISTRIBUTION]
        timeKeys = len(ratingDistribution[list(ratingDistribution.keys())[0]])
        CMR = np.zeros(timeKeys)
        for time_key in range(timeKeys):
            noOfReviews = 0
            ratingSum = 0
            for key in ratingDistribution:
                if time_key == 0:
                    currentNoOfReviews = ratingDistribution[key][time_key]
                else:
                    currentNoOfReviews = ratingDistribution[key][time_key] - ratingDistribution[key][time_key - 1]
                noOfReviews += currentNoOfReviews
                ratingSum += currentNoOfReviews * key
            if noOfReviews:
                CMR[time_key] = float(ratingSum) / noOfReviews
        CMR_series = Series(CMR, name=bnss_key)
        CMR_list.append(CMR_series)
    df = DataFrame(CMR_list)
    df.to_csv(os.path.join(plotDir, 'bnss_stats', 'CMR.csv'))
    print df.head()

def EarlyGame(csvFolder, signals_to_be_extracted, ratio, case, susp_stats='susp_stats', stratify=False):
    plotDir = os.path.join(os.path.join(csvFolder, os.pardir), 'stats')
    suspDir = os.path.join(csvFolder, os.pardir, susp_stats, 'early_stage_%s' % case)
    if 'Yelp' in csvFolder:
        time_dir = '1-M'
    else:
        time_dir = '1-W'

    np.random.seed(5)
    statistics_for_current_bnss_dir, bnssKeys = extractBnssKeys(plotDir, time_dir)
    print(len(set(bnssKeys)), len(bnssKeys))
    train_idx = np.random.choice(np.arange(len(bnssKeys)), int(0.3*len(bnssKeys)))
    test_idx = np.setdiff1d(np.arange(len(bnssKeys)), train_idx)
    train_bnsses, test_bnsses = np.array(bnssKeys)[train_idx], np.array(bnssKeys)[test_idx]
    count_business = 0
    count_min = 0
    total = 0
    bnss_credit_af_detection_dict = dict()

    all_signals = [AVG_RATING, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS, NO_OF_REVIEWS, RATING_ENTROPY]

    statistics_for_train_bnsses, statistics_for_train_measures = poolEarlyData(plotDir, time_dir, all_signals,
                                                                               time_key=30, bnss_keys=train_bnsses)
    if stratify:
        car_of_train_bnsses = statistics_for_train_bnsses[AVG_RATING]
        print(len(car_of_train_bnsses))
        low_CAR_idx = [i for i in range(len(car_of_train_bnsses)) if car_of_train_bnsses[i] < 3]
        high_CAR_idx = np.setdiff1d(np.arange(len(car_of_train_bnsses)), low_CAR_idx)
        print(max(high_CAR_idx))
        train_stats_for_low_car = dict()
        train_stats_for_high_car = dict()
        stratified_stats = dict()
        for key in statistics_for_train_bnsses:
            print(len(statistics_for_train_bnsses[key]))
            train_stats_for_low_car[key] = statistics_for_train_bnsses[key][low_CAR_idx]
            train_stats_for_high_car[key] = statistics_for_train_bnsses[key][high_CAR_idx]
            stratified_stats[key] = {'low': train_stats_for_low_car[key],
                                     'high': train_stats_for_high_car[key]}
        constraints = dict()
        for signal in all_signals:
            constraints[signal] = {'low': maximizeNoiseInCDF(train_stats_for_low_car[signal], ratio=ratio),
                                   'high': maximizeNoiseInCDF(train_stats_for_high_car[signal], ratio=ratio)}
            print signal, constraints[signal]
        statistics_for_train_bnsses = stratified_stats
    else:
        print(statistics_for_train_bnsses.keys())
        constraints = dict()
        for signal in all_signals:
            constraints[signal] = maximizeNoiseInCDF(statistics_for_train_bnsses[signal], ratio=ratio)
            print signal, constraints[signal]

    print " constraints", constraints

    # test_bnsses = ['99']
    for bnss_key in test_bnsses:
        print("bnss_key", bnss_key)
        with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
            statistics_for_current_bnss = pickle.load(file)
            if len(statistics_for_current_bnss[StatConstants.AVERAGE_RATING]) == 98:
                continue
        attackReults = chooseEarlyAttackStrategyForEachBnss(statistics_for_train_bnsses,
                                                            statistics_for_current_bnss, plotDir, bnss_key,
                                                            suspDir, count_min, constraints, case, time_dir, stratify)
        # choose evasion strategy for each business

        if attackReults != -1:

            if bnss_key not in bnss_credit_af_detection_dict:
                bnss_credit_af_detection_dict[bnss_key] = 0

            count_business += 1

            avgRatingsAfAttack, noOfReviewsAfAttack, entropyAfAttack, ratingDivergenceAfAttack, noOfPositiveReviewsAfAttack, \
            startTimeKeyForPrediction, stopAttackTime, count_min, succeed_min_time, noisesForAvgRating, \
            noisesForCMR, noisesForRank, review_num, statistics_for_test_bnss = attackReults

            # poolData(plotDir, '1-W', all_signals, startTimeKeyForPrediction, bnss_key)
            total += review_num

            EarlyOfflineDetectAnalysisWithinAttack(suspDir, bnss_key, signals_to_be_extracted, avgRatingsAfAttack, noOfReviewsAfAttack,
                                              entropyAfAttack, stopAttackTime, noisesForAvgRating, noisesForCMR,
                                              noisesForRank, statistics_for_train_bnsses,
                                              statistics_for_test_bnss, stratify)




def OfflineGameV2(csvFolder, signals_to_be_extracted, noiseForNoOfReviews, ratio, case, top=0.2, recall=(0.8, 0.2), random=0, susp_stats='susp_stats'):
    print 'case is:', case
    plotDir = os.path.join(os.path.join(csvFolder, os.pardir), 'stats')

    # randomly choosing attack strategy or choosing one specific attack strategy
    if random:
        suspDir = os.path.join(csvFolder, os.pardir, susp_stats, 'random_%s' % random)
    else:
        suspDir = os.path.join(csvFolder, os.pardir, susp_stats, 'case_%s' % case)

    if not os.path.exists(suspDir):
        os.mkdir(suspDir)

    if 'Yelp' in csvFolder:
        time_dir = '1-M'
    else:
        time_dir = '1-W'

    statistics_for_current_bnss_dir, bnssKeys = extractBnssKeys(plotDir, time_dir)
    # extractCMRtoDataFrame(plotDir, statistics_for_current_bnss_dir, bnssKeys)
    # bnssKeys = bnssKeys[:1]
    count_business = 0
    count_min = 0
    total = 0
    bnss_credit_af_detection_dict = dict()

    all_signals = [DEVIATION_OF_AVG_RATING, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS,
                               DIFFERENCE_OF_RATING_ENTROPY, NO_OF_REVIEWS, RATING_ENTROPY,
                               NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]

    for bnss_key in bnssKeys:
        attackReults = chooseAttackStrategyForEachBnss(statistics_for_current_bnss_dir, plotDir, suspDir, time_dir,
                                                         all_signals, noiseForNoOfReviews, bnss_key, count_min,
                                                         case, ratio=ratio, recall=recall, random=random)
        # choose evasion strategy for each business
        if attackReults != -1:

            if bnss_key not in bnss_credit_af_detection_dict:
                bnss_credit_af_detection_dict[bnss_key] = 0

            count_business += 1

            avgRatingErrorAfAttack, avgRatingAfAttack, noOfReviewsAfAttack, entropyAfAttack, ratingDivergenceAfAttack, \
            noOfPositiveReviewsAfAttack, startTimeKeyForPrediction, stopAttackTime, \
            statistics_for_all_bnsses, statistics_for_all_measures, count_min, succeed_min_time, noisesForAvgRating, \
            noisesForAvgRatingAfDetection, noisesForCMR, noisesForCMRAfDetection, noisesForRank, noisesForRankAfDetection,\
               review_num  = attackReults

            # poolData(plotDir, '1-W', all_signals, startTimeKeyForPrediction, bnss_key)
            total += review_num

            OfflineDetectAnalysisWithinAttack(suspDir, bnss_key, signals_to_be_extracted,
                                              avgRatingErrorAfAttack, avgRatingAfAttack, noOfReviewsAfAttack, entropyAfAttack,
                                              ratingDivergenceAfAttack, stopAttackTime, noOfPositiveReviewsAfAttack,
                                              noisesForAvgRating, noisesForAvgRatingAfDetection, noisesForCMR, noisesForCMRAfDetection,
                                              noisesForRank, noisesForRankAfDetection, statistics_for_all_bnsses,
                                              statistics_for_all_measures)

    # count_for_FP, all_timeKeys, count_for_stop_attack, all_attack_times, credit_mean, credit_af_detection_mean, \
    # avg_credit_for_CMR, avg_creidt_for_CMR_af_detection, avg_rank, avg_rank_af_detection, \
    # count_for_all_burst, count_for_success_of_detection_of_burst = rankSuspiciousScoreForMultipleBnsses(suspDir, top)
    # count_for_success_of_detection = all_timeKeys - count_for_FP
    # attackTimes = all_attack_times - count_for_stop_attack

    # print count_for_FP, count_for_success_of_detection, all_timeKeys, credit_mean, credit_af_detection_mean, count_business, attackTimes, \
    #     count_for_success_of_detection_of_burst, total


def OfflineGameV3(signals_to_be_extracted, noiseForNoOfReviews, ratio, case, top=0.2, is_random=False):
    # csvFolder = '/home/sjge17/sjge/timeseries/data'
    # csvFolder = '/home/sjge17/sjge/Amazon Dataset/Apps for Android/'
    print 'ratio is %s' % ratio
    csvFolder = '/Users/geshuaijun/Amazon Dataset/Apps for Android/'
    # csvFolder = '/Users/geshuaijun/Yelp Dataset/YelpChi/'
    plotDir = os.path.join(os.path.join(csvFolder, os.pardir), 'stats')
    suspDir = os.path.join(os.path.join(plotDir, os.pardir, 'susp_stats', 'case_%s' % case))
    if not os.path.exists(suspDir):
        os.mkdir(suspDir)
    statistics_for_current_bnss_dir, bnssKeys = extractBnssKeys(plotDir, '1-W')
    # bnssKeys = bnssKeys[:5]
    count_business = 0
    count_for_all_bursts = 0
    count_min = 0
    bnss_credit_af_detection_dict = dict()

    all_signals = [DEVIATION_OF_AVG_RATING, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS,
                   DIFFERENCE_OF_RATING_ENTROPY, NO_OF_REVIEWS, RATING_ENTROPY,
                   NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]

    for bnss_key in bnssKeys:
        attackReults = chooseEvasionStrategyForEachBnssWithoutDetection(statistics_for_current_bnss_dir, plotDir, '1-W',
                                                       all_signals, noiseForNoOfReviews, bnss_key,
                                                       count_min, case, ratio=ratio, is_random=is_random)

        if attackReults != -1:

            if bnss_key not in bnss_credit_af_detection_dict:
                bnss_credit_af_detection_dict[bnss_key] = 0

            count_business += 1
            avgRatingErrorAfAttack, avgRatingAfAttack, noOfReviewsAfAttack, entropyAfAttack, ratingDivergenceAfAttack, \
            noOfPositiveReviewsAfAttack, startTimeKeyForPrediction, stopAttackTime, \
            statistics_for_all_bnsses, statistics_for_all_measures = attackReults
            newOfflineDetectAnalysisWithinAttack(suspDir, bnss_key, signals_to_be_extracted,
                                              avgRatingErrorAfAttack, avgRatingAfAttack, noOfReviewsAfAttack,
                                              entropyAfAttack,
                                              ratingDivergenceAfAttack, stopAttackTime, noOfPositiveReviewsAfAttack,
                                                statistics_for_all_bnsses,
                                              statistics_for_all_measures)


