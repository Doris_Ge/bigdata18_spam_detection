import os
import pickle
import pandas as pd
from IPython import embed

def produceSpEagleCSV(susp_dir, case):
    attack_stats_dir = os.path.join(susp_dir, 'early_stage_%s' % case, 'attack_stats')
    test_df = pd.read_csv(os.path.join(susp_dir, 'early_stage_%s' % case, 'early_%s_DETER.csv' % case), index_col=0)
    print(test_df.head())
    bnssKeys = set(test_df['bnss_key'])
    bnss_to_test_times = dict()
    for ls in test_df[['bnss_key', 'attack_time', 'Adaptive ($w^s$)', 'All', 'Random',
                       'Max', 'Max Ensemble', 'Avg Ensemble', 'Best_P']].itertuples():
        bnss_key, attack_time = ls[1:3]
        suspicious_scores = ls[3:]
        if bnss_key not in bnss_to_test_times:
            bnss_to_test_times[bnss_key] = list()
        bnss_to_test_times[bnss_key].append([attack_time, suspicious_scores])


    # file_list_size = []
    # for root, dirs, files in os.walk(attack_stats_dir):
    #     for name in files:
    #         file_list_size.append((name, os.path.getsize(os.path.join(attack_stats_dir, name))))
    #     file_list_size = sorted(file_list_size, key=lambda x: x[1], reverse=True)
    #
    # bnssKeys = [file_name for file_name,
    #                           size in file_list_size]
    print(len(bnssKeys))
    attack_stats_for_all_windows = list()
    count = 0
    for bnss_key in bnssKeys:
        attack_stats = pickle.load(open(os.path.join(attack_stats_dir, bnss_key), 'rb'))
        rating_for_attack = attack_stats['rating for attack']
        start_dates = attack_stats['attack_start_date']
        end_dates = attack_stats['attack_end_date']
        stop_attack_time = attack_stats['stop attack']
        test_times, suspicious_scores = zip(*bnss_to_test_times[bnss_key])

        for idx, i in enumerate(test_times):
            scores = list(suspicious_scores[idx])
            count += 1
            if i in stop_attack_time:
                rating_for_attack[i] = [0, 0, 0, 0, 0]
            attack_stats_for_all_windows.append([bnss_key, start_dates[i], end_dates[i]] + list(rating_for_attack[i]) +
                                                                                scores)

    df = pd.DataFrame(attack_stats_for_all_windows, columns=['bnss_id', 'start_date', 'end_date', '1', '2', '3', '4',
                                                             '5', 'Adaptive ($w^s$)', 'All', 'Random',
                                                            'Max', 'Max Ensemble', 'Avg Ensemble', 'Best P'])
    df.to_csv(os.path.join(attack_stats_dir, '..', 'attack_stats_%s.csv' % case))
    print(df.head())
    print(len(df)*1.0/count)

def splitYelpChiAndYelpNYC(susp_dir, case):
    attack_csv = os.path.join(susp_dir, 'early_stage_%s' % case, 'attack_stats_%s.csv' % case)
    df = pd.read_csv(attack_csv, index_col=0)
    chi_bnss = list()
    nyc_bnss = list()
    for idx, row in df.iterrows():
        try:
            bnss_id = int(row['bnss_id'])
            nyc_bnss.append(idx)
        except:
            chi_bnss.append(idx)

    df_chi = df.loc[chi_bnss, :]
    df_nyc = df.loc[nyc_bnss, :]
    print(len(set(df_chi['bnss_id'])), len(set(df_nyc['bnss_id'])))
    print(df_chi.head())
    print(df_nyc.head())
    df_chi.to_csv(os.path.join(susp_dir, 'early_stage_%s' % case, 'attack_stats_%s_chi.csv' % case))
    df_nyc.to_csv(os.path.join(susp_dir, 'early_stage_%s' % case, 'attack_stats_%s_nyc.csv' % case))

def transferYelpChiBnss():
    with open("../data/metadata.dms") as f:
        data = f.readlines()

    lines = list()
    for line in data:
        line = line.strip()
        lines.append(line.split())

    df_chi = pd.DataFrame(lines, columns=["user_id", "bnss_id", "rating", "label", "date"])[-61541:]
    print(df_chi.head())
    df = pd.read_csv("../data/Yelp Dataset/YelpChi/output_meta_yelpResData_NRYRcleaned.csv", index_col=0)
    print(df.head())
    bnssid_to_idx = dict()

    for i, bnss_id in enumerate(list(df["bnss_id"])):
        if bnss_id not in bnssid_to_idx:
            bnssid_to_idx[bnss_id] = list(df_chi["bnss_id"])[i]
        continue

    print(len(bnssid_to_idx))
    df_chi_early_e1 = pd.read_csv("../data/Yelp Dataset/susp_stats/early_stage_1/attack_stats_1_chi.csv", index_col=0)
    bnss_ids = [bnssid_to_idx[bnss_id] for bnss_id in df_chi_early_e1["bnss_id"]]
    print(bnss_ids[:10])

    df_chi_early_e1["bnss_id"] = bnss_ids
    print(df_chi_early_e1.head())
    df_chi_early_e1.to_csv("../data/data/YelpChi_Early_E1.csv")

    df_chi_early_e1 = pd.read_csv("../data/Yelp Dataset/susp_stats/early_stage_2/attack_stats_2_chi.csv",index_col=0)
    bnss_ids = [bnssid_to_idx[bnss_id] for bnss_id in df_chi_early_e1["bnss_id"]]

    df_chi_early_e1["bnss_id"] = bnss_ids
    df_chi_early_e1.to_csv("../data/data/YelpChi_Early_E2.csv")

    df_chi_early_e1 = pd.read_csv("../data/Yelp Dataset/susp_stats/case_4/attack_stats_4_chi.csv", index_col=0)
    bnss_ids = [bnssid_to_idx[bnss_id] for bnss_id in df_chi_early_e1["bnss_id"]]

    df_chi_early_e1["bnss_id"] = bnss_ids
    df_chi_early_e1.to_csv("../data/data/YelpChi_Late_E4.csv")





susp_stats = '../data/Yelp Dataset/susp_stats/'
# produceSpEagleCSV(susp_stats, case=1)
transferYelpChiBnss()
# splitYelpChiAndYelpNYC(susp_stats, case=1)