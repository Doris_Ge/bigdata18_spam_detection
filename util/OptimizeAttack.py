from AttackAnalysis import *
import DetectAnalysis
from numpy.random import randint, random
import pandas as pd
from copy import deepcopy
from IPython import embed


def optimizeRatingForAttack(x, ratingBfAttack, ratingBfAttackKeys):
    alpha, beta, gamma, _lambda = x[0], x[1], x[2], x[3]
    S = f_S(ratingBfAttack, ratingBfAttackKeys, alpha, beta, gamma, _lambda)
    Z = f_Z(gamma, S)
    ratingForAttack = [np.exp(S[i]/(1+gamma))/Z for i in range(len(ratingBfAttackKeys))]
    return ratingForAttack, ratingBfAttackKeys, S, Z

def indicate_f(i):
    if i in [1,2,3]:
        return 0
    if i in [4,5]:
        return 1


def f_S(ratingBfAttack, ratingBfAttackKeys,alpha,beta,gamma,_lambda):
    S = list()
    for i in range(len(ratingBfAttackKeys)):
        S_i = np.log(ratingBfAttack[i])-1-(alpha-beta)*ratingBfAttackKeys[i]-_lambda*indicate_f(ratingBfAttackKeys[i])-gamma
        S.append(S_i)
    return S


def f_Z(gamma, S):
    S = np.array(S)
    return np.sum(np.exp(S/(1+gamma)))


def f_optimize(ratingBfAttack, ratingBfAttackKeys, U, B, P, H):
    def LagrangeDualFunction(x):
        alpha, beta, gamma, _lambda = x[0], x[1], x[2], x[3]
        ratingForAttack, ratingForAttackKeys, S, Z = optimizeRatingForAttack(x, ratingBfAttack, ratingBfAttackKeys)
        return -(-(1+gamma)*np.log(Z)-(1+gamma)*np.sum(ratingForAttack)-alpha*U+beta*B-_lambda*P+gamma*H)
    return  LagrangeDualFunction


def fprime_optimize(ratingBfAttack, ratingBfAttackKeys, U, B, P, H):
    def calculateGradient(x):
        alpha, beta, gamma, _lambda = x[0], x[1], x[2], x[3]
        ratingForAttack, ratingForAttackKeys, S, Z = optimizeRatingForAttack(x, ratingBfAttack, ratingBfAttackKeys)
        I = [indicate_f(ratingForAttackKeys[i]) for i in range(len(ratingForAttackKeys))]
        gradientOfAlphaForL = np.sum([ratingForAttack[i]*ratingForAttackKeys[i]
                                      for i in range(len(ratingForAttackKeys))]) - U
        gradientOfBetaForL = np.sum([-ratingForAttack[i]*ratingForAttackKeys[i]
                                      for i in range(len(ratingForAttackKeys))]) + B
        gradientOfLambdaForL = np.sum([ratingForAttack[i]*I[i]
                                      for i in range(len(ratingForAttackKeys))]) - P
        gradientOfGammaForL = - np.log(Z) - np.sum([ratingForAttack[i]*(-1-S[i]/(1+gamma))
                                                    for i in range(len(ratingForAttackKeys))]) + H-1
        gradientOfAlphaForL = - gradientOfAlphaForL
        gradientOfBetaForL = - gradientOfBetaForL
        gradientOfLambdaForL = - gradientOfLambdaForL
        gradientOfGammaForL = - gradientOfGammaForL
        return np.array([gradientOfAlphaForL, gradientOfBetaForL, gradientOfGammaForL, gradientOfLambdaForL]).astype(np.float64)
    return calculateGradient


def chooseRatingDistribution(AvgRatingBfAttack, AvgRatingBfT, noOfAllReviewsBfAttack, noOfAllReviewsBfT, noOfTruthfulReviews, rating_distribution, timeKey,
                               AvgRatingNoise, NoOfReviewsNoise, P, H, count_min, succeed_min_time, case):
    lowestBound = -((AvgRatingBfAttack + AvgRatingNoise - AvgRatingNoise/2)*(noOfAllReviewsBfAttack + NoOfReviewsNoise)
                    - AvgRatingBfT * noOfAllReviewsBfT)/ (NoOfReviewsNoise + noOfTruthfulReviews)

    if lowestBound < -5:
        # print "fail to find a viable lowerBound"
        return -1

    tolerance = 0
    rc = -1
    # print "inspect noOfAllReviewsBfAttack", noOfAllReviewsBfAttack == noOfAllReviewsBfT + noOfTruthfulReviews
    while tolerance < float(AvgRatingNoise)/2:
        upperBound = ((AvgRatingBfAttack + AvgRatingNoise) * (
        noOfAllReviewsBfAttack + NoOfReviewsNoise) - AvgRatingBfT * noOfAllReviewsBfT) / (NoOfReviewsNoise + noOfTruthfulReviews)
        lowerBound = -((AvgRatingBfAttack + AvgRatingNoise - tolerance) * (
        noOfAllReviewsBfAttack + NoOfReviewsNoise) - AvgRatingBfT * noOfAllReviewsBfT) / (NoOfReviewsNoise + noOfTruthfulReviews)
        # print "lower bound is %s." % lowerBound

        if lowerBound < - 5:
            tolerance += 0.25 * AvgRatingNoise
            continue

        if case in [5, 6, 7]:
            ratingBfAttackDict = dict()
            for key in rating_distribution.keys():
                if rating_distribution[key][timeKey]:
                    ratingBfAttackDict[key] = rating_distribution[key][timeKey]
            ratingBfAttackKeys, ratingBfAttack = zip(*sorted(ratingBfAttackDict.iteritems(), key=lambda x: x[0]))
            ratingBfAttack = [float(ratingBfAttack[key]) / noOfAllReviewsBfAttack for key in
                            range(len(ratingBfAttack))]
            # print "inspect ratingDistribution", sum(ratingBfAttack)

        if case in [8, 9]:
            ratingBfAttackKeys = rating_distribution.keys()
            ratingBfAttack = list()
            for key in range(len(ratingBfAttackKeys)):
                ratingBfAttack.append(1.0/len(ratingBfAttackKeys))
            # print "inspect rating distribution bf attack", ratingBfAttack

        # check = check_grad(f_optimize(ratingBfAttack, ratingBfAttackKeys, upperBound, -lowerBound, P, H),
        #                    fprime_optimize(ratingBfAttack, ratingBfAttackKeys, upperBound, -lowerBound, P, H),
        #                    np.array([1, 1, 1, 1]))
        # print 'check:', check

        bounds = [(0, float("inf"))] * 4

        x, nf, rc = fmin_tnc(f_optimize(ratingBfAttack, ratingBfAttackKeys, upperBound, -lowerBound, P, H),
                             np.array([0, 0, 0, 0]),
                             fprime_optimize(ratingBfAttack, ratingBfAttackKeys, upperBound, -lowerBound, P, H),
                             bounds=bounds, disp=False)
       # print "P IS %s, H IS %s" % (P, H)

        if rc == 1 or rc == 2 or rc == 0:
            count_min += 1
            succeed_min_time.append(timeKey)
            # print "Converged %s" % rc
            break
        else:
            rc = -1
            tolerance += 0.25 * AvgRatingNoise

    if rc == -1:
        # if lowerBound < -5:
        #     print "fail to find a viable lowerBound"
        # else:
        #     print "fail to find minimal attack"
        return -1

    ratingAfAttack, ratingAfAttackKeys, S, Z = optimizeRatingForAttack(x, ratingBfAttack, ratingBfAttackKeys)
    # avgRatingAfAttack = (np.sum([ratingAfAttack[i]*ratingAfAttackKeys[i] for i in range(len(ratingAfAttack))]) \
    #                     * (NoOfReviewsNoise + noOfTruthfulReviews) + AvgRatingBfT * noOfAllReviewsBfT)/(noOfAllReviewsBfAttack + NoOfReviewsNoise)
    # print "rating For Attack is %s" % avgRatingAfAttack, AvgRatingBfAttack
    avgRatingAfAttack = np.sum([ratingAfAttack[i]*ratingAfAttackKeys[i] for i in range(len(ratingAfAttack))])
    # print "lowerbound is %s, rating average is %s" % (lowerBound, avgRatingAfAttack)

    if avgRatingAfAttack + 1e-3 < -lowerBound:
        # print "fail to find minimal attack"
        return -1

    return ratingAfAttack, ratingAfAttackKeys, count_min, succeed_min_time


def maximizeNoiseInCDF(statistics_for_all_bnsses_of_one_measure, ratio):
    if len(statistics_for_all_bnsses_of_one_measure) == 0:
        return float('inf')
    sorted_list = sorted(statistics_for_all_bnsses_of_one_measure)
    for i in range(len(sorted_list)):
        if float(i + 1) / len(sorted_list) >= ratio:
            timeKey = i
            break
    return sorted_list[timeKey]


def chooseEarlyAttackStrategyForEachBnss(statistics_for_train, statistics_for_current_bnss, plotDir, bnss_key,
                                         suspDir, count_min, constraints_for_test, case, timeLength, stratify=False):
    length_for_prediction = 30
    df = pd.read_csv(os.path.join(plotDir, 'bnss_stats', 'CMR.csv'), index_col=0)

    firstTimeKey = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]

    startTimeKeyForPrediction = firstTimeKey + 1

    lastTimeKey = startTimeKeyForPrediction + length_for_prediction
    if lastTimeKey > statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]:
        lastTimeKey = statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]
        length_for_prediction = lastTimeKey - startTimeKeyForPrediction

    noOfReviews = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS][startTimeKeyForPrediction:lastTimeKey]
    noOfAllReviews = statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][startTimeKeyForPrediction:lastTimeKey]
    avgRatings = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][startTimeKeyForPrediction:lastTimeKey]
    ratingDistribution = statistics_for_current_bnss[StatConstants.RATING_DISTRIBUTION]
    ratingEntropy = statistics_for_current_bnss[StatConstants.RATING_ENTROPY]
    noisesForAverageRating = list()
    noisesForCMR = list()
    noisesForRank = list()
    noisesForNoOfReviews = list()
    entropyAfAttack = list()
    stopAttackTime = list()
    ratingDivergenceAfAttack = list()
    noOfPositiveReviewsAfAttack = list()
    statistics_of_test_bnss = dict()
    statistics_of_test_bnss[DetectAnalysis.NO_OF_REVIEWS] = statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][
        firstTimeKey:startTimeKeyForPrediction
    ]
    statistics_of_test_bnss[DetectAnalysis.AVG_RATING] = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][
        firstTimeKey:startTimeKeyForPrediction
    ]
    statistics_of_test_bnss[DetectAnalysis.RATING_ENTROPY] = statistics_for_current_bnss[StatConstants.RATING_ENTROPY][
        firstTimeKey:startTimeKeyForPrediction
    ]
    succeed_min_time = list()
    rating_for_attack = dict()
    attack_statistics = dict()
    attack_statistics['CAR bf attack'] = list()
    attack_statistics['NR bf attack'] = list()
    attack_statistics['time key to start attack'] = startTimeKeyForPrediction
    attack_statistics['rating distribution bf attack'] = ratingDistributionBfAttack = deepcopy(ratingDistribution)
    if timeLength == '1-M':
        first_date = datetime(2004, 10, 12)
        time_window = 30
    else:
        first_date = datetime(2002, 1, 26)
        time_window = 7
    start_dates = list()
    end_dates = list()

    for t in range(length_for_prediction - 1):
        TimeKeyForPrediction = t + startTimeKeyForPrediction
        start_dates.append(first_date + timedelta(days=TimeKeyForPrediction * time_window))
        end_dates.append(first_date + timedelta(days=TimeKeyForPrediction * time_window + time_window - 1))
        CMRForBnsses = df[str(TimeKeyForPrediction)].copy()
        rank_series = CMRForBnsses.rank(method='min', ascending=False)
        rankBfAttack, CMRBfAttack = rank_series[bnss_key], CMRForBnsses[bnss_key]

        current_avg_rating = avgRatings[t]
        constraints = dict()
        if stratify:
            if current_avg_rating < 3:
                for signal in constraints_for_test:
                    constraints[signal] = constraints_for_test[signal]['low']
            else:
                for signal in constraints_for_test:
                    constraints[signal] = constraints_for_test[signal]['high']
        else:
            constraints = constraints_for_test


        if TimeKeyForPrediction == firstTimeKey:
            noiseForAvgRating = 5 - current_avg_rating
        else:
            noiseForAvgRatingDiff = constraints[DetectAnalysis.DIFFERENCE_OF_AVG_RATING]

            noiseForAvgRating = avgRatings[t - 1] + noiseForAvgRatingDiff - avgRatings[t]
        # print "avg rating constraint", noiseForAvgRatingDiff

        if noiseForAvgRating > 0:
            NoOfReviews = constraints[DetectAnalysis.NO_OF_REVIEWS]
            NoOfReviewsDiff = constraints[DetectAnalysis.DIFFERENCE_OF_NO_OF_REVIEWS]
            # print("review constraint", NoOfReviews, NoOfReviewsDiff)
            if t == 0:
                noiseForNoOfReviews = NoOfReviews - noOfReviews[t]
            else:
                noiseForNoOfReviews = min(noOfReviews[t - 1] + NoOfReviewsDiff - noOfReviews[t], NoOfReviews - noOfReviews[t])
                print(noOfReviews[t - 1], NoOfReviewsDiff, NoOfReviews)
        else:
            noiseForNoOfReviews = 0

        if case == 2 or case == 1:
            noiseForNoOfReviews = min(noiseForNoOfReviews,
                round(noOfAllReviews[t] * noiseForAvgRating / (5 - avgRatings[t] - noiseForAvgRating)))
            print("noise avg rating",
              round(noOfAllReviews[t] * noiseForAvgRating / (5 - avgRatings[t] - noiseForAvgRating)))

        truthfulRatingDict = dict()
        for key in ratingDistribution.keys():
            if ratingDistribution[key][TimeKeyForPrediction]:
                if TimeKeyForPrediction == firstTimeKey:
                    truthfulRatingDict[key] = ratingDistribution[key][TimeKeyForPrediction]
                else:
                    truthfulRatingDict[key] = ratingDistribution[key][TimeKeyForPrediction] - \
                                              ratingDistribution[key][TimeKeyForPrediction - 1]

            else:
                truthfulRatingDict[key] = 0
        truthfulRatingKeys, truthfulRating = zip(*sorted(truthfulRatingDict.iteritems(), key=lambda x: x[0]))

        # print "review noise and avg rating noise", noiseForNoOfReviews, noiseForAvgRating

        if case == 1:
            if noiseForNoOfReviews > 0 and noiseForAvgRating > 0:
                P = 1
                H = 0
                while True:
                    OptimalRating = chooseRatingDistribution(avgRatings[t], avgRatings[t - 1], noOfAllReviews[t],
                                                             noOfAllReviews[t - 1], noOfReviews[t], ratingDistribution,
                                                             TimeKeyForPrediction, noiseForAvgRating,
                                                             noiseForNoOfReviews, P, H,
                                                             count_min, succeed_min_time, case=8)
                    if OptimalRating == -1:
                        break
                    else:
                        ratingAfAttack, ratingAfAttackKeys, count_min, succeed_min_time = OptimalRating
                        if (ratingAfAttack[0] + ratingAfAttack[1] + ratingAfAttack[2]) > 0.5:
                            print("not suitable", noiseForNoOfReviews, ratingAfAttack, avgRatings[t],
                                  noiseForAvgRating, noOfReviews[t], noOfAllReviews[t])
                            noiseForNoOfReviews = int(noiseForNoOfReviews / 2)
                            if noiseForNoOfReviews < 1:
                                OptimalRating = -1
                                break
                        else:
                            break

                if OptimalRating != -1:
                    ratingAfAttack, ratingAfAttackKeys, count_min, succeed_min_time = OptimalRating
                    ratingAfAttackDict = dict()  # sampling
                    #  "sampling", noiseForNoOfReviews + noOfReviews[t]
                    # print "noise for no of reviews, current no of reviewsf", noiseForNoOfReviews, noOfReviews[t]
                    for i in range(50):
                        rating_counts = samplingRating(int(noiseForNoOfReviews + noOfReviews[t]),
                                                       ratingAfAttack)
                        # print "insepct noiseForNoOfReviews + ARmodelForNoOfReviews[0][t]", noiseForNoOfReviews + ARmodelForNoOfReviews[0][t] == sum(rating_counts)
                        tempRatingForAttack = list()
                        for i in range(len(rating_counts)):
                            if rating_counts[i] > truthfulRating[i]:
                                tempRatingForAttack.append(rating_counts[i] - truthfulRating[i])
                            else:
                                tempRatingForAttack.append(0)
                        rating_counts = np.array(truthfulRating) + np.array(tempRatingForAttack)
                        rating_sampling = rating_counts / sum(rating_counts)
                        # print "inspect rating_sampling:", sum(rating_sampling), sum(ratingAfAttack)
                        averageRatingAfSampling = np.sum(
                            [rating_sampling[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttackKeys))])
                        averageRatingBfSampling = np.sum(
                            [ratingAfAttack[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttackKeys))])
                        try:
                            error = mse(rating_sampling, ratingAfAttack)
                        except:
                            print(rating_sampling, ratingAfAttack)
                            embed()

                        # print averageRatingBfSampling, averageRatingAfSampling
                        if error not in ratingAfAttackDict and averageRatingAfSampling >= averageRatingBfSampling:
                            ratingAfAttackDict[error] = rating_counts

                    if ratingAfAttackDict:
                        ratingAfAttack = ratingAfAttackDict[
                            min(ratingAfAttackDict.keys())]  # the number of reviews of each star
                        # print "rating af attack:", ratingAfAttack
                        ratingForAttack = list()
                        for i in range(len(ratingAfAttack)):
                            ratingForAttack.append(ratingAfAttack[i] - truthfulRating[i])
                        ratingForAttackKeys = ratingAfAttackKeys
                        # print "noise of number of reviews bf attack and af attack", noiseForNoOfReviews, sum(ratingForAttack)
                        noiseForNoOfReviews = sum(ratingForAttack)
                        AvgRatingAfSampling = np.sum(
                            [ratingAfAttack[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttack))]) / sum(
                            ratingAfAttack)
                        print "rating after attack", ratingAfAttack, ratingForAttack, truthfulRating, noOfAllReviews[
                            t], t
                        # print "inspect rating af attack", sum(ratingAfAttack) == noiseForNoOfReviews + \
                        #                                                        noOfReviews[t], noiseForNoOfReviews, noOfReviews[t]
                        # print noiseForNoOfReviews, noOfReviews[t]
                        CMRAfAttackTemp = np.sum(
                            [ratingAfAttack[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttack))]) / \
                                          (noiseForNoOfReviews + noOfReviews[t])

                        if CMRAfAttackTemp < averageRatingBfSampling or CMRAfAttackTemp < CMRBfAttack:
                            OptimalRating = -1
                    else:
                        OptimalRating = -1
            else:
                OptimalRating = -1
        else:
            OptimalRating = 1

        if (OptimalRating != -1 and noiseForNoOfReviews > 0 and noiseForAvgRating > 0):
            # print "succeed finding optimal ratings!"
            if case == 2:
                succeed_min_time.append(TimeKeyForPrediction)
                ratingForAttack = [0, 0, 0, 0, noiseForNoOfReviews]

                ratingForAttackKeys = (1.0, 2.0, 3.0, 4.0, 5.0)
                print "rating after attack", ratingForAttack, truthfulRating, noOfAllReviews[
                    t], t

            ratingForAttack, truthfulRating = np.array(ratingForAttack), np.array(truthfulRating)            # print "rating For attack", ratingForAttack

            ratingDict = dict()

            # ratingForAttack = [round(ratingForAttack[i]*noiseForNoOfReviews) for i in range(len(ratingForAttack))]
            AvgRatingForAttack = np.sum([ratingForAttack[i] * ratingForAttackKeys[i] for i in
                                         range(len(ratingForAttack))]) / noiseForNoOfReviews  # Ep(R)
            AvgRatingBfAttack = avgRatings[t]  # average rating before attack
            rating_for_attack[t] = ratingForAttack

            for timeKey in range(t, length_for_prediction):  # update the average rating from time t when issuing the attack
                avgRatings[timeKey] = (avgRatings[timeKey] * noOfAllReviews[timeKey]
                                       + AvgRatingForAttack * noiseForNoOfReviews) / (
                                          noOfAllReviews[timeKey] + noiseForNoOfReviews)

                noOfAllReviews[timeKey] = noOfAllReviews[timeKey] + noiseForNoOfReviews

                for i in range(len(ratingForAttack)):  # update the cumulative rating distribution
                    ratingDistribution[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] = \
                        ratingDistribution[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] + \
                        ratingForAttack[i]
                    if timeKey > t:
                        ratingDistributionBfAttack[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] = \
                            ratingDistributionBfAttack[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] + \
                            ratingForAttack[i]

            noOfReviews[t] = noOfReviews[t] + noiseForNoOfReviews

            for j in range(len(ratingForAttackKeys)):  # the probability of each star during t
                ratingDict[ratingForAttackKeys[j]] = (ratingDistribution[ratingForAttackKeys[j]][
                                                          t + startTimeKeyForPrediction] -
                                                      ratingDistribution[ratingForAttackKeys[j]][
                                                          t + startTimeKeyForPrediction - 1]) / \
                                                     noOfReviews[t]
            # print('inspect rating af attack', ratingDict.values(), np.array(ratingAfAttack)/sum(ratingAfAttack))
            # print "cmr bf attack:", CMRForBnsses[bnss_key]
            CMRAfAttack = CMRForBnsses[bnss_key] = sum([ratingDict[key] * key for key in ratingForAttackKeys])
            # print "cmr af attack:", CMRForBnsses[bnss_key], CMRAfAttackTemp, CMRAfAttack == CMRAfAttackTemp
            noisesForCMR.append(CMRAfAttack - CMRBfAttack)
            rankAfAttack = CMRForBnsses.rank(method='min', ascending=False)[bnss_key]
            noisesForRank.append(rankBfAttack - rankAfAttack)

            # calculate the KL-divergence
            current_rating = dict()
            cumulative_rating = dict()
            cumulative_rating_sum = np.sum(
                [ratingDistribution[rating][TimeKeyForPrediction - 1] for rating in ratingDistribution]
            )
            current_rating_sum = noOfReviews[t]
            for key in ratingDistribution:
                current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                    key][TimeKeyForPrediction - 1]

                if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                    cumulative_rating_sum += 1e-10

                if current_no_of_reviews == 0:
                    current_rating_sum += 1e-10

            for key in ratingDistribution:
                current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                    key][TimeKeyForPrediction - 1]
                if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                    cumulative_rating[key] = 1e-10 / cumulative_rating_sum
                else:
                    cumulative_rating[key] = float(
                        ratingDistribution[key][TimeKeyForPrediction - 1]) / cumulative_rating_sum

                if current_no_of_reviews == 0:
                    current_rating[key] = 1e-10 / current_rating_sum
                else:
                    current_rating[key] = float(current_no_of_reviews) / current_rating_sum

            # print "After Attack rating distribution: ", np.array(current_rating.values())*current_rating_sum

            divergence = calculateKLdivergence(current_rating, cumulative_rating)
            ratingDivergenceAfAttack.append(divergence)

            noOfPositiveReviewsForCurrentT = ratingDistribution[4][TimeKeyForPrediction] + ratingDistribution[5][t +
                                                                                                                 startTimeKeyForPrediction] - \
                                             ratingDistribution[4][TimeKeyForPrediction - 1] - \
                                             ratingDistribution[5][TimeKeyForPrediction - 1]
            # print("inpesct postive reviews", noOfPositiveReviewsForCurrentT, ratingAfAttack[-1] + ratingAfAttack[-2])
            noOfPositiveReviewsAfAttack.append(noOfPositiveReviewsForCurrentT)
            entropyAfAttack.append(entropyFn(ratingDict))
            # print "deviation of average rating is %s" % (ARmodelForAvgRating[0][t] - yhatDetect)

            noisesForAverageRating.append(avgRatings[t] - AvgRatingBfAttack)  # append noise
            print "noise for avg rating before attack and after attack", noiseForAvgRating, avgRatings[t] - AvgRatingBfAttack
            print "avg rating after attack and bf attack", avgRatings[t], AvgRatingBfAttack
            # print "promotion is %s" % (ARmodelForAvgRating[0][t] - AvgRatingBfAttack), AvgRatingBfAttack

            # all_rating_af_attack = list()
            # for key in ratingDistribution:
            #     all_rating_af_attack.append(ratingDistribution[key][TimeKeyForPrediction])
            # print "car af attack:", ARmodelForAvgRating[0][t], sum([all_rating_af_attack[i]*ratingForAttackKeys[i] for i in range(len(ratingForAttackKeys))])/sum(all_rating_af_attack)

        else:
            entropyAfAttack.append(ratingEntropy[t + startTimeKeyForPrediction])
            current_rating = dict()
            cumulative_rating = dict()
            cumulative_rating_sum = np.sum(
                [ratingDistribution[rating][TimeKeyForPrediction - 1] for rating in ratingDistribution]
            )
            current_rating_sum = noOfReviews[t]
            for key in ratingDistribution:
                current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[key][TimeKeyForPrediction - 1]

                if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                    cumulative_rating_sum += 1e-10

                if current_no_of_reviews == 0:
                    current_rating_sum += 1e-10

            for key in ratingDistribution:
                current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                    key][TimeKeyForPrediction - 1]
                if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                    cumulative_rating[key] = 1e-10 / cumulative_rating_sum
                else:
                    cumulative_rating[key] = float(
                        ratingDistribution[key][TimeKeyForPrediction - 1]) / cumulative_rating_sum

                if current_no_of_reviews == 0:
                    current_rating[key] = 1e-10 / current_rating_sum
                else:
                    try:
                        current_rating[key] = float(current_no_of_reviews) / current_rating_sum
                    except:
                        embed()
            divergence = calculateKLdivergence(current_rating, cumulative_rating)
            # print "current_rating, cumulative_rating", current_rating.values(), sum(current_rating.values()), sum(cumulative_rating.values())
            ratingDivergenceAfAttack.append(divergence)

            noOfPositiveReviewsForCurrentT = ratingDistribution[4][t + startTimeKeyForPrediction] + \
                                             ratingDistribution[5][
                                                 t + startTimeKeyForPrediction] - \
                                             ratingDistribution[4][t + startTimeKeyForPrediction - 1] - \
                                             ratingDistribution[5][t + startTimeKeyForPrediction - 1]

            noOfPositiveReviewsAfAttack.append(noOfPositiveReviewsForCurrentT)
            stopAttackTime.append(t)
            noisesForAverageRating.append(0)
            noisesForCMR.append(0)
            noisesForRank.append(0)

        TimeKeyForPrediction += 1
        noisesForNoOfReviews.append(noiseForNoOfReviews)

    attack_statistics['CAR af attack'] = avgRatings[:-1]
    attack_statistics['NR af attack'] = noOfReviews[:-1]
    attack_statistics['rating distribution af attack'] = ratingDistribution
    attack_statistics['rating for attack'] = rating_for_attack
    attack_statistics['stop attack'] = stopAttackTime
    attack_statistics['noises for CAR'] = noisesForAverageRating
    attack_statistics['noises for NR'] = noisesForNoOfReviews
    attack_statistics['CMR'] = df.ix[bnss_key, :]
    attack_statistics['attack_start_date'] = start_dates
    attack_statistics['attack_end_date'] = end_dates
    # print "rating for attack stats:", attack_statistics['rating for attack']

    if not os.path.exists(os.path.join(suspDir, 'attack_stats')):
        os.mkdir(os.path.join(suspDir, 'attack_stats'))
    with open(os.path.join(suspDir, 'attack_stats', bnss_key), 'w') as file:
        pickle.dump(attack_statistics, file)

    succeed_min_time = [succeed_min_time[i] - startTimeKeyForPrediction for i in range(len(succeed_min_time))]
    # print "stopAttackTime is %s" % stopAttackTime
    return avgRatings[:-1], noOfReviews[:-1], entropyAfAttack, ratingDivergenceAfAttack, noOfPositiveReviewsAfAttack, \
           startTimeKeyForPrediction, stopAttackTime, count_min, succeed_min_time, noisesForAverageRating, \
           noisesForCMR, noisesForRank, statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][
           statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]], statistics_of_test_bnss




# 1. NR
# 2. NR and \Delta-NR
# 3. CAR-DEV, NR
# 4. CAR-DEV and \Delta-CAR, NR
# 5. KL-DIV, NR, CAR-DEV
# 6. KL-DIV and \Delta-EN, NR, CAR-DEV
# 7. KL-DIV and PR, NR, CAR-DEV
# 8. EN, NR, CAR-DEV
# 9. EN, \Delta-EN, NR, CAR-DEV

# Different evasion strategies for different cases
def chooseAttackStrategyForEachBnss(statistics_for_current_bnss_dir, plotDir, suspDir, timeLength, measures,
                                    NoiseForNoOfReviews, bnss_key, count_min, case, ratio, recall=[0.5, 0.5], random=False):
    length_for_prediction = 6
    df = pd.read_csv(os.path.join(plotDir, 'bnss_stats', 'CMR.csv'), index_col=0) # load the CMR csv

    with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
        statistics_for_current_bnss = pickle.load(file)

    if statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY] + 36 < statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]:

        ARmodelForAvgRating = extractSeriesAndARmodel(statistics_for_current_bnss, StatConstants.AVERAGE_RATING,
                                                      length_for_prediction)
        # ARmodelForAvgRating[0]: average rating of months to be predicted
        # ARmodelForAvgRating: test, window, coef, history, startTimeKeyForPrediction, series
        if ARmodelForAvgRating == -1:
            return -1

        startTimeKeyForPrediction = ARmodelForAvgRating[4]
        TimeKeyForPrediction = startTimeKeyForPrediction
        ARmodelForNoOfReviews = extractSeriesAndARmodel(statistics_for_current_bnss, StatConstants.NON_CUM_NO_OF_REVIEWS,
                                                        length_for_prediction)
        # ARmodelForNoOfReviews[0]: non cumulative number of reviews of months to be predicted

        noOfAllReviews = statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][TimeKeyForPrediction:\
            TimeKeyForPrediction + length_for_prediction]
        # number of all reviews: cumulative number of reviews of months to be predicted

        # filter businesses with few reviews
        if np.mean(ARmodelForNoOfReviews[5][startTimeKeyForPrediction-30:startTimeKeyForPrediction]) < 3:
            return -1

        ratingDistribution = statistics_for_current_bnss[StatConstants.RATING_DISTRIBUTION]
        ratingEntropy = statistics_for_current_bnss[StatConstants.RATING_ENTROPY]
        noOfPositiveReviews = statistics_for_current_bnss[StatConstants.NO_OF_POSITIVE_REVIEWS]
        noisesForAverageRating = list()
        noisesForAverageRatingAfDetection = list()
        noisesForCMR = list()
        noisesForCMRAfDetection = list()
        noisesForRank = list()
        noisesForRankAfDetection = list()
        noisesForNoOfReviews = list()
        entropyAfAttack = list()
        avgRatingErrorAfAttack = list()
        stopAttackTime = list()
        ratingDivergenceAfAttack = list()
        noOfPositiveReviewsAfAttack = list()
        statistics_for_all_bnsses, statistics_for_all_measures = DetectAnalysis.poolData(plotDir, timeLength, measures,
                                                                                         startTimeKeyForPrediction, bnss_key)
        succeed_min_time = list()
        rating_for_attack = dict()
        attack_statistics = dict()
        attack_statistics['CAR bf attack'] = list()
        attack_statistics['NR bf attack'] = list()
        attack_statistics['time key to start attack'] = startTimeKeyForPrediction
        attack_statistics['rating distribution bf attack'] = ratingDistributionBfAttack = deepcopy(ratingDistribution)
        if timeLength == '1-M':
            first_date = datetime(2004, 10, 12)
            time_window = 30
        else:
            first_date = datetime(2002, 1, 26)
            time_window = 7
        start_dates = list()
        end_dates = list()

        for t in range(length_for_prediction - 1):
            start_dates.append(first_date + timedelta(days=TimeKeyForPrediction * time_window))
            end_dates.append(first_date + timedelta(days=TimeKeyForPrediction * time_window + time_window - 1))
            CMRForBnsses = df[str(TimeKeyForPrediction)].copy()
            rank_series = CMRForBnsses.rank(method='min', ascending=False)
            rankBfAttack, CMRBfAttack = rank_series[bnss_key], CMRForBnsses[bnss_key]
            noiseForAvgRating = 0.01
            noiseForNoOfReviews = NoiseForNoOfReviews
            attack_statistics['CAR bf attack'].append(ARmodelForAvgRating[0][t])
            attack_statistics['NR bf attack'].append(ARmodelForNoOfReviews[0][t])
            yhatDetect, lag = testARupdateCoefandDatamodel(ARmodelForAvgRating[3], ARmodelForAvgRating[1], ARmodelForAvgRating[2])

            # if random: case can be 0-9
            if random:
                if random == 1:
                    i = np.random.choice(range(1, 10), p=[1.0/9]*9)
                    case = i
                if random == 2:
                    i = np.random.choice(range(1, 10), p=([1.0/16, 1.0/16, 1.0/16, 1.0/2] + [1.0/16]*5))
                    case = i
                print 'case %s' % case
                # ratio = 0.8
                # print "case is %s, noise for number of reviews is %s, ratio is %s" % (case, noiseForNoOfReviews, ratio)

            if case in [3, 4, 5, 6, 7, 8, 9]:
                # determine the threshold for CAR-DEV
                threshold_for_AvgRating = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DEVIATION_OF_AVG_RATING], ratio)
                # determin the threshold for delta CAR

                noiseForAvgRatingDiff = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DIFFERENCE_OF_AVG_RATING], ratio)

                noiseForAvgRatingDeviation, lag, yhatDetect \
                    = maximizeNoiseinTestForAvgRating(ARmodelForAvgRating[0],
                                                  ARmodelForAvgRating[1], ARmodelForAvgRating[2],
                                                  ARmodelForAvgRating[3], ARmodelForAvgRating[5], t, startTimeKeyForPrediction,
                                                  threshold_for_AvgRating, case, noiseForAvgRatingDiff)

                noiseForAvgRating = min(float('inf'), noiseForAvgRatingDeviation)

            if case != 0:
                NoOfReviews = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.NO_OF_REVIEWS], ratio)
                NoOfReviewsDiff = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DIFFERENCE_OF_NO_OF_REVIEWS], ratio)

            if case in [2, 11]:
                noiseForNoOfReviews = min(ARmodelForNoOfReviews[5][TimeKeyForPrediction - 1] + NoOfReviewsDiff - ARmodelForNoOfReviews[0][t],
                                              NoOfReviews - ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t])
                    # noiseForNoOfReviews = min(NoOfReviewsDiff, float('inf'))
            if case in [1, 5, 6, 7, 8, 9, 10]:
                noiseForNoOfReviews = min(float('inf'),
                                              NoOfReviews - ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t])

            if case in [3, 4]:
                if noiseForAvgRating < 0:
                    noiseForNoOfReviews = 0
                else:
                    if ARmodelForAvgRating[0][t] + noiseForAvgRating == 5:
                        noiseForNoOfReviews = NoOfReviews - ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t]
                    else:
                        noiseForNoOfReviews = round(
                            noOfAllReviews[t] * noiseForAvgRating / (5 - ARmodelForAvgRating[0][t] - noiseForAvgRating))
                        noiseForNoOfReviews = min(noiseForNoOfReviews,
                                                  NoOfReviews - ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t])

            P, H = 1, 0
            if case == 7:
                NoOfPositiveReivews = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.NO_OF_POSITIVE_REVIEWS], ratio)
                NoOfPositiveReivewsDiff = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS], ratio)

                NoOfReviewsAddNoise = ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t] + noiseForNoOfReviews
                NoOfPositiveReivewsAddNoise = min(NoOfPositiveReivews, noOfPositiveReviews[startTimeKeyForPrediction-1+t] + NoOfPositiveReivewsDiff)
                P = NoOfPositiveReivewsAddNoise / NoOfReviewsAddNoise

            if case in [6, 9]:
                RatingEntropy = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.RATING_ENTROPY], 1 - ratio)
                RatingEntropyDiff = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DIFFERENCE_OF_RATING_ENTROPY], ratio)
                if case == 9:
                    H = max(ratingEntropy[TimeKeyForPrediction - 1] - RatingEntropyDiff, RatingEntropy)
                else:
                    H = ratingEntropy[TimeKeyForPrediction - 1] - RatingEntropyDiff

            truthfulRatingDict = dict()
            for key in ratingDistribution.keys():
                if ratingDistribution[key][startTimeKeyForPrediction + t]:
                    truthfulRatingDict[key] = ratingDistribution[key][startTimeKeyForPrediction + t] - \
                                              ratingDistribution[key][startTimeKeyForPrediction + t - 1]
                else:
                    noiseForNoOfReviews = 0

            # print "inspect truthful rating and noise for no of reviews:", truthfulRatingDict.values(), sum(truthfulRatingDict.values()), \
            # ARmodelForNoOfReviews[0][t], noiseForNoOfReviews
            # print "80% percentile of delta car and car, noise", noiseForAvgRatingDiff, threshold_for_AvgRating, noiseForAvgRating
            for i in range(1, 6):
                if not truthfulRatingDict.get(i):
                    truthfulRatingDict[i] = 0.0

            truthfulRatingKeys, truthfulRating = zip(*sorted(truthfulRatingDict.iteritems(), key=lambda x: x[0]))
            if case in [5, 6, 7, 8, 9, 10, 11, 12] and noiseForNoOfReviews:
                # if P - (truthfulRating[0] + truthfulRating[1] + truthfulRating[2])/(sum(truthfulRating) + noiseForNoOfReviews) < 0:
                #     noiseForNoOfReviews = 0
                if noiseForNoOfReviews > 0 and noiseForAvgRating > 0:
                    noOfAllReviewsBeforeT= statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][TimeKeyForPrediction - 1]
                    OptimalRating = chooseRatingDistribution(ARmodelForAvgRating[5][TimeKeyForPrediction],
                                                             ARmodelForAvgRating[5][TimeKeyForPrediction - 1],
                                                             noOfAllReviews[t], noOfAllReviewsBeforeT, ARmodelForNoOfReviews[0][t], ratingDistribution,
                                                             TimeKeyForPrediction, noiseForAvgRating, noiseForNoOfReviews, P, H,
                                                             count_min, succeed_min_time, case)
                    if OptimalRating != -1:
                        ratingAfAttack, ratingAfAttackKeys, count_min, succeed_min_time = OptimalRating
                        ratingAfAttackDict = dict()  # sampling
                        # print "sampling", noiseForNoOfReviews + ARmodelForNoOfReviews[0][t]
                        # print noiseForNoOfReviews, ARmodelForNoOfReviews[0][t]
                        for i in range(50):
                            rating_counts = samplingRating(int(noiseForNoOfReviews + ARmodelForNoOfReviews[0][t]),
                                                           ratingAfAttack)
                            # print "insepct noiseForNoOfReviews + ARmodelForNoOfReviews[0][t]", noiseForNoOfReviews + ARmodelForNoOfReviews[0][t] == sum(rating_counts)
                            tempRatingForAttack = list()
                            for i in range(len(rating_counts)):
                                if rating_counts[i] > truthfulRating[i]:
                                    tempRatingForAttack.append(rating_counts[i] - truthfulRating[i])
                                else:
                                    tempRatingForAttack.append(0)
                            rating_counts = np.array(truthfulRating) + np.array(tempRatingForAttack)
                            rating_sampling = rating_counts / sum(rating_counts)
                            # print "inspect rating_sampling:", sum(rating_sampling), sum(ratingAfAttack)
                            averageRatingAfSampling = np.sum(
                                [rating_sampling[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttackKeys))])
                            averageRatingBfSampling = np.sum(
                                [ratingAfAttack[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttackKeys))])
                            error = mse(rating_sampling, ratingAfAttack)
                            # print averageRatingBfSampling, averageRatingAfSampling
                            if error not in ratingAfAttackDict and averageRatingAfSampling >= averageRatingBfSampling:
                                ratingAfAttackDict[error] = rating_counts

                        if ratingAfAttackDict:
                            ratingAfAttack = ratingAfAttackDict[
                                min(ratingAfAttackDict.keys())]  # the number of reviews of each star
                            # print "rating af attack:", ratingAfAttack
                            ratingForAttack = list()
                            for i in range(len(ratingAfAttack)):
                                ratingForAttack.append(ratingAfAttack[i] - truthfulRating[i])
                            ratingForAttackKeys = ratingAfAttackKeys
                            noiseForNoOfReviews = sum(ratingForAttack)
                            AvgRatingAfSampling = np.sum(
                                [ratingAfAttack[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttack))]) / sum(
                                ratingAfAttack)
                            # print "rating after attack", ratingAfAttack, ratingForAttack
                            # print "inspect rating af attack", sum(ratingAfAttack) == noiseForNoOfReviews + ARmodelForNoOfReviews[0][t]
                            # print noiseForNoOfReviews, ARmodelForNoOfReviews[0][t]
                            CMRAfAttackTemp = np.sum([ratingAfAttack[i]*ratingAfAttackKeys[i] for i in range(len(ratingAfAttack))])/ \
                                (noiseForNoOfReviews + ARmodelForNoOfReviews[0][t])

                            if CMRAfAttackTemp < averageRatingBfSampling or CMRAfAttackTemp < CMRBfAttack:
                                OptimalRating = -1
                        else:
                            OptimalRating = -1
                else:
                    OptimalRating = -1
            else:
                OptimalRating = 1

            if case == 0:
                noiseForNoOfReviews = noiseForAvgRating = 0

            if (OptimalRating != -1 and noiseForNoOfReviews > 0 and noiseForAvgRating > 0) or case == 0:
                if case in [0, 1, 2, 3, 4]:
                    succeed_min_time.append(TimeKeyForPrediction)
                    ratingForAttack = [0, 0, 0, 0, noiseForNoOfReviews]

                    ratingForAttackKeys = (1.0, 2.0, 3.0, 4.0, 5.0)
                ratingForAttack, truthfulRating = np.array(ratingForAttack), np.array(truthfulRating)
                # print "rating For attack", ratingForAttack

                ratingDict = dict()

                # ratingForAttack = [round(ratingForAttack[i]*noiseForNoOfReviews) for i in range(len(ratingForAttack))]
                AvgRatingForAttack = np.sum([ratingForAttack[i] * ratingForAttackKeys[i] for i in
                                             range(len(ratingForAttack))]) / noiseForNoOfReviews # Ep(R)
                AvgRatingBfAttack = ARmodelForAvgRating[0][t] #average rating before attack
                rating_for_attack[t] = ratingForAttack

                detectedNoOfReviews1 = int((ratingForAttack[4]+ratingForAttack[3]) * recall[0])
                detectedNoOfReviews2 = int((ratingForAttack[2]+ratingForAttack[1]+ratingForAttack[0]) * recall[1])
                detectedlist1 = list()
                detectedlist2 = list()
                for i in range(4, 2, -1):
                    if ratingForAttack[i] < detectedNoOfReviews1 - sum(detectedlist1):
                        detectedlist1.append(ratingForAttack[i])
                    else:
                        detectedlist1.append(detectedNoOfReviews1 - sum(detectedlist1))
                # print detectedlist1
                for i in range(2, -1, -1):
                    if ratingForAttack[i] < detectedNoOfReviews2 - sum(detectedlist2):
                        detectedlist2.append(ratingForAttack[i])
                    else:
                        detectedlist2.append(detectedNoOfReviews2 - sum(detectedlist2))
                # print detectedlist2
                detectedlist = detectedlist1 + detectedlist2
                detectedlist.reverse()
                ratingForAttackAfDetection = np.array(ratingForAttack) - np.array(detectedlist)
                # print "ratingForAttackAfDetection:", ratingForAttackAfDetection
                RatingForAttackAfDetection = np.sum([ratingForAttackAfDetection[i] * ratingForAttackKeys[i] for i in
                                             range(len(ratingForAttackAfDetection))])
                # print "inspect ratingForAttackAfDetection:", sum(ratingForAttackAfDetection) == noiseForNoOfReviews - detectedNoOfReviews
                AvgRatingAfDetection = (AvgRatingBfAttack * noOfAllReviews[t] + RatingForAttackAfDetection) / \
                                       (noOfAllReviews[t] + sum(ratingForAttackAfDetection))
                noisesForAverageRatingAfDetection.append(AvgRatingAfDetection - AvgRatingBfAttack)
                # print "noisesForAverageRatingAfDetection:", noisesForAverageRatingAfDetection

                for timeKey in range(t, len(ARmodelForAvgRating[0])): # update the average rating from time t when issuing the attack
                    ARmodelForAvgRating[0][timeKey] = (ARmodelForAvgRating[0][timeKey] * noOfAllReviews[timeKey]
                        + AvgRatingForAttack * noiseForNoOfReviews) / (noOfAllReviews[timeKey] + noiseForNoOfReviews)

                    noOfAllReviews[timeKey] = noOfAllReviews[timeKey] + noiseForNoOfReviews

                    for i in range(len(ratingForAttack)): # update the cumulative rating distribution
                        ratingDistribution[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] = \
                            ratingDistribution[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] + \
                            ratingForAttack[i]
                        if timeKey > t:
                            ratingDistributionBfAttack[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] = \
                            ratingDistributionBfAttack[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] + \
                            ratingForAttack[i]

                ARmodelForNoOfReviews[0][t] = ARmodelForNoOfReviews[0][t] + noiseForNoOfReviews

                for j in range(len(ratingForAttackKeys)): # the probability of each star during t
                    ratingDict[ratingForAttackKeys[j]] = (ratingDistribution[ratingForAttackKeys[j]][
                                                              t + startTimeKeyForPrediction] -
                                                          ratingDistribution[ratingForAttackKeys[j]][
                                                              t + startTimeKeyForPrediction - 1]) / \
                                                         ARmodelForNoOfReviews[0][t]

                # print "cmr bf attack:", CMRForBnsses[bnss_key]
                CMRAfAttack = CMRForBnsses[bnss_key] = sum([ratingDict[key]*key for key in ratingForAttackKeys])
                # print "cmr af attack:", CMRForBnsses[bnss_key], CMRAfAttackTemp, CMRAfAttack == CMRAfAttackTemp
                noisesForCMR.append(CMRAfAttack - CMRBfAttack)
                rankAfAttack = CMRForBnsses.rank(method='min', ascending=False)[bnss_key]
                noisesForRank.append(rankBfAttack - rankAfAttack)
                ratingAfDetection = ratingForAttackAfDetection + truthfulRating
                CMRAfDetection = CMRForBnsses[bnss_key] = sum([ratingAfDetection[i]*ratingForAttackKeys[i] for i in range(len(ratingAfDetection))])/ \
                                 sum(ratingAfDetection)
                # print "cmr af detection:", CMRForBnsses[bnss_key]
                noisesForCMRAfDetection.append(CMRAfDetection - CMRBfAttack)
                rankAfDetection = CMRForBnsses.rank(method='min', ascending=False)[bnss_key]
                noisesForRankAfDetection.append(rankBfAttack - rankAfDetection)

                # calculate the KL-divergence
                current_rating = dict()
                cumulative_rating = dict()
                cumulative_rating_sum = np.sum(
                    [ratingDistribution[rating][TimeKeyForPrediction - 1] for rating in ratingDistribution]
                )
                current_rating_sum = ARmodelForNoOfReviews[0][t]
                for key in ratingDistribution:
                    current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                        key][TimeKeyForPrediction - 1]

                    if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                        cumulative_rating_sum += 1e-10

                    if current_no_of_reviews == 0:
                        current_rating_sum += 1e-10

                for key in ratingDistribution:
                    current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                        key][TimeKeyForPrediction - 1]
                    if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                        cumulative_rating[key] = 1e-10 / cumulative_rating_sum
                    else:
                        cumulative_rating[key] = float(ratingDistribution[key][TimeKeyForPrediction - 1]) /cumulative_rating_sum

                    if current_no_of_reviews == 0:
                        current_rating[key]= 1e-10 / current_rating_sum
                    else:
                        current_rating[key] = float(current_no_of_reviews) /current_rating_sum

                # print "After Attack rating distribution: ", np.array(current_rating.values())*current_rating_sum

                divergence = calculateKLdivergence(current_rating, cumulative_rating)
                ratingDivergenceAfAttack.append(divergence)

                noOfPositiveReviewsForCurrentT = ratingDistribution[4][TimeKeyForPrediction] + ratingDistribution[5][t +
                startTimeKeyForPrediction] - ratingDistribution[4][TimeKeyForPrediction - 1] - \
                                      ratingDistribution[5][TimeKeyForPrediction - 1]

                noOfPositiveReviewsAfAttack.append(noOfPositiveReviewsForCurrentT)
                entropyAfAttack.append(entropyFn(ratingDict))
                avgRatingErrorAfAttack.append(abs(ARmodelForAvgRating[0][t] - yhatDetect))
                # print "deviation of average rating is %s" % (ARmodelForAvgRating[0][t] - yhatDetect)

                n = Backtracking_LineSearch(ARmodelForAvgRating[0][t], ARmodelForAvgRating[2], lag) # update the step of gradient descent
                updateCoef(ARmodelForAvgRating[2], n, yhatDetect, ARmodelForAvgRating[0][t], lag) # update coef of AR
                ARmodelForAvgRating[3].append(ARmodelForAvgRating[0][t]) # update history
                noisesForAverageRating.append(ARmodelForAvgRating[0][t] - AvgRatingBfAttack) # append noise
                # print "promotion is %s" % (ARmodelForAvgRating[0][t] - AvgRatingBfAttack), AvgRatingBfAttack

                # all_rating_af_attack = list()
                # for key in ratingDistribution:
                #     all_rating_af_attack.append(ratingDistribution[key][TimeKeyForPrediction])
                # print "car af attack:", ARmodelForAvgRating[0][t], sum([all_rating_af_attack[i]*ratingForAttackKeys[i] for i in range(len(ratingForAttackKeys))])/sum(all_rating_af_attack)

            else:
                n = Backtracking_LineSearch(ARmodelForAvgRating[0][t], ARmodelForAvgRating[2], lag)
                updateCoef(ARmodelForAvgRating[2], n, yhatDetect, ARmodelForAvgRating[0][t], lag)
                ARmodelForAvgRating[3].append(ARmodelForAvgRating[0][t])

                entropyAfAttack.append(ratingEntropy[t + startTimeKeyForPrediction])
                avgRatingErrorAfAttack.append(abs(ARmodelForAvgRating[0][t] - yhatDetect))
                current_rating = dict()
                cumulative_rating = dict()
                cumulative_rating_sum = np.sum(
                    [ratingDistribution[rating][TimeKeyForPrediction - 1] for rating in ratingDistribution]
                )
                current_rating_sum = ARmodelForNoOfReviews[0][t]
                for key in ratingDistribution:
                    current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                        key][TimeKeyForPrediction - 1]

                    if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                        cumulative_rating_sum += 1e-10

                    if current_no_of_reviews == 0:
                        current_rating_sum += 1e-10

                for key in ratingDistribution:
                    current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                        key][TimeKeyForPrediction - 1]
                    if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                        cumulative_rating[key] = 1e-10 / cumulative_rating_sum
                    else:
                        cumulative_rating[key] = float(
                            ratingDistribution[key][TimeKeyForPrediction - 1]) / cumulative_rating_sum

                    if current_no_of_reviews == 0:
                        current_rating[key] = 1e-10 / current_rating_sum
                    else:
                        current_rating[key] = float(current_no_of_reviews) / current_rating_sum
                divergence = calculateKLdivergence(current_rating, cumulative_rating)
                # print "current_rating, cumulative_rating", current_rating.values(), sum(current_rating.values()), sum(cumulative_rating.values())
                ratingDivergenceAfAttack.append(divergence)

                noOfPositiveReviewsForCurrentT = ratingDistribution[4][t + startTimeKeyForPrediction] + ratingDistribution[5][
                    t + startTimeKeyForPrediction] - \
                                      ratingDistribution[4][t + startTimeKeyForPrediction - 1] - \
                                      ratingDistribution[5][t + startTimeKeyForPrediction - 1]
                noOfPositiveReviewsAfAttack.append(noOfPositiveReviewsForCurrentT)
                stopAttackTime.append(t)
                noisesForAverageRating.append(0)
                noisesForAverageRatingAfDetection.append(0)
                noisesForCMR.append(0)
                noisesForCMRAfDetection.append(0)
                noisesForRank.append(0)
                noisesForRankAfDetection.append(0)

            TimeKeyForPrediction += 1
            noisesForNoOfReviews.append(noiseForNoOfReviews)

        attack_statistics['CAR af attack'] = ARmodelForAvgRating[0][:-1]
        attack_statistics['NR af attack'] = ARmodelForNoOfReviews[0][:-1]
        attack_statistics['rating distribution af attack'] = ratingDistribution
        attack_statistics['rating for attack'] = rating_for_attack
        attack_statistics['stop attack'] = stopAttackTime
        attack_statistics['noises for CAR'] = noisesForAverageRating
        attack_statistics['noises for NR'] = noisesForNoOfReviews
        attack_statistics['CMR'] = df.ix[bnss_key, :]
        attack_statistics['attack_start_date'] = start_dates
        attack_statistics['attack_end_date'] = end_dates
        # print "rating for attack stats:", attack_statistics['rating for attack']

        if not os.path.exists(os.path.join(suspDir, 'attack_stats')):
            os.mkdir(os.path.join(suspDir, 'attack_stats'))
        with open(os.path.join(suspDir, 'attack_stats', bnss_key), 'w') as file:
            pickle.dump(attack_statistics, file)


        succeed_min_time = [succeed_min_time[i] - startTimeKeyForPrediction for i in range(len(succeed_min_time))]
        # print "stopAttackTime is %s" % stopAttackTime
        return avgRatingErrorAfAttack, ARmodelForAvgRating[0][:-1], ARmodelForNoOfReviews[0][:-1], entropyAfAttack, \
               ratingDivergenceAfAttack, noOfPositiveReviewsAfAttack, startTimeKeyForPrediction, stopAttackTime,\
               statistics_for_all_bnsses, statistics_for_all_measures, count_min, succeed_min_time, noisesForAverageRating, \
               noisesForAverageRatingAfDetection, noisesForCMR, noisesForCMRAfDetection, noisesForRank, noisesForRankAfDetection, \
               statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]]
    else:
        return -1

def chooseEvasionStrategyForEachBnssWithoutDetection(statistics_for_current_bnss_dir, plotDir, timeLength, measures, NoiseForNoOfReviews, bnss_key, count_min, case, ratio, is_random=False):
    length_for_prediction = 6
    df = pd.read_csv(os.path.join(plotDir, 'bnss_stats', 'CMR.csv'), index_col=0)

    with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
        statistics_for_current_bnss = pickle.load(file)

    if statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY] + 36 \
            < statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]:

        TimeKeyForPrediction = statistics_for_current_bnss[StatConstants.LAST_TIME_KEY] - length_for_prediction + 1
        ARmodelForAvgRating = extractSeriesAndARmodel(statistics_for_current_bnss, StatConstants.AVERAGE_RATING,
                                                     length_for_prediction)
        if ARmodelForAvgRating == -1:
            return -1

        startTimeKeyForPrediction = ARmodelForAvgRating[4]
        rating_bf_attack_at_the_end = ARmodelForAvgRating[0][-2]
        TimeKeyForPrediction = startTimeKeyForPrediction
        # print "Start prediciton from time %s. LAST_TIME_KEY is %s" % (startTimeKeyForPrediction,
        #                                                 statistics_for_current_bnss[StatConstants.LAST_TIME_KEY])
        ARmodelForNoOfReviews = extractSeriesAndARmodel(statistics_for_current_bnss,
                                                        StatConstants.NON_CUM_NO_OF_REVIEWS,
                                                        length_for_prediction)

        noOfAllReviews = statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][TimeKeyForPrediction:\
            TimeKeyForPrediction + length_for_prediction]

        # filter bnss with few reviews
        if np.mean(ARmodelForNoOfReviews[5][startTimeKeyForPrediction-30:startTimeKeyForPrediction]) < 3:
            return -1
        # if np.mean(ARmodelForNoOfReviews[5][startTimeKeyForPrediction - 30:startTimeKeyForPrediction]) < 3 or np.mean(ARmodelForNoOfReviews[5][startTimeKeyForPrediction - 30:startTimeKeyForPrediction])>5:
        #     return -1
        ratingDistribution = statistics_for_current_bnss[StatConstants.RATING_DISTRIBUTION]
        ratingEntropy = statistics_for_current_bnss[StatConstants.RATING_ENTROPY]
        noOfPositiveReviews = statistics_for_current_bnss[StatConstants.NO_OF_POSITIVE_REVIEWS]
        entropyAfAttack = list()
        avgRatingErrorAfAttack = list()
        stopAttackTime = list()
        ratingDivergenceAfAttack = list()
        noOfPositiveReviewsAfAttack = list()
        statistics_for_all_bnsses, statistics_for_all_measures = DetectAnalysis.poolData(plotDir, timeLength, measures,
                                                                                         startTimeKeyForPrediction, bnss_key)
        succeed_min_time = list()
        rating_for_attack = dict()
        attack_statistics = dict()
        attack_statistics['CAR bf attack'] = list()
        attack_statistics['NR bf attack'] = list()
        attack_statistics['time key to start attack'] = startTimeKeyForPrediction
        attack_statistics['rating distribution bf attack'] = ratingDistributionBfAttack = deepcopy(ratingDistribution)

        for t in range(length_for_prediction - 1):
            CMRForBnsses = df[str(TimeKeyForPrediction)].copy()
            rank_series = CMRForBnsses.rank(method='min', ascending=False)
            rankBfAttack, CMRBfAttack = rank_series[bnss_key], CMRForBnsses[bnss_key]
            noiseForAvgRating = 0.01
            noiseForNoOfReviews = NoiseForNoOfReviews
            attack_statistics['CAR bf attack'].append(ARmodelForAvgRating[0][t])
            attack_statistics['NR bf attack'].append(ARmodelForNoOfReviews[0][t])
            yhatDetect, lag = testARupdateCoefandDatamodel(ARmodelForAvgRating[3], ARmodelForAvgRating[1], ARmodelForAvgRating[2])
            # print "inspect whether ARmodelForNoOfReviews is equal", ARmodelForNoOfReviews[0][t:t+6] == ARmodelForNoOfReviews[5][TimeKeyForPrediction:TimeKeyForPrediction+6]
            if is_random:
                cases = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                i = randint(0, 11)
                case = cases[i]
                noiseForNoOfReviews = randint(20, 100)
                ratingForAttack = [0, 0, 0, 0, noiseForNoOfReviews]
                ratio = 0.8
                print "case is %s, noise for number of reviews is %s, ratio is %s" % (case, noiseForNoOfReviews, ratio)

            if case in [3, 4, 5, 6, 7, 8, 9, 10, 11, 12]: # case in 2,3,4,5,7,8,9
                threshold_for_AvgRating = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DEVIATION_OF_AVG_RATING], ratio) #determine the threshold for the deviation of average rating
                # print "threshold for AvgRating is %s" % threshold_for_AvgRating

                noiseForAvgRatingDiff = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DIFFERENCE_OF_AVG_RATING], ratio)
                noiseForAvgRatingDeviation, lag, yhatDetect \
                    = maximizeNoiseinTestForAvgRating(ARmodelForAvgRating[0],
                                                  ARmodelForAvgRating[1], ARmodelForAvgRating[2],
                                                  ARmodelForAvgRating[3], ARmodelForAvgRating[5], t, startTimeKeyForPrediction,
                                                  threshold_for_AvgRating, case, noiseForAvgRatingDiff)

            if case in [10, 11, 12]:
                noiseForAvgRating = min(ARmodelForAvgRating[5][startTimeKeyForPrediction - 1 + t] + noiseForAvgRatingDiff
                                    - ARmodelForAvgRating[0][t], noiseForAvgRatingDeviation) # assume the attackers have the knowledge of the avg rating of current month
            if case in [3, 4, 5, 6, 7, 8, 9]:
                noiseForAvgRating = min(float('inf'), noiseForAvgRatingDeviation)

                # noiseForAvgRating = min(ARmodelForAvgRating[5][startTimeKeyForPrediction - 1 + t] + noiseForAvgRatingDiff
                #                     - ARmodelForAvgRating[0][t], float('inf'))

            if case != 12:
                NoOfReviews = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.NO_OF_REVIEWS], ratio)
                NoOfReviewsDiff = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DIFFERENCE_OF_NO_OF_REVIEWS], ratio)
                # print 'Threshold of No of reviews, No of reviews diff and no of reviews in this timeKey :', \
                #     NoOfReviews, NoOfReviewsDiff, ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t]

            # use the last month to predict

            if case in [2, 11]:
                noiseForNoOfReviews = min(ARmodelForNoOfReviews[5][TimeKeyForPrediction - 1] + NoOfReviewsDiff - ARmodelForNoOfReviews[0][t],
                                              NoOfReviews - ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t])
                    # noiseForNoOfReviews = min(NoOfReviewsDiff, float('inf'))
            if case in [1, 3, 4, 5, 6, 7, 8, 9, 10]:
                noiseForNoOfReviews = min(float('inf'),
                                              NoOfReviews - ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t])

            if case in [3, 4]:
                if noiseForAvgRating < 0:
                    noiseForNoOfReviews = 0
                else:
                    if ARmodelForAvgRating[0][t] + noiseForAvgRating == 5:
                        noiseForNoOfReviews = NoOfReviews - ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t]
                    else:
                        noiseForNoOfReviews = round(
                            noOfAllReviews[t] * noiseForAvgRating / (5 - ARmodelForAvgRating[0][t] - noiseForAvgRating))
                        noiseForNoOfReviews = min(noiseForNoOfReviews,
                                                  NoOfReviews - ARmodelForNoOfReviews[5][startTimeKeyForPrediction + t])

            # if case in [5, 6, 7, 8, 9]:
            #     noiseForAvgRatingDeviation = max(statistics_for_all_bnsses[DetectAnalysis.DEVIATION_OF_AVG_RATING])
            #     noiseForAvgRatingDiff = max(statistics_for_all_bnsses[DetectAnalysis.DIFFERENCE_OF_AVG_RATING])
            #     noiseForAvgRating = max(ARmodelForAvgRating[5][startTimeKeyForPrediction - 1 + t]
            #                             + noiseForAvgRatingDiff - ARmodelForAvgRating[0][t], noiseForAvgRatingDeviation)

            P, H = 1, 0
            if case == 7:
                NoOfPositiveReivews = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.NO_OF_POSITIVE_REVIEWS], ratio)
                NoOfPositiveReivewsDiff = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS], ratio)

                NoOfReviewsAddNoise = ARmodelForNoOfReviews[5][startTimeKeyForPrediction - 1 + t] + noiseForNoOfReviews
                NoOfPositiveReivewsAddNoise = min(NoOfPositiveReivews, noOfPositiveReviews[startTimeKeyForPrediction-1+t] + NoOfPositiveReivewsDiff)
                P = NoOfPositiveReivewsAddNoise / NoOfReviewsAddNoise

            if case in [6, 9]:
                RatingEntropy = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.RATING_ENTROPY], 1 - ratio)
                RatingEntropyDiff = maximizeNoiseInCDF(statistics_for_all_bnsses[DetectAnalysis.DIFFERENCE_OF_RATING_ENTROPY], ratio)
                H = ratingEntropy[startTimeKeyForPrediction - 1 + t] - RatingEntropyDiff

            truthfulRatingDict = dict()
            for key in ratingDistribution.keys():
                if ratingDistribution[key][startTimeKeyForPrediction + t]:
                    truthfulRatingDict[key] = ratingDistribution[key][startTimeKeyForPrediction + t] - \
                                              ratingDistribution[key][startTimeKeyForPrediction + t - 1]
                else:
                    noiseForNoOfReviews = 0

            # print "inspect truthful rating:", truthfulRatingDict.values(), sum(truthfulRatingDict.values()), \
            # ARmodelForNoOfReviews[0][t], noiseForNoOfReviews
            truthfulRatingKeys, truthfulRating = zip(*sorted(truthfulRatingDict.iteritems(), key=lambda x: x[0]))
            if case in [5, 6, 7, 8, 9, 10, 11, 12]:
                # calculating what the distribution of a truthful reviews in time t should be

                if case!= 7:
                    P = 1 - (truthfulRating[0] + truthfulRating[1] + truthfulRating[2])/(sum(truthfulRating) + noiseForNoOfReviews)
                if noiseForNoOfReviews > 0 and noiseForAvgRating > 0:
                    noOfAllReviewsBeforeT= statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][TimeKeyForPrediction - 1]
                    OptimalRating = chooseRatingDistribution(ARmodelForAvgRating[5][TimeKeyForPrediction],
                                                             ARmodelForAvgRating[5][TimeKeyForPrediction - 1],
                                                             noOfAllReviews[t], noOfAllReviewsBeforeT, ARmodelForNoOfReviews[0][t], ratingDistribution,
                                                             TimeKeyForPrediction, noiseForAvgRating, noiseForNoOfReviews, P, H,
                                                             count_min, succeed_min_time, case)
                    if OptimalRating != -1:
                        ratingAfAttack, ratingAfAttackKeys, count_min, succeed_min_time = OptimalRating
                        ratingAfAttackDict = dict()  # sampling
                        for i in range(50):
                            rating_counts = samplingRating(int(noiseForNoOfReviews + ARmodelForNoOfReviews[0][t]),
                                                           ratingAfAttack)
                            # print "insepct noiseForNoOfReviews + ARmodelForNoOfReviews[0][t]", noiseForNoOfReviews + ARmodelForNoOfReviews[0][t] == sum(rating_counts)
                            tempRatingForAttack = list()
                            for i in range(len(rating_counts)):
                                if rating_counts[i] > truthfulRating[i]:
                                    tempRatingForAttack.append(rating_counts[i] - truthfulRating[i])
                                else:
                                    tempRatingForAttack.append(0)
                            rating_counts = np.array(truthfulRating) + np.array(tempRatingForAttack)
                            rating_sampling = rating_counts / sum(rating_counts)
                            # print "inspect rating_sampling:", sum(rating_sampling), sum(ratingAfAttack)
                            averageRatingAfSampling = np.sum(
                                [rating_sampling[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttackKeys))])
                            averageRatingBfSampling = np.sum(
                                [ratingAfAttack[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttackKeys))])
                            error = mse(rating_sampling, ratingAfAttack)
                            # print averageRatingBfSampling, averageRatingAfSampling
                            if error not in ratingAfAttackDict and averageRatingAfSampling >= averageRatingBfSampling:
                                ratingAfAttackDict[error] = rating_counts

                        if ratingAfAttackDict:
                            ratingAfAttack = ratingAfAttackDict[
                                min(ratingAfAttackDict.keys())]  # the number of reviews of each star
                            # print "rating af attack:", ratingAfAttack
                            ratingForAttack = list()
                            for i in range(len(ratingAfAttack)):
                                ratingForAttack.append(ratingAfAttack[i] - truthfulRating[i])
                            ratingForAttackKeys = ratingAfAttackKeys
                            noiseForNoOfReviews = sum(ratingForAttack)
                            AvgRatingAfSampling = np.sum(
                                [ratingAfAttack[i] * ratingAfAttackKeys[i] for i in range(len(ratingAfAttack))]) / sum(
                                ratingAfAttack)
                            # print "inspect rating af attack", sum(ratingAfAttack) == noiseForNoOfReviews + ARmodelForNoOfReviews[0][t]
                            # print "avg rating af attack", AvgRatingAfSampling, averageRatingBfSampling
                            # print (AvgRatingAfAttack*sum(ratingAfAttack)+ARmodelForAvgRating[5][TimeKeyForPrediction-1]*noOfAllReviewsBeforeT)/(noOfAllReviewsBeforeT+sum(ratingAfAttack))-ARmodelForAvgRating[0][t]
                            CMRAfAttackTemp = np.sum([ratingAfAttack[i]*ratingAfAttackKeys[i] for i in range(len(ratingAfAttack))])/ \
                                (noiseForNoOfReviews + ARmodelForNoOfReviews[0][t])
                            if CMRAfAttackTemp < averageRatingBfSampling or CMRAfAttackTemp < CMRBfAttack:
                                OptimalRating = -1
                        else:
                            OptimalRating = -1
                else:
                    OptimalRating = -1
            else:
                OptimalRating = 1
            # print "n_\delata is %s, \delta_t is %s, %s" % (noiseForNoOfReviews, noiseForAvgRating, t)
            if (OptimalRating != -1 and noiseForNoOfReviews > 0 and noiseForAvgRating > 0) or case == 0:
                if case in [0, 1, 2, 3, 4]:
                    succeed_min_time.append(TimeKeyForPrediction)
                    ratingForAttack = [0, 0, 0, 0, noiseForNoOfReviews]

                    ratingForAttackKeys = (1.0, 2.0, 3.0, 4.0, 5.0)
                ratingForAttack, truthfulRating = np.array(ratingForAttack), np.array(truthfulRating)
                # AvgRatingForAttack = np.sum(
                #     [ratingForAttack[i] * ratingForAttackKeys[i] for i in range(len(ratingForAttack))])
                # print "ratingForAttack is %s" % ratingForAttack
                ratingDict = dict()

                # ratingForAttack = [round(ratingForAttack[i]*noiseForNoOfReviews) for i in range(len(ratingForAttack))]
                AvgRatingForAttack = np.sum([ratingForAttack[i] * ratingForAttackKeys[i] for i in
                                             range(len(ratingForAttack))]) / noiseForNoOfReviews # Ep(R)
                AvgRatingBfAttack = ARmodelForAvgRating[0][t] #average rating before attack
                rating_for_attack[t] = ratingForAttack

                for timeKey in range(t, len(ARmodelForAvgRating[0])): # update the average rating from time t when issuing the attack
                    ARmodelForAvgRating[0][timeKey] = (ARmodelForAvgRating[0][timeKey] * noOfAllReviews[timeKey]
                        + AvgRatingForAttack * noiseForNoOfReviews) / (noOfAllReviews[timeKey] + noiseForNoOfReviews)

                    noOfAllReviews[timeKey] = noOfAllReviews[timeKey] + noiseForNoOfReviews

                    for i in range(len(ratingForAttack)): # update the cumulative rating distribution
                        ratingDistribution[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] = \
                            ratingDistribution[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] + \
                            ratingForAttack[i]
                        if timeKey > t:
                            ratingDistributionBfAttack[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] = \
                            ratingDistributionBfAttack[ratingForAttackKeys[i]][timeKey + startTimeKeyForPrediction] + \
                            ratingForAttack[i]

                ARmodelForNoOfReviews[0][t] = ARmodelForNoOfReviews[0][t] + noiseForNoOfReviews

                for j in range(len(ratingForAttackKeys)): # the probability of each star during t
                    ratingDict[ratingForAttackKeys[j]] = (ratingDistribution[ratingForAttackKeys[j]][
                                                              t + startTimeKeyForPrediction] -
                                                          ratingDistribution[ratingForAttackKeys[j]][
                                                              t + startTimeKeyForPrediction - 1]) / \
                                                         ARmodelForNoOfReviews[0][t]

                current_rating = dict()
                cumulative_rating = dict()
                cumulative_rating_sum = np.sum(
                    [ratingDistribution[rating][TimeKeyForPrediction - 1] for rating in ratingDistribution]
                )
                current_rating_sum = ARmodelForNoOfReviews[0][t]
                for key in ratingDistribution:
                    current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                        key][TimeKeyForPrediction - 1]

                    if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                        cumulative_rating_sum += 1e-10

                    if current_no_of_reviews == 0:
                        current_rating_sum += 1e-10

                for key in ratingDistribution:
                    current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                        key][TimeKeyForPrediction - 1]
                    if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                        cumulative_rating[key] = 1e-10 / cumulative_rating_sum
                    else:
                        cumulative_rating[key] = float(ratingDistribution[key][TimeKeyForPrediction - 1]) /cumulative_rating_sum

                    if current_no_of_reviews == 0:
                        current_rating[key]= 1e-10 / current_rating_sum
                    else:
                        current_rating[key] = float(current_no_of_reviews) /current_rating_sum

                # print "After Attack rating distribution: ", np.array(current_rating.values())*current_rating_sum

                divergence = calculateKLdivergence(current_rating, cumulative_rating)
                ratingDivergenceAfAttack.append(divergence)

                noOfPositiveReviewsForCurrentT = ratingDistribution[4][TimeKeyForPrediction] + ratingDistribution[5][t +
                startTimeKeyForPrediction] - ratingDistribution[4][TimeKeyForPrediction - 1] - \
                                      ratingDistribution[5][TimeKeyForPrediction - 1]
                # print "inspect no of positive reviews", statistics_for_current_bnss[StatConstants.NO_OF_POSITIVE_REVIEWS][
                #     TimeKeyForPrediction]+ratingForAttack[3]+ratingForAttack[4] == noOfPositiveReviewsForCurrentT

                noOfPositiveReviewsAfAttack.append(noOfPositiveReviewsForCurrentT)

                entropyAfAttack.append(entropyFn(ratingDict))
                avgRatingErrorAfAttack.append(abs(ARmodelForAvgRating[0][t] - yhatDetect))
                # print "deviation of average rating is %s" % (ARmodelForAvgRating[0][t] - yhatDetect)

                n = Backtracking_LineSearch(ARmodelForAvgRating[0][t], ARmodelForAvgRating[2], lag)
                updateCoef(ARmodelForAvgRating[2], n, yhatDetect, ARmodelForAvgRating[0][t], lag)
                ARmodelForAvgRating[3].append(ARmodelForAvgRating[0][t]) #update the history
                # print "promotion is %s" % (ARmodelForAvgRating[0][t] - AvgRatingBfAttack), AvgRatingBfAttack

                all_rating_af_attack = list()
                for key in ratingDistribution:
                    all_rating_af_attack.append(ratingDistribution[key][TimeKeyForPrediction])
                # print "car af attack:", ARmodelForAvgRating[0][t], sum([all_rating_af_attack[i]*ratingForAttackKeys[i] for i in range(len(ratingForAttackKeys))])/sum(all_rating_af_attack)

            else:
                n = Backtracking_LineSearch(ARmodelForAvgRating[0][t], ARmodelForAvgRating[2], lag)
                updateCoef(ARmodelForAvgRating[2], n, yhatDetect, ARmodelForAvgRating[0][t], lag)
                ARmodelForAvgRating[3].append(ARmodelForAvgRating[0][t])

                entropyAfAttack.append(ratingEntropy[t + startTimeKeyForPrediction])
                avgRatingErrorAfAttack.append(abs(ARmodelForAvgRating[0][t] - yhatDetect))
                current_rating = dict()
                cumulative_rating = dict()
                cumulative_rating_sum = np.sum(
                    [ratingDistribution[rating][TimeKeyForPrediction - 1] for rating in ratingDistribution]
                )
                current_rating_sum = ARmodelForNoOfReviews[0][t]
                for key in ratingDistribution:
                    current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                        key][TimeKeyForPrediction - 1]

                    if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                        cumulative_rating_sum += 1e-10

                    if current_no_of_reviews == 0:
                        current_rating_sum += 1e-10

                for key in ratingDistribution:
                    current_no_of_reviews = ratingDistribution[key][TimeKeyForPrediction] - ratingDistribution[
                        key][TimeKeyForPrediction - 1]
                    if ratingDistribution[key][TimeKeyForPrediction - 1] == 0:
                        cumulative_rating[key] = 1e-10 / cumulative_rating_sum
                    else:
                        cumulative_rating[key] = float(
                            ratingDistribution[key][TimeKeyForPrediction - 1]) / cumulative_rating_sum

                    if current_no_of_reviews == 0:
                        current_rating[key] = 1e-10 / current_rating_sum
                    else:
                        current_rating[key] = float(current_no_of_reviews) / current_rating_sum
                divergence = calculateKLdivergence(current_rating, cumulative_rating)
                # print "current_rating, cumulative_rating", current_rating.values(), sum(current_rating.values()), sum(cumulative_rating.values())
                ratingDivergenceAfAttack.append(divergence)

                noOfPositiveReviewsForCurrentT = ratingDistribution[4][t + startTimeKeyForPrediction] + ratingDistribution[5][
                    t + startTimeKeyForPrediction] - \
                                      ratingDistribution[4][t + startTimeKeyForPrediction - 1] - \
                                      ratingDistribution[5][t + startTimeKeyForPrediction - 1]
                noOfPositiveReviewsAfAttack.append(noOfPositiveReviewsForCurrentT)
                stopAttackTime.append(t)

            TimeKeyForPrediction += 1

        attack_statistics['CAR af attack'] = ARmodelForAvgRating[0][:-1]
        attack_statistics['NR af attack'] = ARmodelForNoOfReviews[0][:-1]
        attack_statistics['rating distribution af attack'] = ratingDistribution
        attack_statistics['rating for attack'] = rating_for_attack
        attack_statistics['stop attack'] = stopAttackTime
        attack_statistics['CMR'] = df.ix[bnss_key, :]
        # print "rating for attack stats:", attack_statistics['rating for attack']

        suspDir = os.path.join(plotDir, os.pardir, 'susp_stats', 'case_%s' % case)
        if not os.path.exists(os.path.join(suspDir, 'attack_stats')):
            os.mkdir(os.path.join(suspDir, 'attack_stats'))
        with open(os.path.join(suspDir, 'attack_stats', bnss_key), 'w') as file:
            pickle.dump(attack_statistics, file)

        # print bnss_key + ' ' + str(noisesForAverageRating)
        # print bnss_key + ' ' + str(noisesForNoOfReviews)
        # print "stopAttackTime is %s" % stopAttackTime
        return avgRatingErrorAfAttack, ARmodelForAvgRating[0][:-1], ARmodelForNoOfReviews[0][:-1], entropyAfAttack, \
               ratingDivergenceAfAttack, noOfPositiveReviewsAfAttack, startTimeKeyForPrediction, stopAttackTime,\
               statistics_for_all_bnsses, statistics_for_all_measures
    else:
        return -1

