import os
import pandas as pd
import numpy as np
from util.TrainClassifier import formAUCMatrixForMultiRandom, formAUCMatrixForAggregatedTrainedModel, trainOnAggregatedDataForCoef
from util.DetectAnalysis import calculateUtilityOfDifferentDetectors
from IPython import embed
from pandas import DataFrame
import matplotlib.pyplot as plt

AVG_RATING = 'Average Rating'
DEVIATION_OF_AVG_RATING = 'Deviation Of Average Rating'
NO_OF_REVIEWS = 'Non Cum No. of Reviews'
RATING_ENTROPY = 'Rating entropy'
KL_DIVERGENCE ='KL Divergence'
DIFFERENCE_OF_AVG_RATING = 'Difference of Average Rating'
DIFFERENCE_OF_NO_OF_REVIEWS = 'Difference Of Number Of Reviews'
DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS = 'Difference Of Number Of Positive Reviews'
DIFFERENCE_OF_RATING_ENTROPY = 'Difference Of Rating Entropy'
NO_OF_POSITIVE_REVIEWS = 'No of +ve Reviews'

# csvFolder = '../data/Amazon Dataset/'
csvFolder = '../data/Yelp Dataset/'

signals = [RATING_ENTROPY, DIFFERENCE_OF_AVG_RATING, NO_OF_REVIEWS, NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE,
               DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS,
               DIFFERENCE_OF_NO_OF_REVIEWS, DIFFERENCE_OF_RATING_ENTROPY, DEVIATION_OF_AVG_RATING]


def calculateMeanAndStd(csv):
    auc_matrices = list()

    for i in range(10):
        if not i:
            susp_stats = 'susp_stats'
        else:
            susp_stats = 'susp_stats_%s' % i

        auc_matrix = pd.read_csv(os.path.join(csvFolder, susp_stats, 'results', csv), index_col=0)
        print(auc_matrix.shape)
        auc_matrices.append(auc_matrix.values)

    auc_matrices = np.array(auc_matrices)
    mean_matrix = np.mean(auc_matrices, axis=0)
    std_matrix = np.std(auc_matrices, axis=0)

    print('mean_matrix')
    print(mean_matrix)
    mean_matrix = DataFrame(mean_matrix, index=auc_matrix.index, columns=auc_matrix.columns)
    mean_matrix.to_csv(os.path.join(csvFolder, 'mean_'+csv))
    print('std_matrix')
    std_matrix = DataFrame(std_matrix, index=auc_matrix.index, columns=auc_matrix.columns)
    std_matrix.to_csv(os.path.join(csvFolder, 'std_'+csv))
    std_matrix = DataFrame(std_matrix, index=auc_matrix.index, columns=auc_matrix.columns)
    std_matrix.to_csv(os.path.join(csvFolder, 'std_' + csv))
    print(std_matrix)


def viewWeightofDifferentModels():
    coef_dict = dict()

    for i in range(10):
        if not i:
            susp_stats = 'susp_stats'
        else:
            susp_stats = 'susp_stats_%s' % i
        susp_dir = os.path.join(csvFolder, susp_stats)
        coef = trainOnAggregatedDataForCoef(susp_dir, signals, case=1)
        coef_dict[i+1] = coef

    coef_matrix = DataFrame(coef_dict.values(), index=coef_dict.keys(), columns=coef.keys())
    print(coef_matrix)
    coef_matrix.to_csv(os.path.join(csvFolder, 'weight_of_models.csv'))

viewWeightofDifferentModels()

# first = True
# if first:
#     for i in range(10):
#         if not i:
#             susp_stats = 'susp_stats'
#         else:
#             susp_stats = 'susp_stats_%s' % i
#
#         if not os.path.exists(os.path.join(csvFolder, susp_stats, 'results', 'random_1_pre_Rating entropy.csv')):
#             for random in range(1, 3):
#                 calculateUtilityOfDifferentDetectors(os.path.join(csvFolder, 'stats'), susp_stats=susp_stats, random=random)
#
#         formAUCMatrixForMultiRandom(os.path.join(csvFolder, 'susp_stats'), signals, susp_stats)
#
#         susp_dir = os.path.join(csvFolder, susp_stats)
#
#         if not os.path.exists(os.path.join(susp_dir, 'results', 'auc_matrix_aggregated.csv')):
#             formAUCMatrixForAggregatedTrainedModel(susp_dir, signals=signals)
#
# calculateMeanAndStd('auc_matrix_aggregated.csv')
# calculateMeanAndStd('auc_matrix_only_random.csv')