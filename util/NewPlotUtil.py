from numpy import genfromtxt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import axes3d, Axes3D  # <-- Note the capitalization!

util_name_map = {'Recall': 'avg_d_util_matrix',
                 'CMR': 'avg_noise_for_cmr_af_detect_matrix',
                 'CAR': 'avg_noise_for_car_af_detect_matrix',
                 'Ranking': 'avg_noise_for_rank_af_detect_matrix'}


# functions for reading data
def load_case(case_id, dataset_name):
    if dataset_name == 'amazon':
        df = pd.read_csv('../data/Amazon Dataset/susp_stats/case_' + str(case_id) + '/case_' + str(case_id) + '_post.csv')
    elif dataset_name == 'yelp':
        df = pd.read_csv('../data/Yelp Dataset/susp_stats/case_' + str(case_id) + '/case_' + str(case_id) + '_post.csv')
    return df


def load_payoff_matrics(util_name, dataset_name):
    if dataset_name == 'amazon':
        df = pd.read_csv('../data/Amazon Dataset/susp_stats/results/' + util_name_map[util_name] + '.csv')
    elif dataset_name == 'yelp':
        df = pd.read_csv('../data/Yelp Dataset/susp_stats/results/' + util_name_map[util_name] + '.csv')

    clm_list = []
    for column in df.columns:
        clm_list.append(column)

    m = df[clm_list[1:len(clm_list) - 2]].values
    # print m
    return m


# plotting functions
def plot_evasion_properties(dfs, dataset_name):
    num_reviews = []
    num_positives = []
    num_negatives = []
    attack_rate = []

    target_cmr_promotion = []
    target_car_promotion = []
    target_ranking_promotion = []

    for i in range(9):
        df = dfs[i]

        # these properties are the same for different defense strategies.
        num_reviews.append(df.loc[0, 'avg_no_of_spam'])
        num_positives.append(df.loc[0, 'avg_no_of_positive_spam'])
        attack_rate.append(df.loc[0, 'attack_rate'])

        target_cmr_promotion.append(df.loc[0, 'avg_noise_for_cmr'])
        target_car_promotion.append(df.loc[0, 'avg_noise_for_car'])
        target_ranking_promotion.append(df.loc[0, 'avg_noise_for_rank'])

    num_reviews = np.array(num_reviews)
    num_positives = np.array(num_positives)

    num_negatives = num_reviews - num_positives

    x = np.arange(9) + 1

    width = 0.4  # the width of the bars: can also be len(x) sequence

    # plot number of fake reviews
    p1 = plt.bar(x, num_negatives, width, color='#788E1E', align='center')
    p2 = plt.bar(x, num_positives, width, bottom=num_negatives, color='#01579b', align='center')
    plt.xlim(0, 10)
    plt.xlabel('Evasion strategies', fontsize=16)
    #    plt.ylabel('Number of reviews', fontsize=16)
    plt.legend((p1[0], p2[0]), ('Negatives', 'Positives'), fontsize=18)
    plt.xticks(np.arange(1, 10, 1), fontsize=12)
    plt.yticks(fontsize=13)
    #    plt.title('number of reviews')
    plt.savefig('./number_spams_' + dataset_name + '.eps')
    plt.show()

    # plot attack rate (#months with attacks/total # months)
    plt.bar(x, attack_rate, width, color='#01579b', align='center')
    plt.xlim(0, 10)
    plt.xlabel('Evasion strategies', fontsize=16)
    #    plt.ylabel('Evasion rates', fontsize=16)
    plt.xticks(np.arange(1, 10, 1), fontsize=12)
    plt.yticks(fontsize=13)
    plt.savefig('./attack_rates_' + dataset_name + '.eps')
    plt.show()

    plt.bar(x, target_cmr_promotion, width, color='#01579b', align='center')
    plt.xlim(0, 10)
    plt.xlabel('Evasion strategies', fontsize=16)
    #    plt.ylabel('CMR promotion target', fontsize=16)
    plt.xticks(np.arange(1, 10, 1), fontsize=12)
    plt.yticks(fontsize=13)
    plt.savefig('./cmr_target_' + dataset_name + '.eps')
    plt.show()

    plt.bar(x, target_car_promotion, width, color='#01579b', align='center')
    plt.xlim(0, 10)
    plt.ylim(0, .0023)
    plt.xlabel('Evasion strategies', fontsize=16)
    #    plt.ylabel('CAR promotion target', fontsize=16)
    plt.xticks(np.arange(1, 10, 1), fontsize=12)
    plt.yticks(fontsize=13)
    plt.savefig('./car_target_' + dataset_name + '.eps')
    plt.show()

    plt.bar(x, target_ranking_promotion, width, color='#01579b', align='center')
    plt.xlim(0, 10)
    plt.xlabel('Evasion strategies', fontsize=16)
    # plt.ylabel('Ranking promotion target', fontsize=16)
    plt.xticks(np.arange(1, 10, 1), fontsize=12)
    plt.yticks(fontsize=13)
    plt.savefig('./ranking_target_' + dataset_name + '.eps')
    plt.show()


from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)


def plot_payoff_matrices(m, util_name, dataset_name):
    fig = plt.figure()
    ax = Axes3D(fig)  # <-- Note the difference from your original code...

    x, y = np.meshgrid(range(1, 10, 1), range(1, 10, 1))
    x = x.ravel()
    y = y.ravel()
    print x
    print y
    z = m  # rows for Evasions, columns for Defenses
    z = z.ravel(order='F')  # column-major order
    bottom = np.zeros_like(z)

    colors = ['#ebf4f4', '#cbefb8', '#a1ead8', '#cbb6ed', '#f2cba2', '#99ade5', '#99ade5', '#7a8cbc', '#b2b7c4']

    # for each defense: for each evasion
    cset = ax.bar3d(x, y, bottom, 0.5, 0.5, z, alpha=0.8, color=colors * 9)
    ax.set_xlim3d(1, 9.7)
    ax.set_ylim3d(1, 9.7)

    ax.set_xlabel('Evasions', fontsize=14)
    ax.set_ylabel('Defenses', fontsize=14)
    ax.set_zlabel(util_name, fontsize=13)

    # the following settings depend on which payoff matrix to plot

    if util_name == 'CMR promotion':
        # 1. for CMR
        ax.set_zticks(np.arange(0, 0.065, 0.01))
        ax.clabel(cset, fontsize=12, inline=1)
        if dataset_name == 'amazon':
            ax.text(1, 1, 0.07, 'Dominating evasion', color='red', fontsize=13)
            a = Arrow3D([2.8, 4.5], [1, 1.5], [0.07, 0.063], mutation_scale=20, lw=1, arrowstyle="-|>", color="k")
            ax.add_artist(a)
        plt.savefig('payoff_cmr_' + dataset_name + '.eps', bbox_inches='tight')

    elif util_name == 'Ranking promotion':
        # 2. for Ranking
        # ax.set_zticks(np.arange(0,,1))
        ax.clabel(cset, fontsize=12, inline=1)
        if dataset_name == 'amazon':
            ax.text(1, 1, 6, 'Dominating evasion', color='red', fontsize=13)
            a = Arrow3D([4, 5], [1, 1.5], [5.9, 5], mutation_scale=20, lw=1, arrowstyle="-|>", color="k")
            ax.add_artist(a)
        plt.savefig('payoff_ranking_' + dataset_name + '.eps', bbox_inches='tight')

    elif util_name == 'avg_noise_for_car_af_detect':
        # 3. for CAR
        # ax.set_zticks(np.arange(0,0.006,0.001))
        ax.clabel(cset, fontsize=12, inline=1)

        plt.savefig('payoff_car_' + dataset_name + '.eps', bbox_inches='tight')

    plt.show()


def plot_defense_payoff(m, dataset_name):
    fig = plt.figure()
    ax = Axes3D(fig)  # <-- Note the difference from your original code...

    x, y = np.meshgrid(range(1, 10, 1), range(1, 10, 1))
    x = x.ravel()
    y = y.ravel()
    #    print x
    #    print y
    z = m
    z = z.ravel(order='C')  # row-major order
    bottom = np.zeros_like(z)

    colors = ['#ebf4f4', '#cbefb8', '#a1ead8', '#cbb6ed', '#f2cba2', '#99ade5', '#99ade5', '#7a8cbc', '#b2b7c4']

    # for each defense: for each evasion
    cset = ax.bar3d(x, y, bottom, 0.5, 0.5, z, alpha=0.75, color=colors * 9)
    ax.set_xlim3d(1, 9.7)
    ax.set_ylim3d(1, 9.7)

    ax.set_xlabel('Defenses', fontsize=14)
    ax.set_ylabel('Evasions', fontsize=14)
    ax.set_zlabel('Recall', fontsize=13)
    ax.clabel(cset, fontsize=12, inline=1)

    if dataset_name == 'amazon':
        ax.text(-1, 4, 1.1, 'worst defense', color='red', fontsize=13)
        a = Arrow3D([1, 1], [4, 4.2], [1.1, 0.7], mutation_scale=20, lw=1, arrowstyle="-|>", color="k")
        ax.add_artist(a)

    plt.savefig('payoff_recall_' + dataset_name + '.eps', bbox_inches='tight')


dataset_names = ['yelp']

for d_name in dataset_names:
    dfs = []
    for i in range(9):
        dfs.append(load_case(i + 1, d_name))

    # plot_evasion_properties(dfs, d_name)

    # d_util = load_payoff_matrics('Recall', d_name)
    # s_cmr_util = load_payoff_matrics('CMR', d_name)
    # s_car_util = load_payoff_matrics('CAR', d_name)
    s_ranking_util = load_payoff_matrics('Ranking', d_name)

    print s_ranking_util.shape
    # plot_payoff_matrices(s_cmr_util, 'CMR promotion', d_name)
    # plot_payoff_matrices(s_car_util, 'CAR promotion', d_name)
    plot_payoff_matrices(s_ranking_util, 'Ranking promotion', d_name)
    plot_defense_payoff(d_util, d_name)

