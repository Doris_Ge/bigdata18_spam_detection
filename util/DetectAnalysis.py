# -*- coding:utf-8 -*-
from util import StatConstants
import numpy as np
import pickle
import os
import math
from util.AttackAnalysis import predictByARupdateCoefandDatamodel, calculateKLdivergence, extractBnssKeys
from util.StatUtil import entropyFn
from numpy import random
from pandas import DataFrame
import pandas as pd
import random
from datetime import datetime
from sklearn.metrics import label_ranking_loss
from sklearn.linear_model import LogisticRegression
from IPython import embed

AVG_RATING = 'Average Rating'
DEVIATION_OF_AVG_RATING = 'Deviation Of Average Rating'
NO_OF_REVIEWS = 'Non Cum No. of Reviews'
RATING_ENTROPY = 'Rating entropy'
KL_DIVERGENCE ='KL Divergence'
DIFFERENCE_OF_AVG_RATING = 'Difference of Average Rating'
DIFFERENCE_OF_NO_OF_REVIEWS = 'Difference Of Number Of Reviews'
DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS = 'Difference Of Number Of Positive Reviews'
DIFFERENCE_OF_RATING_ENTROPY = 'Difference Of Rating Entropy'
NO_OF_POSITIVE_REVIEWS = 'No of +ve Reviews'
EVASION_1 = [NO_OF_REVIEWS]
EVASION_2 = [NO_OF_REVIEWS, DIFFERENCE_OF_NO_OF_REVIEWS]
EVASION_3 = [NO_OF_REVIEWS, DEVIATION_OF_AVG_RATING]
EVASION_4 = [NO_OF_REVIEWS, DEVIATION_OF_AVG_RATING, DIFFERENCE_OF_AVG_RATING]
EVASION_5 = [NO_OF_REVIEWS, DEVIATION_OF_AVG_RATING, KL_DIVERGENCE]
EVASION_6 = [NO_OF_REVIEWS, DEVIATION_OF_AVG_RATING, KL_DIVERGENCE, DIFFERENCE_OF_RATING_ENTROPY]
EVASION_7 = [NO_OF_REVIEWS, DEVIATION_OF_AVG_RATING, KL_DIVERGENCE, NO_OF_POSITIVE_REVIEWS]
EVASION_8 = [NO_OF_REVIEWS, DEVIATION_OF_AVG_RATING, RATING_ENTROPY]
EVASION_9 = [NO_OF_REVIEWS, DEVIATION_OF_AVG_RATING, RATING_ENTROPY, DIFFERENCE_OF_RATING_ENTROPY]

def calculateAnomalyRatio(signal):
    diff_train = signal[0]
    diff = signal[1]
    direction = signal[2]
    diff_train = sorted(diff_train)
    tmp = 0
    # print diff_train, len(diff_train), diff
    for i in range(len(diff_train)):
        if diff >= diff_train[i]:
            if direction == 'H':
                f = 1 - (float(i + 1) / len(diff_train))
            elif direction == 'L':
                f = float(i + 1) / len(diff_train)
            if i == len(diff_train) - 1:
                break
            elif diff < diff_train[i + 1]:
                break
        else:
            if direction == 'H':
                f = 1
            elif direction == 'L':
                f = 0
    # print f
    return f


def set_threshold(signal_dict):
    threshold = dict()
    for signal in signal_dict:
        if signal == DEVIATION_OF_AVG_RATING:
            threshold[signal] = 0.8
        if signal == DIFFERENCE_OF_AVG_RATING:
            threshold[signal] = 0.8
        if signal == DIFFERENCE_OF_NO_OF_REVIEWS:
            threshold[signal] = 0.8
        if signal == NO_OF_REVIEWS:
            threshold[signal] = 0.8
        if signal == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
            threshold[signal] = 0.8
        if signal == NO_OF_POSITIVE_REVIEWS:
            threshold[signal] = 0.8
        if signal == DIFFERENCE_OF_RATING_ENTROPY:
            threshold[signal] = 0.8
        if signal == RATING_ENTROPY:
            threshold[signal] = 0.8
        if signal == KL_DIVERGENCE:
            threshold[signal] = 0.8
    return threshold


def calculateSuspiciousScore(signal_dict):
    f_list = list()
    for i in range(len(signal_dict)):
        f_list.append(calculateAnomalyRatio(signal_dict[i]))
    f_list = np.array(f_list)
    # print f_list
    # print f_list, signal_dict
    suspicious_score = 1 - math.sqrt(np.mean(f_list**2))
    return suspicious_score


def calculateSuspiciousScoreWithDifferentweight(signal_dict, signal_with_min_weight):
    f_list = list()
    weight = dict()
    for signal in signal_dict:
        f_list.append(calculateAnomalyRatio(signal_dict[signal]))

        if signal in signal_with_min_weight:
            weight[signal] = 1.0
        else:
            weight[signal] = 0.1

    f_list = np.array(f_list)
    # print signal_dict[NO_OF_POSITIVE_REVIEWS]
    # print signal_dict[RATING_ENTROPY]
    # print f_list
    weight_values = weight.values()
    mean = np.sum([f_list[j] ** 2 * weight_values[j] for j in range(len(weight_values))]) / sum(weight_values)
    # print weight_values, mean
    suspicious_score = 1 - math.sqrt(mean)
    return suspicious_score, weight_values


def calculateSuspiciousScoreWithRandomWeight(signal_dict):
    f_list = list()
    weight = dict()
    for signal in signal_dict:
        f_list.append(calculateAnomalyRatio(signal_dict[signal]))
        weight[signal] = np.random.random()

    f_list = np.array(f_list)
    weight_values = weight.values()
    mean = np.sum([f_list[j] ** 2 * weight_values[j] for j in range(len(weight_values))]) / sum(weight_values)
    suspicious_score = 1 - math.sqrt(mean)
    return suspicious_score, weight_values


def calculate_f(signal_value, threshold):
    length = len(signal_value[0])
    direction = signal_value[2]
    if direction == 'H':
        sorted_signal = sorted(signal_value[0])
        threshold_value = sorted_signal[int(round(length*threshold))-1]
        if signal_value[1] > threshold_value:
            return 1
        else:
            return 0
    elif direction == 'L':
        sorted_signal = sorted(signal_value[0], reverse=True)
        threshold_value = sorted_signal[int(round(length*threshold))-1]
        if signal_value[1] < threshold_value:
            return 1
        else:
            return 0


def calculateSuspiciousScoreByThreshold(signal_dict, threshold):
    f_list = list()
    for signal in signal_dict:
        signal_dict_value = signal_dict[signal]
        threshold_for_current_signal = threshold[signal]
        f_list.append(calculate_f(signal_dict_value, threshold_for_current_signal))

    suspicious_score = float(sum(f_list))/len(signal_dict)
    return suspicious_score


def calculateSuspiciousScore_improve(signal_dict):
    signal_dict_values = signal_dict.values()
    no_of_reviews = signal_dict[NO_OF_REVIEWS]
    f_list = list()
    weight = dict()
    for signal in signal_dict:
        f_list.append(calculateAnomalyRatio(signal_dict[signal]))
        direction = signal_dict[signal][2]
        if signal == NO_OF_REVIEWS:
            weight[signal] = (signal_dict[signal][1])/ max(signal_dict[signal][0])
        elif signal in [DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_RATING_ENTROPY, DIFFERENCE_OF_NO_OF_REVIEWS]:
            weight[signal] = (signal_dict[signal][1] - min(signal_dict[signal][0]))/(max(signal_dict[signal][0] - min(signal_dict[signal][0])))
        else:
            if direction == 'H':
                weight[signal] = (signal_dict[signal][1]/max(signal_dict[signal][0])*no_of_reviews[1]/max(no_of_reviews[0]))
            elif direction == 'L':
                weight[signal] = (np.log2(5)-signal_dict[signal][1])/max((np.log2(5)-signal_dict[signal][0]))*no_of_reviews[1]/max(no_of_reviews[0])

    f_list = np.array(f_list)
    weight_values = weight.values()
    mean = np.sum([f_list[j]**2*weight_values[j] for j in range(len(weight_values))])/sum(weight_values)
    print "mean is %s" % mean
    if mean<0:
        pass
    # print [weight[j]/sum(weight.values()) for j in weight]
    print weight
    suspicious_score = 1 - math.sqrt(mean)
    return suspicious_score

def poolEarlyData(plotDir, timeLength, measures, time_key, bnss_keys):
    '''
    :param plotDir: folder that stores the statistics of business
    :param timeLength: time window unit('1-W', 1-M')
    :param measures: signals to be extracted
    :param timeKey: time key to start prediction
    :param bnss_key: businesses that provide training data
    :param args:
    :return:
           statistics_of_all_bnsses: a dictionary with extracted signals as keys and statistics of signals of
           training businesses as values
           statistics_of_all_measures: a dictionarly with training businesses as keys and the statistics of signals of
           the corresponding business as values
    '''
    # timeKey is the time to start prediction
    statistics_for_current_bnss_dir, _ = extractBnssKeys(plotDir, timeLength)

    statistics_for_all_bnsses = dict() # store data according to the measure
    statistics_for_all_measures = dict() # use bnss as the first key and measure as the second key to store data
    #bnssKeys = bnssKeys[:5]

    for bnss_key in bnss_keys:
        print bnss_key

        with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
            statistics_for_current_bnss = pickle.load(file)
        first_time_key = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]
        last_time_key = statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]
        timeKey = first_time_key + time_key

        if timeKey > last_time_key:
            timeKey = last_time_key

        print 'Pool Data The first time key is ', first_time_key

        if bnss_key not in statistics_for_all_measures:
            statistics_for_all_measures[bnss_key] = dict()

        for measure in measures:
            if measure in [AVG_RATING, NO_OF_REVIEWS, RATING_ENTROPY, NO_OF_POSITIVE_REVIEWS]:
                statistics_for_current_measure = statistics_for_current_bnss[measure][first_time_key:timeKey]
                if measure not in statistics_for_all_bnsses:
                    statistics_for_all_bnsses[measure] = list()
                statistics_for_all_bnsses[measure] = np.append(statistics_for_all_bnsses[measure],
                                                               statistics_for_current_measure[1:])
                statistics_for_all_measures[bnss_key][measure] = statistics_for_current_measure[1:]

            if measure in [DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS, DIFFERENCE_OF_RATING_ENTROPY,
                           DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]:
                if measure == DIFFERENCE_OF_AVG_RATING:
                    statistics_for_current_measure = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][
                                                     first_time_key:timeKey]
                    difference_of_statistics = np.diff(statistics_for_current_measure)
                    filter_diff = [diff for diff in difference_of_statistics if diff > 0]
                if measure == DIFFERENCE_OF_RATING_ENTROPY:
                    statistics_for_current_measure = statistics_for_current_bnss[StatConstants.RATING_ENTROPY][
                                                     first_time_key:timeKey]
                    difference_of_statistics = - np.diff(statistics_for_current_measure)
                    filter_diff = [diff for diff in difference_of_statistics if diff < 0]
                    filter_diff = np.abs(filter_diff)
                if measure == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                    statistics_for_current_measure = statistics_for_current_bnss[
                                                         StatConstants.NO_OF_POSITIVE_REVIEWS][
                                                     first_time_key:timeKey]
                    difference_of_statistics = np.diff(statistics_for_current_measure)
                    filter_diff = [diff for diff in difference_of_statistics if diff > 0]
                if measure == DIFFERENCE_OF_NO_OF_REVIEWS:
                    statistics_for_current_measure = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS][
                                                     first_time_key:
                                                     timeKey]
                    difference_of_statistics = np.diff(statistics_for_current_measure)
                    filter_diff = [diff for diff in difference_of_statistics if diff > 0]
                if measure not in statistics_for_all_bnsses:
                    statistics_for_all_bnsses[measure] = list()
                statistics_for_all_bnsses[measure] = np.append(statistics_for_all_bnsses[measure], difference_of_statistics)
                statistics_for_all_measures[bnss_key][measure] = statistics_for_current_measure

            if measure == DEVIATION_OF_AVG_RATING:
                statistics_for_current_measure = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][
                                                 first_time_key:timeKey]
                train, test = statistics_for_current_measure[:12], statistics_for_current_measure[12:]
                errorlist = list()
                predictByARupdateCoefandDatamodel(plotDir, train, test, bnss_key, StatConstants.AVERAGE_RATING, None,
                                                  errorlist, None)
                if measure not in statistics_for_all_bnsses:
                    statistics_for_all_bnsses[measure] = list()
                statistics_for_all_bnsses[measure] = np.append(statistics_for_all_bnsses[measure], errorlist)
                statistics_for_all_measures[bnss_key][measure] = errorlist

            if measure == KL_DIVERGENCE:
                statistics_for_current_measure = statistics_for_current_bnss[StatConstants.RATING_DISTRIBUTION]
                cum_no_of_reviews = statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][
                                    first_time_key:timeKey] + 0.0# cumulative number of reviews
                no_of_reviews = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS][
                                first_time_key:timeKey] + 0.0 # number of reiviews in each time interval
                KL_divergence = list()
                cumulative_rating = dict()
                current_rating = dict()
                for i in range(len(cum_no_of_reviews) - 1):  # start from the second time_Key
                    time_Key = i + first_time_key
                    for rating in statistics_for_current_measure:
                        current_no_of_reviews = statistics_for_current_measure[rating][time_Key + 1] - \
                                                      statistics_for_current_measure[rating][time_Key]

                        if statistics_for_current_measure[rating][time_Key] == 0:
                            cum_no_of_reviews[i] += 1e-10

                        if current_no_of_reviews == 0:
                            no_of_reviews[i + 1] += 1e-10

                    for rating in statistics_for_current_measure:
                        current_no_of_reviews = statistics_for_current_measure[rating][time_Key + 1] - \
                                                statistics_for_current_measure[rating][time_Key]

                        if statistics_for_current_measure[rating][time_Key] == 0:
                            cumulative_rating[rating] = 1e-10 /cum_no_of_reviews[i]
                        else:
                            cumulative_rating[rating] = statistics_for_current_measure[rating][time_Key]/cum_no_of_reviews[i]

                        if current_no_of_reviews == 0:
                            current_rating[rating] = 1e-10 /no_of_reviews[i+1]
                        else:
                            current_rating[rating] = current_no_of_reviews/no_of_reviews[i + 1]

                    divergence = calculateKLdivergence(current_rating, cumulative_rating)
                    KL_divergence.append(divergence)
                if measure not in statistics_for_all_bnsses:
                    statistics_for_all_bnsses[measure] = list()
                statistics_for_all_bnsses[measure] = np.append(statistics_for_all_bnsses[measure], KL_divergence)
                statistics_for_all_measures[bnss_key][measure] = KL_divergence

    # if not os.path.exists(os.path.join(plotDir, 'bnsses_stats')):
    #     os.makedirs(os.path.join(plotDir, 'bnsses_stats'))
    #
    # for root, dirs, files in os.walk(os.path.join(plotDir, 'bnsses_stats')):
    #     for file in files:
    #         os.unlink(os.path.join(plotDir, 'bnsses_stats', file))
    #
    # pickle.dump(statistics_for_all_bnsses, open(os.path.join(plotDir, 'bnsses_stats', measure), 'w'))

    return statistics_for_all_bnsses, statistics_for_all_measures



def poolData(plotDir, timeLength, measures, timeKey, bnss_key=None, *args):
    '''
    :param plotDir: folder that stores the statistics of business
    :param timeLength: time window unit('1-W', 1-M')
    :param measures: signals to be extracted
    :param timeKey: time key to start prediction
    :param bnss_key: businesses that provide training data
    :param args:
    :return:
           statistics_of_all_bnsses: a dictionary with extracted signals as keys and statistics of signals of
           training businesses as values
           statistics_of_all_measures: a dictionarly with training businesses as keys and the statistics of signals of
           the corresponding business as values
    '''
    # timeKey is the time to start prediction
    statistics_for_current_bnss_dir, bnssKeys = extractBnssKeys(plotDir, timeLength)
    if bnss_key:
        bnssKeys = [bnss_key]
    statistics_for_all_bnsses = dict() # store data according to the measure
    statistics_for_all_measures = dict() # use bnss as the first key and measure as the second key to store data
    #bnssKeys = bnssKeys[:5]

    for bnss_key in bnssKeys:
        # print bnss_key

        with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
            statistics_for_current_bnss = pickle.load(file)
        first_time_key = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]
        last_time_key = statistics_for_current_bnss[StatConstants.LAST_TIME_KEY]

        if timeKey > last_time_key:
            timeKey = last_time_key

        # skip the early businesses in current time key
        if statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][last_time_key] < 20:
            continue

        if args:
            first_time_key = timeKey - args[0]
        else:
            first_time_key = timeKey - 30
        # extract only statistics of 30 time windows before prediction for estimating the parameters

        # print 'The first time key is ', first_time_key

        if bnss_key not in statistics_for_all_measures:
            statistics_for_all_measures[bnss_key] = dict()

        for measure in measures:
            if measure in [NO_OF_REVIEWS, RATING_ENTROPY, NO_OF_POSITIVE_REVIEWS]:
                statistics_for_current_measure = statistics_for_current_bnss[measure][first_time_key:timeKey]
                if measure not in statistics_for_all_bnsses:
                    statistics_for_all_bnsses[measure] = list()
                statistics_for_all_bnsses[measure] = np.append(statistics_for_all_bnsses[measure],
                                                               statistics_for_current_measure)
                statistics_for_all_measures[bnss_key][measure] = statistics_for_current_measure

            if measure in [DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS, DIFFERENCE_OF_RATING_ENTROPY,
                           DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]:
                if measure == DIFFERENCE_OF_AVG_RATING:
                    statistics_for_current_measure = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][
                                                     first_time_key:timeKey]
                    difference_of_statistics = np.diff(statistics_for_current_measure)
                    filter_diff = [diff for diff in difference_of_statistics if diff > 0]
                if measure == DIFFERENCE_OF_RATING_ENTROPY:
                    statistics_for_current_measure = statistics_for_current_bnss[StatConstants.RATING_ENTROPY][
                                                     first_time_key:timeKey]
                    difference_of_statistics = - np.diff(statistics_for_current_measure)
                    filter_diff = [diff for diff in difference_of_statistics if diff < 0]
                    filter_diff = np.abs(filter_diff)
                if measure == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                    statistics_for_current_measure = statistics_for_current_bnss[
                                                         StatConstants.NO_OF_POSITIVE_REVIEWS][
                                                     first_time_key:timeKey]
                    difference_of_statistics = np.diff(statistics_for_current_measure)
                    filter_diff = [diff for diff in difference_of_statistics if diff > 0]
                if measure == DIFFERENCE_OF_NO_OF_REVIEWS:
                    statistics_for_current_measure = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS][
                                                     first_time_key:
                                                     timeKey]
                    difference_of_statistics = np.diff(statistics_for_current_measure)
                    filter_diff = [diff for diff in difference_of_statistics if diff > 0]
                if measure not in statistics_for_all_bnsses:
                    statistics_for_all_bnsses[measure] = list()
                statistics_for_all_bnsses[measure] = np.append(statistics_for_all_bnsses[measure], difference_of_statistics)
                statistics_for_all_measures[bnss_key][measure] = statistics_for_current_measure

            if measure == DEVIATION_OF_AVG_RATING:
                statistics_for_current_measure = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][
                                                 first_time_key:timeKey]
                train, test = statistics_for_current_measure[:12], statistics_for_current_measure[12:]
                errorlist = list()
                predictByARupdateCoefandDatamodel(plotDir, train, test, bnss_key, StatConstants.AVERAGE_RATING, None,
                                                  errorlist, None)
                if measure not in statistics_for_all_bnsses:
                    statistics_for_all_bnsses[measure] = list()
                statistics_for_all_bnsses[measure] = np.append(statistics_for_all_bnsses[measure], errorlist)
                statistics_for_all_measures[bnss_key][measure] = errorlist

            if measure == KL_DIVERGENCE:
                statistics_for_current_measure = statistics_for_current_bnss[StatConstants.RATING_DISTRIBUTION]
                cum_no_of_reviews = statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][
                                    first_time_key:timeKey] + 0.0# cumulative number of reviews
                no_of_reviews = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS][
                                first_time_key:timeKey] + 0.0 # number of reiviews in each time interval
                KL_divergence = list()
                cumulative_rating = dict()
                current_rating = dict()
                for i in range(len(cum_no_of_reviews) - 1):  # start from the second time_Key
                    time_Key = i + first_time_key
                    for rating in statistics_for_current_measure:
                        current_no_of_reviews = statistics_for_current_measure[rating][time_Key + 1] - \
                                                      statistics_for_current_measure[rating][time_Key]

                        if statistics_for_current_measure[rating][time_Key] == 0:
                            cum_no_of_reviews[i] += 1e-10

                        if current_no_of_reviews == 0:
                            no_of_reviews[i + 1] += 1e-10

                    for rating in statistics_for_current_measure:
                        current_no_of_reviews = statistics_for_current_measure[rating][time_Key + 1] - \
                                                statistics_for_current_measure[rating][time_Key]

                        if statistics_for_current_measure[rating][time_Key] == 0:
                            cumulative_rating[rating] = 1e-10 /cum_no_of_reviews[i]
                        else:
                            cumulative_rating[rating] = statistics_for_current_measure[rating][time_Key]/cum_no_of_reviews[i]

                        if current_no_of_reviews == 0:
                            current_rating[rating] = 1e-10 /no_of_reviews[i+1]
                        else:
                            current_rating[rating] = current_no_of_reviews/no_of_reviews[i + 1]

                    divergence = calculateKLdivergence(current_rating, cumulative_rating)
                    KL_divergence.append(divergence)
                if measure not in statistics_for_all_bnsses:
                    statistics_for_all_bnsses[measure] = list()
                statistics_for_all_bnsses[measure] = np.append(statistics_for_all_bnsses[measure], KL_divergence)
                statistics_for_all_measures[bnss_key][measure] = KL_divergence

    # if not os.path.exists(os.path.join(plotDir, 'bnsses_stats')):
    #     os.makedirs(os.path.join(plotDir, 'bnsses_stats'))
    #
    # for root, dirs, files in os.walk(os.path.join(plotDir, 'bnsses_stats')):
    #     for file in files:
    #         os.unlink(os.path.join(plotDir, 'bnsses_stats', file))
    #
    # pickle.dump(statistics_for_all_bnsses, open(os.path.join(plotDir, 'bnsses_stats', measure), 'w'))

    return statistics_for_all_bnsses, statistics_for_all_measures


def OfflineDetectAnalysisNoChange(plotDir, statistics_for_current_bnss_dir, bnss_key, startTimeKeyForPrediction, avgRatingErrorAfAttack, avgRatingAfAttack, noOfReviewsAfAttack, ratingEntropyAfAttack):
    with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
        statistics_for_current_bnss = pickle.load(file)

    firstTimeKey = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]

    avg_rating = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][firstTimeKey:startTimeKeyForPrediction]
    avg_rating_train, avg_rating_test = avg_rating[:12], avg_rating[12:startTimeKeyForPrediction]


    errorlist = list()
    predictByARupdateCoefandDatamodel(plotDir, avg_rating_train, avg_rating_test, bnss_key, StatConstants.AVERAGE_RATING, None, errorlist, None)


    no_of_reviews_train = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS]
    no_of_reviews_train = no_of_reviews_train[firstTimeKey:startTimeKeyForPrediction]
    entropy_train = statistics_for_current_bnss[StatConstants.RATING_ENTROPY][firstTimeKey:startTimeKeyForPrediction]

    avg_rating_error_diff = np.append(errorlist, avgRatingErrorAfAttack)
    avg_rating = np.append(avg_rating, avgRatingAfAttack)
    no_of_reviews = np.append(no_of_reviews_train[:], noOfReviewsAfAttack)
    ratingEntropy = np.append(entropy_train[:], ratingEntropyAfAttack)
    avg_rating_diff = abs(np.diff(avg_rating))
    no_of_reviews_diff = abs(np.diff(no_of_reviews))
    ratingEntropy_diff = abs(np.diff(ratingEntropy))

    # no_of_reviews = no_of_reviews[1:]
    # ratingEntropy = ratingEntropy[1:]

    suspicious_score_dict = dict()
    attackLength = len(avgRatingErrorAfAttack)

    for i in range(len(avg_rating_error_diff)):
        signal_dict = [(avg_rating_error_diff, avg_rating_error_diff[i], 'H'), (no_of_reviews_diff, no_of_reviews_diff[i+11], 'H'),
                       (ratingEntropy_diff, ratingEntropy_diff[i+11], 'H'), (no_of_reviews, no_of_reviews[i+12], 'H'),
                       (ratingEntropy, ratingEntropy[i+12], 'L')]
        suspicious_score = calculateSuspiciousScore(signal_dict)
        suspicious_score_dict[i] = suspicious_score

    rank = sorted(suspicious_score_dict.keys(), key=lambda x:suspicious_score_dict[x], reverse=True)

    return rank, attackLength

def generateSignalDict(signals_dict, signal, pole, statistics_for_train_bnsses, signalAfAttack, stratify=True,
                       low_idx=None, high_idx=None):
    if stratify:
        high_stats= np.array(signalAfAttack)[high_idx]
        low_stats = np.array(signalAfAttack)[low_idx]
        signal_for_high_months = np.append(statistics_for_train_bnsses[signal]['high'],
                                           high_stats)
        signal_for_low_months = np.append(statistics_for_train_bnsses[signal]['low'],
                                           low_stats)
        signals_dict[signal]={'high': (signal_for_high_months, high_stats, pole),
                              'low': (signal_for_low_months, low_stats, pole)}
    else:
        signal_for_train_months = np.append(statistics_for_train_bnsses[signal],
                                           signalAfAttack)
        signals_dict[signal] = (signal_for_train_months, signalAfAttack, pole)


def EarlyOfflineDetectAnalysisWithinAttack(plotDir, bnss_key, signals_to_be_extracted,
                                    avgRatingAfAttack, noOfReviewsAfAttack, ratingEntropyAfAttack,
                                    stopAttackTime, noisesForAvgRating, noisesForCMR,
                                    noisesForRank, statistics_for_all_bnsses,
                                    statistics_for_test_bnss, stratify=False, *args):

    signals_dict = dict()

    if stratify:
        avg_rating = avgRatingAfAttack
        high_idx = [i for i in range(len(avg_rating)) if avg_rating[i] > 3]
        low_idx = [i for i in range(len(avg_rating)) if avg_rating[i] <= 3]


    if DIFFERENCE_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating = statistics_for_test_bnss[AVG_RATING]
        avg_rating_for_current_bnss = np.append(avg_rating, avgRatingAfAttack)
        avg_rating_diff_for_current_bnss = np.diff(avg_rating_for_current_bnss)
        avg_rating_diff_af_attack = avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):]
        if stratify:
            generateSignalDict(signals_dict, DIFFERENCE_OF_AVG_RATING, 'H', statistics_for_all_bnsses,
                           avg_rating_diff_af_attack, stratify, high_idx=high_idx, low_idx=low_idx)
        else:
            generateSignalDict(signals_dict, DIFFERENCE_OF_AVG_RATING, 'H', statistics_for_all_bnsses,
                               avg_rating_diff_af_attack, stratify)
        # if stratify:
        #     high_stats = avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):][high_idx]
        #     avg_rating_diff_for_high_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_AVG_RATING]['high'],
        #             high_stats)
        #     signals_dict[DIFFERENCE_OF_AVG_RATING]['high'] = (avg_rating_diff_for_high_bnsses, high_stats, 'H', high_idx)
        #     low_stats = avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):][low_idx]
        #     avg_rating_diff_for_low_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_AVG_RATING]['low'],
        #                                                 low_stats)
        #     signals_dict[DIFFERENCE_OF_AVG_RATING]['low'] = (avg_rating_diff_for_low_bnsses, low_stats, 'H', low_idx)
        # else:
        #     avg_rating_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_AVG_RATING],
        #                                            avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):])
        #     signals_dict[DIFFERENCE_OF_AVG_RATING] = (avg_rating_diff_for_all_bnsses,
        #                                           avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):], 'H')

    if NO_OF_REVIEWS in signals_to_be_extracted:
        if stratify:
            generateSignalDict(signals_dict, NO_OF_REVIEWS, 'H', statistics_for_all_bnsses,
                           noOfReviewsAfAttack, stratify, high_idx=high_idx, low_idx=low_idx)
        else:
            generateSignalDict(signals_dict, NO_OF_REVIEWS, 'H', statistics_for_all_bnsses,
                               noOfReviewsAfAttack, stratify)

    if DIFFERENCE_OF_NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_bf_attack = statistics_for_test_bnss[NO_OF_REVIEWS]
        no_of_reviews_for_current_bnss = np.append(no_of_reviews_bf_attack, noOfReviewsAfAttack)
        no_of_reviews_diff_for_current_bnss = np.diff(no_of_reviews_for_current_bnss)
        no_of_reviews_diff_af_attack = no_of_reviews_diff_for_current_bnss[-len(noOfReviewsAfAttack):]
        if stratify:
            generateSignalDict(signals_dict, DIFFERENCE_OF_NO_OF_REVIEWS, 'H', statistics_for_all_bnsses,
                              no_of_reviews_diff_af_attack, stratify, high_idx=high_idx, low_idx=low_idx)
        else:
            generateSignalDict(signals_dict, DIFFERENCE_OF_NO_OF_REVIEWS, 'H', statistics_for_all_bnsses,
                               no_of_reviews_diff_af_attack, stratify)


    if RATING_ENTROPY in signals_to_be_extracted:
        if stratify:
            generateSignalDict(signals_dict, RATING_ENTROPY, 'H', statistics_for_all_bnsses,
                              ratingEntropyAfAttack, stratify, high_idx=high_idx, low_idx=low_idx)
        else:
            generateSignalDict(signals_dict, RATING_ENTROPY, 'H', statistics_for_all_bnsses,
                               ratingEntropyAfAttack, stratify)

    suspicious_score_list = list()
    attackLength = len(avgRatingAfAttack)
    burst = 0
    statistics_for_current_bnss = dict()

    if stratify:
        high_i = 0
        low_i = 0
        for i in range(attackLength):
            is_burst = 0
            suspicious_score_signals = dict()
            if i in high_idx:
                for signal in signals_dict:
                    signal_value = signals_dict[signal]['high']
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[1][high_i], signal_value[2], 'high')
                high_i += 1
            else:
                for signal in signals_dict:
                    signal_value = signals_dict[signal]['low']
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[1][low_i], signal_value[2], 'low')
                low_i += 1
                # print(attackLength, len(signal_value[1]), len(signal_value), signal)
            statistics_for_current_bnss[i] = (
                suspicious_score_signals, stopAttackTime, noisesForAvgRating,
                noisesForCMR, noisesForRank)
            # print i, suspicious_score_signals.keys()
            # suspicious_score = calculateSuspiciousScore_improve(suspicious_score_signals)
            # suspicious_score = calculateSuspiciousScoreWithDifferentweight(suspicious_score_signals, [NO_OF_POSITIVE_REVIEWS, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS])
            suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
            if i not in stopAttackTime and burst == 0:
                is_burst = 1
                burst = 1
            suspicious_score_list.append([bnss_key, i, suspicious_score, i in stopAttackTime, noisesForAvgRating[i],
                                          noisesForCMR[i], noisesForRank[i], is_burst])
        print(high_i, low_i, len(high_idx), len(low_idx))

    else:
        for i in range(attackLength):
            is_burst = 0
            suspicious_score_signals = dict()
            for signal in signals_dict:
                signal_value = signals_dict[signal]
                # print(attackLength, len(signal_value[1]), len(signal_value), signal)
                suspicious_score_signals[signal] = (signal_value[0], signal_value[1][i], signal_value[2])

            statistics_for_current_bnss[i] = (
                suspicious_score_signals, stopAttackTime, noisesForAvgRating,
                noisesForCMR, noisesForRank)
            # print i, suspicious_score_signals.keys()
            # suspicious_score = calculateSuspiciousScore_improve(suspicious_score_signals)
            # suspicious_score = calculateSuspiciousScoreWithDifferentweight(suspicious_score_signals, [NO_OF_POSITIVE_REVIEWS, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS])
            suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
            if i not in stopAttackTime and burst == 0:
                is_burst = 1
                burst = 1
            suspicious_score_list.append([bnss_key, i, suspicious_score, i in stopAttackTime, noisesForAvgRating[i],
                                          noisesForCMR[i], noisesForRank[i], is_burst])


    df_for_current_bnss = DataFrame(suspicious_score_list, columns=['bnss_key', 'attack_time', 'suspicious_score',
                                                                    'is_stop_attack', 'noise_for_avg_rating',
                                                                    'noise_for_cmr',
                                                                    'noise_for_rank',
                                                                    'is_burst'])
    # print df_for_current_bnss

    if not os.path.exists(os.path.join(plotDir, 'suspicious_scores')):
        os.mkdir(os.path.join(plotDir, 'suspicious_scores'))
    if not os.path.exists(os.path.join(plotDir, 'suspicious_scores_signals')):
        os.mkdir(os.path.join(plotDir, 'suspicious_scores_signals'))

    csv = os.path.join(os.path.join(plotDir, 'suspicious_scores'), bnss_key) + '.csv'
    df_for_current_bnss.to_csv(csv)

    with open(os.path.join(os.path.join(plotDir, 'suspicious_scores_signals'), bnss_key), 'wb') as f:
        pickle.dump(statistics_for_current_bnss, f)


def OfflineDetectAnalysisWithinAttack(plotDir, bnss_key, signals_to_be_extracted, avgRatingErrorAfAttack, avgRatingAfAttack,
                          noOfReviewsAfAttack, ratingEntropyAfAttack, ratingDivergenceAfAttack, stopAttackTime,
                          noOfPositiveReviewsAfAttack, noiseForAvgRating, noisesForAvgRatingAfDetection, noisesForCMR,
                          noisesForCMRAfDetection, noisesForRank, noisesForRankAfDetection, statistics_for_all_bnsses, statistics_for_all_measures,
                                      *args):

    signals_dict = dict()

    if DEVIATION_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating_error_for_all_bnsses = np.append(statistics_for_all_bnsses[DEVIATION_OF_AVG_RATING],
                                                    avgRatingErrorAfAttack)
        signals_dict[DEVIATION_OF_AVG_RATING] = (avg_rating_error_for_all_bnsses, avgRatingErrorAfAttack, 'H')

    if DIFFERENCE_OF_AVG_RATING in signals_to_be_extracted:
        if args:
            avg_rating = args[1][bnss_key][DIFFERENCE_OF_AVG_RATING]
        else:
            avg_rating = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_AVG_RATING]
        # avg_rating = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_AVG_RATING]
        avg_rating_for_current_bnss = np.append(avg_rating, avgRatingAfAttack)
        avg_rating_diff_for_current_bnss = np.diff(avg_rating_for_current_bnss)
        avg_rating_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_AVG_RATING],
                                                   avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):])
        signals_dict[DIFFERENCE_OF_AVG_RATING] = (avg_rating_diff_for_all_bnsses,
                                                  avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):], 'H')

    if NO_OF_REVIEWS in signals_to_be_extracted:
        if args:
            no_of_reviews_for_all_bnsses = np.append(args[0][NO_OF_REVIEWS], noOfReviewsAfAttack)
        else:
            no_of_reviews_for_all_bnsses = np.append(statistics_for_all_bnsses[NO_OF_REVIEWS], noOfReviewsAfAttack)
        # print "defender length in no of reivews:", len(no_of_reviews_for_all_bnsses)
        signals_dict[NO_OF_REVIEWS] = (no_of_reviews_for_all_bnsses, noOfReviewsAfAttack, 'H')

    if DIFFERENCE_OF_NO_OF_REVIEWS in signals_to_be_extracted:
        if args:
            no_of_reviews_bf_attack = args[1][bnss_key][DIFFERENCE_OF_NO_OF_REVIEWS]
        else:
            no_of_reviews_bf_attack = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_NO_OF_REVIEWS]
        # no_of_reviews_bf_attack = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_NO_OF_REVIEWS]
        no_of_reviews_for_current_bnss = np.append(no_of_reviews_bf_attack, noOfReviewsAfAttack)
        no_of_reviews_diff_for_current_bnss = np.diff(no_of_reviews_for_current_bnss)
        no_of_reviews_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_NO_OF_REVIEWS],
                                                      no_of_reviews_diff_for_current_bnss[-len(noOfReviewsAfAttack):])
        signals_dict[DIFFERENCE_OF_NO_OF_REVIEWS] = (no_of_reviews_diff_for_all_bnsses,
                                                     no_of_reviews_diff_for_current_bnss[-len(noOfReviewsAfAttack):],'H')

    if RATING_ENTROPY in signals_to_be_extracted:
        if args:
            rating_entropy_for_all_bnsses = np.append(args[0][RATING_ENTROPY], ratingEntropyAfAttack)
        else:
            rating_entropy_for_all_bnsses = np.append(statistics_for_all_bnsses[RATING_ENTROPY], ratingEntropyAfAttack)
        # rating_entropy_for_all_bnsses = np.append(statistics_for_all_bnsses[RATING_ENTROPY], ratingEntropyAfAttack)
        # print "defender length in RATING ENTROPY:", len(rating_entropy_for_all_bnsses)
        signals_dict[RATING_ENTROPY] = (rating_entropy_for_all_bnsses, ratingEntropyAfAttack, 'L')

    if DIFFERENCE_OF_RATING_ENTROPY in signals_to_be_extracted:
        if args:
            rating_entropy_bf_attack = args[1][bnss_key][DIFFERENCE_OF_RATING_ENTROPY]
        else:
            rating_entropy_bf_attack = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_RATING_ENTROPY]
        # rating_entropy_bf_attack = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_RATING_ENTROPY]
        rating_entropy_for_current_bnss = np.append(rating_entropy_bf_attack, ratingEntropyAfAttack)
        rating_entropy_diff_for_current_bnss = - np.diff(rating_entropy_for_current_bnss)
        rating_entropy_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_RATING_ENTROPY],
                                                       rating_entropy_diff_for_current_bnss[-len(ratingEntropyAfAttack):])
        signals_dict[DIFFERENCE_OF_RATING_ENTROPY] = (rating_entropy_diff_for_all_bnsses,
                                                      rating_entropy_diff_for_current_bnss[-len(ratingEntropyAfAttack):], 'H')

    if KL_DIVERGENCE in signals_to_be_extracted:
        if args:
            KL_divergence_for_all_bnsses = np.append(args[0][KL_DIVERGENCE], ratingDivergenceAfAttack)
        else:
            KL_divergence_for_all_bnsses = np.append(statistics_for_all_bnsses[KL_DIVERGENCE], ratingDivergenceAfAttack)
        # KL_divergence_for_all_bnsses = np.append(statistics_for_all_bnsses[KL_DIVERGENCE], ratingDivergenceAfAttack)
        signals_dict[KL_DIVERGENCE] = (KL_divergence_for_all_bnsses, ratingDivergenceAfAttack, 'H')

    if NO_OF_POSITIVE_REVIEWS in signals_to_be_extracted:
        if args:
            no_of_positive_reviews_for_all_bnsses= np.append(args[0][NO_OF_POSITIVE_REVIEWS], noOfPositiveReviewsAfAttack)
        else:
            no_of_positive_reviews_for_all_bnsses = np.append(statistics_for_all_bnsses[NO_OF_POSITIVE_REVIEWS],
                                                              noOfPositiveReviewsAfAttack)
        # no_of_positive_reviews_for_all_bnsses = np.append(statistics_for_all_bnsses[NO_OF_POSITIVE_REVIEWS],noOfPositiveReviewsAfAttack)
        signals_dict[NO_OF_POSITIVE_REVIEWS] = (no_of_positive_reviews_for_all_bnsses, noOfPositiveReviewsAfAttack, 'H')

    if DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS in signals_to_be_extracted:
        if args:
            no_of_positive_reviews_bf_attack = args[1][bnss_key][NO_OF_POSITIVE_REVIEWS]
        else:
            no_of_positive_reviews_bf_attack = statistics_for_all_measures[bnss_key][NO_OF_POSITIVE_REVIEWS]
        # no_of_positive_reviews_bf_attack = statistics_for_all_measures[bnss_key][NO_OF_POSITIVE_REVIEWS]
        no_of_positive_reviews_for_current_bnss = np.append(no_of_positive_reviews_bf_attack,
                                                            noOfPositiveReviewsAfAttack)
        no_of_positive_reviews_diff_for_current_bnss = np.diff(no_of_positive_reviews_for_current_bnss)
        no_of_positive_reviews_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS],
                                                               no_of_positive_reviews_diff_for_current_bnss[-len(noOfPositiveReviewsAfAttack):])
        signals_dict[DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS] = (no_of_positive_reviews_diff_for_all_bnsses,
                                                              no_of_positive_reviews_diff_for_current_bnss[-len(noOfPositiveReviewsAfAttack):], 'H')

    suspicious_score_list = list()
    attackLength = len(avgRatingErrorAfAttack)
    burst = 0
    statistics_for_current_bnss = dict()
    for i in range(attackLength):
        is_burst = 0
        suspicious_score_signals = dict()
        for signal in signals_dict:
            signal_value = signals_dict[signal]
            suspicious_score_signals[signal] = (signal_value[0], signal_value[1][i], signal_value[2])

        statistics_for_current_bnss[i] = (suspicious_score_signals, stopAttackTime, noiseForAvgRating, noisesForAvgRatingAfDetection,
                                          noisesForCMR, noisesForCMRAfDetection, noisesForRank, noisesForRankAfDetection,)
        # print i, suspicious_score_signals.keys()
        # suspicious_score = calculateSuspiciousScore_improve(suspicious_score_signals)
        # suspicious_score = calculateSuspiciousScoreWithDifferentweight(suspicious_score_signals, [NO_OF_POSITIVE_REVIEWS, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS])
        suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
        if i not in stopAttackTime and burst == 0:
            is_burst = 1
            burst = 1
        suspicious_score_list.append([bnss_key, i, suspicious_score, i in stopAttackTime, noiseForAvgRating[i],
                                      noisesForAvgRatingAfDetection[i], noisesForCMR[i],
                          noisesForCMRAfDetection[i], noisesForRank[i], noisesForRankAfDetection[i], is_burst])

    df_for_current_bnss = DataFrame(suspicious_score_list, columns=['bnss_key', 'attack_time', 'suspicious_score',
                                                                    'is_stop_attack', 'noise_for_avg_rating', 'noise_for_car_af_detection',
                                                                    'noise_for_cmr', 'noise_for_cmr_af_detection', 'noise_for_rank',
                                                                    'noise_for_rank_af_detection', 'is_burst'])
    # print df_for_current_bnss

    if not os.path.exists(os.path.join(plotDir, 'suspicious_scores')):
        os.mkdir(os.path.join(plotDir, 'suspicious_scores'))
    if not os.path.exists(os.path.join(plotDir, 'suspicious_scores_signals')):
        os.mkdir(os.path.join(plotDir, 'suspicious_scores_signals'))

    csv = os.path.join(os.path.join(plotDir, 'suspicious_scores'), bnss_key) + '.csv'
    df_for_current_bnss.to_csv(csv)

    with open(os.path.join(os.path.join(plotDir, 'suspicious_scores_signals'), bnss_key), 'w') as f:
        pickle.dump(statistics_for_current_bnss, f)


def calculatePerformanceMetricsForDiffDetectors(plotDir, top, case):
    signals_to_be_extracted = [DEVIATION_OF_AVG_RATING, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS,
                               DIFFERENCE_OF_RATING_ENTROPY, NO_OF_REVIEWS, RATING_ENTROPY,
                               NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]
    metrics = list()
    metrics_pre = list()
    index = list()
    for i in signals_to_be_extracted:
        index.append(i)
        count_for_FP, all_timeKeys, count_for_stop_attack, all_attack_times, \
        credit_mean, credit_af_detection_mean, credit_cmr, credit_cmr_af_detection, \
        rank, rank_af_detection, count_for_all_bursts, count_for_success_of_detection_of_burst = \
            calculteSuspiciousScoreForMultipleBnssAndRank(plotDir, calculateSuspiciousScoreWithDifferentweight, top,
                                                          [i])
        count_for_success_of_detection = all_timeKeys - count_for_FP
        attackTimes = all_attack_times - count_for_stop_attack
        TP = float(count_for_success_of_detection) / all_timeKeys

        successful_rate_of_attack = float(attackTimes - count_for_success_of_detection) / all_attack_times

        times_of_attack = float(attackTimes) / all_attack_times
        if count_for_all_bursts == 0:
            burst_ratio_of_all = 0
        else:
            burst_ratio_of_all = float(count_for_success_of_detection_of_burst) / count_for_all_bursts
        if attackTimes == 0:
            successful_rate_in_attack_times = 0
            FN = 0
            burst_ratio_of_detection = 0
        else:
            successful_rate_in_attack_times = float(attackTimes - count_for_success_of_detection) / attackTimes
            FN = float(attackTimes - count_for_success_of_detection) / attackTimes  # 攻击的窗口中有多少被认为是没有攻击的
            burst_ratio_of_detection = float(count_for_success_of_detection_of_burst) / attackTimes

        F1 = 2.0 / (1.0 / TP + 1.0 / (1 - FN))
        metrics.append(
            [TP, FN, F1, successful_rate_of_attack, credit_mean, credit_af_detection_mean,
             credit_cmr, credit_cmr_af_detection, rank, rank_af_detection, times_of_attack,
             burst_ratio_of_detection, successful_rate_in_attack_times, burst_ratio_of_all])
        metrics_pre.append([count_for_success_of_detection, all_timeKeys, attackTimes,
                            all_attack_times, count_for_success_of_detection_of_burst, count_for_all_bursts])
    index.append('All')
    count_for_FP, all_timeKeys, count_for_stop_attack, all_attack_times, \
    credit_mean, credit_af_detection_mean, credit_cmr, credit_cmr_af_detection, \
    rank, rank_af_detection, count_for_all_bursts, count_for_success_of_detection_of_burst = \
        calculteSuspiciousScoreForMultipleBnssAndRank(plotDir, calculateSuspiciousScore, top, [])
    count_for_success_of_detection = all_timeKeys - count_for_FP
    attackTimes = all_attack_times - count_for_stop_attack
    TP = float(count_for_success_of_detection) / all_timeKeys

    successful_rate_of_attack = float(attackTimes - count_for_success_of_detection) / all_attack_times

    times_of_attack = float(attackTimes) / all_attack_times
    if count_for_all_bursts == 0:
        burst_ratio_of_all = 0
    else:
        burst_ratio_of_all = float(count_for_success_of_detection_of_burst) / count_for_all_bursts
    if attackTimes == 0:
        successful_rate_in_attack_times = 0
        FN = 0
        burst_ratio_of_detection = 0
    else:
        successful_rate_in_attack_times = float(attackTimes - count_for_success_of_detection) / attackTimes
        FN = float(attackTimes - count_for_success_of_detection) / attackTimes  # 攻击的窗口中有多少被认为是没有攻击的
        burst_ratio_of_detection = float(count_for_success_of_detection_of_burst) / attackTimes

    F1 = 2.0 / (1.0 / TP + 1.0 / (1 - FN))
    print  TP, FN, F1, successful_rate_of_attack, credit_mean, credit_af_detection_mean, \
        times_of_attack, \
        burst_ratio_of_detection, successful_rate_in_attack_times, burst_ratio_of_all
    metrics.append([TP, FN, F1, successful_rate_of_attack, credit_mean, credit_af_detection_mean,
             credit_cmr, credit_cmr_af_detection, rank, rank_af_detection, times_of_attack,
             burst_ratio_of_detection, successful_rate_in_attack_times, burst_ratio_of_all])
    metrics_pre.append([count_for_success_of_detection, all_timeKeys, attackTimes,
                        all_attack_times, count_for_success_of_detection_of_burst, count_for_all_bursts])

    df = DataFrame(metrics, columns=['TP', 'FN', 'F1', 'successful_rate_of_attack', 'credit', 'credit_af_detection',
                                     'credit_for_car', 'credit_for_car_af_detection', 'credit_for_rank', 'credit_for_rank_af_detection',
                                     'rate_of_times_of_attack', 'burst_ratio_of_detection', 'successful_rate_in_attack_times',
                                     'burst_ratio_of_all'], index=index)

    df_pre = DataFrame(metrics_pre, columns = ['count for TP', 'all_times_of_detection', 'times_of_attack',
                                               'all_times_of_attack', 'count_for_success_of_detection_of_burst', 'count_for_all_bursts']\
                       ,index=index)

    print df
    df.to_csv(os.path.join(plotDir, 'case_%s' % case)+'.csv')
    print df_pre
    df.to_csv(os.path.join(plotDir, 'case_%s' % case) + '_pre.csv')


def generateMetric(plotDir, df, case, min_removal_rate=0.2, max_removal_rate=0.8, is_random=False, new=False):
    if is_random:
        suspDir = os.path.join(os.path.join(plotDir, os.pardir, 'susp_stats', 'random'))
    else:
        suspDir = os.path.join(os.path.join(plotDir, os.pardir, 'susp_stats', 'case_%s' % case))
    print suspDir
    suspicious_scores_list = df['suspicious_score']
    # print suspicious_scores_list
    df_cmr = pd.read_csv(os.path.join(plotDir, 'bnss_stats', 'CMR.csv'), index_col=0)
    min_susp = min(suspicious_scores_list)
    max_susp = max(suspicious_scores_list)
    new_list = list()
    y_true = np.array([1 - df['is_stop_attack']])
    y_score = np.array([df['suspicious_score']])
    ranking_loss = label_ranking_loss(y_true, y_score)

    for tup in df.itertuples():
        ls = list(tup)
        bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, noiseForAvgRatingAfDetection, \
        noiseForCMR, noiseForCMRAfDetection, noiseForRank, noiseForRankAfDetection, is_burst = ls[1:]
        attack_stats = pickle.load(open(os.path.join(suspDir, 'attack_stats', bnss_key)))
        ratingForAttackDict = attack_stats['rating for attack']
        ratingDistributionBfAttack = attack_stats['rating distribution bf attack']
        ratingDistributionAfAttack = attack_stats['rating distribution af attack']
        time_key = attack_stats['time key to start attack'] + i
        stars = range(1, 6)
        cumulativeRatingBfAttack = list()
        ratingAfAttack = list()
        for key in ratingDistributionBfAttack:
            cumulative_rating = ratingDistributionBfAttack[key][time_key]
            current_rating = ratingDistributionBfAttack[key][time_key] - ratingDistributionBfAttack[key][time_key - 1]
            ratingAfAttack.append(
                ratingDistributionAfAttack[key][time_key] - ratingDistributionAfAttack[key][time_key - 1])
            cumulativeRatingBfAttack.append(cumulative_rating)

        if i in ratingForAttackDict:
            ratingForAttack = ratingForAttackDict[i]
            truthfulRating = np.array(ratingAfAttack) - ratingForAttack
            if sum(truthfulRating) == 0:
                CMRBfAttack = 0.0
            else:
                CMRBfAttack = float(sum([truthfulRating[k] * stars[k] for k in range(len(stars))])) / sum(
                    truthfulRating)
            cumulativeRatingAfAttack = np.array(cumulativeRatingBfAttack) + np.array(ratingForAttack)
            CARAfAttack = float(sum([cumulativeRatingAfAttack[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingAfAttack)
            CARBfAttack = float(sum([cumulativeRatingBfAttack[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingBfAttack)
            # print CARAfAttack, attack_stats['CAR af attack'][i], CARAfAttack-CARBfAttack, attack_stats['noises for CAR'][i]
            CMRForBnsses = df_cmr[str(time_key)]
            rank_series = CMRForBnsses.rank(method='min', ascending=False)
            rankBfAttack, CMRBfAttackFromdf = rank_series[bnss_key], CMRForBnsses[bnss_key]
            choice_list = list()
            removal_rate = min_removal_rate + (suspicious_score - min_susp) / (max_susp - min_susp) * \
                                              (max_removal_rate - min_removal_rate)
            # print "suspicious_score, removal_rate", suspicious_score, removal_rate
            removal_num = int(removal_rate * sum(ratingForAttack))
            # print i, bnss_key, suspicious_score, removal_rate, removal_num
            removal_list = list()
            removedRating = list()
            if len(ratingForAttack) != 5:
                raise ValueError
            for star in stars:
                choice_list = choice_list + [star for num in range(int(ratingForAttack[star - 1]))]
            for sample_time in range(removal_num):
                removal_list.append(np.random.choice(choice_list))
            for star in stars:
                removedRating.append(removal_list.count(star))

            removedPositiveRatio = float(removedRating[3] + removedRating[4]) / (
            ratingForAttack[3] + ratingForAttack[4])
            ratingAfDetection = np.array(ratingForAttack) - np.array(removedRating) + np.array(truthfulRating)
            cumulativeRatingAfDetection = np.array(ratingForAttack) - np.array(removedRating) + np.array(
                cumulativeRatingBfAttack)
            CMRAfDetection = float(sum([ratingAfDetection[k] * stars[k] for k in range(len(stars))])) / sum(
                ratingAfDetection)
            CARAfDetection = float(sum([cumulativeRatingAfDetection[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingAfDetection)
            credit_for_CMR_af_detection = float(CMRAfDetection - CMRBfAttack) / sum(ratingForAttack)
            CMRForBnsses[bnss_key] = CMRAfDetection
            rankAfDetection = CMRForBnsses.rank(method='min', ascending=False)[bnss_key]
            credit_for_rank_af_detection = float(rankBfAttack - rankAfDetection) / sum(ratingForAttack)
            credit_for_CAR_af_detection = float(CARAfDetection - CARBfAttack) / sum(ratingForAttack)
            if new:
                noiseForCMR = float(noiseForCMR) / sum(ratingForAttack)
                noiseForAvgRating = float(noiseForAvgRating) / sum(ratingForAttack)
                noiseForRank = float(noiseForRank) / sum(ratingForAttack)
            new_list.append(
                [bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, credit_for_CAR_af_detection, \
                 noiseForCMR, credit_for_CMR_af_detection, noiseForRank, credit_for_rank_af_detection, is_burst,
                 removedPositiveRatio, sum(ratingForAttack), sum(removedRating),
                 ratingForAttack[3] + ratingForAttack[4], removedRating[3] + removedRating[4]])
        else:
            # print "noiseForCMR:", noiseForCMR
            removedPositiveRatio = 1.0
            new_list.append([bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, noiseForAvgRating,
                             noiseForCMR, noiseForCMR, noiseForRank, noiseForRank, is_burst, removedPositiveRatio, 0.0,
                             0.0, 0.0, 0.0])
    columns = ['bnss_key', 'attack_time', 'suspicious_score', 'is_stop_attack', 'noise_for_avg_rating',
               'noise_for_car_af_detection',
               'noise_for_cmr', 'noise_for_cmr_af_detection',
               'noise_for_rank',
               'noise_for_rank_af_detection',
               'is_burst', 'defender utility',
               'no_of_spam', 'removed_reviews',
               'no_of_+_spam', 'removed_+_spam']
    new_df = DataFrame(new_list, columns=columns)

    attack_times = len(new_df) - sum(new_df['is_stop_attack'])
    avg_noise_for_cmr_af_detection = new_df['noise_for_cmr_af_detection'].mean()
    avg_defender_utility = new_df['defender utility'].mean()
    attack_rate = float(attack_times) / len(new_df)
    avg_noise_for_car_af_detection = new_df['noise_for_car_af_detection'].mean()
    avg_noise_for_car = new_df['noise_for_avg_rating'].mean()
    avg_noise_for_cmr = new_df['noise_for_cmr'].mean()
    avg_no_of_spam = new_df['no_of_spam'].mean()
    avg_no_of_positive_spam = new_df['no_of_+_spam'].mean()
    avg_removed_reviews = new_df['removed_reviews'].mean()
    avg_removed_positive_reviews = new_df['removed_+_spam'].mean()
    avg_noise_for_rank = new_df['noise_for_rank'].mean()
    avg_noise_for_rank_af_detection = new_df['noise_for_rank_af_detection'].mean()
    return avg_defender_utility, avg_noise_for_car, avg_noise_for_car_af_detection, avg_noise_for_cmr, \
        avg_noise_for_cmr_af_detection, avg_noise_for_rank, avg_noise_for_rank_af_detection, \
        attack_rate, avg_no_of_spam, avg_removed_reviews, avg_no_of_positive_spam, avg_removed_positive_reviews, \
        ranking_loss

def generateTestDfForEachCase(suspDir_pre, case, signals):
    df_dict = dict()
    df_score = DataFrame()
    test_df = dict()
    for sig in signals:
        df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                   index_col=0)
        df_score[sig] = df_dict[sig]['suspicious_score']
        test_df[sig] = pd.concat([pd.concat([df_dict[sig][2::5], df_dict[sig][3::5]]), df_dict[sig][4::5]])
    max_list = list()
    print df_score.head()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']
    df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre.csv' % case))
    df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_random.csv' % case))
    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    df_dict['Logit'] = df_dict['All'].copy(True)
    df_dict['Max'] = df_dict['All'].copy(True)
    df_dict['Max']['suspcious_score'] = max_list

    print df_score.head()
    train = pd.concat([df_score[::5], df_score[1::5]])
    test = pd.concat([df_score[2::5], df_score[3::5]])
    test = pd.concat([test, df_score[4::5]])
    model = LogisticRegression('l1')
    model.fit(train.iloc[:, :9], train.iloc[:, 9])
    score = model.score(test.iloc[:, :9], test.iloc[:, 9])
    proba = list()

    for tup in model.predict_proba(test.iloc[:, :9]):
        proba.append(tup[0])
    # print(proba)
    loss_1 = label_ranking_loss([1 - test.iloc[:, 9]], [proba])
    loss_2 = label_ranking_loss([1 - test.iloc[:, 9]], [test[RATING_ENTROPY]])
    loss_3 = label_ranking_loss([1 - test.iloc[:, 9]], [test['Max']])
    loss_4 = label_ranking_loss([1 - test.iloc[:, 9]], [test[NO_OF_REVIEWS]])
    print score, loss_1, loss_2, loss_3, loss_4

    new_dict = {}
    for defense in df_dict:
        if defense != 'Max':
            continue
        print('test if run')
        df_dict[defense] = df_dict[defense][['bnss_key', 'attack_time', 'suspicious_score', 'is_stop_attack', 'noise_for_avg_rating',
               'noise_for_car_af_detection',
               'noise_for_cmr', 'noise_for_cmr_af_detection',
               'noise_for_rank',
               'noise_for_rank_af_detection',
               'is_burst']]
        new_dict[defense] = pd.concat([df_dict[defense][2::5], df_dict[defense][3::5]])
        new_dict[defense] = pd.concat([new_dict[defense], df_dict[defense][4::5]])
        if defense == 'Logit':
            print(len(new_dict[defense]), len(proba))
            assert len(new_dict[defense]) == len(proba)
            new_dict[defense]['suspicious_score'] = proba
        new_dict[defense].to_csv(os.path.join(suspDir_pre, 'case_%s' % case, 'case_%s_post_%s.csv' % (case, defense)))

def calculateUtilityOfDifferentDetectorsV2(plotDir, case, first=False, min_removal_rate=0.2, max_removal_rate=0.8):
    signals = [DEVIATION_OF_AVG_RATING, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS,
                               DIFFERENCE_OF_RATING_ENTROPY, NO_OF_REVIEWS, RATING_ENTROPY,
                               NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]
    suspDir_pre = os.path.join(os.path.join(plotDir, os.pardir, 'susp_stats'))
    if first:
        generateTestDfForEachCase(suspDir_pre, case, signals)

    metrics = list()
    index = list()
    columns = ['avg_d_util', 'avg_noise_for_car', 'avg_noise_for_car_af_detect', 'avg_noise_for_cmr',
               'avg_noise_for_cmr_af_detect', 'avg_noise_for_rank', 'avg_noise_for_rank_af_detect',
               'attack_rate', 'avg_no_of_spam', 'avg_removed_reviews',
               'avg_no_of_positive_spam', 'avg_removed_positive_reviews', 'ranking_loss']

    signals.append('Max')
    signals.append('Logit')
    signals.append('All')
    signals.append('Random')

    for sig in signals:
        print(sig)
        post_df = pd.read_csv(os.path.join(suspDir_pre, 'case_%s' % case, 'case_%s_post_%s.csv' % (case, sig)), index_col=0)
        metrics.append(generateMetric(plotDir, post_df, case))

    df = DataFrame(metrics, columns=columns, index=signals)
    print(df.head())
    df.to_csv(os.path.join(suspDir_pre, 'case_%s' % case, 'case_%s_post_V2.csv' % case))


def earlyDefenderUtil(plotDir, detect_function, case, signal_with_min_weight=[], min_removal_rate=0.2, max_removal_rate=0.8,
                      log=False, susp_stats='susp_stats'):
    if len(signal_with_min_weight) == 1:
        assert detect_function == calculateSuspiciousScoreWithDifferentweight, \
            "detection funtion has to use different weights"

    suspicious_score_list = list()
    bnssKeys = list()
    suspDir = os.path.join(plotDir, os.pardir, susp_stats, 'early_stage_%s' % case)
    for root, dirs, files in os.walk(os.path.join(suspDir, 'suspicious_scores_signals')):
        for name in files:
            bnssKeys.append(name)

    for bnss_key in bnssKeys:
        statistics_for_current_bnss = pickle.load(
            open(os.path.join(os.path.join(suspDir, 'suspicious_scores_signals'), bnss_key)))
        burst = 0
        for i in statistics_for_current_bnss:
            is_burst = 0
            suspicious_score_signals, stopAttackTime, noiseForAvgRating,\
            noiseForCMR, noiseForRank = statistics_for_current_bnss[i]

            if detect_function == calculateSuspiciousScore:
                suspicious_score = detect_function(suspicious_score_signals.values())
            elif detect_function == calculateSuspiciousScoreWithRandomWeight:
                suspicious_score, weight = detect_function(suspicious_score_signals)
            elif detect_function == calculateSuspiciousScoreWithDifferentweight:
                suspicious_score, weight = detect_function(suspicious_score_signals, signal_with_min_weight)
            if i not in stopAttackTime and burst == 0:
                burst = 1
                is_burst = 1
            # print "suspicious score:", suspicious_score
            suspicious_score_list.append(
                [bnss_key, i, suspicious_score, i in stopAttackTime, noiseForAvgRating[i],
                 noiseForCMR[i], noiseForRank[i], is_burst])

    df = DataFrame(suspicious_score_list, columns=['bnss_key', 'attack_time', 'suspicious_score',
                                                   'is_stop_attack', 'noise_for_avg_rating',
                                                   'noise_for_cmr',
                                                   'noise_for_rank',
                                                   'is_burst'])
    suspicious_scores_list = df['suspicious_score']
    # print suspicious_scores_list
    df_cmr = pd.read_csv(os.path.join(plotDir, 'bnss_stats', 'CMR.csv'), index_col=0)
    min_susp = min(suspicious_scores_list)
    max_susp = max(suspicious_scores_list)
    new_list = list()
    y_true = np.array([1 - df['is_stop_attack']])
    y_score = np.array([df['suspicious_score']])
    ranking_loss = label_ranking_loss(y_true, y_score)

    for tup in df.itertuples():
        ls = list(tup)
        bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, \
        noiseForCMR, noiseForRank, is_burst = ls[1:]
        attack_stats = pickle.load(open(os.path.join(suspDir, 'attack_stats', bnss_key)))
        ratingForAttackDict = attack_stats['rating for attack']
        ratingDistributionBfAttack = attack_stats['rating distribution bf attack']
        ratingDistributionAfAttack = attack_stats['rating distribution af attack']
        time_key = attack_stats['time key to start attack'] + i
        stars = range(1, 6)
        cumulativeRatingBfAttack = list()
        ratingAfAttack = list()
        for key in ratingDistributionBfAttack:
            cumulative_rating = ratingDistributionBfAttack[key][time_key]
            current_rating = ratingDistributionBfAttack[key][time_key] - ratingDistributionBfAttack[key][
                time_key - 1]
            ratingAfAttack.append(
                ratingDistributionAfAttack[key][time_key] - ratingDistributionAfAttack[key][time_key - 1])
            cumulativeRatingBfAttack.append(cumulative_rating)

        if i in ratingForAttackDict:
            ratingForAttack = ratingForAttackDict[i]
            truthfulRating = np.array(ratingAfAttack) - ratingForAttack
            if sum(truthfulRating) == 0:
                CMRBfAttack = 0.0
            else:
                CMRBfAttack = float(sum([truthfulRating[k] * stars[k] for k in range(len(stars))])) / sum(
                    truthfulRating)
            cumulativeRatingAfAttack = np.array(cumulativeRatingBfAttack) + np.array(ratingForAttack)
            CARAfAttack = float(sum([cumulativeRatingAfAttack[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingAfAttack)
            CARBfAttack = float(sum([cumulativeRatingBfAttack[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingBfAttack)
            # print CARAfAttack, attack_stats['CAR af attack'][i], CARAfAttack-CARBfAttack, attack_stats['noises for CAR'][i]
            CMRForBnsses = df_cmr[str(time_key)]
            rank_series = CMRForBnsses.rank(method='min', ascending=False)
            rankBfAttack, CMRBfAttackFromdf = rank_series[bnss_key], CMRForBnsses[bnss_key]
            choice_list = list()
            removal_rate = min_removal_rate + (suspicious_score - min_susp) / (max_susp - min_susp) * \
                                              (max_removal_rate - min_removal_rate)
            # print "suspicious_score, removal_rate", suspicious_score, removal_rate
            removal_num = int(removal_rate * sum(ratingForAttack))
            # print i, bnss_key, suspicious_score, removal_rate, removal_num
            removal_list = list()
            removedRating = list()
            if len(ratingForAttack) != 5:
                raise ValueError
            for star in stars:
                choice_list = choice_list + [star for num in range(int(ratingForAttack[star - 1]))]
            for sample_time in range(removal_num):
                removal_list.append(np.random.choice(choice_list))
            for star in stars:
                removedRating.append(removal_list.count(star))

            removedPositiveRatio = float(removedRating[3] + removedRating[4]) / (
                ratingForAttack[3] + ratingForAttack[4])
            ratingAfDetection = np.array(ratingForAttack) - np.array(removedRating) + np.array(truthfulRating)
            cumulativeRatingAfDetection = np.array(ratingForAttack) - np.array(removedRating) + np.array(
                cumulativeRatingBfAttack)
            CMRAfDetection = float(sum([ratingAfDetection[k] * stars[k] for k in range(len(stars))])) / sum(
                ratingAfDetection)
            CARAfDetection = float(
                sum([cumulativeRatingAfDetection[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingAfDetection)
            credit_for_CMR_af_detection = float(CMRAfDetection - CMRBfAttack) / sum(ratingForAttack)
            CMRForBnsses[bnss_key] = CMRAfDetection
            rankAfDetection = CMRForBnsses.rank(method='min', ascending=False)[bnss_key]
            credit_for_rank_af_detection = float(rankBfAttack - rankAfDetection) / sum(ratingForAttack)
            credit_for_CAR_af_detection = float(CARAfDetection - CARBfAttack) / sum(ratingForAttack)
            noiseForCMR = float(noiseForCMR) / sum(ratingForAttack)
            noiseForAvgRating = float(noiseForAvgRating) / sum(ratingForAttack)
            noiseForRank = float(noiseForRank) / sum(ratingForAttack)
            new_list.append(
                [bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, credit_for_CAR_af_detection, \
                 noiseForCMR, credit_for_CMR_af_detection, noiseForRank, credit_for_rank_af_detection, is_burst,
                 removedPositiveRatio, sum(ratingForAttack), sum(removedRating),
                 ratingForAttack[3] + ratingForAttack[4], removedRating[3] + removedRating[4]])
        else:
            # print "noiseForCMR:", noiseForCMR
            removedPositiveRatio = 1.0
            new_list.append([bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, noiseForAvgRating,
                             noiseForCMR, noiseForCMR, noiseForRank, noiseForRank, is_burst, removedPositiveRatio,
                             0.0, 0.0, 0.0, 0.0])


    columns = ['bnss_key', 'attack_time', 'suspicious_score', 'is_stop_attack', 'noise_for_avg_rating',
               'noise_for_car_af_detection',
               'noise_for_cmr', 'noise_for_cmr_af_detection',
               'noise_for_rank',
               'noise_for_rank_af_detection',
               'is_burst', 'defender utility',
               'no_of_spam', 'removed_reviews',
               'no_of_+_spam', 'removed_+_spam']
    new_df = DataFrame(new_list, columns=columns)
    print(len(new_list))

    if not os.path.exists(os.path.join(suspDir, os.pardir, 'results')):
        os.mkdir(os.path.join(suspDir, os.pardir, 'results'))

    if log:
        if len(signal_with_min_weight) == 1:
            new_df.to_csv(os.path.join(suspDir, os.pardir, 'results',
                                       'early_%s_pre_%s.csv' % (case, signal_with_min_weight[0])))
        else:
            if detect_function == calculateSuspiciousScore:
                new_df.to_csv(os.path.join(suspDir, os.pardir, 'results', 'early_%s_pre.csv' % case))
            elif detect_function == calculateSuspiciousScoreWithRandomWeight:
                new_df.to_csv(os.path.join(suspDir, os.pardir, 'results', 'early_%s_pre_random.csv' % case))

    attack_times = len(new_df) - sum(new_df['is_stop_attack'])
    avg_noise_for_cmr_af_detection = new_df['noise_for_cmr_af_detection'].mean()
    avg_defender_utility = new_df['defender utility'].mean()
    attack_rate = float(attack_times) / len(new_df)
    avg_noise_for_car_af_detection = new_df['noise_for_car_af_detection'].mean()
    avg_noise_for_car = new_df['noise_for_avg_rating'].mean()
    avg_noise_for_cmr = new_df['noise_for_cmr'].mean()
    avg_no_of_spam = new_df['no_of_spam'].mean()
    avg_no_of_positive_spam = new_df['no_of_+_spam'].mean()
    avg_removed_reviews = new_df['removed_reviews'].mean()
    avg_removed_positive_reviews = new_df['removed_+_spam'].mean()
    avg_noise_for_rank = new_df['noise_for_rank'].mean()
    avg_noise_for_rank_af_detection = new_df['noise_for_rank_af_detection'].mean()
    print avg_defender_utility, avg_noise_for_car, avg_noise_for_car_af_detection, avg_noise_for_cmr, \
        avg_noise_for_cmr_af_detection, avg_noise_for_rank, avg_noise_for_rank_af_detection, \
        attack_rate, avg_no_of_spam, avg_removed_reviews, avg_no_of_positive_spam, avg_removed_positive_reviews, \
        ranking_loss

    return avg_defender_utility, avg_noise_for_car, avg_noise_for_car_af_detection, avg_noise_for_cmr, \
           avg_noise_for_cmr_af_detection, avg_noise_for_rank, avg_noise_for_rank_af_detection, \
           attack_rate, avg_no_of_spam, avg_removed_reviews, avg_no_of_positive_spam, avg_removed_positive_reviews, \
           ranking_loss


# keys in attack_stat_for_current_bnss:
# 'CAR af attack', 'NR af attack', 'rating distribution af attack', 'stop attack', 'noises for CAR'
def newDefenderUtil(plotDir, detect_function, case, signal_with_min_weight=[], min_removal_rate=0.2, max_removal_rate=0.8, random=0, log=False, susp_stats='susp_stats'):
    suspicious_score_list = list()
    bnssKeys = list()
    if random:
        suspDir = os.path.join(plotDir, os.pardir, susp_stats, 'random_%s' % random)
    else:
        suspDir = os.path.join(plotDir, os.pardir, susp_stats, 'case_%s' % case)
    print suspDir
    for root, dirs, files in os.walk(os.path.join(suspDir, 'suspicious_scores_signals')):
        for name in files:
            bnssKeys.append(name)
    # bnssKeys = bnssKeys[:10]

    for bnss_key in bnssKeys:
        statistics_for_current_bnss = pickle.load(
            open(os.path.join(os.path.join(suspDir, 'suspicious_scores_signals'), bnss_key)))
        burst = 0
        for i in statistics_for_current_bnss:
            is_burst = 0
            suspicious_score_signals, stopAttackTime, noiseForAvgRating, noiseForAvgRatingAfDetection, \
            noiseForCMR, noiseForCMRAfDetection, noiseForRank, noiseForRankAfDetection = statistics_for_current_bnss[i]
            if detect_function == calculateSuspiciousScore:
                suspicious_score = detect_function(suspicious_score_signals.values())
            elif detect_function == calculateSuspiciousScoreWithRandomWeight:
                suspicious_score, weight = detect_function(suspicious_score_signals)
            elif detect_function == calculateSuspiciousScoreWithDifferentweight:
                suspicious_score, weight = detect_function(suspicious_score_signals, signal_with_min_weight)
            if i not in stopAttackTime and burst == 0:
                burst = 1
                is_burst = 1
            # print "suspicious score:", suspicious_score
            suspicious_score_list.append(
                [bnss_key, i, suspicious_score, i in stopAttackTime, noiseForAvgRating[i],
                 noiseForAvgRatingAfDetection[i],
                 noiseForCMR[i], noiseForCMRAfDetection[i], noiseForRank[i], noiseForRankAfDetection[i], is_burst])

    df = DataFrame(suspicious_score_list, columns=['bnss_key', 'attack_time', 'suspicious_score',
                                                                  'is_stop_attack', 'noise_for_avg_rating',
                                                                  'noise_for_car_af_detection',
                                                                  'noise_for_cmr', 'noise_for_cmr_af_detection',
                                                                  'noise_for_rank',
                                                                  'noise_for_rank_af_detection',
                                                                  'is_burst'])
    suspicious_scores_list = df['suspicious_score']
    # print suspicious_scores_list
    df_cmr = pd.read_csv(os.path.join(plotDir, 'bnss_stats', 'CMR.csv'), index_col=0)
    min_susp = min(suspicious_scores_list)
    max_susp = max(suspicious_scores_list)
    new_list = list()
    y_true = np.array([1 - df['is_stop_attack']])
    y_score = np.array([df['suspicious_score']])
    ranking_loss = label_ranking_loss(y_true, y_score)

    for tup in df.itertuples():
        ls = list(tup)
        bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, noiseForAvgRatingAfDetection,\
         noiseForCMR, noiseForCMRAfDetection, noiseForRank, noiseForRankAfDetection, is_burst = ls[1:]
        attack_stats = pickle.load(open(os.path.join(suspDir, 'attack_stats', bnss_key)))
        ratingForAttackDict = attack_stats['rating for attack']
        ratingDistributionBfAttack = attack_stats['rating distribution bf attack']
        ratingDistributionAfAttack = attack_stats['rating distribution af attack']
        time_key = attack_stats['time key to start attack'] + i
        stars = range(1, 6)
        cumulativeRatingBfAttack = list()
        ratingAfAttack = list()
        for key in ratingDistributionBfAttack:
            cumulative_rating = ratingDistributionBfAttack[key][time_key]
            current_rating = ratingDistributionBfAttack[key][time_key] - ratingDistributionBfAttack[key][time_key - 1]
            ratingAfAttack.append(ratingDistributionAfAttack[key][time_key] - ratingDistributionAfAttack[key][time_key - 1])
            cumulativeRatingBfAttack.append(cumulative_rating)

        if i in ratingForAttackDict and case != 0:
            ratingForAttack = ratingForAttackDict[i]
            truthfulRating = np.array(ratingAfAttack) - ratingForAttack
            if sum(truthfulRating) == 0:
                CMRBfAttack = 0.0
            else:
                CMRBfAttack = float(sum([truthfulRating[k] * stars[k] for k in range(len(stars))])) / sum(
                    truthfulRating)
            cumulativeRatingAfAttack = np.array(cumulativeRatingBfAttack) + np.array(ratingForAttack)
            CARAfAttack = float(sum([cumulativeRatingAfAttack[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingAfAttack)
            CARBfAttack = float(sum([cumulativeRatingBfAttack[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingBfAttack)
            # print CARAfAttack, attack_stats['CAR af attack'][i], CARAfAttack-CARBfAttack, attack_stats['noises for CAR'][i]
            CMRForBnsses = df_cmr[str(time_key)]
            rank_series = CMRForBnsses.rank(method='min', ascending=False)
            rankBfAttack, CMRBfAttackFromdf = rank_series[bnss_key], CMRForBnsses[bnss_key]
            choice_list = list()
            removal_rate = min_removal_rate + (suspicious_score - min_susp)/(max_susp - min_susp) * \
                                              (max_removal_rate - min_removal_rate)
            # print "suspicious_score, removal_rate", suspicious_score, removal_rate
            removal_num = int(removal_rate * sum(ratingForAttack))
            # print i, bnss_key, suspicious_score, removal_rate, removal_num
            removal_list = list()
            removedRating = list()
            if len(ratingForAttack)!= 5:
                raise ValueError
            for star in stars:
                choice_list = choice_list + [star for num in range(int(ratingForAttack[star - 1]))]
            for sample_time in range(removal_num):
                removal_list.append(np.random.choice(choice_list))
            for star in stars:
                removedRating.append(removal_list.count(star))

            removedPositiveRatio = float(removedRating[3] + removedRating[4]) / (ratingForAttack[3] + ratingForAttack[4])
            ratingAfDetection = np.array(ratingForAttack) - np.array(removedRating) + np.array(truthfulRating)
            cumulativeRatingAfDetection = np.array(ratingForAttack) - np.array(removedRating) + np.array(cumulativeRatingBfAttack)
            CMRAfDetection = float(sum([ratingAfDetection[k]*stars[k] for k in range(len(stars))]))/sum(ratingAfDetection)
            CARAfDetection = float(sum([cumulativeRatingAfDetection[k]*stars[k] for k in range(len(stars))]))/sum(cumulativeRatingAfDetection)
            credit_for_CMR_af_detection = float(CMRAfDetection - CMRBfAttack)/sum(ratingForAttack)
            CMRForBnsses[bnss_key] = CMRAfDetection
            rankAfDetection = CMRForBnsses.rank(method='min', ascending=False)[bnss_key]
            credit_for_rank_af_detection = float(rankBfAttack - rankAfDetection)/sum(ratingForAttack)
            credit_for_CAR_af_detection = float(CARAfDetection - CARBfAttack)/sum(ratingForAttack)
            noiseForCMR = float(noiseForCMR) / sum(ratingForAttack)
            noiseForAvgRating = float(noiseForAvgRating) / sum(ratingForAttack)
            noiseForRank = float(noiseForRank) / sum(ratingForAttack)
            new_list.append([bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, credit_for_CAR_af_detection,\
            noiseForCMR, credit_for_CMR_af_detection, noiseForRank, credit_for_rank_af_detection, is_burst, removedPositiveRatio, sum(ratingForAttack), sum(removedRating),
                             ratingForAttack[3]+ratingForAttack[4], removedRating[3]+removedRating[4]])
        else:
            # print "noiseForCMR:", noiseForCMR
            removedPositiveRatio = 1.0
            new_list.append([bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, noiseForAvgRating,
         noiseForCMR, noiseForCMR, noiseForRank, noiseForRank, is_burst, removedPositiveRatio, 0.0, 0.0, 0.0, 0.0])
    columns = ['bnss_key', 'attack_time', 'suspicious_score', 'is_stop_attack', 'noise_for_avg_rating',
                                                                  'noise_for_car_af_detection',
                                                                  'noise_for_cmr', 'noise_for_cmr_af_detection',
                                                                  'noise_for_rank',
                                                                  'noise_for_rank_af_detection',
                                                                  'is_burst', 'defender utility',
                                                                   'no_of_spam', 'removed_reviews',
                                                                    'no_of_+_spam', 'removed_+_spam']
    new_df = DataFrame(new_list, columns=columns)

    if not os.path.exists(os.path.join(suspDir, os.pardir, 'results')):
        os.mkdir(os.path.join(suspDir, os.pardir, 'results'))

    if log:
        if len(signal_with_min_weight) == 1:
            if random:
                new_df.to_csv(os.path.join(suspDir, os.pardir, 'results',
                                           'random_%s_pre_%s.csv' % (random, signal_with_min_weight[0])))
            else:
                new_df.to_csv(os.path.join(suspDir, os.pardir, 'results', 'case_%s_pre_%s.csv' % (case, signal_with_min_weight[0])))
        else:
            if detect_function == calculateSuspiciousScore:
                if random:
                    new_df.to_csv(os.path.join(suspDir, os.pardir, 'results', 'random_%s_pre.csv' % random))
                else:
                    new_df.to_csv(os.path.join(suspDir, os.pardir, 'results', 'case_%s_pre.csv' % case))
            elif detect_function == calculateSuspiciousScoreWithRandomWeight:
                if random:
                    new_df.to_csv(os.path.join(suspDir, os.pardir, 'results', 'random_%s_pre_random.csv' % random))
                else:
                    new_df.to_csv(os.path.join(suspDir, os.pardir, 'results', 'case_%s_pre_random.csv' % case))

    attack_times = len(new_df) - sum(new_df['is_stop_attack'])
    avg_noise_for_cmr_af_detection = new_df['noise_for_cmr_af_detection'].mean()
    avg_defender_utility = new_df['defender utility'].mean()
    attack_rate = float(attack_times) / len(new_df)
    avg_noise_for_car_af_detection = new_df['noise_for_car_af_detection'].mean()
    avg_noise_for_car = new_df['noise_for_avg_rating'].mean()
    avg_noise_for_cmr = new_df['noise_for_cmr'].mean()
    avg_no_of_spam = new_df['no_of_spam'].mean()
    avg_no_of_positive_spam = new_df['no_of_+_spam'].mean()
    avg_removed_reviews = new_df['removed_reviews'].mean()
    avg_removed_positive_reviews = new_df['removed_+_spam'].mean()
    avg_noise_for_rank =new_df['noise_for_rank'].mean()
    avg_noise_for_rank_af_detection = new_df['noise_for_rank_af_detection'].mean()
    print avg_defender_utility, avg_noise_for_car, avg_noise_for_car_af_detection, avg_noise_for_cmr, \
           avg_noise_for_cmr_af_detection, avg_noise_for_rank, avg_noise_for_rank_af_detection, \
           attack_rate, avg_no_of_spam, avg_removed_reviews, avg_no_of_positive_spam, avg_removed_positive_reviews, \
            ranking_loss

    return avg_defender_utility, avg_noise_for_car, avg_noise_for_car_af_detection, avg_noise_for_cmr, \
           avg_noise_for_cmr_af_detection, avg_noise_for_rank, avg_noise_for_rank_af_detection, \
           attack_rate, avg_no_of_spam, avg_removed_reviews, avg_no_of_positive_spam, avg_removed_positive_reviews, \
           ranking_loss


def calculteSuspiciousScoreForMultipleBnssAndRank(plotDir, detect_function, top, signal_with_min_weight=[]):
    suspicious_score_list = list()
    bnssKeys = list()
    for root, dirs, files in os.walk(os.path.join(plotDir, 'suspicious_scores_signals')):
        for name in files:
            bnssKeys.append(name)

    for bnss_key in bnssKeys:
        statistics_for_current_bnss = pickle.load(open(os.path.join(os.path.join(plotDir, 'suspicious_scores_signals'), bnss_key)))
        burst = 0
        for i in statistics_for_current_bnss:
            is_burst = 0
            suspicious_score_signals, stopAttackTime, noiseForAvgRating, noiseForAvgRatingAfDetection,\
                noiseForCMR, noiseForCMRAfDetection, noiseForRank, noiseForRankAfDetection= statistics_for_current_bnss[i]
            if detect_function == calculateSuspiciousScore:
                suspicious_score = detect_function(suspicious_score_signals.values())
            elif detect_function == calculateSuspiciousScoreWithDifferentweight:
                suspicious_score, weight = detect_function(suspicious_score_signals, signal_with_min_weight)
            if i not in stopAttackTime and burst == 0:
                burst = 1
                is_burst = 1
            suspicious_score_list.append(
                [bnss_key, i, suspicious_score, i in stopAttackTime, noiseForAvgRating[i], noiseForAvgRatingAfDetection[i],
                 noiseForCMR[i], noiseForCMRAfDetection[i], noiseForRank[i], noiseForRankAfDetection[i], is_burst])


    df_for_all_bnsses = DataFrame(suspicious_score_list, columns=['bnss_key', 'attack_time', 'suspicious_score',
                                                                  'is_stop_attack', 'noise_for_avg_rating',
                                                                  'noise_for_car_af_detection',
                                                                  'noise_for_cmr', 'noise_for_cmr_af_detection',
                                                                  'noise_for_rank',
                                                                  'noise_for_rank_af_detection',
                                                                  'is_burst'])
    df_for_all_bnsses.sort_values('suspicious_score', ascending=False, inplace=True)
    print(len(df_for_all_bnsses))
    all_attack_times = len(df_for_all_bnsses)
    count_for_stop_attack = sum(df_for_all_bnsses['is_stop_attack'])
    find_attack_times = len(df_for_all_bnsses) - count_for_stop_attack
    average_credit = float(sum(df_for_all_bnsses['noise_for_avg_rating'])) / find_attack_times
    count_for_burst = sum(df_for_all_bnsses['is_burst'])
    avg_credit_for_CMR = float(sum(df_for_all_bnsses['noise_for_cmr'])) / find_attack_times
    avg_rank = int(sum(df_for_all_bnsses['noise_for_rank']) / find_attack_times)

    topRank = int(round(len(df_for_all_bnsses) * top))
    flag_suspicious_df = df_for_all_bnsses[:topRank]
    unsuspicious_df = df_for_all_bnsses[topRank:]
    # print flag_suspicious_df.head()
    sum_of_suspicious_bnsses = len(flag_suspicious_df)
    average_credit_af_detection = (sum(flag_suspicious_df['noise_for_car_af_detection']) + sum(
        unsuspicious_df['noise_for_avg_rating'])) / find_attack_times
    avg_credit_for_CMR_af_detection = (sum(
        flag_suspicious_df['noise_for_cmr_af_detection']) + sum(unsuspicious_df['noise_for_cmr'])) / find_attack_times
    avg_rank_af_detection = int((sum(flag_suspicious_df['noise_for_rank_af_detection']) + sum(
        unsuspicious_df['noise_for_rank'])) / find_attack_times)
    count_for_FP = sum(flag_suspicious_df['is_stop_attack'])
    count_for_success_of_detection_of_burst = sum(flag_suspicious_df['is_burst'])
    print count_for_FP, sum_of_suspicious_bnsses, count_for_stop_attack, all_attack_times, \
           average_credit, average_credit_af_detection, avg_credit_for_CMR, avg_credit_for_CMR_af_detection,\
        avg_rank, avg_rank_af_detection, count_for_burst, count_for_success_of_detection_of_burst

    return count_for_FP, sum_of_suspicious_bnsses, count_for_stop_attack, all_attack_times, \
           average_credit, average_credit_af_detection, avg_credit_for_CMR, avg_credit_for_CMR_af_detection,\
        avg_rank, avg_rank_af_detection, count_for_burst, count_for_success_of_detection_of_burst


def rankSuspiciousScoreForMultipleBnsses(plotDir, top):
    suspicious_scores_dir = os.path.join(plotDir, 'suspicious_scores')
    df_for_all_bnsses = DataFrame()
    for root, dirs, files in os.walk(suspicious_scores_dir):
        for name in files:
            df_for_current_bnss = pd.read_csv(os.path.join(suspicious_scores_dir, name), index_col=0)
            df_for_all_bnsses = pd.concat([df_for_all_bnsses, df_for_current_bnss], axis=0)
    # print df_for_all_bnsses

    df_for_all_bnsses.sort_values('suspicious_score', ascending=False, inplace=True)
    all_attack_times = len(df_for_all_bnsses)
    count_for_stop_attack = sum(df_for_all_bnsses['is_stop_attack'])
    find_attack_times = (len(df_for_all_bnsses) - count_for_stop_attack)
    average_credit = float(sum(df_for_all_bnsses['noise_for_avg_rating'])) / find_attack_times
    count_for_burst = sum(df_for_all_bnsses['is_burst'])
    avg_credit_for_CMR = float(sum(df_for_all_bnsses['noise_for_cmr'])) / find_attack_times
    avg_rank = int(sum(df_for_all_bnsses['noise_for_rank'])/ find_attack_times)
    # print df_for_all_bnsses[['bnss_key', 'suspicious_score', 'noise_for_car_af_detection']]

    topRank = int(round(len(df_for_all_bnsses)*top))
    print "the number of bnss is", len(df_for_all_bnsses)
    flag_suspicious_df = df_for_all_bnsses[:topRank]
    unsuspicious_df = df_for_all_bnsses[topRank:]
    print flag_suspicious_df.head()
    sum_of_suspicious_bnsses = len(flag_suspicious_df)
    average_credit_af_detection = (sum(flag_suspicious_df['noise_for_car_af_detection']) + sum(unsuspicious_df['noise_for_avg_rating'])) / find_attack_times
    avg_credit_for_CMR_af_detection = (sum(flag_suspicious_df['noise_for_cmr_af_detection']) + sum(unsuspicious_df['noise_for_cmr'])) / find_attack_times
    avg_rank_af_detection = int((sum(flag_suspicious_df['noise_for_rank_af_detection']) + sum(unsuspicious_df['noise_for_rank'])) / find_attack_times)
    count_for_FP = sum(flag_suspicious_df['is_stop_attack'])
    count_for_success_of_detection_of_burst = sum(flag_suspicious_df['is_burst'])

    return count_for_FP, sum_of_suspicious_bnsses, count_for_stop_attack, all_attack_times, \
           average_credit, average_credit_af_detection, avg_credit_for_CMR, avg_credit_for_CMR_af_detection, \
           avg_rank, avg_rank_af_detection, count_for_burst, count_for_success_of_detection_of_burst


def OfflineDetectAnalysisWithAllData(plotDir, timeLength, statistics_for_current_bnss_dir, bnss_key, signals_to_be_extracted, startTimeKeyForPrediction, avgRatingErrorAfAttack, avgRatingAfAttack,
                          noOfReviewsAfAttack, ratingEntropyAfAttack, ratingDivergenceAfAttack,
                          noOfPositiveReviewsAfAttack, statistics_for_all_bnsses, statistics_for_all_measures):
    with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
        statistics_for_current_bnss = pickle.load(file)

    firstTimeKey = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]
    signals_dict = dict()

    if DEVIATION_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating_error_for_all_bnsses = np.append(statistics_for_all_bnsses[DEVIATION_OF_AVG_RATING], avgRatingErrorAfAttack)
        avg_rating_error_bf_attack = statistics_for_all_measures[bnss_key][DEVIATION_OF_AVG_RATING]
        avg_rating_error_for_current_bnss = np.append(avg_rating_error_bf_attack, avgRatingErrorAfAttack)
        signals_dict[DEVIATION_OF_AVG_RATING] = (avg_rating_error_for_all_bnsses, avg_rating_error_for_current_bnss, 'H')

    if DIFFERENCE_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_AVG_RATING]
        avg_rating_for_current_bnss = np.append(avg_rating, avgRatingAfAttack)
        avg_rating_diff_for_current_bnss = np.diff(avg_rating_for_current_bnss)
        avg_rating_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_AVG_RATING],
                                                   avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):])
        signals_dict[DIFFERENCE_OF_AVG_RATING] = (avg_rating_diff_for_all_bnsses, avg_rating_diff_for_current_bnss, 'H')

    if NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_for_all_bnsses = np.append(statistics_for_all_bnsses[NO_OF_REVIEWS], noOfReviewsAfAttack)
        no_of_reviews_bf_attack = statistics_for_all_measures[bnss_key][NO_OF_REVIEWS]
        no_of_reviews_for_current_bnss = np.append(no_of_reviews_bf_attack, noOfReviewsAfAttack)
        signals_dict[NO_OF_REVIEWS] = (no_of_reviews_for_all_bnsses, no_of_reviews_for_current_bnss, 'H')

    if DIFFERENCE_OF_NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_bf_attack = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_NO_OF_REVIEWS]
        no_of_reviews_for_current_bnss = np.append(no_of_reviews_bf_attack, noOfReviewsAfAttack)
        no_of_reviews_diff_for_current_bnss = np.diff(no_of_reviews_for_current_bnss)
        no_of_reviews_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_NO_OF_REVIEWS],
                                                      no_of_reviews_diff_for_current_bnss[-len(noOfReviewsAfAttack):])
        signals_dict[DIFFERENCE_OF_NO_OF_REVIEWS] = (no_of_reviews_diff_for_all_bnsses, no_of_reviews_diff_for_current_bnss, 'H')

    if RATING_ENTROPY in signals_to_be_extracted:
        rating_entropy_for_all_bnsses = np.append(statistics_for_all_bnsses[RATING_ENTROPY], ratingEntropyAfAttack)
        rating_entropy_bf_attack = statistics_for_all_measures[bnss_key][RATING_ENTROPY]
        rating_entropy_for_current_bnss = np.append(rating_entropy_bf_attack, ratingEntropyAfAttack)
        signals_dict[RATING_ENTROPY] = (rating_entropy_for_all_bnsses, rating_entropy_for_current_bnss, 'L')

    if DIFFERENCE_OF_RATING_ENTROPY in signals_to_be_extracted:
        rating_entropy_bf_attack = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_RATING_ENTROPY]
        rating_entropy_for_current_bnss = np.append(rating_entropy_bf_attack, ratingEntropyAfAttack)
        rating_entropy_diff_for_current_bnss = - np.diff(rating_entropy_for_current_bnss)
        rating_entropy_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_RATING_ENTROPY],
                                                       rating_entropy_diff_for_current_bnss[-len(ratingEntropyAfAttack):])
        signals_dict[DIFFERENCE_OF_RATING_ENTROPY] = (rating_entropy_diff_for_all_bnsses, rating_entropy_diff_for_current_bnss, 'H')

    if KL_DIVERGENCE in signals_to_be_extracted:
        KL_divergence_for_all_bnsses = np.append(statistics_for_all_bnsses[KL_DIVERGENCE], ratingDivergenceAfAttack)
        KL_divergence_bf_attack = statistics_for_all_measures[bnss_key][KL_DIVERGENCE]
        KL_divergence_for_current_bnss = np.append(KL_divergence_bf_attack, ratingDivergenceAfAttack)
        signals_dict[KL_DIVERGENCE] = (KL_divergence_for_all_bnsses, KL_divergence_for_current_bnss, 'H')

    if NO_OF_POSITIVE_REVIEWS in signals_to_be_extracted:
        no_of_positive_reviews_for_all_bnsses = np.append(statistics_for_all_bnsses[NO_OF_POSITIVE_REVIEWS],noOfPositiveReviewsAfAttack)
        no_of_positive_reviews_bf_attack = statistics_for_all_measures[bnss_key][NO_OF_POSITIVE_REVIEWS]
        no_of_positive_reviews_for_current_bnss = np.append(no_of_positive_reviews_bf_attack, noOfPositiveReviewsAfAttack)
        signals_dict[NO_OF_POSITIVE_REVIEWS] = (no_of_positive_reviews_for_all_bnsses, no_of_positive_reviews_for_current_bnss, 'H')

    if DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS in signals_to_be_extracted:
        no_of_positive_reviews_bf_attack = statistics_for_all_measures[bnss_key][NO_OF_POSITIVE_REVIEWS]
        no_of_positive_reviews_for_current_bnss = np.append(no_of_positive_reviews_bf_attack,
                                                            noOfPositiveReviewsAfAttack)
        no_of_positive_reviews_diff_for_current_bnss = np.diff(no_of_positive_reviews_for_current_bnss)
        no_of_positive_reviews_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS],
                                                               no_of_positive_reviews_diff_for_current_bnss[-len(noOfPositiveReviewsAfAttack):])
        signals_dict[DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS] = (no_of_positive_reviews_diff_for_all_bnsses, no_of_positive_reviews_diff_for_current_bnss, 'H')
    # no_of_reviews = no_of_reviews[1:]
    # ratingEntropy = ratingEntropy[1:]
    suspicious_score_dict = dict()
    attackLength = len(avgRatingErrorAfAttack)
    if DEVIATION_OF_AVG_RATING in signals_to_be_extracted:
        for i in range(len(avg_rating_error_for_current_bnss)):
            suspicious_score_signals = dict()
            for signal in signals_dict:
                signal_value = signals_dict[signal]
                if signal == DEVIATION_OF_AVG_RATING:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[1][i], signal_value[2])
                elif signal in [DIFFERENCE_OF_RATING_ENTROPY, DIFFERENCE_OF_NO_OF_REVIEWS, DIFFERENCE_OF_AVG_RATING,
                                DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE]:
                    if i+11 == len(signal_value[1]):
                        print 'stop'
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[1][i + 11], signal_value[2])
                elif signal in [NO_OF_REVIEWS, RATING_ENTROPY, NO_OF_POSITIVE_REVIEWS]:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[1][i + 12], signal_value[2])
            # print i, suspicious_score_signals.keys()
            # suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
            # suspicious_score = calculateSuspiciousScore_improve(suspicious_score_signals)
            # suspicious_score = calculateSuspiciousScoreWithDifferentweight(suspicious_score_signals, [NO_OF_POSITIVE_REVIEWS, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS])
            suspicious_score, weight = calculateSuspiciousScoreWithRandomWeight(suspicious_score_signals)
            suspicious_score_dict[i] = suspicious_score
    else:
        for i in range(startTimeKeyForPrediction - firstTimeKey - 1 - 11 + 5):
            suspicious_score_signals = dict()
            for signal in signals_dict:
                signal_value = signals_dict[signal]
                if signal in [DIFFERENCE_OF_RATING_ENTROPY, DIFFERENCE_OF_NO_OF_REVIEWS, DIFFERENCE_OF_AVG_RATING,
                              KL_DIVERGENCE]:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[1][i + 11], signal_value[2])
                if signal in [NO_OF_REVIEWS, RATING_ENTROPY, NO_OF_POSITIVE_REVIEWS]:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[1][i + 12], signal_value[2])
            suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
            suspicious_score_dict[i] = suspicious_score



    rank = sorted(suspicious_score_dict.keys(), key=lambda x:suspicious_score_dict[x], reverse=True)
    print rank
    return rank, attackLength


def OfflineDetectAnalysis(plotDir, signals_to_be_extracted, statistics_for_current_bnss_dir, bnss_key,
                          startTimeKeyForPrediction, avgRatingErrorAfAttack, avgRatingAfAttack,
                          noOfReviewsAfAttack, ratingEntropyAfAttack, ratingDivergenceAfAttack,
                          noOfPositiveReviewsAfAttack):
    with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
        statistics_for_current_bnss = pickle.load(file)

    firstTimeKey = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]
    signals_dict = dict()
    avg_rating = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][firstTimeKey:startTimeKeyForPrediction]
    cum_no_of_reviews = statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][firstTimeKey:startTimeKeyForPrediction]
    rating_distribution = statistics_for_current_bnss[StatConstants.RATING_DISTRIBUTION]
    no_of_reviews_train = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS]
    no_of_reviews_train = no_of_reviews_train[firstTimeKey:startTimeKeyForPrediction]
    entropy_train = statistics_for_current_bnss[StatConstants.RATING_ENTROPY][firstTimeKey:startTimeKeyForPrediction]
    no_of_reviews = np.append(no_of_reviews_train[:], noOfReviewsAfAttack)
    ratingEntropy = np.append(entropy_train[:], ratingEntropyAfAttack)

    if DEVIATION_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating_train, avg_rating_test = avg_rating[:12], avg_rating[12:]
        errorlist = list()
        predictByARupdateCoefandDatamodel(plotDir, avg_rating_train, avg_rating_test, bnss_key, StatConstants.AVERAGE_RATING, None, errorlist, None)
        avg_rating_error_diff = np.append(errorlist, avgRatingErrorAfAttack)
        signals_dict[DEVIATION_OF_AVG_RATING] = (avg_rating_error_diff, 'H')

    if DIFFERENCE_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating = np.append(avg_rating, avgRatingAfAttack)
        avg_rating_diff = abs(np.diff(avg_rating))
        signals_dict[DIFFERENCE_OF_AVG_RATING] = (avg_rating_diff, 'H')

    if NO_OF_REVIEWS in signals_to_be_extracted:
        signals_dict[NO_OF_REVIEWS] = (no_of_reviews, 'H')

    if DIFFERENCE_OF_NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_diff = abs(np.diff(no_of_reviews))
        signals_dict[DIFFERENCE_OF_NO_OF_REVIEWS] = (no_of_reviews_diff, 'H')

    if RATING_ENTROPY in signals_to_be_extracted:
        signals_dict[RATING_ENTROPY] = (ratingEntropy, 'L')

    if DIFFERENCE_OF_RATING_ENTROPY in signals_to_be_extracted:
        ratingEntropy_diff = abs(np.diff(ratingEntropy))
        signals_dict[DIFFERENCE_OF_RATING_ENTROPY] = (ratingEntropy_diff, 'H')

    if KL_DIVERGENCE in signals_to_be_extracted:
        current_rating = dict()
        cumulative_rating = dict()
        KL_divergence = list()
        for i in range(len(no_of_reviews_train)-1):
            for rating in rating_distribution:
                timeKey = i + firstTimeKey
                cumulative_rating[rating] = rating_distribution[rating][timeKey]/ cum_no_of_reviews[i]
                if no_of_reviews[i + 1]:
                    current_rating[rating] = (rating_distribution[rating][timeKey + 1] -
                                              rating_distribution[rating][timeKey]) / \
                                             no_of_reviews[i + 1]
                else:
                    current_rating[rating] = 0
            divergence = calculateKLdivergence(current_rating, cumulative_rating)
            KL_divergence.append(divergence)
        KL_divergence = np.append(KL_divergence, ratingDivergenceAfAttack)
        signals_dict[KL_DIVERGENCE] = (KL_divergence, 'H')

    if NO_OF_POSITIVE_REVIEWS in signals_to_be_extracted:
        no_of_positive_reviews_bf_attack = list()
        for i in range(len(no_of_reviews_train)):
            timeKey = i + firstTimeKey
            no_of_positive_reviews_i = rating_distribution[4][timeKey] - rating_distribution[4][
                timeKey-1] + rating_distribution[5][timeKey] - rating_distribution[5][timeKey-1]
            no_of_positive_reviews_bf_attack.append(no_of_positive_reviews_i)
        no_of_positive_reviews = np.append(no_of_positive_reviews_bf_attack, noOfPositiveReviewsAfAttack)
        signals_dict[NO_OF_POSITIVE_REVIEWS] = (no_of_positive_reviews, 'H')

    # no_of_reviews = no_of_reviews[1:]
    # ratingEntropy = ratingEntropy[1:]

    suspicious_score_dict = dict()
    attackLength = len(avgRatingErrorAfAttack)
    if DEVIATION_OF_AVG_RATING in signals_to_be_extracted:
        for i in range(len(avg_rating_error_diff)):
            suspicious_score_signals = dict()
            for signal in signals_dict:
                signal_value = signals_dict[signal]
                if signal == DEVIATION_OF_AVG_RATING:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[0][i], signal_value[1])
                elif signal in [DIFFERENCE_OF_RATING_ENTROPY, DIFFERENCE_OF_NO_OF_REVIEWS, DIFFERENCE_OF_AVG_RATING,
                                KL_DIVERGENCE]:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[0][i + 11], signal_value[1])
                elif signal in [NO_OF_REVIEWS, RATING_ENTROPY, NO_OF_POSITIVE_REVIEWS]:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[0][i + 12], signal_value[1])
            suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
            suspicious_score_dict[i] = suspicious_score
    else:
        for i in range(len(no_of_reviews)-1):
            suspicious_score_signals = dict()
            for signal in signals_dict:
                signal_value = signals_dict[signal]
                if signal == DEVIATION_OF_AVG_RATING:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[0][i], signal_value[1])
                elif signal in [DIFFERENCE_OF_RATING_ENTROPY, DIFFERENCE_OF_NO_OF_REVIEWS, DIFFERENCE_OF_AVG_RATING,
                                KL_DIVERGENCE]:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[0][i], signal_value[1])
                elif signal in [NO_OF_REVIEWS, RATING_ENTROPY, NO_OF_POSITIVE_REVIEWS]:
                    suspicious_score_signals[signal] = (signal_value[0], signal_value[0][i + 1], signal_value[1])
            suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
            suspicious_score_dict[i] = suspicious_score

    rank = sorted(suspicious_score_dict.keys(), key=lambda x:suspicious_score_dict[x], reverse=True)

    return rank, attackLength


def OnlineDetectAnalysisNoChange(plotDir, statistics_for_current_bnss_dir, bnss_key, startTimeKeyForPrediction, \
                          avgRatingErrorAfAttack, avgRatingAfAttack, noOfReviewsAfAttack, ratingEntropyAfAttack, top, \
                          count_for_success, count_for_FP, all_attack_times, all_timeKeys, stopAttackTime):
    with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
        statistics_for_current_bnss = pickle.load(file)

    firstTimeKey = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]

    avg_rating = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][firstTimeKey:startTimeKeyForPrediction]
    avg_rating_train, avg_rating_test = avg_rating[:12], avg_rating[12:startTimeKeyForPrediction]
    errorlist = list()
    predictByARupdateCoefandDatamodel(plotDir, avg_rating_train, avg_rating_test, bnss_key, StatConstants.AVERAGE_RATING, None, errorlist, None)


    no_of_reviews_train = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS]
    no_of_reviews_train = no_of_reviews_train[firstTimeKey:startTimeKeyForPrediction] #time before attack
    entropy_train = statistics_for_current_bnss[StatConstants.RATING_ENTROPY][firstTimeKey:startTimeKeyForPrediction]

    avg_rating_diff = np.append(errorlist, avgRatingErrorAfAttack)
    attackLength = len(avgRatingErrorAfAttack)
    no_of_reviews = np.append(no_of_reviews_train[:], noOfReviewsAfAttack)
    ratingEntropy = np.append(entropy_train[:], ratingEntropyAfAttack)
    no_of_reviews_diff = abs(np.diff(no_of_reviews))
    ratingEntropy_diff = abs(np.diff(ratingEntropy))
    no_of_reviews_diff_bfattack = no_of_reviews_diff[:len(no_of_reviews_diff)-attackLength]
    no_of_reviews_diff_afattack = no_of_reviews_diff[len(no_of_reviews_diff)-attackLength:]
    ratingEntropy_diff_bfattack = ratingEntropy_diff[:len(no_of_reviews_diff)-attackLength]
    ratingEntropy_diff_afattack = ratingEntropy_diff[len(ratingEntropy_diff)-attackLength:]
    ratingEntropy_bfattack = ratingEntropy[1:][:len(ratingEntropy[1:]-attackLength)]
    no_of_reviews_bfattack = no_of_reviews[1:][:len(no_of_reviews[1:]-attackLength)]
    ratingEntropy_afattack = ratingEntropy[1:][len(ratingEntropy[1:]-attackLength):]
    no_of_reviews_afattack = no_of_reviews[1:][len(no_of_reviews[1:]-attackLength):]

    suspicious_score_dict = dict()

    for i in range(len(errorlist)):
        signal_dict = [(errorlist, errorlist[i], 'H'), (no_of_reviews_diff_bfattack, no_of_reviews_diff_bfattack[i], 'H'),
                       (ratingEntropy_diff_bfattack, ratingEntropy_diff_bfattack[i], 'H'),
                       (no_of_reviews_bfattack, no_of_reviews_bfattack[i], 'H'),
                       (ratingEntropy_bfattack, ratingEntropy_bfattack[i], 'L')]
        suspicious_score = calculateSuspiciousScore(signal_dict)
        suspicious_score_dict[i] = suspicious_score

    lengthBfAttack = len(suspicious_score_dict)
    for i in range(len(avgRatingErrorAfAttack)):
        all_attack_times += 1
        signal_dict = [(errorlist, avgRatingErrorAfAttack[i], 'H'),
                       (no_of_reviews_diff_bfattack, no_of_reviews_diff_afattack[i], 'H'),
                       (ratingEntropy_diff_bfattack, ratingEntropy_diff_afattack[i], 'H'),
                       (no_of_reviews_bfattack, no_of_reviews_afattack[i], 'H'),
                       (ratingEntropy_bfattack, ratingEntropy_afattack[i], 'L')]
        suspicious_score = calculateSuspiciousScore(signal_dict)
        suspicious_score_dict[i+lengthBfAttack] = suspicious_score
        rank = sorted(suspicious_score_dict.keys(), key=lambda x: suspicious_score_dict[x], reverse=True)
        topRank = int(top*len(rank))
        if i+lengthBfAttack in rank[:topRank] and (i not in stopAttackTime):
            count_for_success += 1
            count_for_FP += topRank - 1
        else:
            count_for_FP += topRank

        all_timeKeys += topRank

    return count_for_success, count_for_FP, topRank, all_attack_times, all_timeKeys


def OnlineDetectAnalysis(plotDir, signals_to_be_extracted, statistics_for_current_bnss_dir, bnss_key, startTimeKeyForPrediction, \
                          avgRatingErrorAfAttack, avgRatingAfAttack, noOfReviewsAfAttack, ratingEntropyAfAttack, ratingDivergenceAfAttack,
                         noOfPositiveReviewsAfAttack, top, count_for_success, count_for_FP, all_attack_times, all_timeKeys, stopAttackTime):
    with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
        statistics_for_current_bnss = pickle.load(file)

    firstTimeKey = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]

    avg_rating = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][firstTimeKey:startTimeKeyForPrediction]
    avg_rating_train, avg_rating_test = avg_rating[:12], avg_rating[12:]
    errorlist = list()
    predictByARupdateCoefandDatamodel(plotDir, avg_rating_train, avg_rating_test, bnss_key, StatConstants.AVERAGE_RATING, None, errorlist, None)
    signals_dict_bf_attack = dict()
    signals_dict_af_attack = dict()

    no_of_reviews_train = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS]
    no_of_reviews_train = no_of_reviews_train[firstTimeKey:startTimeKeyForPrediction] #time before attack
    entropy_train = statistics_for_current_bnss[StatConstants.RATING_ENTROPY][firstTimeKey:startTimeKeyForPrediction]
    rating_distribution = statistics_for_current_bnss[StatConstants.RATING_DISTRIBUTION]
    cum_no_of_reviews = statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][firstTimeKey:startTimeKeyForPrediction]

    attackLength = len(avgRatingErrorAfAttack)
    no_of_reviews = np.append(no_of_reviews_train[:], noOfReviewsAfAttack)
    ratingEntropy = np.append(entropy_train[:], ratingEntropyAfAttack)


    suspicious_score_dict = dict()
    if DEVIATION_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating_train, avg_rating_test = avg_rating[:12], avg_rating[12:startTimeKeyForPrediction]
        errorlist = list()
        predictByARupdateCoefandDatamodel(plotDir, avg_rating_train, avg_rating_test, bnss_key, StatConstants.AVERAGE_RATING, None, errorlist, None)
        signals_dict_bf_attack[DEVIATION_OF_AVG_RATING] = (errorlist, 'H')
        signals_dict_af_attack[DEVIATION_OF_AVG_RATING] = (avgRatingErrorAfAttack, 'H')

    if DIFFERENCE_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating = np.append(avg_rating, avgRatingAfAttack)
        avg_rating_diff = abs(np.diff(avg_rating))
        avg_rating_diff_bf_attack = avg_rating_diff[:len(avg_rating_diff) - attackLength]
        avg_rating_diff_af_attack = avg_rating_diff[len(avg_rating_diff) - attackLength:]
        signals_dict_bf_attack[DIFFERENCE_OF_AVG_RATING] = (avg_rating_diff_bf_attack, 'H')
        signals_dict_af_attack[DIFFERENCE_OF_AVG_RATING] = (avg_rating_diff_af_attack, 'H')

    if NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_bfattack = no_of_reviews[1:][:len(no_of_reviews[1:]) - attackLength]
        no_of_reviews_afattack = no_of_reviews[1:][len(no_of_reviews[1:]) - attackLength:]
        signals_dict_bf_attack[NO_OF_REVIEWS] = (no_of_reviews_bfattack, 'H')
        signals_dict_af_attack[NO_OF_REVIEWS] = (no_of_reviews_afattack, 'H')

    if DIFFERENCE_OF_NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_diff = abs(np.diff(no_of_reviews))
        no_of_reviews_diff_bfattack = no_of_reviews_diff[:len(no_of_reviews_diff) - attackLength]
        no_of_reviews_diff_afattack = no_of_reviews_diff[len(no_of_reviews_diff) - attackLength:]
        signals_dict_bf_attack[DIFFERENCE_OF_NO_OF_REVIEWS] = (no_of_reviews_diff_bfattack, 'H')
        signals_dict_af_attack[DIFFERENCE_OF_NO_OF_REVIEWS] = (no_of_reviews_diff_afattack, 'H')

    if RATING_ENTROPY in signals_to_be_extracted:
        ratingEntropy_bfattack = ratingEntropy[1:][:len(ratingEntropy[1:]) - attackLength]
        ratingEntropy_afattack = ratingEntropy[1:][len(ratingEntropy[1:]) - attackLength:]
        signals_dict_bf_attack[RATING_ENTROPY] = (ratingEntropy_bfattack, 'L')
        signals_dict_af_attack[RATING_ENTROPY] = (ratingEntropy_afattack, 'L')

    if DIFFERENCE_OF_RATING_ENTROPY in signals_to_be_extracted:
        ratingEntropy_diff = abs(np.diff(ratingEntropy))
        ratingEntropy_diff_bfattack = ratingEntropy_diff[:len(ratingEntropy_diff) - attackLength]
        ratingEntropy_diff_afattack = ratingEntropy_diff[len(ratingEntropy_diff) - attackLength:]
        signals_dict_bf_attack[DIFFERENCE_OF_RATING_ENTROPY] = (ratingEntropy_diff_bfattack, 'H')
        signals_dict_af_attack[DIFFERENCE_OF_RATING_ENTROPY] = (ratingEntropy_diff_afattack, 'H')

    if KL_DIVERGENCE in signals_to_be_extracted:
        current_rating = dict()
        cumulative_rating = dict()
        KL_divergence_bf_attack = list()
        for i in range(len(no_of_reviews_train) - 1):
            for rating in rating_distribution:
                timeKey = i + firstTimeKey
                cumulative_rating[rating] = rating_distribution[rating][timeKey] / cum_no_of_reviews[i]
                current_rating[rating] = (rating_distribution[rating][timeKey + 1] - rating_distribution[rating][
                    timeKey]) / no_of_reviews_train[i + 1]
            divergence = calculateKLdivergence(current_rating, cumulative_rating)
            KL_divergence_bf_attack.append(divergence)
        signals_dict_bf_attack[KL_DIVERGENCE] = (KL_divergence_bf_attack, 'H')
        signals_dict_af_attack[KL_DIVERGENCE] = (ratingDivergenceAfAttack, 'H')

    if NO_OF_POSITIVE_REVIEWS in signals_to_be_extracted:
        no_of_positive_reviews_bf_attack = list()
        for i in range(len(no_of_reviews_train)):
            timeKey = i + firstTimeKey
            no_of_positive_reviews_i = rating_distribution[4][timeKey] - rating_distribution[4][
                timeKey - 1] + rating_distribution[5][timeKey] - rating_distribution[5][timeKey - 1]
            no_of_positive_reviews_bf_attack.append(no_of_positive_reviews_i)
        no_of_positive_reviews_bf_attack = no_of_positive_reviews_bf_attack[1:]
        signals_dict_bf_attack[NO_OF_POSITIVE_REVIEWS] = (no_of_positive_reviews_bf_attack, 'H')
        signals_dict_af_attack[NO_OF_POSITIVE_REVIEWS] = (noOfPositiveReviewsAfAttack, 'H')

    for i in range(len(errorlist)):
        suspicious_score_signals_bf_attack = dict()
        suspicious_score_signals_af_attack = dict()
        for signal in signals_dict_bf_attack:
            signal_value = signals_dict_bf_attack[signal]
            if signal == DEVIATION_OF_AVG_RATING:
                suspicious_score_signals_bf_attack[signal] = (signal_value[0], signal_value[0][i], signal_value[1])
            else:
                suspicious_score_signals_bf_attack[signal] = (signal_value[0], signal_value[0][i+11], signal_value[1])
        suspicious_score = calculateSuspiciousScore(suspicious_score_signals_bf_attack.values())
        suspicious_score_dict[i] = suspicious_score

    lengthBfAttack = len(suspicious_score_dict)
    for i in range(len(avgRatingErrorAfAttack)):
        all_attack_times += 1
        for signal in signals_dict_af_attack:
            signal_value_bf_attack = signals_dict_bf_attack[signal]
            signal_value_af_attack = signals_dict_af_attack[signal]
            suspicious_score_signals_af_attack[signal] = (signal_value_bf_attack[0], signal_value_af_attack[0][i], signal_value_af_attack[1])
        suspicious_score = calculateSuspiciousScore(suspicious_score_signals_af_attack.values())
        suspicious_score_dict[i+lengthBfAttack] = suspicious_score
        rank = sorted(suspicious_score_dict.keys(), key=lambda x: suspicious_score_dict[x], reverse=True)
        topRank = int(top*len(rank)) + 1
        if i+lengthBfAttack in rank[:topRank] and (i not in stopAttackTime):
            count_for_success += 1
            count_for_FP += topRank - 1
        else:
            count_for_FP += topRank
        signals_dict_bf_attack = updateSignalValues(signals_dict_bf_attack, signals_dict_af_attack, i)
        all_timeKeys += topRank

    return count_for_success, count_for_FP, topRank, all_attack_times, all_timeKeys


def updateSignalValues(signals_dict_bf_attack, signals_dict_af_attack, i):
    for signal in signals_dict_af_attack:
       np.append(signals_dict_bf_attack[signal][0], signals_dict_af_attack[signal][0][i])

    return signals_dict_bf_attack


def OnlineDetectAnalysisWithAllData(plotDir, signals_to_be_extracted, statistics_for_current_bnss_dir, bnss_key, startTimeKeyForPrediction, \
                          avgRatingErrorAfAttack, avgRatingAfAttack, noOfReviewsAfAttack, ratingEntropyAfAttack, ratingDivergenceAfAttack,
                         noOfPositiveReviewsAfAttack, top, count_for_success, count_for_FP, all_attack_times, all_timeKeys, stopAttackTime,
                                    statistics_for_all_bnsses, statistics_for_all_measures):
    with open(os.path.join(statistics_for_current_bnss_dir, bnss_key)) as file:
        statistics_for_current_bnss = pickle.load(file)

    firstTimeKey = statistics_for_current_bnss[StatConstants.FIRST_TIME_KEY]

    avg_rating = statistics_for_current_bnss[StatConstants.AVERAGE_RATING][firstTimeKey:startTimeKeyForPrediction]
    avg_rating_train, avg_rating_test = avg_rating[:12], avg_rating[12:]
    errorlist = list()
    predictByARupdateCoefandDatamodel(plotDir, avg_rating_train, avg_rating_test, bnss_key, StatConstants.AVERAGE_RATING, None, errorlist, None)
    signals_dict_bf_attack = dict()
    signals_dict_af_attack = dict()

    no_of_reviews_train = statistics_for_current_bnss[StatConstants.NON_CUM_NO_OF_REVIEWS]
    no_of_reviews_train = no_of_reviews_train[firstTimeKey:startTimeKeyForPrediction] #time before attack
    entropy_train = statistics_for_current_bnss[StatConstants.RATING_ENTROPY][firstTimeKey:startTimeKeyForPrediction]
    rating_distribution = statistics_for_current_bnss[StatConstants.RATING_DISTRIBUTION]
    cum_no_of_reviews = statistics_for_current_bnss[StatConstants.NO_OF_REVIEWS][firstTimeKey:startTimeKeyForPrediction]

    attackLength = len(avgRatingErrorAfAttack)
    no_of_reviews = np.append(no_of_reviews_train[:], noOfReviewsAfAttack)
    ratingEntropy = np.append(entropy_train[:], ratingEntropyAfAttack)


    suspicious_score_dict = dict()
    if DEVIATION_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating_train, avg_rating_test = avg_rating[:12], avg_rating[12:startTimeKeyForPrediction]
        errorlist = list()
        predictByARupdateCoefandDatamodel(plotDir, avg_rating_train, avg_rating_test, bnss_key, StatConstants.AVERAGE_RATING, None, errorlist, None)
        signals_dict_bf_attack[DEVIATION_OF_AVG_RATING] = (errorlist, 'H')
        signals_dict_af_attack[DEVIATION_OF_AVG_RATING] = (avgRatingErrorAfAttack, 'H')

    if DIFFERENCE_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating = np.append(avg_rating, avgRatingAfAttack)
        avg_rating_diff = abs(np.diff(avg_rating))
        avg_rating_diff_bf_attack = avg_rating_diff[:len(avg_rating_diff) - attackLength]
        avg_rating_diff_af_attack = avg_rating_diff[len(avg_rating_diff) - attackLength:]
        signals_dict_bf_attack[DIFFERENCE_OF_AVG_RATING] = (avg_rating_diff_bf_attack, 'H')
        signals_dict_af_attack[DIFFERENCE_OF_AVG_RATING] = (avg_rating_diff_af_attack, 'H')

    if NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_bfattack = no_of_reviews[1:][:len(no_of_reviews[1:]) - attackLength]
        no_of_reviews_afattack = no_of_reviews[1:][len(no_of_reviews[1:]) - attackLength:]
        signals_dict_bf_attack[NO_OF_REVIEWS] = (no_of_reviews_bfattack, 'H')
        signals_dict_af_attack[NO_OF_REVIEWS] = (no_of_reviews_afattack, 'H')

    if DIFFERENCE_OF_NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_diff = abs(np.diff(no_of_reviews))
        no_of_reviews_diff_bfattack = no_of_reviews_diff[:len(no_of_reviews_diff) - attackLength]
        no_of_reviews_diff_afattack = no_of_reviews_diff[len(no_of_reviews_diff) - attackLength:]
        signals_dict_bf_attack[DIFFERENCE_OF_NO_OF_REVIEWS] = (no_of_reviews_diff_bfattack, 'H')
        signals_dict_af_attack[DIFFERENCE_OF_NO_OF_REVIEWS] = (no_of_reviews_diff_afattack, 'H')

    if RATING_ENTROPY in signals_to_be_extracted:
        ratingEntropy_bfattack = ratingEntropy[1:][:len(ratingEntropy[1:]) - attackLength]
        ratingEntropy_afattack = ratingEntropy[1:][len(ratingEntropy[1:]) - attackLength:]
        signals_dict_bf_attack[RATING_ENTROPY] = (ratingEntropy_bfattack, 'L')
        signals_dict_af_attack[RATING_ENTROPY] = (ratingEntropy_afattack, 'L')

    if DIFFERENCE_OF_RATING_ENTROPY in signals_to_be_extracted:
        ratingEntropy_diff = abs(np.diff(ratingEntropy))
        ratingEntropy_diff_bfattack = ratingEntropy_diff[:len(ratingEntropy_diff) - attackLength]
        ratingEntropy_diff_afattack = ratingEntropy_diff[len(ratingEntropy_diff) - attackLength:]
        signals_dict_bf_attack[DIFFERENCE_OF_RATING_ENTROPY] = (ratingEntropy_diff_bfattack, 'H')
        signals_dict_af_attack[DIFFERENCE_OF_RATING_ENTROPY] = (ratingEntropy_diff_afattack, 'H')

    if KL_DIVERGENCE in signals_to_be_extracted:
        current_rating = dict()
        cumulative_rating = dict()
        KL_divergence_bf_attack = list()
        for i in range(len(no_of_reviews_train) - 1):
            for rating in rating_distribution:
                timeKey = i + firstTimeKey
                cumulative_rating[rating] = rating_distribution[rating][timeKey] / cum_no_of_reviews[i]
                current_rating[rating] = (rating_distribution[rating][timeKey + 1] - rating_distribution[rating][
                    timeKey]) / no_of_reviews_train[i + 1]
            divergence = calculateKLdivergence(current_rating, cumulative_rating)
            KL_divergence_bf_attack.append(divergence)
        signals_dict_bf_attack[KL_DIVERGENCE] = (KL_divergence_bf_attack, 'H')
        signals_dict_af_attack[KL_DIVERGENCE] = (ratingDivergenceAfAttack, 'H')

    if NO_OF_POSITIVE_REVIEWS in signals_to_be_extracted:
        no_of_positive_reviews_bf_attack = list()
        for i in range(len(no_of_reviews_train)):
            timeKey = i + firstTimeKey
            no_of_positive_reviews_i = rating_distribution[4][timeKey] - rating_distribution[4][
                timeKey - 1] + rating_distribution[5][timeKey] - rating_distribution[5][timeKey - 1]
            no_of_positive_reviews_bf_attack.append(no_of_positive_reviews_i)
        no_of_positive_reviews_bf_attack = no_of_positive_reviews_bf_attack[1:]
        signals_dict_bf_attack[NO_OF_POSITIVE_REVIEWS] = (no_of_positive_reviews_bf_attack, 'H')
        signals_dict_af_attack[NO_OF_POSITIVE_REVIEWS] = (noOfPositiveReviewsAfAttack, 'H')

    for i in range(len(errorlist)):
        suspicious_score_signals_bf_attack = dict()
        suspicious_score_signals_af_attack = dict()
        for signal in signals_dict_bf_attack:
            signal_value = signals_dict_bf_attack[signal]
            if signal == DEVIATION_OF_AVG_RATING:
                suspicious_score_signals_bf_attack[signal] = (signal_value[0], signal_value[0][i], signal_value[1])
            else:
                suspicious_score_signals_bf_attack[signal] = (signal_value[0], signal_value[0][i+11], signal_value[1])
        suspicious_score = calculateSuspiciousScore(suspicious_score_signals_bf_attack.values())
        suspicious_score_dict[i] = suspicious_score

    lengthBfAttack = len(suspicious_score_dict)
    for i in range(len(avgRatingErrorAfAttack)):
        all_attack_times += 1
        for signal in signals_dict_af_attack:
            signal_value_bf_attack = signals_dict_bf_attack[signal]
            signal_value_af_attack = signals_dict_af_attack[signal]
            suspicious_score_signals_af_attack[signal] = (signal_value_bf_attack[0], signal_value_af_attack[0][i], signal_value_af_attack[1])
        suspicious_score = calculateSuspiciousScore(suspicious_score_signals_af_attack.values())
        suspicious_score_dict[i+lengthBfAttack] = suspicious_score
        rank = sorted(suspicious_score_dict.keys(), key=lambda x: suspicious_score_dict[x], reverse=True)
        topRank = int(top*len(rank)) + 1
        if i+lengthBfAttack in rank[:topRank] and (i not in stopAttackTime):
            count_for_success += 1
            count_for_FP += topRank - 1
        else:
            count_for_FP += topRank
        signals_dict_bf_attack = updateSignalValues(signals_dict_bf_attack, signals_dict_af_attack, i)
        all_timeKeys += topRank

    return count_for_success, count_for_FP, topRank, all_attack_times, all_timeKeys

def formMetricMatrix(plotDir, metric, post_file='case_%s_post.csv'):
    TP_list = list()
    for case in range(1, 10):
        csv = os.path.join(os.path.join(plotDir, 'case_%s' % case), post_file % case)
        df = pd.read_csv(csv, index_col=0)
        print "case %s" % case
        print df
        TP_list.append(df[metric])
    df_matrix = DataFrame(TP_list)
    df_matrix.index = ['Evasion %s' % i for i in range(1, 10)]
    print df_matrix
    df_matrix.to_csv(os.path.join(plotDir, 'results', '%s_matrix.csv' % metric))


def calculateUtilityOfDifferentDetectorsForEarlyStage(plotDir, case, mode=1, susp_stats='susp_stats'):
    if mode == 1:
        signals_to_be_extracted = [DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS,
                                    NO_OF_REVIEWS, RATING_ENTROPY]
    else:
        signals_to_be_extracted = [EVASION_1, EVASION_2, EVASION_3, EVASION_4, EVASION_5, EVASION_6, EVASION_7,
                                     EVASION_8, EVASION_9]

    metrics = list()
    columns = ['avg_d_util', 'avg_noise_for_car', 'avg_noise_for_car_af_detect', 'avg_noise_for_cmr',
               'avg_noise_for_cmr_af_detect', 'avg_noise_for_rank', 'avg_noise_for_rank_af_detect',
               'attack_rate', 'avg_no_of_spam', 'avg_removed_reviews',
               'avg_no_of_positive_spam', 'avg_removed_positive_reviews','ranking_loss']

    index = list()
    count = 1
    for i in signals_to_be_extracted:
        if mode == 1:
            metrics.append(earlyDefenderUtil(plotDir, calculateSuspiciousScoreWithDifferentweight, case, [i], log=True, susp_stats=susp_stats))
        elif mode == 2:
            metrics.append(earlyDefenderUtil(plotDir, calculateSuspiciousScoreWithDifferentweight, case, i,  log=True, susp_stats=susp_stats))
        if mode == 1:
            index.append(i)
        else:
            index.append('Subset %s' % count)
            count += 1

    metrics.append(earlyDefenderUtil(plotDir, calculateSuspiciousScore, case, log=True, susp_stats=susp_stats))
    metrics.append(earlyDefenderUtil(plotDir, calculateSuspiciousScoreWithRandomWeight, case, log=True, susp_stats=susp_stats))
    index.append('All')
    index.append('Random')

    df = DataFrame(metrics, columns=columns, index=index)


    suspDir = os.path.join(os.path.join(plotDir, os.pardir, susp_stats, 'early_stage_%s' % case))
    post_file = os.path.join(suspDir, 'early_post_%s.csv' % case)

    if not os.path.exists(suspDir):
        os.mkdir(suspDir)
    df.to_csv(post_file)


def calculateUtilityOfDifferentDetectors(plotDir, case=1, mode=1, random=0, susp_stats='susp_stats'):
    if mode == 1:
        signals_to_be_extracted = [DEVIATION_OF_AVG_RATING, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS,
                               DIFFERENCE_OF_RATING_ENTROPY, NO_OF_REVIEWS, RATING_ENTROPY,
                               NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]
    else:
        signals_to_be_extracted = [EVASION_1, EVASION_2, EVASION_3, EVASION_4, EVASION_5, EVASION_6, EVASION_7,
                                     EVASION_8, EVASION_9]

    metrics = list()
    columns = ['avg_d_util', 'avg_noise_for_car', 'avg_noise_for_car_af_detect', 'avg_noise_for_cmr',
               'avg_noise_for_cmr_af_detect', 'avg_noise_for_rank', 'avg_noise_for_rank_af_detect',
               'attack_rate', 'avg_no_of_spam', 'avg_removed_reviews',
               'avg_no_of_positive_spam', 'avg_removed_positive_reviews','ranking_loss']

    index = list()
    count = 1
    for i in signals_to_be_extracted:
        if mode == 1:
            metrics.append(newDefenderUtil(plotDir, calculateSuspiciousScoreWithDifferentweight, case, [i],  log=True, random=random, susp_stats=susp_stats))
        elif mode == 2:
            metrics.append(newDefenderUtil(plotDir, calculateSuspiciousScoreWithDifferentweight, case, i,  log=True, random=random, susp_stats=susp_stats))
        if mode == 1:
            index.append(i)
        else:
            index.append('Subset %s' % count)
            count += 1

    metrics.append(newDefenderUtil(plotDir, calculateSuspiciousScore, case,  log=True, random=random, susp_stats=susp_stats))
    metrics.append(newDefenderUtil(plotDir, calculateSuspiciousScoreWithRandomWeight, case,  log=True, random=random, susp_stats=susp_stats))
    index.append('All')
    index.append('Random')
    df = DataFrame(metrics, columns=columns, index=index)

    if random:
        suspDir = os.path.join(os.path.join(plotDir, os.pardir, susp_stats, 'random_%s' % random))
        post_file = os.path.join(suspDir, 'random_%s' % random) + '_post.csv'
    else:
        suspDir = os.path.join(os.path.join(plotDir, os.pardir, susp_stats, 'case_%s' % case))
        post_file = os.path.join(suspDir, 'case_%s' % case) + '_post.csv'

    if not os.path.exists(suspDir):
        os.mkdir(suspDir)
    df.to_csv(post_file)


def newOfflineDetectAnalysisWithinAttack(plotDir, bnss_key, signals_to_be_extracted, avgRatingErrorAfAttack, avgRatingAfAttack,
                          noOfReviewsAfAttack, ratingEntropyAfAttack, ratingDivergenceAfAttack, stopAttackTime,
                          noOfPositiveReviewsAfAttack, statistics_for_all_bnsses, statistics_for_all_measures):

    signals_dict = dict()

    if DEVIATION_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating_error_for_all_bnsses = np.append(statistics_for_all_bnsses[DEVIATION_OF_AVG_RATING],
                                                    avgRatingErrorAfAttack)
        signals_dict[DEVIATION_OF_AVG_RATING] = (avg_rating_error_for_all_bnsses, avgRatingErrorAfAttack, 'H')

    if DIFFERENCE_OF_AVG_RATING in signals_to_be_extracted:
        avg_rating = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_AVG_RATING]
        avg_rating_for_current_bnss = np.append(avg_rating, avgRatingAfAttack)
        avg_rating_diff_for_current_bnss = np.diff(avg_rating_for_current_bnss)
        avg_rating_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_AVG_RATING],
                                                   avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):])
        signals_dict[DIFFERENCE_OF_AVG_RATING] = (avg_rating_diff_for_all_bnsses,
                                                  avg_rating_diff_for_current_bnss[-len(avgRatingAfAttack):], 'H')

    if NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_for_all_bnsses = np.append(statistics_for_all_bnsses[NO_OF_REVIEWS], noOfReviewsAfAttack)
        signals_dict[NO_OF_REVIEWS] = (no_of_reviews_for_all_bnsses, noOfReviewsAfAttack, 'H')

    if DIFFERENCE_OF_NO_OF_REVIEWS in signals_to_be_extracted:
        no_of_reviews_bf_attack = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_NO_OF_REVIEWS]
        no_of_reviews_for_current_bnss = np.append(no_of_reviews_bf_attack, noOfReviewsAfAttack)
        no_of_reviews_diff_for_current_bnss = np.diff(no_of_reviews_for_current_bnss)
        no_of_reviews_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_NO_OF_REVIEWS],
                                                      no_of_reviews_diff_for_current_bnss[-len(noOfReviewsAfAttack):])
        signals_dict[DIFFERENCE_OF_NO_OF_REVIEWS] = (no_of_reviews_diff_for_all_bnsses,
                                                     no_of_reviews_diff_for_current_bnss[-len(noOfReviewsAfAttack):],'H')

    if RATING_ENTROPY in signals_to_be_extracted:
        rating_entropy_for_all_bnsses = np.append(statistics_for_all_bnsses[RATING_ENTROPY], ratingEntropyAfAttack)
        signals_dict[RATING_ENTROPY] = (rating_entropy_for_all_bnsses, ratingEntropyAfAttack, 'L')

    if DIFFERENCE_OF_RATING_ENTROPY in signals_to_be_extracted:
        rating_entropy_bf_attack = statistics_for_all_measures[bnss_key][DIFFERENCE_OF_RATING_ENTROPY]
        rating_entropy_for_current_bnss = np.append(rating_entropy_bf_attack, ratingEntropyAfAttack)
        rating_entropy_diff_for_current_bnss = - np.diff(rating_entropy_for_current_bnss)
        rating_entropy_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_RATING_ENTROPY],
                                                       rating_entropy_diff_for_current_bnss[-len(ratingEntropyAfAttack):])
        signals_dict[DIFFERENCE_OF_RATING_ENTROPY] = (rating_entropy_diff_for_all_bnsses,
                                                      rating_entropy_diff_for_current_bnss[-len(ratingEntropyAfAttack):], 'H')

    if KL_DIVERGENCE in signals_to_be_extracted:
        KL_divergence_for_all_bnsses = np.append(statistics_for_all_bnsses[KL_DIVERGENCE], ratingDivergenceAfAttack)
        signals_dict[KL_DIVERGENCE] = (KL_divergence_for_all_bnsses, ratingDivergenceAfAttack, 'H')

    if NO_OF_POSITIVE_REVIEWS in signals_to_be_extracted:
        no_of_positive_reviews_for_all_bnsses = np.append(statistics_for_all_bnsses[NO_OF_POSITIVE_REVIEWS],noOfPositiveReviewsAfAttack)
        signals_dict[NO_OF_POSITIVE_REVIEWS] = (no_of_positive_reviews_for_all_bnsses, noOfPositiveReviewsAfAttack, 'H')

    if DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS in signals_to_be_extracted:
        no_of_positive_reviews_bf_attack = statistics_for_all_measures[bnss_key][NO_OF_POSITIVE_REVIEWS]
        no_of_positive_reviews_for_current_bnss = np.append(no_of_positive_reviews_bf_attack,
                                                            noOfPositiveReviewsAfAttack)
        no_of_positive_reviews_diff_for_current_bnss = np.diff(no_of_positive_reviews_for_current_bnss)
        no_of_positive_reviews_diff_for_all_bnsses = np.append(statistics_for_all_bnsses[DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS],
                                                               no_of_positive_reviews_diff_for_current_bnss[-len(noOfPositiveReviewsAfAttack):])
        signals_dict[DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS] = (no_of_positive_reviews_diff_for_all_bnsses,
                                                              no_of_positive_reviews_diff_for_current_bnss[-len(noOfPositiveReviewsAfAttack):], 'H')

    suspicious_score_list = list()
    attackLength = len(avgRatingErrorAfAttack)
    burst = 0
    statistics_for_current_bnss = dict()
    for i in range(attackLength):
        is_burst = 0
        suspicious_score_signals = dict()
        for signal in signals_dict:
            signal_value = signals_dict[signal]
            suspicious_score_signals[signal] = (signal_value[0], signal_value[1][i], signal_value[2])
            print signal, suspicious_score_signals[signal]
        statistics_for_current_bnss[i] = (suspicious_score_signals, stopAttackTime)
        # print i, suspicious_score_signals.keys()
        # suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
        # suspicious_score = calculateSuspiciousScore_improve(suspicious_score_signals)
        # suspicious_score = calculateSuspiciousScoreWithDifferentweight(suspicious_score_signals, [NO_OF_POSITIVE_REVIEWS, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS])
        suspicious_score = calculateSuspiciousScore(suspicious_score_signals.values())
        if i not in stopAttackTime and burst == 0:
            is_burst = 1
            burst = 1
        suspicious_score_list.append([bnss_key, i, suspicious_score, i in stopAttackTime, is_burst])

    df_for_current_bnss = DataFrame(suspicious_score_list, columns=['bnss_key', 'attack_time', 'suspicious_score',
                                                                    'is_stop_attack', 'is_burst'])
    # print df_for_current_bnss

    if not os.path.exists(os.path.join(plotDir, 'suspicious_scores')):
        os.mkdir(os.path.join(plotDir, 'suspicious_scores'))
    if not os.path.exists(os.path.join(plotDir, 'suspicious_scores_signals')):
        os.mkdir(os.path.join(plotDir, 'suspicious_scores_signals'))

    csv = os.path.join(os.path.join(plotDir, 'suspicious_scores'), bnss_key) + '.csv'
    df_for_current_bnss.to_csv(csv)

    with open(os.path.join(os.path.join(plotDir, 'suspicious_scores_signals'), bnss_key), 'w') as f:
        pickle.dump(statistics_for_current_bnss, f)


def newDetectorUtil(plotDir, detect_function, case, signal_with_min_weight=[], min_removal_rate=0.2, max_removal_rate=0.7):
    suspicious_score_list = list()
    bnssKeys = list()
    suspDir = os.path.join(os.path.join(plotDir, os.pardir, 'susp_stats', 'case_%s' % case))
    print suspDir
    for root, dirs, files in os.walk(os.path.join(suspDir, 'suspicious_scores_signals')):
        for name in files:
            bnssKeys.append(name)

    for bnss_key in bnssKeys:
        statistics_for_current_bnss = pickle.load(
            open(os.path.join(os.path.join(suspDir, 'suspicious_scores_signals'), bnss_key)))
        burst = 0
        for i in statistics_for_current_bnss:
            is_burst = 0
            suspicious_score_signals, stopAttackTime = statistics_for_current_bnss[i]
            if detect_function == calculateSuspiciousScore:
                suspicious_score = detect_function(suspicious_score_signals.values())
            elif detect_function == calculateSuspiciousScoreWithDifferentweight:
                suspicious_score = detect_function(suspicious_score_signals, signal_with_min_weight)
            if i not in stopAttackTime and burst == 0:
                burst = 1
                is_burst = 1
            suspicious_score_list.append(
                [bnss_key, i, suspicious_score, i in stopAttackTime, is_burst])

    df = DataFrame(suspicious_score_list, columns=['bnss_key', 'attack_time', 'suspicious_score',
                                                                  'is_stop_attack', 'is_burst'])
    suspicious_scores_list = df['suspicious_score']
    df_cmr = pd.read_csv(os.path.join(plotDir, 'bnss_stats', 'CMR.csv'), index_col=0)
    min_susp = min(suspicious_scores_list)
    max_susp = max(suspicious_scores_list)
    new_list = list()
    y_true = np.array([1 - df['is_stop_attack']])
    y_score = np.array([df['suspicious_score']])
    ranking_loss = label_ranking_loss(y_true, y_score)

    for tup in df.itertuples():
        ls = list(tup)
        bnss_key, i, suspicious_score, is_stopAttack, is_burst = ls[1:]
        attack_stats = pickle.load(open(os.path.join(suspDir, 'attack_stats', bnss_key)))
        ratingForAttackDict= attack_stats['rating for attack']
        ratingDistributionBfAttack = attack_stats['rating distribution bf attack']
        ratingDistributionAfAttack = attack_stats['rating distribution af attack']
        time_key = attack_stats['time key to start attack'] + i
        stars = range(1, 6)
        cumulativeRatingBfAttack = list()
        ratingAfAttack = list()
        cumulativeRatingAfAttackTemp = list()

        for key in ratingDistributionBfAttack:
            cumulative_rating = ratingDistributionBfAttack[key][time_key]
            cumulativeRatingAfAttackTemp.append(ratingDistributionAfAttack[key][time_key])
            current_rating = ratingDistributionBfAttack[key][time_key] - ratingDistributionBfAttack[key][time_key - 1]
            ratingAfAttack.append(ratingDistributionAfAttack[key][time_key] - ratingDistributionAfAttack[key][time_key - 1])
            cumulativeRatingBfAttack.append(cumulative_rating)

        # ratingAfAttack = np.array(truthfulRating) + np.array(ratingForAttack)
        # CMRAfAttack = float(sum([ratingAfAttack]*stars[k] for k in range(len(stars))))/sum(ratingAfAttack)
        # credit_for_CMR_bf_attack = CMRAfAttack - CMRBfAttack

        if i in ratingForAttackDict:
            CMRForBnsses = df_cmr[str(time_key)].copy()
            rank_series = CMRForBnsses.rank(method='min', ascending=False)
            rankBfAttack, CMRBfAttackFromdf = rank_series[bnss_key], CMRForBnsses[bnss_key]
            ratingForAttack = ratingForAttackDict[i]
            truthfulRating = np.array(ratingAfAttack) - ratingForAttack
            if sum(truthfulRating) == 0:
                CMRBfAttack = 0.0
            else:
                CMRBfAttack = float(sum([truthfulRating[k] * stars[k] for k in range(len(stars))])) / sum(
                    truthfulRating)
            CMRAfAttack = float(sum([ratingAfAttack[k] * stars[k] for k in range(len(stars))])) / sum(
                ratingAfAttack)
            cumulativeRatingAfAttack = np.array(cumulativeRatingBfAttack) + np.array(ratingForAttack)
            CARAfAttack = float(sum([cumulativeRatingAfAttack[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingAfAttack)
            CARBfAttack = float(sum([cumulativeRatingBfAttack[k] * stars[k] for k in range(len(stars))])) / sum(
                cumulativeRatingBfAttack)
            CMRForBnsses[bnss_key] = CMRAfAttack
            rankAfAttack = CMRForBnsses.rank(method='min', ascending=False)[bnss_key]
            noiseForAvgRating = CARAfAttack - CARBfAttack
            noiseForCMR = CMRAfAttack - CMRBfAttack
            noiseForRank = rankBfAttack - rankAfAttack
            # print CARAfAttack, attack_stats['CAR af attack'][i], float(sum([cumulativeRatingAfAttackTemp[k] * stars[k] for k in range(len(stars))])) / sum(
            #     cumulativeRatingAfAttackTemp), attack_stats['CAR bf attack'][i], CARBfAttack, noiseForAvgRating
            choice_list = list()
            removal_rate = min_removal_rate + (suspicious_score - min_susp)/(max_susp - min_susp) * \
                                              (max_removal_rate - min_removal_rate)
            removal_num = int(removal_rate * sum(ratingForAttack))
            removal_list = list()
            removedRating = list()
            if len(ratingForAttack)!= 5:
                raise ValueError
            for star in stars:
                choice_list = choice_list + [star for num in range(int(ratingForAttack[star - 1]))]
            for sample_time in range(removal_num):
                removal_list.append(np.random.choice(choice_list))
            for star in stars:
                removedRating.append(removal_list.count(star))

            removedPositiveRatio = float(removedRating[3] + removedRating[4]) / (ratingForAttack[3] + ratingForAttack[4])
            ratingAfDetection = np.array(ratingForAttack) - np.array(removedRating) + np.array(truthfulRating)
            cumulativeRatingAfDetection = np.array(ratingForAttack) - np.array(removedRating) + np.array(cumulativeRatingBfAttack)
            CMRAfDetection = float(sum([ratingAfDetection[k]*stars[k] for k in range(len(stars))]))/sum(ratingAfDetection)
            CARAfDetection = float(sum([cumulativeRatingAfDetection[k]*stars[k] for k in range(len(stars))]))/sum(cumulativeRatingAfDetection)
            credit_for_CMR_af_detection = CMRAfDetection - CMRBfAttack
            CMRForBnsses[bnss_key] = CMRAfDetection
            rankAfDetection = CMRForBnsses.rank(method='min', ascending=False)[bnss_key]
            # ratingAfAttack1 = np.array(ratingForAttack)+np.array(truthfulRating)
            # # print "rating Af attack", ratingAfAttack1, ratingAfAttack, ratingForAttack, attack_stats['NR bf attack'], sum(truthfulRating)
            # CMRAfAttack = float(sum([ratingAfAttack[k]*stars[k] for k in range(len(stars))]))/sum(ratingAfAttack)
            # print CMRAfAttack, attack_stats['CMR'][i]
            # print CMRAfAttack, CMRAfDetection,
            # CMRForBnsses[bnss_key] = CMRAfAttack
            # rankAfAttack = CMRForBnsses.rank(method='min', ascending=False)[bnss_key]
            # print rankAfDetection, rankBfAttack, rankAfAttack, rankBfAttack-rankAfAttack, rankBfAttack-rankAfDetection, \
            #     noiseForRank, noiseForRankAfDetection, i
            credit_for_rank_af_detection = rankBfAttack - rankAfDetection
            credit_for_CAR_af_detection = CARAfDetection - CARBfAttack
            new_list.append([bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, credit_for_CAR_af_detection,\
         noiseForCMR, credit_for_CMR_af_detection, noiseForRank, credit_for_rank_af_detection, is_burst, removedPositiveRatio, sum(ratingForAttack), sum(removedRating),
                             ratingForAttack[3]+ratingForAttack[4], removedRating[3]+removedRating[4]])
        else:
            removedPositiveRatio = 1.0
            noiseForAvgRating = 0
            noiseForCMR = 0
            noiseForRank = 0
            new_list.append([bnss_key, i, suspicious_score, is_stopAttack, noiseForAvgRating, noiseForAvgRating,
         noiseForCMR, noiseForCMR, noiseForRank, noiseForRank, is_burst, removedPositiveRatio, 0.0, 0.0, 0.0, 0.0])
    columns = ['bnss_key', 'attack_time', 'suspicious_score', 'is_stop_attack', 'noise_for_avg_rating',
                                                                  'noise_for_car_af_detection',
                                                                  'noise_for_cmr', 'noise_for_cmr_af_detection',
                                                                  'noise_for_rank',
                                                                  'noise_for_rank_af_detection',
                                                                  'is_burst', 'defender utility',
                                                                   'no_of_spam', 'removed_reviews',
                                                                    'no_of_+_spam', 'removed_+_spam']
    new_df = DataFrame(new_list, columns=columns)
    attack_times = len(new_df) - sum(new_df['is_stop_attack'])
    avg_noise_for_cmr_af_detection = float(sum(new_df['noise_for_cmr_af_detection']))/attack_times
    avg_defender_utility = new_df['defender utility'].mean()
    attack_rate = float(attack_times) / len(new_df)
    avg_noise_for_car_af_detection = float(sum(new_df['noise_for_car_af_detection']))/attack_times
    avg_noise_for_car = float(sum(new_df['noise_for_avg_rating']))/attack_times
    avg_noise_for_cmr = float(sum(new_df['noise_for_cmr']))/attack_times
    avg_no_of_spam = new_df['no_of_spam'].mean()
    avg_no_of_positive_spam = new_df['no_of_+_spam'].mean()
    avg_removed_reviews = new_df['removed_reviews'].mean()
    avg_removed_positive_reviews = new_df['removed_+_spam'].mean()
    avg_noise_for_rank = float(sum(new_df['noise_for_rank']))/attack_times
    avg_noise_for_rank_af_detection = float(sum(new_df['noise_for_rank_af_detection'])) /attack_times
    print avg_defender_utility, avg_noise_for_car, avg_noise_for_car_af_detection, avg_noise_for_cmr, \
           avg_noise_for_cmr_af_detection, avg_noise_for_rank, avg_noise_for_rank_af_detection, \
           attack_rate, avg_no_of_spam, avg_removed_reviews, avg_no_of_positive_spam, avg_removed_positive_reviews, \
            ranking_loss

    return avg_defender_utility, avg_noise_for_car, avg_noise_for_car_af_detection, avg_noise_for_cmr, \
           avg_noise_for_cmr_af_detection, avg_noise_for_rank, avg_noise_for_rank_af_detection, \
           attack_rate, avg_no_of_spam, avg_removed_reviews, avg_no_of_positive_spam, avg_removed_positive_reviews, \
           ranking_loss



if __name__ == '__main__':
    # csvFolder = '/home/sjge17/sjge/timeseries/data'
    # csvFolder = '/home/sjge17/sjge/Amazon Dataset/Apps for Android/'
    csvFolder = '../data/Amazon Dataset/Apps for Android/'
    # csvFolder = '../data/Yelp Dataset/YelpChi+YelpNYC'
    case = 5
    mode = 1
    print case
    bf_attack = datetime.now()
    plotDir = os.path.join(os.path.join(os.path.join(csvFolder, os.pardir), 'stats'))
    suspDir = os.path.join(os.path.join(os.path.join(csvFolder, os.pardir), 'susp_stats'))

    for case in range(2, 10):
         calculateUtilityOfDifferentDetectors(plotDir, case, mode=mode)
         # calculateUtilityOfDifferentDetectorsV2(plotDir, case, first=True)

    formMetricMatrix(suspDir, 'avg_d_util', 'case_%s_post_V2.csv')
    formMetricMatrix(suspDir, 'avg_noise_for_car_af_detect', 'case_%s_post_V2.csv')
    formMetricMatrix(suspDir, 'avg_noise_for_cmr_af_detect', 'case_%s_post_V2.csv')
    formMetricMatrix(suspDir, 'avg_noise_for_rank_af_detect', 'case_%s_post_V2.csv')
    formMetricMatrix(suspDir, 'ranking_loss', 'case_%s_post_V2.csv')
    formMetricMatrix(suspDir, 'attack_rate', 'case_%s_post_V2.csv')
    af_attack = datetime.now()
    print af_attack - bf_attack



