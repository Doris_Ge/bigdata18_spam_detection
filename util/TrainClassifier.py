from math import ceil
from util.DetectAnalysis import newDefenderUtil, calculateSuspiciousScoreWithDifferentweight, \
    calculateSuspiciousScore, calculateSuspiciousScoreWithRandomWeight, generateMetric
import pandas as pd
import numpy as np
import os
import pickle
from pandas import DataFrame, concat
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import label_ranking_loss
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import precision_recall_curve, roc_auc_score, roc_curve
from sklearn.externals import joblib
from matplotlib import pyplot as plt
from AttackAnalysis import extractBnssKeys

AVG_RATING = 'Average Rating'
DEVIATION_OF_AVG_RATING = 'Deviation Of Average Rating'
NO_OF_REVIEWS = 'Non Cum No. of Reviews'
RATING_ENTROPY = 'Rating entropy'
KL_DIVERGENCE ='KL Divergence'
DIFFERENCE_OF_AVG_RATING = 'Difference of Average Rating'
DIFFERENCE_OF_NO_OF_REVIEWS = 'Difference Of Number Of Reviews'
DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS = 'Difference Of Number Of Positive Reviews'
DIFFERENCE_OF_RATING_ENTROPY = 'Difference Of Rating Entropy'
NO_OF_POSITIVE_REVIEWS = 'No of +ve Reviews'


def calculatePercentilePoint(history, percentile):
    history = sorted(history)
    length = len(history)
    i = ceil(length * percentile)
    return history[int(i)-1]


def generateDataFrames(plotDir, case, signals):
    for i in signals:
        newDefenderUtil(plotDir,calculateSuspiciousScoreWithDifferentweight, case, signal_with_min_weight=[i],log=True)
    newDefenderUtil(plotDir, calculateSuspiciousScore, case, log=True)
    newDefenderUtil(plotDir, calculateSuspiciousScoreWithRandomWeight, case, log=True)


def adaptiveSuspiciousScore(suspDir_pre, case, signals):
    df_dict = dict()
    df_score = DataFrame()
    for sig in signals:
        df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                   index_col=0)
        df_score[sig] = df_dict[sig]['suspicious_score']
    # df_score['is_stop_attack'] = df_dict[signals[0]]['is_stop_attack']

    max_list = list()
    min_list = list()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        new_ls = list()
        for u in ls:
            if u == max_ls:
                new_ls.append(1)
            else:
                new_ls.append(0)
        max_list.append(new_ls)

    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        min_ls = min(ls)
        new_ls = list()
        for u in ls:
            if u == min_ls:
                new_ls.append(1)
            else:
                new_ls.append(0)
        min_list.append(new_ls)

    df_max = DataFrame(max_list, columns=signals)
    df_max['is_stop_attack'] = df_dict[signals[0]]['is_stop_attack']
    df_min = DataFrame(min_list, columns=signals)
    df_min['is_stop_attack'] = df_dict[signals[0]]['is_stop_attack']

    df_max_true = df_max[df_max['is_stop_attack']==False]
    df_max_false = df_max[df_max['is_stop_attack']==True]

    df_min_true = df_min[df_min['is_stop_attack']==False]
    df_min_false = df_min[df_min['is_stop_attack'] == True]
    for sig in signals:
        print 'max and attack', sig, sum(df_max_true[sig])

    for sig in signals:
        print 'max and no attack', sig, sum(df_max_false[sig])

    for sig in signals:
        print 'min and attack', sig, sum(df_min_true[sig])

    for sig in signals:
        print 'min and no attack', sig, sum(df_min_false[sig])

    score_list = list()
    ytrue = 1 - df_dict[signals[0]]['is_stop_attack']
    print df_score.head()
    for tup in df_score.itertuples():
        ls = list(tup)
        score_list.append(max(ls[1:]))

    rating_entropy_ranking_loss = label_ranking_loss([ytrue], [df_dict[RATING_ENTROPY]['suspicious_score']])
    adaptive_ranking_loss = label_ranking_loss([ytrue], [score_list])
    print rating_entropy_ranking_loss, adaptive_ranking_loss


def generateData(suspDir_pre, case, signals):
    bnssKeys = list()
    signals_to_extract = [DEVIATION_OF_AVG_RATING, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS,
                          DIFFERENCE_OF_RATING_ENTROPY, NO_OF_REVIEWS, RATING_ENTROPY,
                          NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]

    suspDir = os.path.join(suspDir_pre, 'case_%s' % case)
    print suspDir
    for root, dirs, files in os.walk(os.path.join(suspDir, 'suspicious_scores_signals')):
        for name in files:
            bnssKeys.append(name)

    df_dict = dict()
    df_label = DataFrame()
    for sig in signals:
        df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (case, sig)), index_col=0)
        df_label[sig] = df_dict[sig]['suspicious_score']

    new_df_label_list=list()
    for tup in df_label.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        new_ls = list()
        for u in ls:
            if u == max_ls:
                new_ls.append(1)
            else:
                new_ls.append(0)
        new_df_label_list.append(new_ls)

    df_label = DataFrame(new_df_label_list, columns=signals)
    df_label['is_stop_attack'] = df_dict[signals[0]]['is_stop_attack']
    print df_label.head()
    df_list = list()
    for bnss_key in bnssKeys:
        statistics_for_current_bnss = pickle.load(
            open(os.path.join(os.path.join(suspDir, 'suspicious_scores_signals'), bnss_key)))
        for i in statistics_for_current_bnss:
            suspicious_score_signals, stopAttackTime, noiseForAvgRating, noiseForAvgRatingAfDetection, \
            noiseForCMR, noiseForCMRAfDetection, noiseForRank, noiseForRankAfDetection = statistics_for_current_bnss[i]
            f_list = list()

            f_list.append(calculatePercentilePoint(suspicious_score_signals[RATING_ENTROPY][0], 0.25))
            f_list.append(calculatePercentilePoint(suspicious_score_signals[RATING_ENTROPY][0], 0.50))
            f_list.append(calculatePercentilePoint(suspicious_score_signals[RATING_ENTROPY][0], 0.75))
            entropy = suspicious_score_signals[RATING_ENTROPY][1]
            f_list.append(entropy)
            delta_entropy = suspicious_score_signals[delta_entropy][1]

            delta_car = suspicious_score_signals[DIFFERENCE_OF_AVG_RATING][1]
            f_list.append(delta_car)
            no_of_reviews = suspicious_score_signals[NO_OF_REVIEWS][1]
            f_list.append(no_of_reviews)
            no_of_p_reviews = suspicious_score_signals[NO_OF_POSITIVE_REVIEWS][1]
            f_list.append(no_of_p_reviews)

            df_list.append(f_list)
    df = DataFrame(df_list, columns=['25% Entropy', '50% Entropy', '75% Entropy', 'sig_'+ RATING_ENTROPY,'sig_' + DIFFERENCE_OF_AVG_RATING,
                                     'sig_' +  NO_OF_REVIEWS, 'sig_'+ NO_OF_POSITIVE_REVIEWS])

    df = concat([df, df_label], axis=1)
    print df
    df.to_csv(os.path.join(suspDir_pre, 'results', 'DT_data.csv'))


def trainClassifierForSuspiciousScore(suspDir_pre, case):
    df_dict = dict()
    df_score = DataFrame()
    for sig in signals:
        df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                   index_col=0)
        df_score[sig] = df_dict[sig]['suspicious_score']
    max_list = list()
    print df_score.head()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']
    df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre.csv' % case))
    df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_random.csv' % case))
    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    print df_score.head()
    train = concat([df_score[::5], df_score[1::5]])
    test = concat([df_score[2::5], df_score[3::5]])
    test = concat([test, df_score[4::5]])
    model = LogisticRegression('l1')
    model.fit(train.iloc[:, :9], train.iloc[:, 9])
    score = model.score(test.iloc[:, :9], test.iloc[:, 9])
    proba =list()
    for tup in model.predict_proba(test.iloc[:, :9]):
        proba.append(tup[0])
    print model.classes_
    # print proba
    loss_1 = label_ranking_loss([1-test.iloc[:, 9]], [proba])
    loss_2 = label_ranking_loss([1 - test.iloc[:, 9]], [test[RATING_ENTROPY]])
    loss_3 = label_ranking_loss([1 - test.iloc[:, 9]], [test['Max']])

    print score, loss_1, loss_2, loss_3
    precision_recall_dict = dict()
    for column in df_score:
        if column != 'label':
            if column == DEVIATION_OF_AVG_RATING:
                key = 'CAR-DEV'
            elif column == DIFFERENCE_OF_AVG_RATING:
                key = '$\Delta$CAR'
            elif column == NO_OF_REVIEWS:
                key = 'NR'
            elif column == DIFFERENCE_OF_NO_OF_REVIEWS:
                key = '$\Delta$NR'
            elif column == RATING_ENTROPY:
                key = 'EN'
            elif column == DIFFERENCE_OF_RATING_ENTROPY:
                key = '$\Delta$EN'
            elif column == KL_DIVERGENCE:
                key = 'KL-DIV'
            elif column == NO_OF_POSITIVE_REVIEWS:
                key = 'PR'
            elif column == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                key = '$\Delta$PR'
            else:
                key = column
            precision_recall_dict[key] = precision_recall_curve(1 - test.iloc[:, 9], test[column])
    precision_recall_dict['Adaptive ($w^s$)'] = precision_recall_curve(1 - test.iloc[:, 9], proba)
    # precision_recall_dict['All'] = precision_recall_curve(1-test.iloc[:, 9], test['All'])
    # precision_recall_dict['Max'] = precision_recall_curve(1-test.iloc[:, 9], test[RATING_ENTROPY])
    # precision_recall_dict['Random'] = precision_recall_curve(1-test.iloc[:, 9], test['Random'])

    colors = ['red', 'cyan', 'purple', 'orange', 'seagreen', 'deepskyblue', 'yellow', 'peru', 'brown', 'grey',
              'pink', 'coral', 'lawngreen']
    markers = ['', '', 'o', '<', 's', '', '', '', 'v', '>', '^', '', 'D']
    ls = ['-', '-', '--', '-.', '-', '-', '-', '-', ':', '-', '-', '-', '-']
    # pickle.dump(precision_recall_dict, open('./results/precision_recall_dict', 'w'))
    # precision_recall_dict = pickle.load(open('./results/precision_recall_dict'))
    lines = dict()
    i = 0

    grayLevel = 0.2
    transparancy = 1
    for key in precision_recall_dict:
        lines[key], = plt.plot(precision_recall_dict[key][1], precision_recall_dict[key][0], marker=markers[i],
                               color=colors[i], ms=4, markevery=90, lw=1.0,
                               markerfacecolor=(grayLevel, grayLevel, grayLevel, transparancy),
                               markeredgecolor=(grayLevel, grayLevel, grayLevel, transparancy))
        i += 1

    plt.legend(lines.values(), lines.keys(), fontsize=10, ncol=3, loc='lower right')

    plt.ylim(0, 1.02)
    plt.xlabel('Recall', fontsize=16)
    plt.ylabel('Precision', fontsize=16)
    plt.xticks(fontsize=12)
    plt.xticks(fontsize=12)
    plt.savefig(os.path.join(suspDir_pre, 'results', 'precision_recall_curve.eps'))
    plt.show()

def plotAUCForEachCase(suspDir_pre, signals, case=1, random=0):
    df_dict = dict()
    df_score = DataFrame()
    for sig in signals:
        if random:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'random_%s_pre_%s.csv' % (random, sig)),
                                   index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'random_%s_pre.csv' % random), index_col=0)
            df_dict['Random'] = pd.read_csv(
                os.path.join(suspDir_pre, 'results', 'random_%s_pre_random.csv' % case), index_col=0)
        else:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre.csv' % case), index_col=0)
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_random.csv' % case), index_col=0)
        df_score[sig] = df_dict[sig]['suspicious_score']

    max_list = list()
    # print df_score.head()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']

    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    # print df_score.head()
    train = concat([df_score[::5], df_score[1::5]])
    test = concat([df_score[2::5], df_score[3::5]])
    test = concat([test, df_score[4::5]])
    model = LogisticRegression('l1')
    model.fit(train.iloc[:, :9], train.iloc[:, 9])
    score = model.score(test.iloc[:, :9], test.iloc[:, 9])
    if random:
        joblib.dump(model, os.path.join(suspDir_pre, 'results', 'random_%s_model.pkl' % random))
    else:
        joblib.dump(model, os.path.join(suspDir_pre, 'results', 'case_%s_model.pkl' % case))
    proba =list()
    for tup in model.predict_proba(test.iloc[:, :9]):
        proba.append(tup[0])
    print model.classes_
    # print proba
    loss_1 = label_ranking_loss([1-test.iloc[:, 9]], [proba])
    loss_2 = label_ranking_loss([1 - test.iloc[:, 9]], [test[RATING_ENTROPY]])
    loss_3 = label_ranking_loss([1 - test.iloc[:, 9]], [test['Max']])
    print score, loss_1, loss_2, loss_3
    roc_curve_dict = dict()
    roc_dict = dict()
    for column in df_score:
        if column != 'label':
            if column == DEVIATION_OF_AVG_RATING:
                key = 'CAR-DEV'
            elif column == DIFFERENCE_OF_AVG_RATING:
                key = '$\Delta$CAR'
            elif column == NO_OF_REVIEWS:
                key = 'NR'
            elif column == DIFFERENCE_OF_NO_OF_REVIEWS:
                key = '$\Delta$NR'
            elif column == RATING_ENTROPY:
                key = 'EN'
            elif column == DIFFERENCE_OF_RATING_ENTROPY:
                key = '$\Delta$EN'
            elif column == KL_DIVERGENCE:
                key = 'KL-DIV'
            elif column == NO_OF_POSITIVE_REVIEWS:
                key = 'PR'
            elif column == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                key = '$\Delta$PR'
            else:
                key = column
            roc_curve_dict[key] = precision_recall_curve(1-test.iloc[:, 9], test[column])
            roc_dict[key] = roc_auc_score(1-test.iloc[:, 9], test[column])
    roc_curve_dict['Adaptive ($w^s$)'] = precision_recall_curve(1-test.iloc[:, 9], proba)
    roc_dict['Adaptive ($w^s$)'] = roc_auc_score(1 - test.iloc[:, 9], proba)
    print(str(roc_dict))
    print('max auc:', max(roc_dict.keys(), key=lambda x:roc_dict[x]), 'case:', case, "random", random)

    return roc_dict

def plotAUCForEarlyStage(suspDir_pre, signals, case=1, random=0):
    df_dict = dict()
    df_score = DataFrame()
    for sig in signals:
        if random:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'random_%s_pre_%s.csv' % (random, sig)),
                                   index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'random_%s_pre.csv' % random), index_col=0)
            df_dict['Random'] = pd.read_csv(
                os.path.join(suspDir_pre, 'results', 'random_%s_pre_random.csv' % case), index_col=0)
        else:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'early_%s_pre_%s.csv' % (case, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'early_%s_pre.csv' % case), index_col=0)
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'early_%s_pre_random.csv' % case), index_col=0)
        df_score[sig] = df_dict[sig]['suspicious_score']

    max_list = list()
    # print df_score.head()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']

    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    df_score['bnss_key'] = df_dict[signals[0]]['bnss_key']
    df_score['attack_time'] = df_dict[signals[0]]['attack_time']
    # print df_score.head()
    train = pd.DataFrame()
    train_idx = list()
    for idx, row in df_score.iterrows():
        if row['attack_time'] < 11:
            train_idx.append(idx)
    train = df_score.loc[train_idx, :]
    test_idx = np.setdiff1d(np.arange(len(df_score)), train_idx)
    test = df_score.loc[test_idx, :]
    print(1 - test.iloc[:, 4])
    df_score.pop('attack_time')
    df_score.pop('bnss_key')

    model = LogisticRegression('l1')
    model.fit(train.iloc[:, :4], train.iloc[:, 4])
    score = model.score(test.iloc[:, :4], test.iloc[:, 4])
    if random:
        joblib.dump(model, os.path.join(suspDir_pre, 'results', 'random_%s_model.pkl' % random))
    else:
        joblib.dump(model, os.path.join(suspDir_pre, 'results', 'early_%s_model.pkl' % case))
    proba =list()
    for tup in model.predict_proba(test.iloc[:, :4]):
        proba.append(tup[0])
    print model.classes_
    # print proba
    loss_1 = label_ranking_loss([1-test.iloc[:, 4]], [proba])
    loss_2 = label_ranking_loss([1 - test.iloc[:, 4]], [test[RATING_ENTROPY]])
    loss_3 = label_ranking_loss([1 - test.iloc[:, 4]], [test['Max']])
    print score, loss_1, loss_2, loss_3
    roc_curve_dict = dict()
    roc_dict = dict()
    for column in df_score:
        if column != 'label':
            if column == DEVIATION_OF_AVG_RATING:
                key = 'CAR-DEV'
            elif column == DIFFERENCE_OF_AVG_RATING:
                key = '$\Delta$CAR'
            elif column == NO_OF_REVIEWS:
                key = 'NR'
            elif column == DIFFERENCE_OF_NO_OF_REVIEWS:
                key = '$\Delta$NR'
            elif column == RATING_ENTROPY:
                key = 'EN'
            elif column == DIFFERENCE_OF_RATING_ENTROPY:
                key = '$\Delta$EN'
            elif column == KL_DIVERGENCE:
                key = 'KL-DIV'
            elif column == NO_OF_POSITIVE_REVIEWS:
                key = 'PR'
            elif column == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                key = '$\Delta$PR'
            else:
                key = column
            roc_curve_dict[key] = precision_recall_curve(1-test.iloc[:, 4], test[column])
            roc_dict[key] = roc_auc_score(1-test.iloc[:, 4], test[column])
    roc_curve_dict['Adaptive ($w^s$)'] = precision_recall_curve(1-test.iloc[:, 4], proba)
    roc_dict['Adaptive ($w^s$)'] = roc_auc_score(1 - test.iloc[:, 4], proba)
    print(str(roc_dict))
    print('max auc:', max(roc_dict.keys(), key=lambda x:roc_dict[x]), 'case:', case, "random", random)
    return roc_dict

def trainClassifier(csv, signal):
    data = pd.read_csv(csv, index_col=0)
    data_true = data[data['is_stop_attack'] == False]
    data_false = data[data['is_stop_attack'] == True]
    for sig in signal:
        print 'true', sig, sum(data_true[sig])
        print 'false', sig, sum(data_false[sig])
    # print sum(data[RATING_ENTROPY]), sum(data[DIFFERENCE_OF_AVG_RATING]), sum(data[NO_OF_REVIEWS]), sum(data[NO_OF_POSITIVE_REVIEWS])
    print len(data)
    train = concat([data[::5], data[1::5]])
    test = concat([data[2::5], data[3::5]])
    test = concat([test, data[4::5]])
    # train = concat([train, data[2::5]])
    # train = concat([train, data[3::5]])
    # print train.head()
    # test = data[4::5]
    print test
    DecisionTree = DecisionTreeClassifier()
    DecisionTree.fit(train.iloc[:, :7], train.iloc[:, 7])
    score = DecisionTree.score(test.iloc[:, :7], test.iloc[:, 7])
    print score

def crossValidation(suspDir_pre, signals, case):
    df_dict = dict()
    df_score = DataFrame()
    for sig in signals:
        df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                   index_col=0)
        df_score[sig] = df_dict[sig]['suspicious_score']
    max_list = list()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']
    df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre.csv' % case), index_col=0)
    df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_random.csv' % case), index_col=0)
    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    df_dict['Logit'] = df_dict['All'].copy(True)
    split_score = dict()
    score_of_logit = list()
    for i in range(5):
        split_score[i] = df_score[i::5]
    for i in range(5):
        test = split_score[i]
        train = None
        for j in range(5):
            if j == i:
                continue
            if train is None:
                train = split_score[j]
            else:
                train = concat([train, split_score[j]])
        model = LogisticRegression('l1')
        model.fit(train.iloc[:, :9], train.iloc[:, 9])
        score = model.score(test.iloc[:, :9], test.iloc[:, 9])
        proba = list()
        for tup in model.predict_proba(test.iloc[:, :9]):
            proba.append(tup[0])
        score_of_logit.append(proba)
        print model.classes_
        # print proba
        loss_1 = label_ranking_loss([1 - test.iloc[:, 9]], [proba])
        loss_2 = label_ranking_loss([1 - test.iloc[:, 9]], [test[RATING_ENTROPY]])
        loss_3 = label_ranking_loss([1 - test.iloc[:, 9]], [test['Max']])
        print score, loss_1, loss_2, loss_3
        precision_recall_dict = dict()
        for column in df_score:
            if column != 'label':
                if column == DEVIATION_OF_AVG_RATING:
                    key = 'CAR-DEV'
                elif column == DIFFERENCE_OF_AVG_RATING:
                    key = '$\Delta$CAR'
                elif column == NO_OF_REVIEWS:
                    key = 'NR'
                elif column == DIFFERENCE_OF_NO_OF_REVIEWS:
                    key = '$\Delta$NR'
                elif column == RATING_ENTROPY:
                    key = 'EN'
                elif column == DIFFERENCE_OF_RATING_ENTROPY:
                    key = '$\Delta$EN'
                elif column == KL_DIVERGENCE:
                    key = 'KL-DIV'
                elif column == NO_OF_POSITIVE_REVIEWS:
                    key = 'PR'
                elif column == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                    key = '$\Delta$PR'
                else:
                    key = column
                precision_recall_dict[key] = precision_recall_curve(1 - test.iloc[:, 9], test[column])
        precision_recall_dict['Adaptive ($w^s$)'] = precision_recall_curve(1 - test.iloc[:, 9], proba)
        # precision_recall_dict['All'] = precision_recall_curve(1-test.iloc[:, 9], test['All'])
        # precision_recall_dict['Max'] = precision_recall_curve(1-test.iloc[:, 9], test[RATING_ENTROPY])
        # precision_recall_dict['Random'] = precision_recall_curve(1-test.iloc[:, 9], test['Random'])

        colors = ['red', 'cyan', 'purple', 'orange', 'seagreen', 'deepskyblue', 'yellow', 'peru', 'brown', 'grey',
                  'pink', 'coral', 'lawngreen']
        markers = ['', '', 'o', '<', 's', '', '', '', 'v', '>', '^', '', 'D']
        ls = ['-', '-', '--', '-.', '-', '-', '-', '-', ':', '-', '-', '-', '-']
        # pickle.dump(precision_recall_dict, open('./results/precision_recall_dict', 'w'))
        # precision_recall_dict = pickle.load(open('./results/precision_recall_dict'))
        lines = dict()
        i = 0

        grayLevel = 0.2
        transparancy = 1
        for key in precision_recall_dict:
            lines[key], = plt.plot(precision_recall_dict[key][1], precision_recall_dict[key][0], marker=markers[i],
                                   color=colors[i], ms=4, markevery=90, lw=1.0,
                                   markerfacecolor=(grayLevel, grayLevel, grayLevel, transparancy),
                                   markeredgecolor=(grayLevel, grayLevel, grayLevel, transparancy))
            i += 1

        plt.legend(lines.values(), lines.keys(), fontsize=10, ncol=3, loc='lower right')

        plt.ylim(0.4, 1.02)
        plt.xlabel('Recall', fontsize=16)
        plt.ylabel('Precision', fontsize=16)
        plt.xticks(fontsize=12)
        plt.xticks(fontsize=12)
        plt.savefig('./precision_recall_curve.eps')
        # plt.show()
    suspicious_score_of_logit = list()
    for i in range(len(score_of_logit[0])):
        suspicious_score_of_logit.extend([score_of_logit[j][i] for j in range(len(score_of_logit))])
    print(suspicious_score_of_logit[:5])
    print(score_of_logit[0][0], score_of_logit[1][0], score_of_logit[2][0], score_of_logit[3][0], score_of_logit[4][0])
    df_dict['Logit']['suspicious_score'] = suspicious_score_of_logit
    df_dict['Logit'].to_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_Logit.csv' % case))

    post_df = pd.read_csv(os.path.join(suspDir_pre, 'case_%s' % case, 'case_%s_post.csv' % case), index_col=0)
    df = df_dict['Logit'][['bnss_key', 'attack_time', 'suspicious_score', 'is_stop_attack', 'noise_for_avg_rating',
                          'noise_for_car_af_detection', 'noise_for_cmr', 'noise_for_cmr_af_detection', 'noise_for_rank',
                          'noise_for_rank_af_detection', 'is_burst']]
    df_all = df_dict['All'][['bnss_key', 'attack_time', 'suspicious_score', 'is_stop_attack', 'noise_for_avg_rating',
                          'noise_for_car_af_detection', 'noise_for_cmr', 'noise_for_cmr_af_detection', 'noise_for_rank',
                          'noise_for_rank_af_detection', 'is_burst']]
    if 'Logit' not in post_df.index.values:
        index = post_df.index.values
        cols = post_df.columns.values
        print(post_df.values.shape)
        index = np.append(index, 'Logit')
        print(cols, index)
        metric = np.array(generateMetric(os.path.join(suspDir_pre, '../stats'), df, case, 0.2, 0.8))
        print(metric.shape)
        post_df = DataFrame(np.concatenate([post_df.values, [metric]]), columns=cols, index=index)

    post_df.to_csv(os.path.join(suspDir_pre, 'case_%s' % case, 'case_%s_post.csv' % case))
    print(post_df)

def exploreCorrelation(suspDir_pre, signals, case):
        df_dict = dict()
        df_score = DataFrame()
        for sig in signals:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                       index_col=0)
            if sig == DEVIATION_OF_AVG_RATING:
                key = 'CAR-DEV'
            elif sig == DIFFERENCE_OF_AVG_RATING:
                key = '$\Delta$CAR'
            elif sig == NO_OF_REVIEWS:
                key = 'NR'
            elif sig == DIFFERENCE_OF_NO_OF_REVIEWS:
                key = '$\Delta$NR'
            elif sig == RATING_ENTROPY:
                key = 'EN'
            elif sig == DIFFERENCE_OF_RATING_ENTROPY:
                key = '$\Delta$EN'
            elif sig == KL_DIVERGENCE:
                key = 'KL-DIV'
            elif sig == NO_OF_POSITIVE_REVIEWS:
                key = 'PR'
            elif sig == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                key = '$\Delta$PR'
            df_score[key] = df_dict[sig]['suspicious_score']
        max_list = list()
        print df_score.head()
        for tup in df_score.itertuples():
            ls = list(tup)
            ls = ls[1:]
            # print ls
            max_ls = max(ls)
            max_list.append(max_ls)
        df_score['label'] = df_dict[signals[0]]['is_stop_attack']
        df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre.csv' % case))
        df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_random.csv' % case))
        df_score['All'] = df_dict['All']['suspicious_score']
        df_score['Random'] = df_dict['Random']['suspicious_score']
        df_score['Max'] = max_list
        print df_score.head()
        train = concat([df_score[1::5], df_score[2::5]])
        # test = concat([df_score[2::5], df_score[3::5]])
        # test = concat([test, df_score[4::5]])
        train = concat([train, df_score[3::5]])
        train = concat([train, df_score[4::5]])
        test = df_score[::5]
        model = LogisticRegression('l1')
        model.fit(train.iloc[:, :9],1 - train.iloc[:, 9])
        score = model.score(test.iloc[:, :9], 1 - test.iloc[:, 9])
        proba = list()
        print model.classes_
        for tup in model.predict_proba(test.iloc[:, :9]):
            proba.append(tup[1])
        test['Classifier'] = proba
        print len(test)
        loss_1 = label_ranking_loss([1 - test.iloc[:, 9]], [proba])
        print loss_1, score
        print model.coef_
        weight, = model.coef_
        sorted_test = test.sort_values(by='Classifier', ascending=False)
        sorted_test_tp = sorted_test[sorted_test['label'] == False][:500]

        for i in range(9):
            column = sorted_test_tp.iloc[:, i]
            sorted_test_tp.iloc[:, i] = column*weight[i]

        legend = list()
        lines = list()
        i = 0
        markers = ['.', '.', '.', '.', '*', '.', '+', 'x', '_']
        for sig in sorted_test_tp.columns.values[:9]:
            if sig != 'EN':
                line, = plt.plot(sorted_test_tp['EN'], sorted_test_tp[sig], markers[i])
                lines.append(line)
                legend.append(sig+' (%.2f)' % weight[i])
            i += 1

        # plt.xlim([0.5, 5])
        lgd = plt.legend(lines, legend, bbox_to_anchor=(1, 1.02), fontsize=12, labelspacing=1.9) #  #,  mode="expand", borderaxespad=0.)
        for i in range(len(lgd.legendHandles)):
            lgd.legendHandles[i]._legmarker.set_markersize(12)
        corr_df = sorted_test_tp[sorted_test_tp.columns.values[:9]].corr()
        print corr_df
        plt.xlabel('$w_{1}$$x_{1}$', fontsize=12)
        plt.xticks(fontsize=9)
        plt.yticks(fontsize=9)
        plt.ylabel('$w_{i}$$x_{i}$', fontsize=12)
        plt.savefig(os.path.join(suspDir_pre, 'results', 'correlation.eps'), bbox_inches='tight', bbox_extra_artists=(lgd,))

        plt.tight_layout()
        plt.show()

def formAUCMatrix(susp_dir):
    auc_matrix = list()
    index = list()

    for case in range(1, 10):
        auc_dict = plotAUCForEachCase(susp_dir, case=case)
        auc_matrix.append(auc_dict.values())
        index.append('Evasion %s' % case)

    columns = auc_dict.keys()
    auc_matrix.append(plotAUCForEachCase(susp_dir, random=1).values())
    index.append('Random 1')
    index.append('Random 2')
    auc_matrix.append(plotAUCForEachCase(susp_dir, random=2).values())
    auc_matrix_df = DataFrame(auc_matrix, index=index, columns=columns)
    print(auc_matrix_df.head())
    auc_matrix_df.to_csv(os.path.join(susp_dir, 'results', 'auc_matrix.csv'))


def testEnsembleModel(susp_dir, case=1, random=0, first=False):
    df_dict = dict()
    df_score = DataFrame()
    models = dict()
    for i in range(1, 10):
        if first:
            plotAUCForEachCase(susp_dir, i)
        models[i] = joblib.load(os.path.join(susp_dir, 'results', 'case_%s_model.pkl' % i))
    for sig in signals:
        if random:
            df_dict[sig] = pd.read_csv(os.path.join(susp_dir, 'results', 'random_%s_pre_%s.csv' % (random, sig)),
                                   index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(susp_dir, 'results', 'random_%s_pre.csv' % random))
            df_dict['Random'] = pd.read_csv(os.path.join(susp_dir, 'results', 'random_%s_pre_random.csv' % random))
        else:
            df_dict[sig] = pd.read_csv(os.path.join(susp_dir, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(susp_dir, 'results', 'case_%s_pre.csv' % case))
            df_dict['Random'] = pd.read_csv(os.path.join(susp_dir, 'results', 'case_%s_pre_random.csv' % case))
        df_score[sig] = df_dict[sig]['suspicious_score']
    if random:
        trained_model = joblib.load(os.path.join(susp_dir, 'results', 'random_%s_model.pkl' % random))
    else:
        trained_model = joblib.load(os.path.join(susp_dir, 'results', 'case_%s_model.pkl' % case))


    max_list = list()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']
    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    # train = concat([df_score[::5], df_score[1::5]])
    test = concat([df_score[2::5], df_score[3::5]])
    test = concat([test, df_score[4::5]])

    trained_proba = list()
    for tup in trained_model.predict_proba(test.iloc[:, :9]):
        trained_proba.append(tup[0])

    probas = list()
    for i in models:
        proba = list()
        for tup in models[i].predict_proba(test.iloc[:, :9]):
            proba.append(tup[0])
        probas.append(proba)
    probas = np.array(probas)
    # print(probas[:, :10])
    print('probas', probas.shape)
    max_ensemble_proba = np.max(probas, axis=0)
    print('max_probas', max_ensemble_proba.shape)
    avg_ensemble_proba = np.mean(probas, axis=0)
    print('avg_probas', max_ensemble_proba.shape)
    roc_dict = dict()
    roc_curve_dict = dict()
    for column in df_score:
        if column != 'label':
            if column == DEVIATION_OF_AVG_RATING:
                key = 'CAR-DEV'
            elif column == DIFFERENCE_OF_AVG_RATING:
                key = '$\Delta$CAR'
            elif column == NO_OF_REVIEWS:
                key = 'NR'
            elif column == DIFFERENCE_OF_NO_OF_REVIEWS:
                key = '$\Delta$NR'
            elif column == RATING_ENTROPY:
                key = 'EN'
            elif column == DIFFERENCE_OF_RATING_ENTROPY:
                key = '$\Delta$EN'
            elif column == KL_DIVERGENCE:
                key = 'KL-DIV'
            elif column == NO_OF_POSITIVE_REVIEWS:
                key = 'PR'
            elif column == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                key = '$\Delta$PR'
            else:
                key = column
            roc_dict[key] = roc_auc_score(1 - test.iloc[:, 9], test[column])
            roc_curve_dict[key] = roc_curve(1 - test.iloc[:, 9], test[column])

    roc_dict['Max Ensemble'] = roc_auc_score(1 - test.iloc[:, 9], max_ensemble_proba)
    roc_curve_dict['Max Ensemble'] = roc_curve(1 - test.iloc[:, 9], max_ensemble_proba)
    roc_dict['Avg Ensemble'] = roc_auc_score(1 - test.iloc[:, 9], avg_ensemble_proba)
    roc_curve_dict['Avg Ensemble'] = roc_curve(1 - test.iloc[:, 9], avg_ensemble_proba)
    roc_dict['Adaptive ($w^s$)'] = roc_auc_score(1 - test.iloc[:, 9], trained_proba)
    roc_curve_dict['Adaptive ($w^s$)'] = roc_curve(1 - test.iloc[:, 9], trained_proba)
    print(roc_dict)
    print(max(roc_dict.keys(), key=lambda x:roc_dict[x]), 'case', case, 'random', random)
    colors = ['red', 'cyan', 'purple', 'orange', 'seagreen', 'deepskyblue', 'yellow', 'peru', 'brown', 'grey',
              '#152eff', '#fb5ffc', 'lawngreen', 'black', '#4984b8']
    markers = ['', '', 'o', '<', 's', '', '', '', 'v', '>', '^', '', 'D', '', '']
    # pickle.dump(precision_recall_dict, open('./results/precision_recall_dict', 'w'))
    # precision_recall_dict = pickle.load(open('./results/precision_recall_dict'))
    lines = dict()
    i = 0

    grayLevel = 0.2
    transparancy = 1
    print(roc_curve_dict.keys())
    for key in roc_curve_dict:
        lines[key], = plt.plot(roc_curve_dict[key][0], roc_curve_dict[key][1], marker=markers[i],
                               color=colors[i], ms=4, markevery=90, lw=1.0,
                               markerfacecolor=(grayLevel, grayLevel, grayLevel, transparancy),
                               markeredgecolor=(grayLevel, grayLevel, grayLevel, transparancy))
        i += 1

    plt.legend(lines.values(), lines.keys(), fontsize=10, ncol=3, loc='lower right')

    plt.ylim(0, 1.02)
    plt.xlabel('False Positive Rate', fontsize=14)
    plt.ylabel('True Positive Rate', fontsize=14)
    plt.xticks(fontsize=12)
    plt.xticks(fontsize=12)
    if random:
        plt.savefig(os.path.join(susp_dir, 'results', 'roc_curve_random_%s.eps' % random))
    else:
        plt.savefig(os.path.join(susp_dir, 'results', 'roc_curve_case_%s.eps' % case))
    plt.close()
    return roc_dict

def testEnsembleModelForCase(susp_dir, signals, case, random, first=False):
    df_dict = dict()
    df_score = DataFrame()
    models = dict()
    if first:
        plotAUCForEachCase(susp_dir, random=random)
    models[random] = joblib.load(os.path.join(susp_dir, 'results', 'random_%s_model.pkl' % random))
    for sig in signals:
        df_dict[sig] = pd.read_csv(os.path.join(susp_dir, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                   index_col=0)
        df_score[sig] = df_dict[sig]['suspicious_score']
    df_dict['All'] = pd.read_csv(os.path.join(susp_dir, 'results', 'case_%s_pre.csv' % case))
    df_dict['Random'] = pd.read_csv(os.path.join(susp_dir, 'results', 'case_%s_pre_random.csv' % case))

    max_list = list()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']
    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    # train = concat([df_score[::5], df_score[1::5]])
    test = concat([df_score[2::5], df_score[3::5]])
    test = concat([test, df_score[4::5]])
    probas = list()
    for random in models:
        proba = list()
        for tup in models[random].predict_proba(test.iloc[:, :9]):
            proba.append(tup[0])
        probas.append(proba)
    probas = np.array(probas)
    print('probas', probas.shape)
    max_ensemble_proba = np.max(probas, axis=0)
    print('max_probas', max_ensemble_proba.shape)
    avg_ensemble_proba = np.mean(probas, axis=0)
    print('avg_probas', max_ensemble_proba.shape)
    roc_curve_dict = dict()
    roc_dict = dict()
    for column in df_score:
        if column != 'label':
            if column == DEVIATION_OF_AVG_RATING:
                key = 'CAR-DEV'
            elif column == DIFFERENCE_OF_AVG_RATING:
                key = '$\Delta$CAR'
            elif column == NO_OF_REVIEWS:
                key = 'NR'
            elif column == DIFFERENCE_OF_NO_OF_REVIEWS:
                key = '$\Delta$NR'
            elif column == RATING_ENTROPY:
                key = 'EN'
            elif column == DIFFERENCE_OF_RATING_ENTROPY:
                key = '$\Delta$EN'
            elif column == KL_DIVERGENCE:
                key = 'KL-DIV'
            elif column == NO_OF_POSITIVE_REVIEWS:
                key = 'PR'
            elif column == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                key = '$\Delta$PR'
            else:
                key = column
            roc_dict[key] = roc_auc_score(1 - test.iloc[:, 9], test[column])
    roc_dict['Max Ensemble'] = roc_auc_score(1 - test.iloc[:, 9], max_ensemble_proba)
    roc_dict['Avg Ensemble'] = roc_auc_score(1 - test.iloc[:, 9], avg_ensemble_proba)
    print(roc_dict)
    print(max(roc_dict.keys(), key=lambda x:roc_dict[x]))



def formAUCMatrixForEnsembleModel(susp_dir):
    auc_dict = dict()
    for case in range(1, 10):
        auc = testEnsembleModel(susp_dir, case=case)
        auc_dict['Evasion %s' % case] = auc.values()

    for random in range(1, 3):
        auc = testEnsembleModel(susp_dir, random=random)
        auc_dict['Random %s' % random] = auc.values()

    auc_matrix = DataFrame(auc_dict.values(), index=auc_dict.keys(), columns=auc.keys())
    print(auc_matrix)
    auc_matrix.to_csv(os.path.join(susp_dir, 'results', 'auc_matrix_ensemble.csv'))


def trainOnAggregatedEarlyData(suspDir_pre, signals, random=0, case=1, first=False, susp_stats='susp_stats'):

    models = dict()

    if case == 0 or random == 0:
        susp_dir = os.path.join(suspDir_pre, '..', 'susp_stats')
    else:
        susp_dir = suspDir_pre

    for i in range(1, 3):
        if first:
            plotAUCForEarlyStage(susp_dir, signals, i)
        models[i] = joblib.load(os.path.join(susp_dir, 'results', 'early_%s_model.pkl' % i))

    all_train = DataFrame()

    for i in range(1, 3):
        df_dict = dict()
        df_score = DataFrame()
        for sig in signals:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'early_%s_pre_%s.csv' % (i, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'early_%s_pre.csv' % i),
                                         index_col=0)
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'early_%s_pre_random.csv' % i),
                                            index_col=0)
            df_score[sig] = df_dict[sig]['suspicious_score']
        max_list = list()
        # print df_score.head()
        for tup in df_score.itertuples():
            ls = list(tup)
            ls = ls[1:]
            # print ls
            max_ls = max(ls)
            max_list.append(max_ls)
        df_score['label'] = df_dict[signals[0]]['is_stop_attack']
        df_score['All'] = df_dict['All']['suspicious_score']
        df_score['Random'] = df_dict['Random']['suspicious_score']
        df_score['Max'] = max_list
        df_score['bnss_key'] = df_dict[signals[0]]['bnss_key']
        df_score['attack_time'] = df_dict[signals[0]]['attack_time']
        # print df_score.head()
        train = pd.DataFrame()
        train_idx = list()
        for idx, row in df_score.iterrows():
            if row['attack_time'] < 11:
                train_idx.append(idx)
        train = df_score.loc[train_idx, :]
        train.pop('attack_time')
        train.pop('bnss_key')
        # print(len(train))
        all_train = concat([all_train, train])

    df_dict = dict()
    df_score = DataFrame()
    for sig in signals:
        if random:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, '..', susp_stats, 'results', 'random_%s_pre_%s.csv' % (random, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, '..', susp_stats, 'results', 'random_%s_pre.csv' % random))
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, '..', susp_stats, 'results', 'random_%s_pre_random.csv' % random))
        else:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'early_%s_pre_%s.csv' % (case, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'early_%s_pre.csv' % case))
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'early_%s_pre_random.csv' % case))

        df_score[sig] = df_dict[sig]['suspicious_score']
    max_list = list()
    # print df_score.head()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']
    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    df_score['bnss_key'] = df_dict[signals[0]]['bnss_key']
    df_score['attack_time'] = df_dict[signals[0]]['attack_time']
    print df_score.head()

    # test = pd.DataFrame()
    # for i in range(11, 29):
    #     test = concat([test, df_score[i::29]])
    test_idx = np.setdiff1d(np.arange(len(df_score)), train_idx)
    test = df_score.loc[test_idx, :]
    # print(len(test))
    model = LogisticRegression('l1')
    model.fit(all_train.iloc[:, :4], all_train.iloc[:, 4])
    print(model.predict(test.iloc[:, :4]))
    score = model.score(1 - test.iloc[:, :4], test.iloc[:, 4])
    proba = list()
    for tup in model.predict_proba(test.iloc[:, :4]):
        proba.append(tup[0])
    print model.classes_
    print proba
    loss_1 = label_ranking_loss([1 - test.iloc[:, 4]], [proba])
    loss_2 = label_ranking_loss([1 - test.iloc[:, 4]], [test[RATING_ENTROPY]])
    loss_3 = label_ranking_loss([1 - test.iloc[:, 4]], [test['Max']])
    print score, loss_1, loss_2, loss_3

    probas = list()
    for i in models:
        p = list()
        for tup in models[i].predict_proba(test.iloc[:, :4]):
            p.append(tup[0])
        probas.append(p)
    probas = np.array(probas)
    # print(probas[:, :10])
    max_ensemble_proba = np.max(probas, axis=0)
    avg_ensemble_proba = np.mean(probas, axis=0)

    roc_dict = dict()
    key_column_dict = dict()
    columns = list()
    for column in all_train:
        if column != 'label':
            if column == DEVIATION_OF_AVG_RATING:
                key = 'CAR-DEV'
                columns.append(key)
            elif column == DIFFERENCE_OF_AVG_RATING:
                key = '$\Delta$CAR'
                columns.append(key)
            elif column == NO_OF_REVIEWS:
                key = 'NR'
                columns.append(key)
            elif column == DIFFERENCE_OF_NO_OF_REVIEWS:
                key = '$\Delta$NR'
                columns.append(key)
            elif column == RATING_ENTROPY:
                key = 'EN'
                columns.append(key)
            elif column == DIFFERENCE_OF_RATING_ENTROPY:
                key = '$\Delta$EN'
                columns.append(key)
            elif column == KL_DIVERGENCE:
                key = 'KL-DIV'
                columns.append(key)
            elif column == NO_OF_POSITIVE_REVIEWS:
                key = 'PR'
                columns.append(key)
            elif column == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                key = '$\Delta$PR'
                columns.append(key)
            else:
                key = column
            key_column_dict[key] = column
            roc_dict[key] = roc_auc_score(1 - test.iloc[:, 4], test[column])
    roc_dict['Adaptive ($w^s$)'] = roc_auc_score(1 - test.iloc[:, 4], proba)
    roc_dict['Max Ensemble'] = roc_auc_score(1 - test.iloc[:, 4], max_ensemble_proba)
    roc_dict['Avg Ensemble'] = roc_auc_score(1 - test.iloc[:, 4], avg_ensemble_proba)
    test['Adaptive ($w^s$)'] = proba
    test['Max Ensemble'] = max_ensemble_proba
    test['Avg Ensemble'] = avg_ensemble_proba
    best_p = max(columns, key=lambda x: roc_dict[x])
    print(columns, best_p)
    test['Best_P'] = test[key_column_dict[best_p]]
    print(test[:5])
    test.to_csv(os.path.join(suspDir_pre, '..', susp_stats, 'early_stage_%s' % case, 'early_%s_DETER.csv' % case))
    print(roc_dict)
    print(max(roc_dict.keys(), key=lambda x:roc_dict[x]), 'random:', random, 'case:', case)

    return roc_dict

    
def trainOnAggregatedData(suspDir_pre, signals, random=0, case=1, first=False, susp_stats='susp_stats'):

    models = dict()

    if case == 0 or random == 0:
        susp_dir = os.path.join(suspDir_pre, '..', 'susp_stats')
    else:
        susp_dir = suspDir_pre

    for i in range(1, 10):
        if first:
            plotAUCForEachCase(susp_dir, signals, i)
        models[i] = joblib.load(os.path.join(susp_dir, 'results', 'case_%s_model.pkl' % i))

    all_train = DataFrame()

    for i in range(1, 10):
        df_dict = dict()
        df_score = DataFrame()
        for sig in signals:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (i, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre.csv' % i),
                                         index_col=0)
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_random.csv' % i),
                                            index_col=0)
            df_score[sig] = df_dict[sig]['suspicious_score']
        max_list = list()
        # print df_score.head()
        for tup in df_score.itertuples():
            ls = list(tup)
            ls = ls[1:]
            # print ls
            max_ls = max(ls)
            max_list.append(max_ls)
        df_score['label'] = df_dict[signals[0]]['is_stop_attack']
        df_score['All'] = df_dict['All']['suspicious_score']
        df_score['Random'] = df_dict['Random']['suspicious_score']
        df_score['Max'] = max_list
        # print df_score.head()
        train = concat([df_score[::5], df_score[1::5]])
        # print(len(train))
        all_train = concat([all_train, train])

    df_dict = dict()
    df_score = DataFrame()
    for sig in signals:
        if random:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, '..', susp_stats, 'results', 'random_%s_pre_%s.csv' % (random, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, '..', susp_stats, 'results', 'random_%s_pre.csv' % random))
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, '..', susp_stats, 'results', 'random_%s_pre_random.csv' % random))
        else:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre.csv' % case))
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_random.csv' % case))

        df_score[sig] = df_dict[sig]['suspicious_score']
    max_list = list()
    # print df_score.head()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']
    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    df_score['bnss_key'] = df_dict[signals[0]]['bnss_key']
    df_score['attack_time'] = df_dict[signals[0]]['attack_time']
    # print df_score.head()
    test = concat([df_score[2::5], df_score[3::5]])
    test = concat([test, df_score[4::5]])
    # print(len(test))
    model = LogisticRegression('l1')
    model.fit(all_train.iloc[:, :9], all_train.iloc[:, 9])
    # print(model.predict(test.iloc[:, :9]))
    score = model.score(1 - test.iloc[:, :9], test.iloc[:, 9])
    proba = list()
    for tup in model.predict_proba(test.iloc[:, :9]):
        proba.append(tup[0])
    # print model.classes_
    # print proba
    loss_1 = label_ranking_loss([1 - test.iloc[:, 9]], [proba])
    loss_2 = label_ranking_loss([1 - test.iloc[:, 9]], [test[RATING_ENTROPY]])
    loss_3 = label_ranking_loss([1 - test.iloc[:, 9]], [test['Max']])
    # print score, loss_1, loss_2, loss_3

    probas = list()
    for i in models:
        p = list()
        for tup in models[i].predict_proba(test.iloc[:, :9]):
            p.append(tup[0])
        probas.append(p)
    probas = np.array(probas)
    # print(probas[:, :10])
    max_ensemble_proba = np.max(probas, axis=0)
    avg_ensemble_proba = np.mean(probas, axis=0)

    roc_dict = dict()
    key_column_dict = dict()
    columns = list()
    for column in all_train:
        if column != 'label':
            if column == DEVIATION_OF_AVG_RATING:
                key = 'CAR-DEV'
                columns.append(key)
            elif column == DIFFERENCE_OF_AVG_RATING:
                key = '$\Delta$CAR'
                columns.append(key)
            elif column == NO_OF_REVIEWS:
                key = 'NR'
                columns.append(key)
            elif column == DIFFERENCE_OF_NO_OF_REVIEWS:
                key = '$\Delta$NR'
                columns.append(key)
            elif column == RATING_ENTROPY:
                key = 'EN'
                columns.append(key)
            elif column == DIFFERENCE_OF_RATING_ENTROPY:
                key = '$\Delta$EN'
                columns.append(key)
            elif column == KL_DIVERGENCE:
                key = 'KL-DIV'
                columns.append(key)
            elif column == NO_OF_POSITIVE_REVIEWS:
                key = 'PR'
                columns.append(key)
            elif column == DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS:
                key = '$\Delta$PR'
                columns.append(key)
            else:
                key = column
            key_column_dict[key] = column
            roc_dict[key] = roc_auc_score(1 - test.iloc[:, 9], test[column])
    roc_dict['Adaptive ($w^s$)'] = roc_auc_score(1 - test.iloc[:, 9], proba)
    roc_dict['Max Ensemble'] = roc_auc_score(1 - test.iloc[:, 9], max_ensemble_proba)
    roc_dict['Avg Ensemble'] = roc_auc_score(1 - test.iloc[:, 9], avg_ensemble_proba)
    test['Adaptive ($w^s$)'] = proba
    test['Max Ensemble'] = max_ensemble_proba
    test['Avg Ensemble'] = avg_ensemble_proba
    best_p = max(columns, key=lambda x: roc_dict[x])
    # print(columns, best_p)
    test['Best_P'] = test[key_column_dict[best_p]]
    # print(test[:5])
    test.to_csv(os.path.join(suspDir_pre, '..', susp_stats, 'case_%s' % case, 'case_%s_DETER.csv' % case))

    return roc_dict

def trainOnAggregatedDataForCoef(suspDir_pre, signals, random=0, case=1, first=False, susp_stats='susp_stats'):

    models = dict()

    for i in range(1, 10):
        if first:
            plotAUCForEachCase(suspDir_pre, signals, i)
        models[i] = joblib.load(os.path.join(suspDir_pre, 'results', 'case_%s_model.pkl' % i))

    all_train = DataFrame()

    for i in range(1, 10):
        df_dict = dict()
        df_score = DataFrame()
        for sig in signals:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (i, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre.csv' % i),
                                         index_col=0)
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_random.csv' % i),
                                            index_col=0)
            df_score[sig] = df_dict[sig]['suspicious_score']
        max_list = list()
        # print df_score.head()
        for tup in df_score.itertuples():
            ls = list(tup)
            ls = ls[1:]
            # print ls
            max_ls = max(ls)
            max_list.append(max_ls)
        df_score['label'] = df_dict[signals[0]]['is_stop_attack']
        df_score['All'] = df_dict['All']['suspicious_score']
        df_score['Random'] = df_dict['Random']['suspicious_score']
        df_score['Max'] = max_list
        # print df_score.head()
        train = concat([df_score[::5], df_score[1::5]])
        # print(len(train))
        all_train = concat([all_train, train])

    df_dict = dict()
    df_score = DataFrame()
    for sig in signals:
        if random:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, '..', susp_stats, 'results', 'random_%s_pre_%s.csv' % (random, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, '..', susp_stats, 'results', 'random_%s_pre.csv' % random))
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, '..', susp_stats, 'results', 'random_%s_pre_random.csv' % random))
        else:
            df_dict[sig] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_%s.csv' % (case, sig)),
                                       index_col=0)
            df_dict['All'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre.csv' % case))
            df_dict['Random'] = pd.read_csv(os.path.join(suspDir_pre, 'results', 'case_%s_pre_random.csv' % case))

        df_score[sig] = df_dict[sig]['suspicious_score']
    max_list = list()
    # print df_score.head()
    for tup in df_score.itertuples():
        ls = list(tup)
        ls = ls[1:]
        # print ls
        max_ls = max(ls)
        max_list.append(max_ls)
    df_score['label'] = df_dict[signals[0]]['is_stop_attack']
    df_score['All'] = df_dict['All']['suspicious_score']
    df_score['Random'] = df_dict['Random']['suspicious_score']
    df_score['Max'] = max_list
    # print df_score.head()
    test = concat([df_score[2::5], df_score[3::5]])
    test = concat([test, df_score[4::5]])
    # print(len(test))
    model = LogisticRegression('l1')
    model.fit(all_train.iloc[:, :9], all_train.iloc[:, 9])
    score = model.score(test.iloc[:, :9], test.iloc[:, 9])

    print(model.coef_)
    coef_dict = dict()
    for i in range(len(model.coef_[0])):
        key = all_train.columns[i]
        coef_dict[key] = model.coef_[0][i]

    return coef_dict

def formAUCMatrixForAggregatedTrainedModel(susp_dir, signals):

    auc_dict = dict()

    # print(susp_dir)
    for random in range(1, 3):
        if random == 1:
            auc = trainOnAggregatedData(susp_dir, signals, random=random, first=True)
        else:
            auc = trainOnAggregatedData(susp_dir, signals, random=random)
        auc_dict['Random %s' % random] = auc

    for case in range(1, 10):
        auc = trainOnAggregatedData(susp_dir, signals, case=case)
        auc_dict['Evasion %s' % case] = auc

    auc_matrix = DataFrame(auc_dict.values(), index=auc_dict.keys(), columns=auc.keys())
    # print(auc_matrix)
    auc_matrix.to_csv(os.path.join(susp_dir, 'results', 'auc_matrix_aggregated.csv'))


def formAUCMatrixForAggregatedEarlyTrainedModel(susp_dir, signals):

    auc_dict = dict()

    print(susp_dir)

    for case in range(1, 3):
        if case == 1:
            auc = trainOnAggregatedEarlyData(susp_dir, signals, case=case, first=True)
        else:
            auc = trainOnAggregatedEarlyData(susp_dir, signals, case=case)
        auc_dict['Evasion %s' % case] = auc

    auc_matrix = DataFrame(auc_dict.values(), index=auc_dict.keys(), columns=auc.keys())
    print(auc_matrix)
    auc_matrix.to_csv(os.path.join(susp_dir, 'results', 'early_auc_matrix_aggregated.csv'))


def formAUCMatrixForMultiRandom(susp_dir, signals, susp_stats):
    auc_dict = dict()
    for random in range(1, 3):
        auc =  trainOnAggregatedData(susp_dir, signals, random=random, susp_stats=susp_stats)
        auc_dict['Random_%s' % random] = auc

    auc_matrix = DataFrame(auc_dict.values(), index=auc_dict.keys(), columns=auc.keys())
    print(auc_matrix)
    auc_matrix.to_csv(os.path.join(susp_dir, '..', susp_stats, 'results', 'auc_matrix_only_random.csv'))





if __name__ == '__main__':
    case = 4
    plotDir = '/Users/geshuaijun/Amazon Dataset/stats'
    signals = [RATING_ENTROPY, DIFFERENCE_OF_AVG_RATING, NO_OF_REVIEWS, NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE,
               DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS,
               DIFFERENCE_OF_NO_OF_REVIEWS, DIFFERENCE_OF_RATING_ENTROPY, DEVIATION_OF_AVG_RATING]
    # generateDataFrames(plotDir, case, signals)
    # susp_dir = '../data/Amazon Dataset/susp_stats'
    susp_dir = '../data/Yelp Dataset/susp_stats'
    formAUCMatrixForAggregatedTrainedModel(susp_dir, signals=signals)
    susp_dir = '../data/Amazon Dataset/susp_stats'
    formAUCMatrixForAggregatedTrainedModel(susp_dir, signals=signals)








