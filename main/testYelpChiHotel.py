from datetime import datetime
import os
import sys

import AppUtil

from util.data_reader_utils.yelp_utils.YelpNYCMetaDataReader import YelpNYCMetaDataReader



if __name__ == "__main__":

    csvFolder = '../data/Yelp Dataset/YelpChi+YelpNYC/'
    # csvFolder = '/Users/geshuaijun/Yelp Dataset/YelpChi+YelpNYC/'
    currentDateTime = datetime.now().strftime('%d-%b--%H:%M')
    plotDir = os.path.join(os.path.join(csvFolder, os.pardir), 'stats')
    AppUtil.extractAndSerializeBnssStatisticsForBnss(csvFolder, plotDir, bnss_list_start=0, bnss_list_end=413, timeLength='1-M', rdr=YelpNYCMetaDataReader())
