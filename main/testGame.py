# -*- coding:utf-8 -*-

import sys
import os

# sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

from util.GameUtil import OfflineGameV2, extractCMRtoDataFrame, extractBnssKeys, OfflineGameV3, EarlyGame
from datetime import datetime
from util.DetectAnalysis import *
import time
from util.TrainClassifier import formAUCMatrixForAggregatedTrainedModel, trainOnAggregatedData, \
    trainOnAggregatedEarlyData, formAUCMatrixForAggregatedEarlyTrainedModel
import shutil
from pandas import DataFrame


def main(csvFolder, plotDir, case=1, ratio=0.8, top=0.2, noiseForNoOfReviews=100, random=0, susp_stats='susp_stats',
         repeat=True):
    beforeStat = datetime.now()
    signals_to_be_extracted = [DEVIATION_OF_AVG_RATING, DIFFERENCE_OF_AVG_RATING, DIFFERENCE_OF_NO_OF_REVIEWS,
                               DIFFERENCE_OF_RATING_ENTROPY, NO_OF_REVIEWS, RATING_ENTROPY,
                               NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE, DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS]

    preDir = os.path.join(os.path.join(csvFolder, os.pardir), susp_stats)
    if not os.path.exists(preDir):
        os.mkdir(preDir)

    if random:
        suspDir = os.path.join(os.path.join(csvFolder, os.pardir), susp_stats, 'random_%s' % random)
    else:
        suspDir = os.path.join(os.path.join(csvFolder, os.pardir), susp_stats, 'case_%s' % case)

    if not os.path.exists(suspDir):
        os.mkdir(suspDir)
    print plotDir

    for root, dirs, files in os.walk(os.path.join(suspDir, 'suspicious_scores')):
        for name in files:
            if os.path.exists(os.path.join(os.path.join(suspDir, 'suspicious_scores'), name)):
                os.unlink(os.path.join(os.path.join(suspDir, 'suspicious_scores'), name))

    for root, dirs, files in os.walk(os.path.join(suspDir, 'suspicious_scores_signals')):
        for name in files:
            if os.path.exists(os.path.join(os.path.join(suspDir, 'suspicious_scores_signals'), name)):
                os.unlink(os.path.join(os.path.join(suspDir, 'suspicious_scores_signals'), name))

    if repeat:
        OfflineGameV2(csvFolder, signals_to_be_extracted, noiseForNoOfReviews, ratio, case, top=0.2, random=random,
                      susp_stats=susp_stats)
    else:
        OfflineGameV2(csvFolder, signals_to_be_extracted, noiseForNoOfReviews, ratio, case, top=0.2, random=random)

    print case

    afterStat = datetime.now()
    print afterStat - beforeStat

    return preDir

# csvFolder = '../data/Amazon Dataset/Apps for Android/'
csvFolder = '../data/Yelp Dataset/YelpChi+YelpNYC'
plotDir = os.path.join(os.path.join(csvFolder, os.pardir), 'stats')

ratio = 0.8
top = 0.2
noiseForNoOfReviews = 100
# repeat = False

signals = [RATING_ENTROPY, DIFFERENCE_OF_AVG_RATING, NO_OF_REVIEWS, NO_OF_POSITIVE_REVIEWS, KL_DIVERGENCE,
               DIFFERENCE_OF_NO_OF_POSITIVE_REVIEWS,
               DIFFERENCE_OF_NO_OF_REVIEWS, DIFFERENCE_OF_RATING_ENTROPY, DEVIATION_OF_AVG_RATING]

for case in range(1, 10):
    main(csvFolder, plotDir, case=case, ratio=ratio, noiseForNoOfReviews=noiseForNoOfReviews)



'''
Early Game
'''
# case = 1
# susp_dir = os.path.join(csvFolder, '..', 'susp_stats')
# start = time.time()
# susp_stats = 'susp_stats'
# EarlyGame(csvFolder, signals, ratio, case, stratify=False)
# end = time.time()
# print "Elapsed time is %.1fs" % (end - start)



'''
Repeat experiments to get auc mean and std
'''
# for i in range(1, 10):
#     if repeat:
#         count = 0
#         for root, dirs, files in os.walk(os.path.join(csvFolder, os.pardir)):
#             for dir in dirs:
#                 if 'susp_stats' in dir:
#                     count += 1
#             break
#         susp_stats = 'susp_stats_{}'.format(count)
#     else:
#         susp_stats = 'susp_stats'
#
#     print(susp_stats)
#
#     for case in range(5, 10):
#         print(case)
#         susp_dir = main(csvFolder, plotDir, case=case, susp_stats=susp_stats)
#
#     for random in range(1, 3):
#         susp_dir = main(csvFolder, plotDir,random=random, susp_stats=susp_stats)
#
#     for case in range(1, 5):
#         dest_dir = os.path.join(susp_dir, 'case_%s' % case)
#         if not os.path.exists(dest_dir):
#             src_dir = os.path.join(susp_dir, os.pardir, 'susp_stats', 'case_%s' % case)
#             shutil.copytree(src_dir, dest_dir)
#
#     for case in range(1, 10):
#         calculateUtilityOfDifferentDetectors(plotDir, case, susp_stats=susp_stats)
#
#     for random in range(1, 3):
#         calculateUtilityOfDifferentDetectors(plotDir, susp_stats=susp_stats, random=random)
#
#     formAUCMatrixForAggregatedTrainedModel(susp_dir, signals=signals)
#
# # formAUCMatrixForAggregatedTrainedModel('../data/Amazon Dataset/susp_stats_1', signals=signals)
# end = datetime.now()
# print end - start, 'has passed'
#
# csvFolder = '../data/Amazon Dataset/Apps for Android/'
# plotDir = os.path.join(os.path.join(csvFolder, os.pardir), 'stats')
#
# for case in range(5, 10):
#     for i in range(1, 10):
#         main(csvFolder, plotDir, case=case, repeat=repeat)



