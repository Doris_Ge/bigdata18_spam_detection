from datetime import datetime
import os
import sys

import AppUtil
import ThresholdHelper
from util import StatConstants
from util.data_reader_utils.yelp_utils.YelpChiMetaDataReader import YelpChiMetaDataReader
from util.data_reader_utils.amazon_utils.AmazonAppDataReader import AmazonAppDataReader
import matplotlib

if __name__ == "__main__":
    '''
    if len(sys.argv) != 2:
        print 'Usage: python -m \"main.testSWM\" csvFolder'
        sys.exit()
    '''
    csvFolder = '../data/Amazon Dataset/Apps for Android/'
    currentDateTime = datetime.now().strftime('%d-%b--%H:%M')
    plotDir = os.path.join(os.path.join(csvFolder, os.pardir), 'stats')
    AppUtil.extractAndSerializeBnssStatisticsForBnss(csvFolder, plotDir, bnss_list_start=0, bnss_list_end=0, timeLength='1-W', rdr=AmazonAppDataReader())