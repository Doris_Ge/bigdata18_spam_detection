# Datasets
We have two datasets under ./data direcotry.
1. Yelp Dataset. The output_meta_yelpResData_NRYRcleaned.csv which is under ./data/Yelp Dataset/YelpChi+YelpNYC/ contains the ratings collected from Yelp for restaurants in NYC and Chicago and is used in our experiments.
2. Amazon Dataset. The ratings_Apps_for_Android_more_than_1000.csv which is under ./data/Amazon Dataset/Apps for Android/ contains the ratings for Apps which have more than 1000 reviews on Amazon and is used in our experiments.

# Steps to simulate the game between spammers and defenders

1. Run main/testGame.py. By setting  the case, you can choose the evasion strategy from evastions 1-9 for spammers. By setting the ratio, you can change the extent that the spammers want to camouflage themselves. If ratio is 0.8, then the spammers want to control the signals of the attacked months within p% in the CDF. By setting the is_random, you can decide whether spammers will randomly attack.
2.  Run DetectAnalysis.py. By setting the case, you can detect spammers camouflaged by different evasions using different defense strategies.
3. If you have simulated all the evasions for spammers and detections for defenders by setting case from 1-9 in step 1-2. You can successfully get the utility matrices after step 2.
4. Run TrainClassifier.py. You can get  a logistic classifier and plot Figure 5 and Figure 6 in the paper.

# Some important python files:

testYelpChiHotel.py: Running this file can extract some basic statistics from raw data. Statistics of each business will be dumped to bnss_stats/[bnss_id].pkl.

testGame.py: Running this file can simulate evasion strategies for each business under different cases and some attack statistics will be dumped to attack_stats/[bnss_id].pkl. Some data related to suspicious scores and suspicious signals can be respectively dumped to suspicious_scores/[bnss_id].pkl and suspicious_scores_signals/[bnss_id].pkl.

DetectAnalysis.py: Some data related to suspicious scores and suspicious signals can be respectively dumped to suspicious_scores/[bnss_id].pkl and suspicious_scores_signals/[bnss_id].pkl. Some metrics which can be used to calculate utitlities matrices are dumped to case_[1-9].csv and case_[1-9]_pre.csv. By running this file, you can get the utility matrices of different metrics for spammers and defenders.

TrainClassifier.py: You can get the suspicious scores calculated using 9 different defense strategies for the same month. Based on those suspicious scores and the label, a logistic classifier can be trained.

# Architectures for some important python files:

* main/testGame.py/OfflineGameV2(imported from GameUtil.py)

  * util/OptimizeAttack.py/chooseAttackStrategyForEachBnss:

    choose the evasion strategy when attacking each business each time and dump the attack statstics into a pickle file in the directory data/susp_dir/case_[1-9]/attack_stats/ with bnss_id as the file name.

  * util/DetectAnalysis.py/offlineDetectAnalysisWithinAttack:

    prepare data for calculating suspicious scores later and dump the data into data/susp_dir/case_ [1-9]/suspicious_scores and data/susp_dir/case_[1-9]/suspcious_scores_signals.

* util/DetectAnalysis.py/calculateUtitlityOfDifferentDetectors

  generate a csv which contains metrics related to defender's utility under different defense strategies

  * util/DetectAnalysis.py/newDefenderUtil

    calculate suspicious score of each month/week of each business and calculate defender's utility and other relevant metrics based on suspicious scores.

* util/DetectAnalysis.py/formMetricMatrix

  * form utility matrices for defenders and spammers

* util/TrainClassifier.py

  ​

# Some Important data files

In SpamDetection/data directory, there are two subdirectories, Amazon Dataset and Yelp Dataset, which contain raw data and processed data for each dataset.

* Amazaon Dataset/ Yelp Dataset

  * Apps for Android (Amazon)/ YelpChi+YelpNYC (Yelp): raw data and filtered raw data

  * stats/bnss_stats/1-W/: pickle files for each business

    By loading a pickle file, we can get a dictionary which contains some metadata for business.

    useful keys for each dictionary are:

    * 'BNSS_ID': str. ID for current business
    * 'First Time Key': int. when business receives the first review
    * 'Last Time Key': int. when business receives the last review
    * 'Average Rating': numpy.ndarray. time series of average rating for current business
    * 'No of +ve Reviews': numpy.ndarray. time series of number of positive reviews for current business
    * 'No of Reviews':  numpy.ndarray. time series of number of reviews for current busniness
    * 'Rating entropy': numpy.ndarray.  time series of rating entropy for current busniness
    * 'Rating Distribution': numpy.ndarray. time series of rating distribution for current busniness
    * 'Non Cum No. of Reviews:' numpy.ndarray.  time series of non-cumulative number of reviews for current busniness
    * 'No of -ve Reviews': numpy.ndarray.  time series of number of negative reviews for current busniness

  * susp_stats/case_[1-9]/attack_stats/

    By loading a pickle file, we can get a dictionary  which contains some data before and after attack.

    keys for each dictionary are:

    * 'CAR bf attack': list.  Cumulative average rating before attack during the months/weeks when the spammers want to attack. Here are 5 months.
    * 'NR bf attack': list.  Non-cumulative number of reviews  before attack during the months/weeks when the spammers want to attack. Here are 5 months.
    * 'time key to start attack': int. when spammers start to attack.
    * 'rating distribution bf attack': dict. Cumulative rating distribution before attack. The keys of dictionary are the ratings.
    * 'CAR af attack': list. Cumulative average rating after attack during the months when spammers attempt to attack.
    * 'NR af attack':  list. Non-cumulative number of reivews after attack. 
    * 'rating distribution bf attack':  dict. Cumulative rating distribution before attack. 
    * 'rating for attack': dict. Rating distribution of spam reviews during the months when spammers attempt to attack.
    * 'stop attack':  list. The months when spammers attempt to attack but fail to find a viable attack strategy. 
    * 'noises for CAR': list. The difference between cumulative average rating after attack and before attack.
    * 'noises for NR': list. The difference between non-cumulative number of reviews after attack and before attack.
    * 'CMR': pandas.core.series.Series. Average rating of a month of current business from the first time key to the last time key before attack.

  * susp_stats/case_[1-9]/suspicious_scores/

    By loading a csv file, we can get a DataFrame whose columns are

    * 'bnss_key': business id.

    * 'attack_time': the $i$ th(start from 0) month they attack.

    * 'suspicious_score': the suspicious score of $i$ th  month.

    * 'is_stop_attack': whether the spammers stop attack in the $i$ th  month.

    * 'noise_for_avg_rating': noise for cumulative average rating of the $i$ th month.

    * 'noise_for_car_af_detection': noise for cumulative average rating after detection of the $i$ th  month.

    * 'noise_for_cmr': noise for current month rating after detection of the $i$ th month.

    * 'noise_for_cmr_af_detection': noise for current month rating after detection of the $i$ th  month

    * 'noise_for_rank': noise for ranking of the $i$ month

    * 'noise_for_rank_af_detection': noise for ranking after detection of the $i$ month

    * 'is_burst': whether the month is the first one that the spammers succeed in launching the spam campaign.

  * susp_stats/case_[1-9]/suspicious_scores_signals/

    By loading a pickle file, we can get a dictionary  whose value contains historical data for current business, time keys when spammers stop their attack and noises for average rating, average rating after detection, current month rating, current month rating after detection, ranking and ranking after detection. The keys of this dict are the ith month they attack. For example, if it is the first month spammers attack, the key will be 0.

  * susp_stats/case_[1-9]/

    By loading a case_[1-9].csv, we can get a DataFrame whose indexs are the defense strategies and whose columns are

    * 'avg_d_util': the removed positive spam reviews ratio
    * 'avg_noise_for_car': average noise for cumulative average rating
    * 'avg_noise_for_car_af_detect': average noise for cumulative average rating after detection
    * 'avg_noise_for_cmr': average noise for current month rating
    * 'avg_noise_for_cmr_af_detect': average noise for current month rating after detection
    * 'avg_noise_for_rank': average noise for ranking
    * 'avg_noise_for_rank_af_detect': average noise for ranking after detection
    * 'attack_rate': the percentage of months/weeks attacked by spammers in all months/weeks they attempted to attack
    * 'avg_no_of_spam': average number of spam reviews spammers have sent
    * 'avg_removed_reviews': average number of spam reviews removed by defender
    * 'avg_no_of_positive_spam': average number of positive spam reviews spammers have sent
    * 'avg_removed_positive_reviews': average number of positvie spam reviews removed by defender
    * 'ranking_loss': ranking_loss of every defender strategy

    ​

# Some detailed explanations about important function:

* GameUtil.py: OfflineGameV2(signals_to_be_extracted, noiseForNoOfReviews, ratio, case, top=0.2, recall=(0.8, 0.2), is_random=False)

   Simulate evasions and calculate suspicious score of each signal

  * Inputs:
    * signals_to_be_extracted: signals will be used in detection
    * noiseForNoOfReviews: no of all reviews from spammers
    * ratio: p% in signal's CDF
    * case: which evasion spammers want to use
    * top: the percentage the defenders consider suspicious
    * is_random: whether spammers will randomly select an evasion stragegy from evasion 1-9

* OptimizeAttack.py: chooseAttackStrategyForEachBnss(statistics_for_current_bnss_dir, plotDir, suspDir, timeLength, measures, NoiseForNoOfReviews, bnss_key, case, ratio, recall=[0.5, 0.5], is_random=False):

  * Inputs:

    * statistics_for_current_bnss_dir: a directory where the statistics for businesses are stored.
    * suspDir: a directory where the statistics for suspiciousness are dumped
    * timeLength: a subdirectory that could be '1-W' or '1-M'
    * measures: different signals
    * NoiseForNoOfReviews: no of all reviews from spammers
    * bnss_key: which business is the target business
    * case: which evasion is used to attack,
    * ratio: p% in signal's CDF
    * top: the percentage the defenders consider suspicious
    * recall: the recall of the classifier
    * is_random: whether spammers will randomly select an evasion stragegy from evasion 1-9

  * Outputs:

    * avgRatingErrorAfAttack:CAR-DEV after each weekly/monthly attack
    * ARmodelForAvgRating[0][-1]: CAR after attack
    * ARmodelForNoOfReviews[0][:-1]: No of reviews after attack
    * entropyAfAttack: rating entropy after attack
    * ratingDivergenceAfAttack: rating divergence after attack
    * noOfPositiveReviewsAfAttack: no of positive reivews after attack
    * startTimeKeyForPrediction: the number of time key to start attack
    * stopAttackTime: the time keys (start from 0) that the spammers fail to find a viable attack
    * statistics_for_all_bnsses: historical statistics of each business
    * statistics_for_all_measures: historical statistics of each business





